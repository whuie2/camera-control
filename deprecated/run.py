import code
import readline
import rlcompleter
import lib.andor as andor
from pathlib import Path
import importlib
import cam_control as cc

def reload():
    importlib.reload(cc)

if __name__ == "__main__":
    cam = andor.iXon888()
    # console
    vars = globals()
    vars.update(locals())
    readline.set_completer(rlcompleter.Completer(vars).complete)
    readline.parse_and_bind("tab: complete")
    code.InteractiveConsole(vars).interact(
        banner="Simple IPython-like control script for Andor EMCCD"
    )
    cam.disconnect()

