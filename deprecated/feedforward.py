from queue import Queue
import dearpygui.dearpygui as dpg
import numpy as np
import serial
import lib.image as image

def feedforward(img_queue: Queue, fdfwd_idx: [int, int, ...], roi):
    imgs_per_loop = len(fdfwd_idx) # number of images expected to be analyzed per acquisition loop
    # print(fdfwd_idx)
    current_idx = 0
    roi_totals = []
    x,y,w,h = roi
    threshhold = 12.0

    arduino = serial.Serial(port="COM3", baudrate=9600, timeout=0.1)

    readout_rate = 1.0  # MHz
    preamp = 2
    em_gain = 100
    count_bias = 500.0
    QE = 0.8
    while dpg.get_value("is_acquiring"):
        while not img_queue.empty():
            roi_totals.append(
                image.to_photons(
                    img_queue.get()[x:x+w, y:y+h].astype(np.float64),
                    readout_rate,
                    preamp,
                    em_gain,
                    count_bias,
                    QE,
                )[0].sum()
            )
            if current_idx % imgs_per_loop == 0:
                # do sth
                arduino.write(bytes("0001", 'utf-8'))
            # if current_idx % imgs_per_loop == 1:
                # do sth
            # if current_idx % imgs_per_loop == 2:
                # do sth

            current_idx += 1
    return





