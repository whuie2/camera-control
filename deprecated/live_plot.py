import queue

import matplotlib.image
import numpy as np
from multiprocessing import Queue
import matplotlib.pyplot as plt
import matplotlib.animation as animatoin
from typing import Tuple

def update_img(i, img_queue:Queue, im:matplotlib.image.AxesImage):
    try:
        img = img_queue.get_nowait()
    except queue.Empty:
        return [im]
    im.set_array(img)
    return [im]

def main(img_queue: Queue, img_dim: Tuple[int, int]):
    fig, ax = plt.subplots()
    base = np.random.randint(500, 600, img_dim[0] * img_dim[1], dtype=int).reshape(img_dim)
    # ax.set_xlim(0,img_dim[1]-1)
    # ax.set_ylim(0,img_dim[0]-1)
    # ax.set_xticks(np.arange(0, img_dim[1], 1, dtype=int))
    # ax.set_yticks(np.arange(0, img_dim[0], 1, dtype=int))
    ax.set_xticks([])
    ax.set_yticks([])
    im = ax.imshow(base, cmap='gray', vmin=500, vmax=600, aspect='equal')#, extent=(0, img_dim[1]-1, 0, img_dim[0]-1))
    plt.colorbar(im, location='top')
    anim = animatoin.FuncAnimation(fig, update_img, fargs=(img_queue, im), interval=2, repeat=False, blit=True)
    plt.show()
    print("plotting stopped")
    return
