import serial
import lib.andor as andor
import time
import numpy as np
from typing import Dict
from pathlib import Path
from multiprocessing import Process, Queue
import live_plot

def test_mthd(global_vars: Dict):
    pass

def setup(emccd: andor.iXon888):
    emccd.connect()
    if emccd.is_connected():
        # print(emccd.get_full_info())
        # print(emccd.get_acquisition_params())
        print(
            "Camera connected"
        )
        # emccd.set_roi(np.array([377, 397, 692, 702], dtype=int)) # hstart, hend, vstart, vend
        emccd.set_roi([377, 397, 692, 702])
        # emccd.start_acquisition()
        # time.sleep(3)
        # emccd.stop_acquisition()
        # cam.clear_acquisition
    else:
        print("cam connection failed")

def start_acquisition(emccd: andor.iXon888):
    datadir = Path(r"C:\Users\Covey Lab\Documents\Andor Solis\atomic_data")
    date = "20230323"

    # vars to change here:
    save = False
    label = "ff-series-xor"
    counter = 23

    threshold = 12
    spl = 6
    live_show = False
    # -----------------------------------

    # if not emccd.setup_kinetics_mode():
        # print("camera not set up for acquisition")
        # return

    outfile = None
    if save:
        if not datadir.is_dir():
            print(f":: mkdir -p {datadir}")
            datadir.mkdir(parents=True)
        while datadir.joinpath(f"{label}_{counter:03.0f}.npy").is_file():
            counter += 1
        try:
            outfile = open(datadir.joinpath(f"{label}_{counter:03.0f}.npy"), 'xb')
        except FileExistsError:
            print("data file already exists, please double")
            return

    ard = serial.Serial(port="COM4", baudrate=int(2e6), timeout=0.1)
    readout_rate = 1.0  # MHz
    preamp = 2
    em_gain = 100
    count_bias = 500.0
    QE = 0.8
    current_idx = 0
    e_per_count = 3.89
    img_list = []
    img_queue = Queue()
    img_dim = emccd.get_roi_dim()
    roi_queue = Queue()

    emccd.start_acquisition()
    print("acquisition started, use ctrl-c to stop")
    print(emccd.get_full_info())
    if live_show:
        plot_proc = Process(target=live_plot.main, args=(img_queue, img_dim,))
        plot_proc.start()

    try:
        while True:
            # emccd.wait_for_image(timeout=None)
            img = emccd.read_oldest_image()
            if img is None:
                continue
            img_list.append(emccd.read_oldest_image())
            print(img)
            x,y,w,h = (9, 3, 3, 3)  # modify this as needed
            roi = img[
                y: y+h,
                x: x+w,
            ]
            img_queue.put_nowait(img_list[-1])
            # roi_queue.put_nowait(roi)
            photons = ((roi - count_bias) * e_per_count / em_gain / QE).sum()
            # always same as first
            # if current_idx == 0:
            #     initial_state = photons >= threshold
            #     do_pulse = True
            # elif current_idx == spl - 1:
            #     do_pulse = True
            # elif current_idx % 2 == 1:
            #     ff_state = photons >= threshold
            #     do_pulse = initial_state ^ ff_state
            # else:
            #     do_pulse = True

            # # alternating diff/same as first
            # if current_idx == 0:
            #     initial_state = photons >= threshold
            #     do_pulse = True
            # elif current_idx == spl - 1:
            #     do_pulse=True
            # elif current_idx % 4 == 1:
            #     ff_state = photons >= threshold
            #     do_pulse = initial_state ^ ff_state
            # else:
            #     do_pulse = True

            # ard.write(b'1100\n' if do_pulse else b'1111\n')
            # print(f"{current_idx:3.0f} : {int(photons):4.0f} => {str(do_pulse)}")
            current_idx = (current_idx + 1) % spl

    except KeyboardInterrupt:
        print("keyboard interrupt detected")
        emccd.stop_acquisition()
        if save:
            np.save(outfile, imgs)
            outfile.close()
        print("acquisition stopped")
        ard.close()
        print("please close live window")
        if live_show:
            plot_proc.join()
    return

