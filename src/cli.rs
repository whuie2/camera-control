#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return, clippy::or_fun_call)]

use std::{
    path::PathBuf,
};
use anyhow::Result;
use clap::Parser;
use once_cell::sync::Lazy;
use timetagger::prelude::TimeTaggerUltra;
use camera_control::{
    print_flush,
    cli::run_cli,
    config::{
        Config,
        load_def_config,
    },
    camera::{
        IXon888,
    },
};

static DEF_CONFIG_FILE: Lazy<PathBuf>
    = Lazy::new(|| PathBuf::from("config.toml"));

#[derive(Parser, Debug)]
#[command(author = "Covey Lab")]
#[command(version = "0.1.0")]
#[command(about = "Andor iXon 888 EMCCD interactive console")]
#[command(long_about = None)]
struct Cli {
    /// Use this config file
    #[arg(short = 'c', long = "config", default_value_t = String::from("config.toml"))]
    config_file: String,

    // /// Camera index.
    // #[arg(short = 'C', long = "camera-index", default_value_t = 0)]
    // cam_idx: usize,

    /// Don't initialize the camera settings after connecting.
    #[arg(short = 'I', long = "no-init")]
    no_init: bool,

    /// In regular mode, add extra terminal output from the camera.
    #[arg(short = 'v', long = "camera-verbose")]
    camera_verbose: bool,

    /// Connect to a simulated dummy camera instead of the real one.
    #[arg(short = 'D', long = "dummy")]
    dummy: bool,

    /// In dummy mode, disable extra terminal output.
    #[arg(short = 'Q', long = "dummy-quiet")]
    dummy_quiet: bool,
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    println!("Load config from '{}'", cli.config_file);
    let config: Config = load_def_config(cli.config_file.clone())?;

    println!("Connect to camera");
    let mut cam
        = if cli.dummy {
            IXon888::new().connect_dummy(!cli.dummy_quiet)
        } else {
            IXon888::new().connect(cli.camera_verbose)
        }
        .expect("couldn't connect to camera");

    if cli.no_init {
        println!("Warning: camera settings are uninitialized");
    } else if cli.camera_verbose || (cli.dummy && !cli.dummy_quiet) {
        println!("[camera] set config:");
        cam.set_config(&config)?;
        println!("done");
    } else {
        print_flush!("[camera] set config ... ");
        cam.set_config(&config)?;
        println!("done");
    }

    let tt: Option<TimeTaggerUltra>
        = if config.a("timetagger.use")? {
            println!("Connect to timetagger");
            let serial: String = config.a("timetagger.serial")?;
            Some(TimeTaggerUltra::connect(&serial)?)
        } else {
            None
        };

    run_cli(
        cam,
        cli.config_file,
        tt,
    )?;

    return Ok(());
}

