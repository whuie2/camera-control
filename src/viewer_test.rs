use std::{
    thread,
    time::Duration,
};
use crossbeam::{
    channel::{
        unbounded,
        Receiver,
        Sender,
    },
};
use iced::Application;
use ndarray as nd;
use rand::{
    prelude as rnd,
    Rng,
};
use camera_control::{
    // print_flush,
    println_flush,
    colors::{
        ColorMap,
        ColorMapBound,
    },
    viewer::*,
};

fn main() {
    let (tx, rx): (Sender<ToViewer>, Receiver<ToViewer>) = unbounded();
    let tx_proc = tx.clone();

    let sender_thread = thread::spawn(move || {
        let mut counter: usize = 0;
        let mut rng = rnd::thread_rng();
        loop {
            if counter < 100 {
                if counter == 0 {
                    thread::sleep(Duration::from_millis(2000));
                }
                counter += 1;
                thread::sleep(Duration::from_millis(100));
                let a: nd::Array2<u16> = nd::Array2::from_shape_fn(
                    (10, 50),
                    |_| rng.gen_range(0..100)
                );
                match tx.send(ToViewer::Frame(a)) {
                    Ok(()) => { continue; }
                    Err(_) => { break; }
                }
            } else {
                break;
            }
        }
        println_flush!("sender: finish");
        thread::sleep(Duration::from_millis(3000));
    });

    let proc_sender_thread = thread::spawn(move || {
        let mut counter: usize = 0;
        let mut rng = rnd::thread_rng();
        loop {
            if counter < 20 {
                if counter == 0 {
                    thread::sleep(Duration::from_millis(2000));
                }
                counter += 1;
                thread::sleep(Duration::from_millis(200));
                let a: nd::Array2<f64> = nd::Array2::from_shape_fn(
                    (20, 10),
                    |_| rng.gen()
                );
                match tx_proc.send(ToViewer::ProcFrame(a)) {
                    Ok(()) => { continue; }
                    Err(_) => { break; }
                }
            } else {
                break;
            }
        }
        println_flush!("sender_proc: finish");
    });

    FrameViewer::run(
        ViewerConfig::new_settings(
            rx,
            (
                ColorMap::new_grayscale_clip(),
                (0.0.into(), ColorMapBound::Auto),
            ),
            Some((
                ColorMap::new_grayscale_clip(),
                (0.0.into(), 1.0.into()),
            )),
            // None,
        )
    )
    .expect("bad viewer run");

    sender_thread.join()
        .expect("couldn't join sender thread");
    proc_sender_thread.join()
        .expect("couldn't join proc sender thread");
}

