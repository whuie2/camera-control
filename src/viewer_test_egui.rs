#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return)]

use std::{
    thread,
    time::Duration,
};
use crossbeam::channel::{
    Receiver,
    Sender,
    unbounded,
};
use ndarray as nd;
use rand::{
    self as rnd,
    Rng,
};
use camera_control::{
    println_flush,
    colors::{
        ColorMap,
        ColorMapBound,
    },
    viewer::{
        FrameViewer,
        ToViewer,
        ViewerConfig,
    },
};

fn doit() {
    let (tx, rx): (Sender<ToViewer>, Receiver<ToViewer>) = unbounded();
    let tx_proc = tx.clone();


    let sender_thread = thread::spawn(move || {
        let mut counter: usize = 0;
        let mut rng = rnd::thread_rng();
        loop {
            if counter < 100 {
                if counter == 0 {
                    thread::sleep(Duration::from_millis(2000));
                }
                counter += 1;
                thread::sleep(Duration::from_millis(100));
                let a: nd::Array2<u16> = nd::Array2::from_shape_fn(
                    (10, 50),
                    |_| rng.gen_range(0..100)
                );
                match tx.send(ToViewer::Frame(a)) {
                    Ok(()) => { continue; }
                    Err(_) => { break; }
                }
            } else {
                break;
            }
        }
        println_flush!("sender: finish");
        thread::sleep(Duration::from_millis(3000));
        println_flush!("sender: exit");
    });
    println!("launched sender thread");

    let proc_sender_thread = thread::spawn(move || {
        let mut counter: usize = 0;
        let mut rng = rnd::thread_rng();
        loop {
            if counter < 20 {
                if counter == 0 {
                    thread::sleep(Duration::from_millis(2000));
                }
                counter += 1;
                thread::sleep(Duration::from_millis(200));
                let a: nd::Array2<f64> = nd::Array2::from_shape_fn(
                    (20, 10),
                    |_| rng.gen()
                );
                match tx_proc.send(ToViewer::ProcFrame(a)) {
                    Ok(()) => { continue; }
                    Err(_) => { break; }
                }
            } else {
                break;
            }
        }
        println_flush!("sender_proc: finish");
        println_flush!("sender_proc: exit");
    });
    println!("launched proc sender thread");

    let config = ViewerConfig {
        source: rx,
        colorbar: (
            ColorMap::new_grayscale(),
            (ColorMapBound::Fixed(0.0), ColorMapBound::Auto),
        ),
        // proc_colorbar: None,
        proc_colorbar: Some((
            ColorMap::new_grayscale_clip(),
            (ColorMapBound::Fixed(0.0), ColorMapBound::Fixed(1.0)),
        )),
    };
    println!("launching frame viewer");
    FrameViewer::run(config)
        .expect("error running frame viewer");

    sender_thread.join()
        .expect("couldn't join sender thread");
    println!("sender thread successfully closed and joined");
    proc_sender_thread.join()
        .expect("couldn't join proc sender thread");
    println!("proc sender thread successfully closed and joined");
}

fn main() {
    doit();
    // doit();
}

