from __future__ import annotations
from pathlib import Path
from itertools import product
import os
import shutil
import sys
import toml
import numpy as np
import matplotlib.pyplot as pp
import lib.image as image
from lib.image import ROI, S
import lib.params as params
from lib.params import (ParamSpec as PS, load_params, DataSet)
import lib.plotting as plotting

def qq(X):
    print(X)
    return X

# datadir = Path(r"C:\Users\Covey Lab\Documents\Andor Solis\atomic_data")
datadir = Path("/home/coveylab/Documents/Data/atomic_data")
date = "20240909"

infiles = [
    datadir.joinpath(date).joinpath(infile)
    for infile in [
        "reg-hist_043.npy"
    ]
]

# exptdir = Path(r"\\ANACONDA\Users\EW\Documents\Data\tweezer atoms")
exptdir = Path("/media/anaconda/Documents/Data/tweezer atoms")
# cameradir = Path(r"C:\Users\Covey Lab\Documents\camera-control")
cameradir = Path("/home/coveylab/Documents/Control/camera-control")
# select variables to track from the driving script
# the order of this list must strictly follow the scan order
# each PS == ParamSpec follows the format
#   PS(name: str, mapping_func: lambda, unit: str)
# with the last two optional
paramslist = [
    ### const
    # PS("dispenser", "A"),

    ### 676 2D scan
    # PS("awg_trigger_step", "steps"),
    # PS("p_676", "mW"),
    # PS("tweezer_676_freq_mod", "MHz"),

    ### init
    # PS("shims_blue_fb", "V"),
    # PS("shims_blue_lr", "V"),
    # PS("shims_blue_ud", "V"),
    # PS("t0", "ms", lambda t: 1000.0 * t),
    # PS("u_nominal", "uK"),

    ### cmot
    # PS("shims_cmot_fb", "V"),
    # PS("shims_cmot_lr", "V"),
    # PS("shims_cmot_ud", "V"),

    ### load
    # PS("shims_smear_fb", "V"),
    # PS("shims_smear_lr", "V"),
    # PS("shims_smear_ud", "V"),
    # PS("tau_load", "ms", lambda t: 1000.0 * t),

    ### cool
    # PS("shims_cool_fb", "G"),
    # PS("shims_cool_lr", "G"),
    PS("shims_cool_ud", "G"),
    PS("tau_cool", "ms", lambda t: 1000.0 * t),

    ### bluecool
    # PS("hholtz_bluecool", "G"),
    # PS("tau_bluecool", "ms", lambda t: 1000.0 * t),
    PS("p_bluecool", "Vpp"),
    PS("det_bluecool", "MHz"),

    ### eit
    PS("tau_eit", "ms", lambda t: 1000.0 * t),
    PS("p_bluecouple", "dBm"),
    PS("det_bluecouple", "MHz"),
    PS("p_blueprobe", "Isat"),
    # PS("det_blueprobe", "MHz"),

    ### pump
    # PS("u_pump", "uK"),
    # PS("shims_pump_fb", "G"),
    # PS("shims_pump_lr", "G"),
    # PS("shims_pump_ud", "G"),
    # PS("hholtz_pump", "G"),
    PS("tau_pump", "ms", lambda t: 1000.0 * t),
    # PS("p_pump", "'Isat'"),
    PS("det_pump", "MHz"),
    # PS("det_pump_chirp", "MHz"),

    ### clockraman
    PS("tau_clockraman_pulse", "ms", lambda t: 1000.0 * t),
    # PS("p_clockraman", "V"),
    # PS("det_clockraman", "kHz"),

    ### telecom
    PS("tau_telecom", "ms", lambda t: 1000.0 * t),
    # PS("det_telecom", "MHz"),

    ### mag
    # PS("shims_mag_fb", "G"),
    # PS("shims_mag_lr", "G"),
    # PS("shims_mag_ud", "G"),
    # PS("tau_mag", "ms", lambda t: 1000.0 * t),

    ### clock ramsey
    # PS("phi_clock", "deg"),

    ### test
    # PS("u_test", "uK"),
    # PS("shims_test_fb", "G"),
    # PS("shims_test_lr", "G"),
    # PS("shims_test_ud", "G"),
    # PS("hholtz_test", "G"),
    # PS("tau_test", "ms", lambda t: 1000.0 * t),
    # PS("p_test_cmot_beg", "dBm"),
    # PS("p_test_cmot_end", "dBm"),
    # PS("det_test_cmot_beg", "MHz"),
    # PS("det_test_cmot_end", "MHz"),
    # PS("p_test_probe_am", "Isat"),

    ### lifetime
    # PS("u_lifetime", "uK"),
    # PS("shims_lifetime_fb", "G"),
    # PS("shims_lifetime_lr", "G"),
    # PS("shims_lifetime_ud", "G"),
    # PS("hholtz_lifetime", "G"),
    # PS("tau_lifetime", "ms", lambda t: 1000.0 * t),
    # PS("p_lifetime_cmot_beg", "dBm"),
    # PS("p_lifetime_cmot_end", "dBm"),
    # PS("det_lifetime_cmot_beg", "MHz"),
    # PS("det_lifetime_cmot_end", "MHz"),
    # PS("p_lifetime_probe_am", "Isat"),

    ### rampdown
    # PS("U_rampdown", "uK"),
    PS("tau_rampdown", "ms", lambda t: 1000.0 * t),

    ### release-recapture
    PS("tau_tof", "us", lambda t: 1e6 * t),

    ### clock
    PS("U_clock", "uK"),
    PS("tau_clock", "ms", lambda t: 1e3 * t),
    # PS("tau_clock_lifetime", "ms", lambda t: 1e3 * t),
    PS("det_clock", "MHz"),

    ### axcool
    # PS("shims_axcool_fb", "G"),
    # PS("shims_axcool_lr", "G"),
    # PS("shims_axcool_ud", "G"),
    # PS("hholtz_axcool", "G"),
    # PS("tau_axcool", "ms", lambda t: 1000.0 * t),
    # PS("p_axcool", "'Isat'"),
    # PS("det_axcool", "MHz"),

    ### idlescan
    PS("tau_idle_scan", "ms", lambda t: 1000.0 * t),

    ### image
    # PS("shims_probe_fb", "G"),
    # PS("shims_probe_lr", "G"),
    # PS("shims_probe_ud", "G"),
    # PS("hholtz_probe", "G"),
    PS("tau_probe", "ms", lambda t: 1000.0 * t),
    PS("tau_offset", "ms", lambda t: 1000.0 * t),
    PS("p_probe_am", "Isat"),

    ### cooling - serial
    PS("p_cool", "Isat"),
    PS("det_cool", "MHz"),

    ### probing - serial
    # PS("p_probe", "dBm"),
    PS("det_probe", "MHz"),
    # PS("det_probe_2", "MHz"),

    ### mag - serial
    # PS("shims_mag_amp_fb", "G"),
    # PS("shims_mag_amp_lr", "G"),
    PS("f_mag", "kHz", lambda f: 1000.0 * f),
    # PS("f_mag2", "kHz", lambda f: 1000.0 * f),
    # PS("phi_mag_2", "degrees",),

    ### telecom - serial
    PS("tau_telecom_pulse", "us", lambda t: 1e6 * t),
    PS("reg_loop_num", "",),

]

# extra variables not listed above
# items follow the format
# parameter_name: str | (str str) -> value: float | numpy.ndarray[ndim=1]
extra_params = {
}

### camera settings
readout_rate: float = 1.0 # MHz
preamp: int = 2.0
em_gain: int = 200
count_bias: float = 500.0
QE: float = 0.8
# image_dim: float = (20, 10) # (w, h)

### data selection options
roi_dim: list[int, 2] = [3, 3] # [ w, h ]
roi_locs: list[list[int, 2]] = [ # [ x (j), y (i) ]
    # [64, 2],
    # [71, 2],
    # [77, 2],
    # [83, 2],
    # [90, 2],

    # [ 62, 3], # 0
    # [ 67, 3], # 1
    # [ 71, 2], # 2
    # [ 76, 2], # 3
    # [ 80, 2], # 4
    # [ 85, 2], # 5
    # [ 89, 2], # 6
    # [ 94, 2], # 7
    # [ 98, 2], # 8
    # [103, 2], # 9

    # [ 15, 5], #  0
    # [ 20, 5], #  1
    # [ 25, 5], #  2
    # [ 30, 5], #  3
    # [ 34, 5], #  4
    # [ 39, 5], #  5
    # [ 44, 4], #  6
    # [ 49, 5], #  7
    # [ 54, 4], #  8
    # [ 58, 4], #  9
    # [ 63, 4], # 10
    # [ 68, 4], # 11
    # [ 73, 4], # 12
    # [ 78, 4], # 13
    # [ 83, 4], # 14
    # [ 87, 4], # 15
    # [ 92, 4], # 16
    # [ 97, 4], # 17
    # [102, 4], # 18
    # [106, 4], # 19

    # [ 18, 5], #  0
    # [ 24, 5], #  1
    # [ 28, 5], #  2
    # [ 33, 5], #  3
    # [ 38, 5], #  4
    # [ 43, 5], #  5
    # [ 48, 5], #  6
    # [ 52, 5], #  7
    # [ 57, 5], #  8
    # [ 62, 5], #  9
    [ 65, 5], # 10
    # [ 72, 5], # 11
    # [ 76, 5], # 12
    # [ 81, 5], # 13
    # [ 86, 5], # 14
    # [ 91, 5], # 15
    # [ 96, 5], # 16
    # [101, 5], # 17
    # [105, 5], # 18
    # [110, 5], # 19

]
num_images: int = 3 # expect two shots per param config
optim_pad: int = 0 # additional padding area for ROI optimization
# automatically find nominal ROI positions based on __avg__ for the `find_rois` brightest sites
find_rois: int = len(roi_locs)
# find_rois: int = 0
dist_limit: float = 3.0 # maximum allowable distance a found ROI can be from a nominal ROI
hist_bin_size: int = 3
threshold: float = 8.0
tt_hist_bin_size: int = 2
tt_threshold: float = 24.0

### processing options
# take only this number of shots or fewer (-1 guarantees the whole stack)
take: int = None
# delete pre-existing sub-directories and files generated by this script
renew_files: bool = True
# umbrella switch for all plotting
do_plotting: bool = True
# switch for only plotting related to individual parameter configs
do_indiv_plotting: bool = True
render_whole_image: bool = False
render_subimages: bool = False
plot_hist_totals: bool = True
plot_array_average_hist: bool = False
draw_roi_guides: bool = True
# sort parameter axes according to parameter array values
sort_axes: bool = True
# plot against two variables using slices instead of a color plot
force_lines: bool = True
# plot MPC/FAT/survival as slices versus these parameters (must be 1 or 2)
plot_versus: list[str] = ["det_probe"]
# for multiline plots, plot versus this `plot_versus` axis (must be 0 or 1)
multiline_plot_versus: int = 1
# plot info on this shot
shot_num: int = 0
# which shots to calculate survival
survival_sel: tuple[int, int] = (0, 1)
# add violin plots to MPC lineplots
violins_mpc: bool = False
# normalize MPC line plots to the max along the plotting axis
normalize_mpc: bool = False
# plot histograms/MPC/FAT for each ROI independently
# (requires len(plot_versus) == 1)
indep_rois: bool = True
plot_cmap = "jet"
image_cmap = "gray"

### derived quantities -- do not edit directly
rois: list[ROI] = [ROI(loc, roi_dim, optim_pad) for loc in roi_locs]
plot_versus_axes = [paramslist.index(p) for p in plot_versus]
if len(plot_versus_axes) not in {1, 2}:
    print("can only plot against 1 or 2 axes")
    sys.exit(1)

################################################################################

def process_file(filepath: Path, printflag: bool=True):
    if printflag:
        print(f"read image data from {filepath}")
    # image_data = image.load_image(filepath.parent, filepath.name)
    # image_data = image.load_image_pll(filepath, "frames.bin", image_dim)
    image_data = image.load_image_npy(filepath.parent, filepath.name)
    tt_data_path = filepath.with_name(filepath.name.replace(".npy", "_tt.npy"))
    if tt_data_path.exists():
        tt_data = image.load_timetagger_npy(tt_data_path.parent, tt_data_path.name)
    else:
        tt_data = None
    if take is not None:
        if take > 0:
            image_data = image_data[:take, :, :]
            if tt_data is not None:
                tt_data = tt_data[:take]
        else:
            image_data = image_data[take:, :, :]
            if tt_data is not None:
                tt_data = tt_data[take:]
    print(image_data.shape)

    paramsdir = exptdir.joinpath(date).joinpath(filepath.stem)
    if printflag:
        print(f"read params data from {paramsdir.joinpath('params.toml')}")
    params, reps = load_params(
        paramsdir,
        "params.toml",
        paramslist,
        extra_params
    )

    outdir = filepath.parent.joinpath(filepath.stem)
    if outdir.is_dir() and renew_files:
        already = os.listdir(outdir)
        for x in already:
            if x in {
                "__avg__.png",
                "roi_totals.npz",
                "above_threshold.npz",
                "fat_data.npz",
                "mpc_data.npz",
                "survival_data.npz",
                "camera-config.toml",
                "analysis-settings.toml",
            }:
                target = outdir.joinpath(x)
                print(f":: rm {target}")
                os.remove(str(target))
            elif x in {
                "fat",
                "hist",
                "img",
                "mpc",
                "sequencing",
                "survival",
            }:
                target = outdir.joinpath(x)
                print(f":: rm -rf {target}")
                shutil.rmtree(str(target), ignore_errors=True)
    elif not outdir.is_dir():
        print(f":: mkdir -p {outdir}")
        outdir.mkdir(parents=True)

    sequencingdir = outdir.joinpath("sequencing")
    if not sequencingdir.is_dir():
        print(f":: mkdir -p {sequencingdir}")
        sequencingdir.mkdir(parents=True)
    shutil.copy(paramsdir.joinpath("params.toml"), sequencingdir)
    shutil.copy(paramsdir.joinpath("comments.txt"), sequencingdir)
    shutil.copy(
        cameradir.joinpath("config.toml"),
        outdir.joinpath("camera-config.toml"),
    )

    analysis_settings = {
        "camera_settings": {
            "readout_rate": readout_rate,
            "preamp": preamp,
            "em_gain": em_gain,
            "count_bias": count_bias,
            "QE": QE,
        },
        "data_selection": {
            "infile": str(filepath),
            "rois": {
                "dim": roi_dim,
                "locs": roi_locs,
            },
            "num_images": num_images,
            "optim_pad": optim_pad,
            "hist_bin_size": hist_bin_size,
            "threshold": threshold,
        },
    }
    with outdir.joinpath("analysis-settings.toml").open('w') as out:
        toml.dump(analysis_settings, out)

    if printflag:
        print(f"compute derived quantities")
    image_photons = image.to_photons(
        image_data,
        readout_rate,
        preamp,
        em_gain,
        count_bias,
        QE,
    )

    avg = image_photons[0].mean(axis=0)
    rois_nom = (
        image.find_rois(
            avg,
            find_rois,
            tuple(roi_dim),
            rois,
            dist_limit,
            optim_pad,
        )
        if find_rois > 0 else rois
    )

    fig, ax = plotting.render_image(
        img=avg,
        cmap=image_cmap,
        vmin=0.0,
        vmax=None,
        clabel="Photons",
        title=None,
        draw_guides=draw_roi_guides,
        rois=rois_nom,
        roi_optim_locs=np.array([reps * [[roi_loc, roi_dim]] for roi_loc in roi_locs]),
        draw_optim_locs=optim_pad > 0,
    )
    fig.tight_layout()
    fig.savefig(outdir.joinpath("__avg__.png"))
    pp.close(fig)

    dataset = DataSet.from_raw(
        params,
        reps,
        num_images,
        np.array(image_photons),
        rois_nom,
        threshold,
        tt_threshold,
        tt_data,
        sort_axes,
        survival_sel,
    )

    if printflag:
        print("write data to files")

    def gen_slicer(
        arr: np.ndarray,
        with_reps: bool=True,
        with_shots: bool=True,
    ) -> tuple[slice | int]:
        axissel = [0]
        axislabelsel = ["site"]
        if with_reps:
            axissel.append(1)
            axislabelsel.append("rep")
        axislabelsel += paramslist
        if with_shots:
            axissel.append(len(arr.shape) - 1)
            axislabelsel.append("shot")
        return tuple([
            S[:] if axis in axissel or axislabel in plot_versus else 0
            for (axis, axislabel) in enumerate(axislabelsel)
        ])

    sl = gen_slicer(dataset.roi_totals[0], with_reps=True, with_shots=True)

    np.savez(
        str(outdir.joinpath("roi_totals.npz")),
        **{p[0]: v for p, v in dataset.params.items()},
        roi_totals=dataset.roi_totals[0],
        roi_totals_err=dataset.roi_totals[1],
        versus=plot_versus,
        sliced=dataset.roi_totals[0][sl],
        sliced_err=dataset.roi_totals[1][sl],
        **(
            dict(
                timetagger=dataset.tt_data[0],
                timetagger_err=dataset.tt_data[1],
            )
            if dataset.tt_data is not None else dict()
        ),
    )

    above_threshold = dataset.roi_totals[0] >= threshold
    
    sl = gen_slicer(above_threshold, with_reps=True, with_shots=True)
    np.savez(
        str(outdir.joinpath("above_threshold.npz")),
        **{p[0]: v for p, v in dataset.params.items()},
        above_threshold=above_threshold,
        versus=plot_versus,
        sliced=above_threshold[sl],
        **(
            dict(timetagger=dataset.tt_data[0] >= tt_threshold)
            if dataset.tt_data is not None else dict()
        ),
    )

    sl = gen_slicer(dataset.mpc[0], with_reps=False, with_shots=True)
    np.savez(
        str(outdir.joinpath("mpc_data.npz")),
        **{p[0]: v for p, v in dataset.params.items()},
        mpc=dataset.mpc[0],
        mpc_err=dataset.mpc[1],
        versus=plot_versus,
        sliced=dataset.mpc[0][sl],
        sliced_err=dataset.mpc[1][sl],
        **(
            dict(
                timetagger=dataset.tt_mpc[0],
                timetagger_err=dataset.tt_mpc[1],
            )
            if dataset.tt_mpc is not None else dict()
        ),
    )

    sl = gen_slicer(dataset.fat[0], with_reps=False, with_shots=True)
    np.savez(
        str(outdir.joinpath("fat_data.npz")),
        **{p[0]: v for p, v in dataset.params.items()},
        fat=dataset.fat[0],
        fat_err=dataset.fat[1],
        versus=plot_versus,
        sliced=dataset.fat[0][sl],
        sliced_err=dataset.fat[1][sl],
        **(
            dict(
                timetagger=dataset.tt_fat[0],
                timetagger_err=dataset.tt_fat[1],
            )
            if dataset.tt_fat is not None else dict()
        ),
    )

    if dataset.survival is not None:
        sl = gen_slicer(dataset.survival[0], with_reps=False, with_shots=False)
        np.savez(
            str(outdir.joinpath("survival_data.npz")),
            **{p[0]: v for p, v in dataset.params.items()},
            survival=dataset.survival[0],
            survival_err=dataset.survival[1],
            versus=plot_versus,
            sliced=dataset.survival[0][sl],
            sliced_err=dataset.survival[1][sl],
            **(
                dict(
                    timetagger=dataset.tt_survival[0],
                    timetagger_err=dataset.tt_survival[1],
                )
                if dataset.tt_survival is not None else dict()
            ),
        )

    if do_plotting:
        outdir_mpc = outdir.joinpath("mpc")
        outdir_fat = outdir.joinpath("fat")
        outdir_survival = outdir.joinpath("survival")
        for sub_outdir, flag in [
            ( outdir_mpc,      True           and do_plotting ),
            ( outdir_fat,      True           and do_plotting ),
            ( outdir_survival, num_images > 1 and do_plotting ),
        ]:
            if flag and not sub_outdir.is_dir():
                print(f":: mkdir -p {sub_outdir}")
                sub_outdir.mkdir(parents=True)

        outdir_img = outdir.joinpath("img")
        outdir_subimg = outdir.joinpath("subimg")
        outdir_hist = outdir.joinpath("hist")
        for sub_outdir, flag in [
            ( outdir_img,    render_whole_image and do_indiv_plotting ),
            ( outdir_subimg, render_subimages   and do_indiv_plotting ),
            ( outdir_hist,   plot_hist_totals   and do_indiv_plotting ),
        ]:
            if flag and not sub_outdir.is_dir():
                print(f":: mkdir -p {sub_outdir}")
                sub_outdir.mkdir(parents=True)

        if printflag:
            print("render output visuals")
        newline = "\n"
        rspace = sum(int(np.log10(len(X)) + 1) for X in dataset.params.values())

        ####################################

        if printflag:
            print("  aggregated quantities:")
        
        title_fmt = (
            filepath.name + "\n"
            + "_".join(
                f"{{:0{np.floor(np.log10(len(v))) + 1:.0f}.0f}}"
                for k, v in dataset.params.values_indexed()
                if k not in plot_versus_axes
            ) + "\n"
            + " | ".join(
                f"{p[0]}={{:+.3f}}{p[1]}{newline if k % 4 == 3 else ''}"
                for k, p in enumerate(
                    p_ for k_, p_ in dataset.params.keys_indexed()
                    if k_ not in plot_versus_axes
                )
            ) + "\n"
            + f"{shot_num = :.0f}"
        )
        png_name = (
            "_".join(
                f"{{:0{np.floor(np.log10(len(v))) + 1:.0f}.0f}}"
                for k, v in dataset.params.values_indexed()
                if k not in plot_versus_axes
            ) + ".png"
        )
        svg_name = (
            "_".join(
                f"{{:0{np.floor(np.log10(len(v))) + 1:.0f}.0f}}"
                for k, v in dataset.params.values_indexed()
                if k not in plot_versus_axes
            ) + ".svg"
        )

        plotvals, iterlabels, iterator = dataset.iter_agg(
            plot_versus_axes,
            shot_num,
        )
        pltlabels, pltvalues = zip(*plotvals)
        for (
            Q,
            V,
            roi_totals,
            mpc,
            fat,
            survival,
            tt_mpc,
            tt_fat,
            tt_survival
        ) in iterator:
            if printflag:
                print("\r  ", *Q, rspace * " ", end="", flush=True)

            if len(pltlabels) == 1:
                fig, ax = plotting.lineplot(
                    x=pltvalues[0],
                    y=mpc[0],
                    err=mpc[1],
                    pop=roi_totals[0] if violins_mpc else None,
                    xlabel=(
                        f"{pltlabels[0][0]} [{pltlabels[0][1]}]" if len(pltvalues[0]) > 1
                        else f"Site index  ({pltlabels[0][0]}={pltvalues[0][0]:+.3f}{pltlabels[0][1]})"
                    ),
                    ylabel="Mean photon count",
                    title=title_fmt.format(*Q, *V),
                    normalize=normalize_mpc,
                    indep_rois=indep_rois,
                )
                fig.tight_layout()
                fig.savefig(outdir_mpc.joinpath(png_name.format(*Q)))
                pp.close(fig)

                if tt_mpc is not None:
                    fig, ax = plotting.lineplot(
                        x=pltvalues[0],
                        y=tt_mpc[0],
                        err=tt_mpc[1],
                        pop=None,
                        xlabel=(
                            f"{pltlabels[0][0]} [{pltlabels[0][1]}]" if len(pltvalues[0]) > 1
                            else f"({pltlabels[0][0]}={pltvalues[0][0]:+.3f}{pltlabels[0][1]})"
                        ),
                        ylabel="Mean timetagger count",
                        title=title_fmt.format(*Q, *V),
                        normalize=normalize_mpc,
                        indep_rois=False,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_mpc.joinpath("tt_" + png_name.format(*Q)))
                    pp.close(fig)

                fig, ax = plotting.lineplot(
                    x=pltvalues[0],
                    y=fat[0],
                    err=fat[1],
                    pop=None,
                    xlabel=(
                        f"{pltlabels[0][0]} [{pltlabels[0][1]}]" if len(pltvalues[0]) > 1
                        else f"Site index  ({pltlabels[0][0]}={pltvalues[0][0]:+.3f}{pltlabels[0][1]})"
                    ),
                    ylabel="Fraction above threshold",
                    title=title_fmt.format(*Q, *V),
                    normalize=False,
                    indep_rois=indep_rois,
                )
                fig.tight_layout()
                fig.savefig(outdir_fat.joinpath(png_name.format(*Q)))
                pp.close(fig)

                if tt_fat is not None:
                    fig, ax = plotting.lineplot(
                        x=pltvalues[0],
                        y=tt_fat[0],
                        err=tt_fat[1],
                        pop=None,
                        xlabel=(
                            f"{pltlabels[0][0]} [{pltlabels[0][1]}]" if len(pltvalues[0]) > 1
                            else f"({pltlabels[0][0]}={pltvalues[0][0]:+.3f}{pltlabels[0][1]})"
                        ),
                        ylabel="Timetagger fraction above threshold",
                        title=title_fmt.format(*Q, *V),
                        normalize=False,
                        indep_rois=False,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_fat.joinpath("tt_" + png_name.format(*Q)))
                    pp.close(fig)

                if survival is not None:
                    fig, ax = plotting.lineplot(
                        x=pltvalues[0],
                        y=survival[0],
                        err=survival[1],
                        pop=None,
                        xlabel=(
                            f"{pltlabels[0][0]} [{pltlabels[0][1]}]" if len(pltvalues[0]) > 1
                            else f"Site index  ({pltlabels[0][0]}={pltvalues[0][0]:+.3f}{pltlabels[0][1]})"
                        ),
                        ylabel="Survival fraction",
                        title=title_fmt.format(*Q, *V),
                        normalize=False,
                        indep_rois=indep_rois,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_survival.joinpath(png_name.format(*Q)))
                    pp.close(fig)

                if tt_survival is not None:
                    fig, ax = plotting.lineplot(
                        x=pltvalues[0],
                        y=tt_survival[0],
                        err=tt_survival[1],
                        pop=None,
                        xlabel=(
                            f"{pltlabels[0][0]} [{pltlabels[0][1]}]" if len(pltvalues[0]) > 1
                            else f"({pltlabels[0][0]}={pltvalues[0][0]:+.3f}{pltlabels[0][1]})"
                        ),
                        ylabel="Timetagger survival fraction",
                        title=title_fmt.format(*Q, *V),
                        normalize=False,
                        indep_rois=False,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_survival.joinpath("tt_" + png_name.format(*Q)))
                    pp.close(fig)

            elif force_lines:
                fig, ax = plotting.multilineplot(
                    x=pltvalues[1],
                    y=pltvalues[0],
                    Z=mpc[0].mean(axis=0),
                    ERR=np.sqrt((mpc[1]**2).sum(axis=0)) / mpc.shape[1],
                    POP=roi_totals[0].mean(axis=0) if violins_mpc else None,
                    xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                    ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                    zlabel="Mean photon count",
                    title=title_fmt.format(*Q, *V),
                    versus_axis=multiline_plot_versus,
                    normalize=normalize_mpc,
                )
                fig.tight_layout()
                fig.savefig(outdir_mpc.joinpath(png_name.format(*Q)))
                pp.close(fig)

                if tt_mpc is not None:
                    fig, ax = plotting.multilineplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=tt_mpc[0].mean(axis=0),
                        ERR=np.sqrt((tt_mpc[1]**2).sum(axis=0)) / tt_mpc.shape[1],
                        POP=None,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        zlabel="Mean timetagger count",
                        title=title_fmt.format(*Q, *V),
                        versus_axis=multiline_plot_versus,
                        normalize=normalize_mpc,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_mpc.joinpath("tt_" + png_name.format(*Q)))
                    pp.close(fig)

                fig, ax = plotting.multilineplot(
                    x=pltvalues[1],
                    y=pltvalues[0],
                    Z=fat[0].mean(axis=0),
                    ERR=np.sqrt((fat[1]**2).sum(axis=0)) / fat.shape[1],
                    POP=None,
                    xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                    ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                    zlabel="Fraction above threshold",
                    title=title_fmt.format(*Q, *V),
                    versus_axis=multiline_plot_versus,
                    normalize=False,
                )
                fig.tight_layout()
                fig.savefig(outdir_fat.joinpath(png_name.format(*Q)))
                pp.close(fig)

                if tt_fat is not None:
                    fig, ax = plotting.multilineplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=tt_fat[0].mean(axis=0),
                        ERR=np.sqrt((tt_fat[1]**2).sum(axis=0)) / tt_fat.shape[1],
                        POP=None,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        zlabel="Timetagger fraction above threshold",
                        title=title_fmt.format(*Q, *V),
                        versus_axis=multiline_plot_versus,
                        normalize=False,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_fat.joinpath("tt_" + png_name.format(*Q)))
                    pp.close(fig)

                if survival is not None:
                    fig, ax = plotting.multilineplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=survival[0].mean(axis=0),
                        ERR=np.sqrt((survival[1]**2).sum(axis=0))
                            / survival.shape[1],
                        POP=None,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        zlabel="Survival fraction",
                        title=title_fmt.format(*Q, *V),
                        versus_axis=multiline_plot_versus,
                        normalize=normalize_mpc,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_survival.joinpath(png_name.format(*Q)))
                    pp.close(fig)

                if tt_survival is not None:
                    fig, ax = plotting.multilineplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=tt_survival[0].mean(axis=0),
                        ERR=np.sqrt((tt_survival[1]**2).sum(axis=0))
                            / tt_survival.shape[1],
                        POP=None,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        zlabel="Timetagger survival fraction",
                        title=title_fmt.format(*Q, *V),
                        versus_axis=multiline_plot_versus,
                        normalize=False,
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_survival.joinpath("tt_" + png_name.format(*Q)))
                    pp.close(fig)

            else:
                (fig, ax), (fig_err, ax_err) = plotting.colorplot(
                    x=pltvalues[1],
                    y=pltvalues[0],
                    Z=mpc[0].mean(axis=0),
                    ERR=np.sqrt((mpc[1]**2).sum(axis=0)) / mpc.shape[1],
                    cmap=plot_cmap,
                    xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                    ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                    clabel="Mean photon count",
                    title=title_fmt.format(*Q, *V),
                )
                fig.tight_layout()
                fig.savefig(outdir_mpc.joinpath(png_name.format(*Q)))
                fig_err.savefig(
                    outdir_mpc.joinpath(
                        Path(png_name.format(*Q)).stem + "_err.png"
                    )
                )
                pp.close(fig)
                pp.close(fig_err)

                if tt_mpc is not None:
                    (fig, ax), (fig_err, ax_err) = plotting.colorplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=tt_mpc[0].mean(axis=0),
                        ERR=np.sqrt((tt_mpc[1]**2).sum(axis=0)) / tt_mpc.shape[1],
                        cmap=plot_cmap,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        clabel="Mean timetagger count",
                        title=title_fmt.format(*Q, *V),
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_mpc.joinpath("tt_" + png_name.format(*Q)))
                    fig_err.savefig(
                        outdir_mpc.joinpath(
                            Path("tt_" + png_name.format(*Q)).stem + "_err.png"
                        )
                    )
                    pp.close(fig)
                    pp.close(fig_err)

                (fig, ax), (fig_err, ax_err) = plotting.colorplot(
                    x=pltvalues[1],
                    y=pltvalues[0],
                    Z=fat[0].mean(axis=0),
                    ERR=np.sqrt((fat[1]**2).sum(axis=0)) / fat.shape[1],
                    cmap=plot_cmap,
                    xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                    ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                    clabel="Fraction above threshold",
                    title=title_fmt.format(*Q, *V),
                )
                fig.tight_layout()
                fig.savefig(outdir_fat.joinpath(png_name.format(*Q)))
                fig_err.savefig(
                    outdir_fat.joinpath(
                        Path(png_name.format(*Q)).stem + "_err.png"
                    )
                )
                pp.close(fig)
                pp.close(fig_err)

                if tt_fat is not None:
                    (fig, ax), (fig_err, ax_err) = plotting.colorplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=tt_fat[0].mean(axis=0),
                        ERR=np.sqrt((tt_fat[1]**2).sum(axis=0)) / tt_fat.shape[1],
                        cmap=plot_cmap,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        clabel="Timetagger fraction above threshold",
                        title=title_fmt.format(*Q, *V),
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_fat.joinpath("tt_" + png_name.format(*Q)))
                    fig_err.savefig(
                        outdir_fat.joinpath(
                            Path("tt_" + png_name.format(*Q)).stem + "_err.png"
                        )
                    )
                    pp.close(fig)
                    pp.close(fig_err)

                if survival is not None:
                    (fig, ax), (fig_err, ax_err) = plotting.colorplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=survival[0].mean(axis=0),
                        ERR=np.sqrt((survival[1]**2).sum(axis=0))
                            / survival.shape[1],
                        cmap=plot_cmap,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        clabel="Survival fraction",
                        title=title_fmt.format(*Q, *V),
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_survival.joinpath(png_name.format(*Q)))
                    fig_err.savefig(
                        outdir_survival.joinpath(
                            Path(png_name.format(*Q)).stem + "_err.png"
                        )
                    )
                    pp.close(fig)
                    pp.close(fig_err)

                if tt_survival is not None:
                    (fig, ax), (fig_err, ax_err) = plotting.colorplot(
                        x=pltvalues[1],
                        y=pltvalues[0],
                        Z=tt_survival[0].mean(axis=0),
                        ERR=np.sqrt((tt_survival[1]**2).sum(axis=0))
                            / tt_survival.shape[1],
                        cmap=plot_cmap,
                        xlabel=f"{pltlabels[1][0]} [{pltlabels[1][1]}]",
                        ylabel=f"{pltlabels[0][0]} [{pltlabels[0][1]}]",
                        clabel="Timetagger survival fraction",
                        title=title_fmt.format(*Q, *V),
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_survival.joinpath("tt_" + png_name.format(*Q)))
                    fig_err.savefig(
                        outdir_survival.joinpath(
                            Path("tt_" + png_name.format(*Q)).stem + "_err.png"
                        )
                    )
                    pp.close(fig)
                    pp.close(fig_err)

        if printflag:
            print("")

        ####################################

        if printflag:
            print("  populational quantities:")

        title_fmt = (
            filepath.name + "\n"
            + "_".join(
                f"{{:0{np.floor(np.log10(len(v))) + 1:.0f}.0f}}"
                for v in dataset.params.values()
            ) + "\n"
            + " | ".join(
                f"{p[0]}={{:+.3f}}{p[1]}{newline if k % 4 == 3 else ''}"
                for k, p in dataset.params.keys_indexed()
            ) + "\n"
            + f"{shot_num = :.0f}"
        )
        png_name = (
            "_".join(
                f"{{:0{np.floor(np.log10(len(v))) + 1:.0f}.0f}}"
                for v in dataset.params.values()
            ) + ".png"
        )

        image_vmax = dataset.img_avg[(
            *(len(paramslist) * [S[:]]), int(shot_num), S[:], S[:]
        )].max()
        subimage_vmax = dataset.roi_img_avg[(
            S[:], *(len(paramslist) * [S[:]]), int(shot_num), S[:], S[:]
        )].max()
        hist_vmax = dataset.roi_totals[(
            0, S[:], S[:], *(len(paramslist) * [S[:]]), int(shot_num)
        )].max()
        hist_vmin = dataset.roi_totals[(
            0, S[:], S[:], *(len(paramslist) * [S[:]]), int(shot_num)
        )].min()
        tt_hist_vmax = dataset.tt_data[(
            0, S[:], *(len(paramslist) * [S[:]]), int(shot_num)
        )].max() if dataset.tt_data is not None else None
        tt_hist_vmin = dataset.tt_data[(
            0, S[:], *(len(paramslist) * [S[:]]), int(shot_num)
        )].min() if dataset.tt_data is not None else None
        iterlabels, iterator = dataset.iter_indiv(shot_num)
        for (
            Q,
            V,
            img,
            locs,
            subimgs,
            roi_totals,
            tt_totals,
        ) in iterator:
            if printflag:
                print("\r  ", *Q, rspace * " ", end="", flush=True)
            # whole image
            if render_whole_image and do_indiv_plotting:
                fig, ax = plotting.render_image(
                    img=img,
                    cmap=image_cmap,
                    vmin=0.0,
                    vmax=image_vmax,
                    clabel="Photons",
                    title=title_fmt.format(*Q, *V),
                    draw_guides=draw_roi_guides,
                    rois=rois_nom,
                    roi_optim_locs=locs,
                    draw_optim_locs=optim_pad > 0,
                )
                fig.tight_layout()
                fig.savefig(outdir_img.joinpath(png_name.format(*Q)))
                pp.close(fig)

            # render each sub-image
            if render_subimages and do_indiv_plotting:
                fig, axs = plotting.render_subimages(
                    imgs=subimgs,
                    cmap=image_cmap,
                    vmin=0.0,
                    vmax=subimage_vmax,
                    clabel="Photons",
                    title=title_fmt.format(*Q, *V),
                    draw_guides=draw_roi_guides,
                )
                fig.tight_layout()
                fig.savefig(outdir_subimg.joinpath(png_name.format(*Q)))
                pp.close(fig)

            # histogram
            if plot_hist_totals and do_indiv_plotting:
                fig, ax = plotting.histogram(
                    data=roi_totals[0],
                    mean=roi_totals[0].mean(),
                    mean_err=np.sqrt((roi_totals[1]**2).sum())
                        / roi_totals.shape[1] / roi_totals.shape[2],
                    threshold=threshold,
                    bins=np.arange(
                        min(hist_vmin, -3 * hist_bin_size),
                        hist_vmax + hist_bin_size,
                        hist_bin_size
                    ),
                    density=True,
                    xlabel="Photons",
                    ylabel="Prob. density",
                    title=title_fmt.format(*Q, *V),
                )
                # ax.set_xlim(hist_vmin, roi_totals[0].max() + 2 * hist_bin_size)
                fig.tight_layout()
                fig.savefig(outdir_hist.joinpath(png_name.format(*Q)))
                pp.close(fig)
                if indep_rois:
                    fig, ax = plotting.subimages_histogram(
                        data=roi_totals[0],
                        means=roi_totals[0].mean(axis=1),
                        means_err=np.sqrt((roi_totals[1]**2).sum(axis=1))
                            / roi_totals.shape[2],
                        threshold=threshold,
                        bins=np.arange(
                            min(hist_vmin, -3 * hist_bin_size),
                            hist_vmax + hist_bin_size,
                            hist_bin_size
                        ),
                        density=True,
                        xlabel="Photons",
                        ylabel="Prob. density",
                        title=title_fmt.format(*Q, *V),
                    )
                    fig.tight_layout()
                    fig.savefig(outdir_hist.joinpath(
                        png_name.format(*Q).replace(".png", "_indep.png")
                    ))
                    # fig.savefig(outdir_hist.joinpath(
                    #     png_name.format(*Q).replace(".png", "_indep.svg")
                    # ))
                    pp.close(fig)
            if plot_array_average_hist:
                roi_total = roi_totals[0].flatten()
                roi_total_err = roi_totals[1].flatten()
                fig, ax = plotting.histogram(
                    data=roi_total,
                    mean=roi_total.mean(),
                    mean_err=np.sqrt((roi_total_err**2).sum())
                        / roi_total_err.shape[0],
                    threshold=threshold,
                    bins=np.arange(
                        min(hist_vmin, -3 * hist_bin_size),
                        hist_vmax + hist_bin_size,
                        hist_bin_size
                    ),
                    density=True,
                    xlabel="Photons",
                    ylabel="Prob. density",
                    title=title_fmt.format(*Q, *V),
                )
                # ax.set_xlim(hist_vmin, roi_totals[0].max() + 2 * hist_bin_size)
                fig.tight_layout()
                fig.savefig(outdir_hist.joinpath(png_name.format(*Q).replace(".png", "_avg.png")))
                pp.close(fig)

            if (
                (tt_totals is not None)
                and (plot_hist_totals or plot_array_average_hist)
            ):
                fig, ax = plotting.histogram(
                    data=tt_totals[0],
                    mean=tt_totals[0].mean(),
                    mean_err=np.sqrt((tt_totals[1]**2).sum()) / tt_totals.shape[1],
                    threshold=tt_threshold,
                    bins=np.arange(
                        min(tt_hist_vmin, -3 * tt_hist_bin_size),
                        tt_hist_vmax + tt_hist_bin_size,
                        tt_hist_bin_size
                    ),
                    density=True,
                    xlabel="Timetagger counts",
                    ylabel="Prob. density",
                    title=title_fmt.format(*Q, *V),
                )
                fig.tight_layout()
                fig.savefig(outdir_hist.joinpath("tt_" + png_name.format(*Q)))
                pp.close(fig)

        if printflag:
            print("")
    print("done.")

if __name__ == "__main__":
    for infile in infiles:
        process_file(infile)

