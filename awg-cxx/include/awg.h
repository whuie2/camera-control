#pragma once
#include <memory>
#include <vector>
#include "rust/cxx.h"
#include "awg-cxx/awg-control/Cpp/lib/AWG.h"
#include "awg-cxx/awg-control/Cpp/lib/waveform.h"

class PageAlignedMem {
public:
    PageAlignedMem(void *mem);
    ~PageAlignedMem();

    void *get_ptr();
private:
    void *ptr;
};

class WaveformData {
public:
    WaveformData(void *mem, int64_t datalen);
    ~WaveformData();

    void *get_mem();
    int64_t get_datalen();
private:
    void *mem;
    int64_t datalen;
};

class AWGBox {
public:
    AWGBox(int);
    ~AWGBox();

    AWG &get_awg();
private:
    AWG awg;
};

std::unique_ptr<AWGBox> new_awg(int idx);

void awg_reset(AWGBox &awg);
int awg_get_card_idx(AWGBox &awg);
int awg_get_serial_number(AWGBox &awg);
int64_t awg_get_inst_mem_size(AWGBox &awg);
int awg_get_bytes_per_sample(AWGBox &awg);
int64_t awg_get_max_sample_rate(AWGBox &awg);
void awg_set_sample_rate(AWGBox &awg, int64_t sample_rate);
int64_t awg_get_sample_rate(AWGBox &awg);

void awg_set_active_channels(AWGBox &awg, const std::vector<int> &channels);
void awg_set_channel_amp(AWGBox &awg, int ch, int amp);
void awg_set_channel_stop_level(AWGBox &awg, int ch, int stop_level);
void awg_toggle_channel_output(AWGBox &awg, int ch, bool enable);
void awg_set_channel(AWGBox &awg, int ch, int amp, int stop_level, bool enable);
int awg_get_channel_count(AWGBox &awg);
rust::Vec<int> awg_get_channel_activated(AWGBox &awg);
void awg_set_channel_diff_mode(AWGBox &awg, int ch_pair, bool enable);
void awg_set_channel_double_mode(AWGBox &awg, int ch_pair, bool enable);

void awg_write_setup(AWGBox &awg);
void awg_card_run(AWGBox &awg);
void awg_card_stop(AWGBox &awg);
void awg_wait_ready(AWGBox &awg);
void awg_set_timeout(AWGBox &awg, int timeout_ms);
int awg_get_timeout(AWGBox &awg);
void awg_print_card_status(AWGBox &awg);

void awg_toggle_trigger(AWGBox &awg, bool enable);
void awg_force_trigger(AWGBox &awg);
void awg_wait_trigger(AWGBox &awg);
void awg_set_trig_mask_or(AWGBox &awg, const std::vector<int> &masks);
void awg_set_trig_mask_and(AWGBox &awg, const std::vector<int> &masks);
void awg_set_trig_mode(AWGBox &awg, int ch, int mode);
void awg_set_trig_term(AWGBox &awg, int term);
int awg_get_trig_term(AWGBox &awg);
void awg_set_trig_coupling(AWGBox &awg, int ch, int coupling);
int awg_get_trig_coupling(AWGBox &awg, int ch);
void awg_set_trig_level(AWGBox &awg, int ch, int level);
int awg_get_trig_level(AWGBox &awg, int ch);
void awg_set_trig_rearm_level(AWGBox &awg, int ch, int level);
int64_t awg_get_trig_rearm_level(AWGBox &awg, int ch);

void awg_init_replay_mode_seq(AWGBox &awg, int n_segments);
void awg_set_seq_mode_step(AWGBox &awg, uint64_t step, uint64_t segment, uint64_t next_step, uint64_t n_loop, int condition);
void awg_write_seq_mode_segment(AWGBox &awg, int segment, PageAlignedMem &p_data_buffer, int size);

void awg_prep_data_transfer(AWGBox &awg, PageAlignedMem &p_data_buffer, uint64_t buffer_len, int buf_type, int dir, uint32_t notify_size, uint64_t board_mem_offs);
void awg_start_data_transfer(AWGBox &awg);
/**/ void awg_wait_data_transfer(AWGBox &awg);
void awg_stop_data_transfer(AWGBox &awg);

void awg_set_clock_mode(AWGBox &awg, int mode);
int awg_get_clock_mode(AWGBox &awg);
void awg_set_ref_clock_freq(AWGBox &awg, int64_t frequency);
void awg_set_clock_out(AWGBox &awg, bool enable);
int64_t awg_get_clock_out_freq(AWGBox &awg);

// std::unique_ptr<PageAlignedMem> awg_get_page_aligned_mem(AWGBox &awg, uint64_t size);
// void awg_free_page_aligned_mem(AWGBox &awg, std::unique_ptr<PageAlignedMem> p_data_buffer, uint64_t size);

/******************************************************************************/

std::unique_ptr<ArrayWaveform> new_arraywaveform();

std::unique_ptr<PageAlignedMem> arraywaveform_get_data_buffer(ArrayWaveform &waveform);
int64_t arraywaveform_get_data_len(ArrayWaveform &waveform);

void arraywaveform_set_sampling_rate(ArrayWaveform &waveform, ulong rate);
ulong arraywaveform_get_sampling_rate(ArrayWaveform &waveform);
void arraywaveform_set_freq_resolution(ArrayWaveform &waveform, ulong resolution);
ulong arraywaveform_get_freq_resolution(ArrayWaveform &waveform);

void arraywaveform_set_freq_tones_spaced(ArrayWaveform &waveform, int center, int spacing, int num_tones);
void arraywaveform_set_freq_tones(ArrayWaveform &waveform, const std::vector<int> &tones);
rust::Vec<double> arraywaveform_get_freq_tones(ArrayWaveform &waveform);
void arraywaveform_set_phases(ArrayWaveform &waveform, const std::vector<double> &phases);
rust::Vec<double> arraywaveform_get_phases(ArrayWaveform &waveform);
void arraywaveform_set_amplitudes(ArrayWaveform &waveform, const std::vector<double>& amplitudes);
rust::Vec<double> arraywaveform_get_amplitudes(ArrayWaveform &waveform);
void arraywaveform_set_default_params(ArrayWaveform &waveform);
void arraywaveform_save_params(ArrayWaveform &waveform, const std::string &filename);
void arraywaveform_load_params(ArrayWaveform &waveform, const std::string &filename);
void arraywaveform_print_params(ArrayWaveform &waveform);

ulong arraywaveform_get_min_sample_len(ArrayWaveform &waveform, ulong sampling_rate, ulong frequency_resolution);
ulong arraywaveform_get_sample_len(ArrayWaveform &waveform, double tau, ulong sampling_rate, ulong frequency_resolution);

std::unique_ptr<WaveformData> arraywaveform_get_static_waveform(ArrayWaveform &waveform);
std::unique_ptr<WaveformData> arraywaveform_get_trick_waveform(ArrayWaveform &waveform, const std::vector<int> &site_index, double df, double tau_move, double tau_stay);

void arraywaveform_save_waveform(ArrayWaveform &waveform, const std::string &filename);

/******************************************************************************/

std::unique_ptr<PageAlignedMem> waveformdata_get_mem(WaveformData &data);
int64_t waveformdata_get_datalen(WaveformData &data);

