#include "awg-cxx/awg-control/Cpp/lib/AWG.h"
#include "awg-cxx/include/awg.h"
#include "rust/cxx.h"

/* converters */

std::set<int> int_vector_to_int_set(const std::vector<int> &vals) {
    std::set<int> new_vals;
    for (const int &v : vals)
        new_vals.insert(v);
    return new_vals;
}

rust::Vec<int> int_set_to_int_vec(std::set<int> vals) {
    rust::Vec<int> new_vals;
    for (int v : vals)
        new_vals.push_back(v);
    return new_vals;
}

rust::Vec<double> to_f64_vec(std::vector<double> &v) {
    rust::Vec<double> out;
    for (double f : v)
        out.push_back(f);
    return out;
}

AWG::CHANNEL_STOPLVL to_channel_stop_level(int level) {
    switch (level) {
        case SPCM_STOPLVL_ZERO:
            return AWG::CHANNEL_STOPLVL::ZERO;
        case SPCM_STOPLVL_LOW:
            return AWG::CHANNEL_STOPLVL::LOW;
        case SPCM_STOPLVL_HIGH:
            return AWG::CHANNEL_STOPLVL::HIGH;
        case SPCM_STOPLVL_HOLDLAST:
            return AWG::CHANNEL_STOPLVL::HOLDLAST;
        default:
            return AWG::CHANNEL_STOPLVL::ZERO;
    }
}

AWG::REPLAY_MODE to_replay_mode(int mode) {
    switch (mode) {
        case SPC_REP_STD_SINGLE:
            return AWG::REPLAY_MODE::SINGLE;
        case SPC_REP_STD_MULTI:
            return AWG::REPLAY_MODE::MULTI;
        case SPC_REP_STD_GATE:
            return AWG::REPLAY_MODE::GATE;
        case SPC_REP_STD_SINGLERESTART:
            return AWG::REPLAY_MODE::SINGLERESTART;
        case SPC_REP_STD_SEQUENCE:
            return AWG::REPLAY_MODE::SEQUENCE;
        case SPC_REP_FIFO_SINGLE:
            return AWG::REPLAY_MODE::FIFO_SINGLE;
        case SPC_REP_FIFO_MULTI:
            return AWG::REPLAY_MODE::FIFO_MULTI;
        case SPC_REP_FIFO_GATE:
            return AWG::REPLAY_MODE::FIFO_GATE;
        default:
            return AWG::REPLAY_MODE::SINGLE;
    }
}

AWG::BUFFER_TYPE to_buffer_type(int type) {
    switch (type) {
        case SPCM_BUF_DATA:
            return AWG::BUFFER_TYPE::DATA;
        case SPCM_BUF_ABA:
            return AWG::BUFFER_TYPE::ABA;
        case SPCM_BUF_TIMESTAMP:
            return AWG::BUFFER_TYPE::TIMESTAMP;
        default:
            return AWG::BUFFER_TYPE::DATA;
    }
}

AWG::TRANSFER_DIR to_transfer_dir(int dir) {
    switch (dir) {
        case SPCM_DIR_PCTOCARD:
            return AWG::TRANSFER_DIR::PCTOCARD;
        case SPCM_DIR_CARDTOPC:
            return AWG::TRANSFER_DIR::CARDTOPC;
        case SPCM_DIR_CARDTOGPU:
            return AWG::TRANSFER_DIR::CARDTOGPU;
        case SPCM_DIR_GPUTOCARD:
            return AWG::TRANSFER_DIR::GPUTOCARD;
        default:
            return AWG::TRANSFER_DIR::PCTOCARD;
    }
}

AWG::CLOCK_MODE to_clock_mode(int mode) {
    switch (mode) {
        case SPC_CM_INTPLL:
            return AWG::CLOCK_MODE::INTPLL;
        case SPC_CM_EXTREFCLOCK:
            return AWG::CLOCK_MODE::EXTREFCLK;
        default:
            return AWG::CLOCK_MODE::INTPLL;
    }
}

AWG::TRIGGER_MASK to_trigger_mask(int mask) {
    switch (mask) {
        case SPC_TMASK_NONE:
            return AWG::TRIGGER_MASK::NONE;
        case SPC_TMASK_SOFTWARE:
            return AWG::TRIGGER_MASK::SOFTWARE;
        case SPC_TMASK_EXT0:
            return AWG::TRIGGER_MASK::EXT0;
        case SPC_TMASK_EXT1:
            return AWG::TRIGGER_MASK::EXT1;
        default:
            return AWG::TRIGGER_MASK::NONE;
    }
}

std::vector<AWG::TRIGGER_MASK> to_mask_vector(const std::vector<int> &masks) {
    std::vector<AWG::TRIGGER_MASK> new_masks;
    for (int m : masks)
        new_masks.push_back(to_trigger_mask(m));
    return new_masks;
}

AWG::TRIGGER_MODE to_trigger_mode(int mode) {
    switch (mode) {
        case SPC_TM_NONE:
            return AWG::TRIGGER_MODE::NONE;
        case SPC_TM_POS:
            return AWG::TRIGGER_MODE::POS;
        case SPC_TM_NEG:
            return AWG::TRIGGER_MODE::NEG;
        case SPC_TM_POS | SPC_TM_REARM:
            return AWG::TRIGGER_MODE::POS_REARM;
        case SPC_TM_NEG | SPC_TM_REARM:
            return AWG::TRIGGER_MODE::NEG_REARM;
        case SPC_TM_BOTH:
            return AWG::TRIGGER_MODE::BOTH;
        case SPC_TM_HIGH:
            return AWG::TRIGGER_MODE::HIGH;
        case SPC_TM_LOW:
            return AWG::TRIGGER_MODE::LOW;
        case SPC_TM_WINENTER:
            return AWG::TRIGGER_MODE::WINENTER;
        case SPC_TM_WINLEAVE:
            return AWG::TRIGGER_MODE::WINLEAVE;
        case SPC_TM_INWIN:
            return AWG::TRIGGER_MODE::INWIN;
        case SPC_TM_OUTSIDEWIN:
            return AWG::TRIGGER_MODE::OUTSIDEWIN;
        default:
            return AWG::TRIGGER_MODE::NONE;
    }
}

AWG::SEQ_LOOPCONDITION to_seq_loop_condition(int64_t condition) {
    switch (condition) {
        case SPCSEQ_ENDLOOPALWAYS:
            return AWG::SEQ_LOOPCONDITION::ALWAYS;
        case SPCSEQ_ENDLOOPONTRIG:
            return AWG::SEQ_LOOPCONDITION::ONTRIG;
        case SPCSEQ_END:
            return AWG::SEQ_LOOPCONDITION::END;
        default:
            return AWG::SEQ_LOOPCONDITION::ALWAYS;
    }
}

/* PageAlignedMem */

PageAlignedMem::PageAlignedMem(void *mem) {
    this->ptr = mem;
}

void *PageAlignedMem::get_ptr() {
    return this->ptr;
}

/* WaveformData */

WaveformData::WaveformData(void *mem, int64_t datalen) {
    this->mem = mem;
    this->datalen = datalen;
}

void *WaveformData::get_mem() {
    return this->mem;
}

int64_t WaveformData::get_datalen() {
    return this->datalen;
}

/* AWGBox */

AWGBox::AWGBox(int idx) {
    this->awg.open(idx);
}

AWGBox::~AWGBox() {
    this->awg.close();
}

AWG &AWGBox::get_awg() {
    return this->awg;
}

/* AWG methods */

void awg_reset(AWGBox &awg) {
    awg.get_awg().reset();
}

int awg_get_card_idx(AWGBox &awg) {
    return awg.get_awg().getCardIdx();
}

int awg_get_serial_number(AWGBox &awg) {
    return awg.get_awg().getSerialNumber();
}

int64_t awg_get_inst_mem_size(AWGBox &awg) {
    return awg.get_awg().getInstMemSize();
}

int awg_get_bytes_per_sample(AWGBox &awg) {
    return awg.get_awg().getBytesPerSample();
}

int64_t awg_get_max_sample_rate(AWGBox &awg) {
    return awg.get_awg().getMaxSampleRate();
}

void awg_set_sample_rate(AWGBox &awg, int64_t sample_rate) {
    awg.get_awg().setSampleRate(sample_rate);
}

int64_t awg_get_sample_rate(AWGBox &awg) {
    return awg.get_awg().getSampleRate();
}

void awg_set_active_channels(AWGBox &awg, const std::vector<int> &channels) {
    awg.get_awg().setActiveChannels(int_vector_to_int_set(channels));
}

void awg_set_channel_amp(AWGBox &awg, int ch, int amp) {
    awg.get_awg().setChannelAmp(ch, amp);
}

void awg_set_channel_stop_level(AWGBox &awg, int ch, int stop_level) {
    awg.get_awg().setChannelStopLvl(ch, to_channel_stop_level(stop_level));
}

void awg_toggle_channel_output(AWGBox &awg, int ch, bool enable) {
    awg.get_awg().toggleChannelOutput(ch, enable);
}

void awg_set_channel(
    AWGBox &awg,
    int ch,
    int amp,
    int stop_level,
    bool enable
) {
    awg.get_awg()
        .setChannel(
            ch,
            amp,
            to_channel_stop_level(stop_level),
            enable
        );
}

int awg_get_channel_count(AWGBox &awg) {
    return awg.get_awg().getChannelCount();
}

rust::Vec<int> awg_get_channel_activated(AWGBox &awg) {
    return int_set_to_int_vec(awg.get_awg().getChannelActivated());
}

void awg_set_channel_diff_mode(AWGBox &awg, int ch_pair, bool enable) {
    awg.get_awg().setChannelDiffMode(ch_pair, enable);
}

void awg_set_channel_double_mode(AWGBox &awg, int ch_pair, bool enable) {
    awg.get_awg().setChannelDoubleMode(ch_pair, enable);
}

void awg_write_setup(AWGBox &awg) {
    awg.get_awg().writeSetup();
}

void awg_card_run(AWGBox &awg) {
    awg.get_awg().cardRun();
}

void awg_card_stop(AWGBox &awg) {
    awg.get_awg().cardStop();
}

void awg_wait_ready(AWGBox &awg) {
    awg.get_awg().waitReady();
}

void awg_set_timeout(AWGBox &awg, int timeout_ms) {
    awg.get_awg().setTimeOut(timeout_ms);
}

int awg_get_timeout(AWGBox &awg) {
    return awg.get_awg().getTimeOut();
}

void awg_print_card_status(AWGBox &awg) {
    awg.get_awg().printCardStatus();
}

void awg_toggle_trigger(AWGBox &awg, bool enable) {
    awg.get_awg().toggleTrigger(enable);
}

void awg_force_trigger(AWGBox &awg) {
    awg.get_awg().forceTrigger();
}

void awg_wait_trigger(AWGBox &awg) {
    awg.get_awg().waitTrigger();
}

void awg_set_trig_mask_or(AWGBox &awg, const std::vector<int> &masks) {
    awg.get_awg().setTrigMaskOr(to_mask_vector(masks));
}

void awg_set_trig_mask_and(AWGBox &awg, const std::vector<int> &masks) {
    awg.get_awg().setTrigMaskAnd(to_mask_vector(masks));
}

void awg_set_trig_mode(AWGBox &awg, int ch, int mode) {
    awg.get_awg().setTrigMode(ch, to_trigger_mode(mode));
}

void awg_set_trig_term(AWGBox &awg, int term) {
    awg.get_awg().setTrigTerm(term);
}

int awg_get_trig_term(AWGBox &awg) {
    return awg.get_awg().getTrigTerm();
}

void awg_set_trig_coupling(AWGBox &awg, int ch, int coupling) {
    awg.get_awg().setTrigCoupling(ch, coupling);
}

int awg_get_trig_coupling(AWGBox &awg, int ch) {
    return awg.get_awg().getTrigCoupling(ch);
}

void awg_set_trig_level(AWGBox &awg, int ch, int level) {
    awg.get_awg().setTrigLvl(ch, level);
}

int awg_get_trig_level(AWGBox &awg, int ch) {
    return awg.get_awg().getTrigLvl(ch);
}

void awg_set_trig_rearm_level(AWGBox &awg, int ch, int level) {
    awg.get_awg().setTrigRearmLvl(ch, level);
}

int64_t awg_get_trig_rearm_level(AWGBox &awg, int ch) {
    return awg.get_awg().getTrigRearmLvl(ch);
}

void awg_init_replay_mode_seq(AWGBox &awg, int n_segments) {
    awg.get_awg().initReplayModeSeq(n_segments);
}

void awg_set_seq_mode_step(
    AWGBox &awg,
    uint64_t step,
    uint64_t segment,
    uint64_t next_step,
    uint64_t n_loop,
    int condition
) {
    awg.get_awg()
        .setSeqModeStep(
            step,
            segment,
            next_step,
            n_loop,
            to_seq_loop_condition(condition)
        );
}

void awg_write_seq_mode_segment(
    AWGBox &awg,
    int segment,
    PageAlignedMem &p_data_buffer,
    int size
) {
    awg.get_awg()
        .writeSeqModeSegment(
            segment,
            p_data_buffer.get_ptr(),
            size
        );
}

void awg_prep_data_transfer(
    AWGBox &awg,
    PageAlignedMem &p_data_buffer,
    uint64_t buffer_len,
    int buf_type,
    int dir,
    uint32_t notify_size,
    uint64_t board_mem_offs
) {
    awg.get_awg()
        .prepDataTransfer(
            p_data_buffer.get_ptr(),
            buffer_len,
            to_buffer_type(buf_type),
            to_transfer_dir(dir),
            notify_size,
            board_mem_offs
        );
}

void awg_start_data_transfer(AWGBox &awg) {
    awg.get_awg().startDataTransfer();
}

void awg_stop_data_transfer(AWGBox &awg) {
    awg.get_awg().stopDataTransfer();
}

void awg_set_clock_mode(AWGBox &awg, int mode) {
    awg.get_awg().setClockMode(to_clock_mode(mode));
}

int awg_get_clock_mode(AWGBox &awg) {
    return awg.get_awg().getClockMode();
}

void awg_set_ref_clock_freq(AWGBox &awg, int64_t frequency) {
    awg.get_awg().setRefClkFreq(frequency);
}

void awg_set_clock_out(AWGBox &awg, bool enable) {
    awg.get_awg().setClockOut(enable);
}

int64_t awg_get_clock_out_freq(AWGBox &awg) {
    return awg.get_awg().getClockOutFreq();
}

// std::unique_ptr<PageAlignedMem> awg_get_page_aligned_mem(
//     AWGBox &awg,
//     uint64_t size
// ) {
//     std::unique_ptr<PageAlignedMem>
//         page_aligned_mem(new PageAlignedMem(awg.get_awg().getPageAlignedMem(size)));
//     return page_aligned_mem;
// }
//
// void awg_free_page_aligned_mem(
//     AWGBox &awg,
//     std::unique_ptr<PageAlignedMem> p_data_buffer,
//     uint64_t size
// ) {
//     awg.get_awg()
//         .freePageAlignedMem(
//             p_data_buffer.release()->get_ptr(),
//             size
//         );
// }

/******************************************************************************/

std::unique_ptr<ArrayWaveform> new_arraywaveform() {
    std::unique_ptr<ArrayWaveform>
        waveform_ptr_unique(new ArrayWaveform());
    return waveform_ptr_unique;
}

std::unique_ptr<PageAlignedMem> arraywaveform_get_data_buffer(
    ArrayWaveform &waveform
) {
    std::unique_ptr<PageAlignedMem>
        page_aligned_mem(new PageAlignedMem(waveform.getDataBuffer()));
    return page_aligned_mem;
}

int64_t arraywaveform_get_data_len(ArrayWaveform &waveform) {
    return waveform.getDataLen();
}

void arraywaveform_set_sampling_rate(ArrayWaveform &waveform, ulong rate) {
    waveform.setSamplingRate(rate);
}

ulong arraywaveform_get_sampling_rate(ArrayWaveform &waveform) {
    return waveform.getSamplingRate();
}

void arraywaveform_set_freq_resolution(
    ArrayWaveform &waveform,
    ulong resolution
) {
    waveform.setFreqResolution(resolution);
}

ulong arraywaveform_get_freq_resolution(ArrayWaveform &waveform) {
    return waveform.getFreqResolution();
}

void arraywaveform_set_freq_tones_spaced(
    ArrayWaveform &waveform,
    int center,
    int spacing,
    int num_tones
) {
    waveform.setFreqTone(center, spacing, num_tones);
}

void arraywaveform_set_freq_tones(
    ArrayWaveform &waveform,
    const std::vector<int> &tones
) {
    waveform.setFreqTone(tones);
}

rust::Vec<double> arraywaveform_get_freq_tones(ArrayWaveform &waveform) {
    std::vector<double> tones = waveform.getFreqTone();
    return to_f64_vec(tones);
}

void arraywaveform_set_phases(
    ArrayWaveform &waveform,
    const std::vector<double> &phases
) {
    waveform.setPhase(phases);
}

rust::Vec<double> arraywaveform_get_phases(ArrayWaveform &waveform) {
    std::vector<double> phases = waveform.getPhase();
    return to_f64_vec(phases);
}

void arraywaveform_set_amplitudes(
    ArrayWaveform &waveform,
    const std::vector<double>& amplitudes
) {
    waveform.setAmplitude(amplitudes);
}

rust::Vec<double> arraywaveform_get_amplitudes(ArrayWaveform &waveform) {
    std::vector<double> amplitudes = waveform.getAmplitude();
    return to_f64_vec(amplitudes);
}

void arraywaveform_set_default_params(ArrayWaveform &waveform) {
    waveform.setDefaultParam();
}

void arraywaveform_save_params(
    ArrayWaveform &waveform,
    const std::string &filename
) {
    waveform.saveParam(filename);
}

void arraywaveform_load_params(
    ArrayWaveform &waveform,
    const std::string &filename
) {
    waveform.loadParam(filename);
}

void arraywaveform_print_params(ArrayWaveform &waveform) {
    waveform.printParam();
}

ulong arraywaveform_get_min_sample_len(
    ArrayWaveform &waveform,
    ulong sampling_rate,
    ulong frequency_resolution
) {
    return waveform.getMinSampleLen(sampling_rate, frequency_resolution);
}

ulong arraywaveform_get_sample_len(
    ArrayWaveform &waveform,
    double tau,
    ulong sampling_rate,
    ulong frequency_resolution
) {
    return waveform.getSampleLen(tau, sampling_rate, frequency_resolution);
}

std::unique_ptr<WaveformData> arraywaveform_get_static_waveform(
    ArrayWaveform &waveform
) {
    auto [mem, datalen] = waveform.getStaticWaveform();
    std::unique_ptr<WaveformData>
        data_ptr_unique(new WaveformData(mem, datalen));
    return data_ptr_unique;
}

std::unique_ptr<WaveformData> arraywaveform_get_trick_waveform(
    ArrayWaveform &waveform,
    const std::vector<int> &site_index,
    double df,
    double tau_move,
    double tau_stay
) {
    std::set<int> sites = int_vector_to_int_set(site_index);
    auto [mem, datalen]
        = waveform.getTrickWaveform(sites, df, tau_move, tau_stay);
    std::unique_ptr<WaveformData>
        data_ptr_unique(new WaveformData(mem, datalen));
    return data_ptr_unique;
}

void arraywaveform_save_waveform(
    ArrayWaveform &waveform,
    const std::string &filename
) {
    waveform.saveWaveform(filename);
}

/******************************************************************************/

std::unique_ptr<PageAlignedMem> waveformdata_get_mem(WaveformData &data) {
    std::unique_ptr<PageAlignedMem>
        mem_ptr_unique(new PageAlignedMem(data.get_mem()));
    return mem_ptr_unique;
}

int64_t waveformdata_get_datalen(WaveformData &data) {
    return data.get_datalen();
}

