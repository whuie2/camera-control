pub use crate::awg::{
    AWG,
    PageAlignedMem,
    BufferType,
    ChannelStopLevel,
    ClockMode,
    ReplayMode,
    SeqLoopCondition,
    TransferDir,
    TriggerMask,
    TriggerMode,
    AWGError,
    AWGResult,
};

