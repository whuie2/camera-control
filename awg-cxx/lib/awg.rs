//! Structures for interacting with a SPECTRUM Instrumentation M4i.6622-x8
//! arbitrary waveform generator.

#![allow(overflowing_literals)]
#![allow(clippy::enum_clike_unportable_variant)]

use std::{
    collections::HashSet,
    ffi::{ OsStr, OsString },
    f64::consts::TAU,
    fmt,
    fs,
    io::{ Read, Write },
    path::Path,
    pin::Pin,
};
use cxx::{ let_cxx_string, CxxVector, UniquePtr, kind::Trivial };
use thiserror::Error;
use crate::awg_ffi::ffi;

#[derive(Debug, Error)]
pub enum AWGError {
    #[error("AWG error: {0}")]
    DeviceError(String),

    #[error("waveform error: {0}")]
    WaveformError(String),

    #[error("bad file path '{0:?}': must be valid unicode")]
    PathError(OsString),

    #[error("bad waveform filename '{0:?}': must end in '.csv'")]
    WaveformCSV(OsString),

    #[error("uninitialized waveform parameters")]
    WaveformUninit,

    #[error("error writing waveform parameters file: {0}")]
    WaveformWriteError(String),

    #[error("error parsing waveform parameters file: {0}")]
    WaveformParseError(String),

    #[error("unknown value for {0}: {1}")]
    UnknownEnumVal(String, i32),

    #[error("invalid AWG card index returned: {0} (how did this even happen?)")]
    InvalidCardIndex(i32),

    #[error("invalid trigger termination returned: {0} (how did this even happen?)")]
    InvalidTriggerTerm(i32),

    #[error("invalid trigger coupling returned: {0} (how did this even happen?)")]
    InvalidTriggerCoupling(i32),

    #[error("invalid trigger level {0}: must be [-10000, +10000] mV")]
    InvalidTriggerLevel(i32),

    #[error("invalid rearm level {0}: must be [-10000, +10000] mV")]
    InvalidRearmLevel(i32),

    #[error("invalid frequency tone setting: number of tones must match phase and amplitude settings")]
    NumTones,

    #[error("invalid phase setting: number of phases must match frequency tone and amplitude settings")]
    NumPhases,

    #[error("invalid amplitude setting: number of amplitudes must match frequency tone and phase settings")]
    NumAmplitudes,

    #[error("invalid amplitude {0}: must be [0, 2^15 - 1]")]
    InvalidAmplitude(f64),
}
pub type AWGResult<T> = Result<T, AWGError>;

impl AWGError {
    fn as_device_err(except: cxx::Exception) -> Self {
        return Self::DeviceError(except.what().to_string());
    }

    fn as_waveform_err(except: cxx::Exception) -> Self {
        return Self::WaveformError(except.what().to_string());
    }
}

fn collect_cxx_vector<'a, I, T>(vals: I) -> UniquePtr<CxxVector<T>>
where
    I: IntoIterator<Item = &'a T>,
    T: Copy + cxx::vector::VectorElement + cxx::ExternType<Kind = Trivial> + 'a,
{
    let mut cxx_vec: UniquePtr<CxxVector<T>> = CxxVector::new();
    let mut cxx_vec_ref = cxx_vec.pin_mut();
    vals.into_iter().for_each(|v| cxx_vec_ref.as_mut().push(*v));
    return cxx_vec;
}

macro_rules! impl_debug_boring {
    ( $t:ident ) => {
        impl std::fmt::Debug for $t {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, stringify!($t))
            }
        }
    }
}

/// Represents a thin wrapper around a (thin wrapper around a) raw C++ `void*`
/// pointing to a page-aligned slice of memory.
pub struct PageAlignedMem {
    ptr: UniquePtr<ffi::PageAlignedMem>,
}

impl_debug_boring!(PageAlignedMem);

impl PageAlignedMem {
    fn pinned(&mut self) -> Pin<&mut ffi::PageAlignedMem> { self.ptr.pin_mut() }
}

/// Thin wrapper around a raw C++ `void*` pointing to a waveform array stored at
/// a page-aligned slice of memory, along with the array's length.
pub struct WaveformDataPtr {
    ptr: UniquePtr<ffi::WaveformData>,
}

impl_debug_boring!(WaveformDataPtr);

impl WaveformDataPtr {
    fn pinned(&mut self) -> Pin<&mut ffi::WaveformData> { self.ptr.pin_mut() }

    pub fn into_memsize(mut self) -> AWGResult<(PageAlignedMem, u64)> {
        unsafe {
            return ffi::waveformdata_get_mem(self.pinned())
                .map(|ptr| PageAlignedMem { ptr })
                .map_err(AWGError::as_waveform_err)
                .and_then(|buf| {
                    ffi::waveformdata_get_datalen(self.pinned())
                        .map(|len| (buf, len as u64))
                        .map_err(AWGError::as_waveform_err)
                });
        }
    }

    // pub fn get_mem(&mut self) -> AWGResult<PageAlignedMem> {
    //     unsafe {
    //         return ffi::waveformdata_get_mem(self.pinned())
    //             .map(|ptr| PageAlignedMem { ptr })
    //             .map_err(AWGError::as_waveform_err);
    //     }
    // }
    //
    // pub fn get_datalen(&mut self) -> AWGResult<u64> {
    //     unsafe {
    //         return ffi::waveformdata_get_datalen(self.pinned())
    //             .map(|len| len as u64)
    //             .map_err(|err| AWGError::WaveformError(err.to_string()));
    //     }
    // }
}

macro_rules! cpp_enum {
    (
        $doc:literal
        pub enum $name:ident {
            $(
                $vardoc:literal
                $rs:ident = $cpp:ident = $val:literal
            ),* $(,)?
        }
    ) => {
        $(
            #[doc = $vardoc]
            const $cpp: i32 = $val;
        )*

        #[doc = $doc]
        #[derive(Copy, Clone, Debug, PartialEq, Eq)]
        pub enum $name {
            $(
                $rs = $val,
            )*
        }

        impl TryFrom<i32> for $name {
            type Error = AWGError;

            fn try_from(v: i32) -> AWGResult<Self> {
                return match v {
                    $(
                        $val => Ok(Self::$rs),
                    )*
                    x => Err(
                        AWGError::UnknownEnumVal(
                            stringify!($name).to_string(),
                            x
                        )
                    ),
                };
            }
        }
    }
}

cpp_enum!(
    "Defines the analog output state when idling."
    pub enum ChannelStopLevel {
        // ""
        // Tristate = SPCM_STOPLVL_TRISTATE = 0x00000001,
        "-2500 mV"
        Low = SPCM_STOPLVL_LOW = 0x00000002,
        "+2500 mV"
        High = SPCM_STOPLVL_HIGH = 0x00000004,
        "last replaced sample amplitude"
        HoldLast = SPCM_STOPLVL_HOLDLAST = 0x00000008,
        "0 mV"
        Zero = SPCM_STOPLVL_ZERO = 0x00000010,
        // ""
        // Custom = SPCM_STOPLVL_CUSTOM = 0x00000020,
    }
);

cpp_enum!(
    "Defines the card replay mode."
    pub enum ReplayMode {
        "Data generation from memory with programmed loop number of times after each trigger event."
        Single = SPC_REP_STD_SINGLE = 0x00000100,
        "Data generation from memory after multiple trigger events."
        Multi = SPC_REP_STD_MULTI = 0x00000200,
        "Data generation from memory using external gate signal."
        Gate = SPC_REP_STD_GATE = 0x00000400,
        "Continuous data generation after a single trigger event, memory used as FIFO buffer."
        FIFOSingle = SPC_REP_FIFO_SINGLE = 0x00000800,
        "Continuous data generation after multiple trigger events, memory used as FIFO buffer."
        FIFOMulti = SPC_REP_FIFO_MULTI = 0x00001000,
        "Continuous data generation using external gate signal, memory used as FIFO buffer."
        FIFOGate = SPC_REP_FIFO_GATE = 0x00002000,
        // "Continuous data generation after a single trigger event."
        // Continuous = SPC_REP_STD_CONTINUOUS = 0x00004000,
        "Data generation from memory once after each trigger event."
        SingleRestart = SPC_REP_STD_SINGLERESTART = 0x00008000,
        "Data generation from memory with a specially programmed memory sequence."
        Sequence = SPC_REP_STD_SEQUENCE = 0x00040000,
    }
);

cpp_enum!(
    "Available data buffers."
    pub enum BufferType {
        "Main data buffer for acquired or generated samples, used to transfer standard sample data."
        Data = SPCM_BUF_DATA = 1000,
        "Buffer for ABA data, holds the A-DATA (slow samples)."
        ABA = SPCM_BUF_ABA = 2000,
        "Buffer for timestamps."
        Timestamp = SPCM_BUF_TIMESTAMP = 3000,
        // "Write content of the buffer to the log file."
        // Log = SPCM_BUF_LOG = 4000,
    }
);

cpp_enum!(
    "Available ways to transfer data."
    pub enum TransferDir {
        "Transfer from PC to AWG"
        PCToCard = SPCM_DIR_PCTOCARD = 0,
        "Transfer from AWG to PC"
        CardToPC = SPCM_DIR_CARDTOPC = 1,
        "Transfer from AWG to GPU"
        CardToGPU = SPCM_DIR_CARDTOGPU = 2,
        "Transfer from GPU to AWG"
        GPUToCard = SPCM_DIR_GPUTOCARD = 3,
    }
);

cpp_enum!(
    "Available clock signal sources."
    pub enum ClockMode {
        "Internal programmable high precision quart1 for sample clock generation"
        IntPLL = SPC_CM_INTPLL = 0x00000001,
        // "Plain quartz1 (with divider)"
        // Quartz1 = SPC_CM_QUARTZ1 = 0x00000002,
        // "Plain quartz2 (with divider)"
        // Quartz2 = SPC_CM_QUARTZ2 = 0x00000004,
        // "Plain quartz1 (with divider) and put the Q1 clock on the star-hub module"
        // Quartz1DirSync = SPC_CM_QUARTZ1_DIRSYNC = 0x00000200,
        // "Plain quartz2 (with divider) and put the Q2 clock on the star-hub module"
        // Quartz2DirSync = SPC_CM_QUARTZ2_DIRSYNC = 0x00000100,
        // "Direct from external clock"
        // Ext = SPC_CM_EXTERNAL = 0x00000008,
        // "Direct from external clock0 (identical to `Ext`)"
        // Ext0 = SPC_CM_EXTERNAL0 = 0x00000008,
        // "Direct from external clock1"
        // Ext1 = SPC_CM_EXTERNAL1 = 0x00000400,
        // "External clock with programmed divider"
        // ExtDivider = SPC_CM_EXTDIVIDER = 0x00000010,
        "Internal PLL with external reference for sample clock generation"
        ExtRefClock = SPC_CM_EXTREFCLOCK = 0x00000020,
        // "PXI reference clock"
        // PXIRefClock = SPC_CM_PXIREFCLOCK = 0x00000040,
        // Star-hub direct clock (not synchronized)"
        // SHDirect = SPC_CM_SHDIRECT = 0x00000080,
    }
);

cpp_enum!(
    "Select available trigger sources."
    pub enum TriggerMask {
        "No trigger source"
        None = SPC_TMASK_NONE = 0x00000000,
        "Software trigger source, not applicable to AND mask"
        Software = SPC_TMASK_SOFTWARE = 0x00000001,
        "External trigger 0"
        Ext0 = SPC_TMASK_EXT0 = 0x00000002,
        "External trigger 1"
        Ext1 = SPC_TMASK_EXT1 = 0x00000004,
        // ""
        // Ext2 = SPC_TMASK_EXT2 = 0x00000008, 
        // ""
        // Ext3 = SPC_TMASK_EXT3 = 0x00000010, 
        // ""
        // Ext4 = SPC_TMASK_EXT4 = 0x00000020, 
        // ""
        // XIO0 = SPC_TMASK_XIO0 = 0x00000100, 
        // ""
        // XIO1 = SPC_TMASK_XIO1 = 0x00000200, 
        // ""
        // XIO2 = SPC_TMASK_XIO2 = 0x00000400, 
        // ""
        // XIO3 = SPC_TMASK_XIO3 = 0x00000800, 
        // ""
        // XIO4 = SPC_TMASK_XIO4 = 0x00001000, 
        // ""
        // XIO5 = SPC_TMASK_XIO5 = 0x00002000, 
        // ""
        // XIO6 = SPC_TMASK_XIO6 = 0x00004000, 
        // ""
        // XIO7 = SPC_TMASK_XIO7 = 0x00008000, 
        // ""
        // PXI0 = SPC_TMASK_PXI0 = 0x00100000, 
        // ""
        // PXI1 = SPC_TMASK_PXI1 = 0x00200000, 
        // ""
        // PXI2 = SPC_TMASK_PXI2 = 0x00400000, 
        // ""
        // PXI3 = SPC_TMASK_PXI3 = 0x00800000, 
        // ""
        // PXI4 = SPC_TMASK_PXI4 = 0x01000000, 
        // ""
        // PXI5 = SPC_TMASK_PXI5 = 0x02000000, 
        // ""
        // PXI6 = SPC_TMASK_PXI6 = 0x04000000, 
        // ""
        // PXI7 = SPC_TMASK_PXI7 = 0x08000000, 
        // ""
        // PXIStar = SPC_TMASK_PXISTAR = 0x10000000, 
        // ""
        // PXIDStarB = SPC_TMASK_PXIDSTARB = 0x20000000, 
    }
);

cpp_enum!(
    "Available trigger modes."
    pub enum TriggerMode {
        "No triggering"
        None = SPC_TM_NONE = 0x00000000,
        "Trigger on positive (rising) edge"
        Pos = SPC_TM_POS = 0x00000001,
        "Trigger on negative (falling) edge"
        Neg = SPC_TM_NEG = 0x00000002,
        "Trigger on both positive (rising) and negative (falling) edges"
        Both = SPC_TM_BOTH = 0x00000004,
        "Trigger on HIGH levels (above trigger level 0)"
        High = SPC_TM_HIGH = 0x00000008,
        "Trigger on LOW levels (below trigger level 0)"
        Low = SPC_TM_LOW = 0x00000010,
        "Window trigger for entering area between level 0 and level 1"
        WinEnter = SPC_TM_WINENTER = 0x00000020,
        "Window trigger for leaving area between level 0 and level 1"
        WinLeave = SPC_TM_WINLEAVE = 0x00000040,
        "Window trigger for signal inside area between level 0 and level 1"
        InWin = SPC_TM_INWIN = 0x00000080,
        "Window trigger for signal outside area between level 0 and level 1"
        OutsideWin = SPC_TM_OUTSIDEWIN = 0x00000100,
        // ""
        // Spike = SPC_TM_SPIKE = 0x00000200,
        // ""
        // Pattern = SPC_TM_PATTERN = 0x00000400,
        // ""
        // SteepPos = SPC_TM_STEEPPOS = 0x00000800,
        // ""
        // SteepNeg = SPC_TM_STEEPNEG = 0x00001000,
        // ""
        // ExtraMask = SPC_TM_EXTRAMASK = 0xFF000000,
        // ""
        // Rearm = SPC_TM_REARM = 0x01000000,
        // ""
        // PWSmaller = SPC_TM_PW_SMALLER = 0x02000000,
        // ""
        // PWGreater = SPC_TM_PW_GREATER = 0x04000000,
        // ""
        // DoubleEdge = SPC_TM_DOUBLEEDGE = 0x08000000,
        // ""
        // PulseStretch = SPC_TM_PULSESTRETCH = 0x10000000,
        // ""
        // Hysteresis = SPC_TM_HYSTERESIS = 0x20000000,
        "Trigger detection on positive edge, trigger is armed when trigger level 1 is crossed"
        PosRearm = SPC_TM_POS_OR_SPC_TM_REARM = 0x01000001,
        "Trigger detection on negative edge, trigger is armed when trigger level 1 is crossed"
        NegRearm = SPC_TM_NEG_OR_SPC_TM_REARM = 0x01000002,
    }
);

cpp_enum!(
    "Available sequence loop conditions"
    pub enum SeqLoopCondition {
        "If defined loops for the current segment have been replayed, always change to the next step."
        Always = SPCSEQ_ENDLOOPALWAYS = 0x00000000,
        "Change to the next step on trigger detection only after a defined number of loops have been played."
        OnTrigger = SPCSEQ_ENDLOOPONTRIG = 0x40000000,
        "Mark the current step to the last step in the sequence; stop the card afterwards."
        End = SPCSEQ_END = 0x80000000,
    }
);

/// AWG card index.
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum AWGIndex {
    Zero = 0,
    One = 1,
}

impl TryFrom<i32> for AWGIndex {
    type Error = AWGError;

    fn try_from(i: i32) -> AWGResult<Self> {
        return match i {
            0 => Ok(Self::Zero),
            1 => Ok(Self::One),
            x => Err(AWGError::InvalidCardIndex(x)),
        };
    }
}

/// AWG channel pair for use in [`AWG::set_channel_diff_mode`].
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ChannelPair {
    ZeroOne = 0,
    TwoThree = 1,
}

/// Trigger channel.
#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum TriggerChannel {
    Ext0 = 0,
    Ext1 = 1,
}

/// Trigger input termination.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum TriggerTerm {
    High = 0,
    FiftyOhm = 1,
}

impl TryFrom<i32> for TriggerTerm {
    type Error = AWGError;

    fn try_from(i: i32) -> AWGResult<Self> {
        return match i {
            0 => Ok(Self::High),
            1 => Ok(Self::FiftyOhm),
            x => Err(AWGError::InvalidTriggerTerm(x)),
        };
    }
}

/// Trigger channel coupling mode.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum TriggerCoupling {
    DC = 0,
    AC = 1,
}

impl TryFrom<i32> for TriggerCoupling {
    type Error = AWGError;

    fn try_from(i: i32) -> AWGResult<Self> {
        return match i {
            0 => Ok(Self::DC),
            1 => Ok(Self::AC),
            x => Err(AWGError::InvalidTriggerCoupling(x)),
        };
    }
}

/// Main driver to communicate with the AWG.
pub struct AWG {
    awg: UniquePtr<ffi::AWGBox>,
}

unsafe impl Send for AWG { }
unsafe impl Sync for AWG { }

impl_debug_boring!(AWG);

impl AWG {
    /// Open a new connection to the AWG by index.
    pub fn connect(idx: AWGIndex) -> AWGResult<Self> {
        unsafe {
            return ffi::new_awg(idx as i32)
                .map(|awg| Self { awg })
                .map_err(AWGError::as_device_err);
        }
    }

    /// Disconnect from the AWG and drop `self`.
    pub fn disconnect(self) { }

    fn pinned(&mut self) -> Pin<&mut ffi::AWGBox> { self.awg.pin_mut() }

    /// Reset the AWG card, equivalent to a power reset.
    pub fn reset(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_reset(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get the AWG card index.
    pub fn get_card_idx(&mut self) -> AWGResult<AWGIndex> {
        unsafe {
            return ffi::awg_get_card_idx(self.pinned())
                .map_err(AWGError::as_device_err)
                .and_then(|idx| idx.try_into());
        }
    }

    /// Get the AWG card serial number.
    pub fn get_serial_number(&mut self) -> AWGResult<i32> {
        unsafe {
            return ffi::awg_get_serial_number(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Get installed memory size.
    pub fn get_inst_mem_size(&mut self) -> AWGResult<i64> {
        unsafe {
            return ffi::awg_get_inst_mem_size(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Get the sample byte length.
    pub fn get_bytes_per_sample(&mut self) -> AWGResult<i32> {
        unsafe {
            return ffi::awg_get_bytes_per_sample(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Get the maximum sampling rate.
    pub fn get_max_sample_rate(&mut self) -> AWGResult<i64> {
        unsafe {
            return ffi::awg_get_max_sample_rate(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Set the card sampling rate.
    pub fn set_sample_rate(&mut self, sample_rate: i64)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_sample_rate(self.pinned(), sample_rate)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get the current sampling rate.
    pub fn get_sample_rate(&mut self) -> AWGResult<i64> {
        unsafe {
            return ffi::awg_get_sample_rate(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Set the active channels for the next run, resets the last set.
    pub fn set_active_channels<'a, I>(&mut self, channels: I)
        -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a i32>
    {
        unsafe {
            let channels_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(channels);
            ffi::awg_set_active_channels(
                self.pinned(), channels_cxx.as_ref().unwrap())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set the amplitude of a channel.
    pub fn set_channel_amp(&mut self, ch: i32, amp: i32)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_channel_amp(self.pinned(), ch, amp)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set a channel's stop level.
    pub fn set_channel_stop_level(
        &mut self,
        ch: i32,
        stop_level: ChannelStopLevel,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_channel_stop_level(
                self.pinned(), ch, stop_level as i32)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set the output of a channel.
    pub fn set_channel_output(&mut self, ch: i32, onoff: bool)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_toggle_channel_output(self.pinned(), ch, onoff)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set the amplitude, stop level, and output of a channel.
    pub fn set_channel(
        &mut self,
        ch: i32,
        amp: i32,
        stop_level: ChannelStopLevel,
        onoff: bool,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_channel(
                self.pinned(), ch, amp, stop_level as i32, onoff)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get the number of channels available on the card.
    pub fn get_channel_count(&mut self) -> AWGResult<i32> {
        unsafe {
            return ffi::awg_get_channel_count(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Get the number of currently active channels.
    pub fn get_active_channels(&mut self) -> AWGResult<HashSet<i32>> {
        unsafe {
            return ffi::awg_get_channel_activated(self.pinned())
                .map(|channels| channels.into_iter().collect())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Set channels 0 and 1 or 2 and 3 to differential output mode.
    pub fn set_channel_diff_mode(&mut self, ch_pair: ChannelPair, onoff: bool)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_channel_diff_mode(
                self.pinned(), ch_pair as i32, onoff)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set channels 0 and 1 or 2 and 3 to double mode.
    pub fn set_channel_double_mode(&mut self, ch_pair: ChannelPair, onoff: bool)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_channel_double_mode(
                self.pinned(), ch_pair as i32, onoff)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Write the current setup to the card without starting the hardware; some
    /// settings may not be changed while the card is running.
    pub fn write_setup(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_write_setup(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Start the card with all selected settings.
    pub fn card_run(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_card_run(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Stop the card.
    ///
    /// No effect if not currently running.
    pub fn card_stop(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_card_stop(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Wait until the card has finished its current output run.
    pub fn wait_ready(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_wait_ready(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set a timeout time in milliseconds.
    ///
    /// Pass 0 to disable.
    pub fn set_timeout(&mut self, timeout_ms: i32) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_set_timeout(self.pinned(), timeout_ms)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get the current timeout time in milliseconds.
    pub fn get_timeout(&mut self) -> AWGResult<i32> {
        unsafe {
            return ffi::awg_get_timeout(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    /// Print the current card status.
    pub fn print_card_status(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_print_card_status(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set whether trigger detection is enabled.
    pub fn set_trigger(&mut self, onoff: bool) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_toggle_trigger(self.pinned(), onoff)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Send a software trigger equivalent to a physical trigger event.
    pub fn force_trigger(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_force_trigger(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Wait until a trigger event.
    pub fn wait_trigger(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_wait_trigger(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Program the OR trigger mask.
    pub fn set_trig_mask_or<'a, I>(&mut self, masks: I)
        -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a TriggerMask>
    {
        unsafe {
            let mask_ints: Vec<i32>
                = masks.into_iter().map(|m| *m as i32).collect();
            let masks_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(&mask_ints);
            ffi::awg_set_trig_mask_or(
                self.pinned(), masks_cxx.as_ref().unwrap())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Program the AND trigger mask.
    pub fn set_trig_mask_and<'a, I>(&mut self, masks: I)
        -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a TriggerMask>
    {
        unsafe {
            let mask_ints: Vec<i32>
                = masks.into_iter().map(|m| *m as i32).collect();
            let masks_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(&mask_ints);
            ffi::awg_set_trig_mask_and(
                self.pinned(), masks_cxx.as_ref().unwrap())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set the trigger mode of the trigger channel.
    pub fn set_trig_mode(&mut self, ch: TriggerChannel, mode: TriggerMode)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_trig_mode(self.pinned(), ch as i32, mode as i32)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set the trigger input termination.
    pub fn set_trig_term(&mut self, term: TriggerTerm) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_set_trig_term(self.pinned(), term as i32)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get the trigger input termination.
    pub fn get_trig_term(&mut self) -> AWGResult<TriggerTerm> {
        unsafe {
            return ffi::awg_get_trig_term(self.pinned())
                .map_err(AWGError::as_device_err)
                .and_then(|term| term.try_into());
        }
    }

    /// Set a trigger channel's coupling mode.
    pub fn set_trig_coupling(
        &mut self,
        ch: TriggerChannel,
        coupling: TriggerCoupling,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_trig_coupling(
                self.pinned(), ch as i32, coupling as i32)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get a trigger channel's coupling mode.
    pub fn get_trig_coupling(&mut self, ch: TriggerChannel)
        -> AWGResult<TriggerCoupling>
    {
        unsafe {
            return ffi::awg_get_trig_coupling(self.pinned(), ch as i32)
                .map_err(AWGError::as_device_err)
                .and_then(|coup| coup.try_into());
        }
    }

    /// Set a trigger channel's trigger level.
    pub fn set_trig_level(&mut self, ch: TriggerChannel, level: i32)
        -> AWGResult<&mut Self>
    {
        unsafe {
            (-10000..10000).contains(&level).then_some(())
                .ok_or(AWGError::InvalidTriggerLevel(level))?;
            ffi::awg_set_trig_level(self.pinned(), ch as i32, level)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get a trigger channel's trigger level.
    pub fn get_trig_level(&mut self, ch: TriggerChannel) -> AWGResult<i32> {
        unsafe {
            return ffi::awg_get_trig_level(self.pinned(), ch as i32)
                .map_err(AWGError::as_device_err);
        }
    }

    /// Set a trigger channel's rearm level.
    pub fn set_trig_rearm_level(&mut self, ch: TriggerChannel, level: i32)
        -> AWGResult<&mut Self>
    {
        unsafe {
            (-10000..10000).contains(&level).then_some(())
                .ok_or(AWGError::InvalidRearmLevel(level))?;
            ffi::awg_set_trig_rearm_level(self.pinned(), ch as i32, level)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get a trigger channel's rearm level.
    pub fn get_trig_rearm_level(&mut self, ch: TriggerChannel)
        -> AWGResult<i64>
    {
        unsafe {
            return ffi::awg_get_trig_rearm_level(self.pinned(), ch as i32)
                .map_err(AWGError::as_device_err);
        }
    }

    /// Initialize sequence replay mode.
    pub fn init_replay_mode_seq(&mut self, n_segments: i32)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_init_replay_mode_seq(self.pinned(), n_segments)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Configure a step in sequence replay mode.
    pub fn set_seq_mode_step(
        &mut self,
        step: u64,
        segment: u64,
        next_step: u64,
        n_loop: u64,
        condition: SeqLoopCondition,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_seq_mode_step(
                self.pinned(),
                step,
                segment,
                next_step,
                n_loop,
                condition as i32,
            )
            .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Write buffered data into a memory segment in sequence replay mode.
    pub fn write_seq_mode_segment(
        &mut self,
        segment: i32,
        p_data_buffer: &mut PageAlignedMem,
        size: i32,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_write_seq_mode_segment(
                self.pinned(),
                segment,
                p_data_buffer.pinned(),
                size,
            )
            .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Prepare the board for data transfer.
    pub fn prep_data_transfer(
        &mut self,
        p_data_buffer: &mut PageAlignedMem,
        buffer_len: u64,
        buf_type: BufferType,
        dir: TransferDir,
        notify_size: u32,
        board_mem_offs: u64,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_prep_data_transfer(
                self.pinned(),
                p_data_buffer.pinned(),
                buffer_len,
                buf_type as i32,
                dir as i32,
                notify_size,
                board_mem_offs,
            )
            .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Start a data transfer.
    pub fn start_data_transfer(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_start_data_transfer(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Stop the current data transfer.
    pub fn stop_data_transfer(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_stop_data_transfer(self.pinned())
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Set the clock mode.
    pub fn set_clock_mode(&mut self, mode: ClockMode) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_set_clock_mode(self.pinned(), mode as i32)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Get the current clock mode.
    pub fn get_clock_mode(&mut self) -> AWGResult<ClockMode> {
        unsafe {
            return ffi::awg_get_clock_mode(self.pinned())
                .map_err(AWGError::as_device_err)
                .and_then(|mode| mode.try_into());
        }
    }

    /// If in [`ClockMode::ExtRefClock`] mode, set the external clock frequency.
    pub fn set_ref_clock_freq(&mut self, frequency: i64)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::awg_set_ref_clock_freq(self.pinned(), frequency)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Enable or disable clock output.
    pub fn set_clock_out(&mut self, onoff: bool) -> AWGResult<&mut Self> {
        unsafe {
            ffi::awg_set_clock_out(self.pinned(), onoff)
                .map_err(AWGError::as_device_err)?;
            return Ok(self);
        }
    }

    /// Read out the frequency of the internally synthesized clock.
    pub fn get_clock_out_freq(&mut self) -> AWGResult<i64> {
        unsafe {
            return ffi::awg_get_clock_out_freq(self.pinned())
                .map_err(AWGError::as_device_err);
        }
    }

    // pub fn get_page_mem(&mut self, size: u64) -> AWGResult<PageAlignedMem> {
    //     unsafe {
    //         let mem: UniquePtr<ffi::PageAlignedMem>
    //             = ffi::awg_get_page_aligned_mem(self.pinned(), size)?;
    //         return Ok(PageAlignedMem { ptr: mem, size });
    //     }
    // }
    //
    // pub fn free_page_mem(&mut self, mem: PageAlignedMem)
    //     -> AWGResult<&mut Self>
    // {
    //     unsafe {
    //         let PageAlignedMem { ptr, size } = mem;
    //         ffi::awg_free_page_aligned_mem(self.pinned(), ptr, size)?;
    //         return Ok(self);
    //     }
    // }
}

/// Safe interface to waveform parameter configuration.
///
/// All parameters must be initialized before an [`ArrayWaveform`] can be
/// generated:
/// - [`tones`][Self::set_tones]: Frequency tones in hertz
/// - [`phases`][Self::set_phases]: Initial phases for each tone in radians
/// - [`amplitudes`][Self::set_amplitudes]: Amplitudes for each tone
/// - [`sampling_rate`][Self::set_sampling_rate]: Sampling rate in hertz
/// - [`frequency_resolution`][Self::set_frequency_resolution]: Frequency
/// resolution in hertz
#[derive(Clone, Debug)]
pub struct WaveformParams {
    tones: Option<Vec<i32>>,
    phases: Option<Vec<f64>>,
    amplitudes: Option<Vec<f64>>,
    sampling_rate: Option<u64>,
    frequency_resolution: Option<u64>,
}

impl Default for WaveformParams {
    fn default() -> Self { Self::new() }
}

impl WaveformParams {
    /// Create a new, uninitialized set of waveform parameters.
    pub fn new() -> Self {
        Self {
            tones: None,
            phases: None,
            amplitudes: None,
            sampling_rate: None,
            frequency_resolution: None,
        }
    }

    /// Return `true` if all parameters have been initialized.
    pub fn is_init(&self) -> bool {
        self.has_tones()
            && self.has_phases()
            && self.has_amplitudes()
            && self.has_sampling_rate()
            && self.has_frequency_resolution()
    }

    /// Write current parameter settings to a file.
    ///
    /// The file is formatted as a CSV file:
    /// ```text
    /// sampling_rate
    /// frequency_resolution
    /// tones[0],tones[1],...
    /// phases[0],phases[1],...
    /// amplitudes[0],amplitudes[1],...
    /// ```
    ///
    /// The given file name must have a `.csv` extension.
    pub fn save<P>(&self, outfile: P) -> AWGResult<&Self>
    where P: AsRef<Path>
    {
        self.is_init().then_some(())
            .ok_or(AWGError::WaveformUninit)?;
        let outfile = outfile.as_ref();
        outfile.extension().and_then(|ext| (ext == "csv").then_some(()))
            .ok_or(AWGError::WaveformCSV(outfile.as_os_str().to_os_string()))?;
        let mut out
            = fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(outfile)
            .map_err(|err| AWGError::WaveformWriteError(err.to_string()))?;
        writeln!(&mut out, "{}", self.sampling_rate.unwrap())
            .map_err(|err| AWGError::WaveformWriteError(err.to_string()))?;
        writeln!(&mut out, "{}", self.frequency_resolution.unwrap())
            .map_err(|err| AWGError::WaveformWriteError(err.to_string()))?;
        writeln!(&mut out, "{}",
            self.tones.as_ref().unwrap().iter()
                .map(|f| f.to_string())
                .collect::<Vec<String>>()
                .join(",")
        )
        .map_err(|err| AWGError::WaveformWriteError(err.to_string()))?;
        writeln!(&mut out, "{}",
            self.phases.as_ref().unwrap().iter()
                .map(|ph| ph.to_string())
                .collect::<Vec<String>>()
                .join(",")
        )
        .map_err(|err| AWGError::WaveformWriteError(err.to_string()))?;
        writeln!(&mut out, "{}",
            self.amplitudes.as_ref().unwrap().iter()
                .map(|a| a.to_string())
                .collect::<Vec<String>>()
                .join(",")
        )
        .map_err(|err| AWGError::WaveformWriteError(err.to_string()))?;
        Ok(self)
    }

    /// Load parameter settings from a file.
    ///
    /// The file is expected as a CSV file:
    /// ```text
    /// sampling_rate
    /// frequency_resolution
    /// tones[0],tones[1],...
    /// phases[0],phases[1],...
    /// amplitudes[0],amplitudes[1],...
    /// ```
    ///
    /// The given file name must have a `.csv` extension.
    pub fn load<P>(infile: P) -> AWGResult<Self>
    where P: AsRef<Path>
    {
        let infile = infile.as_ref();
        infile.extension().and_then(|ext| (ext == "csv").then_some(()))
            .ok_or(AWGError::WaveformCSV(infile.as_os_str().to_os_string()))?;
        let mut buf = String::new();
        fs::OpenOptions::new()
            .read(true)
            .open(infile)
            .map_err(|err| AWGError::WaveformParseError(err.to_string()))?
            .read_to_string(&mut buf)
            .map_err(|err| AWGError::WaveformParseError(err.to_string()))?;
        let mut lines = buf.split('\n');
        let sampling_rate: u64
            = lines.next()
            .ok_or(AWGError::WaveformParseError(
                "missing sampling rate".to_string()
            ))?
            .parse::<u64>()
            .map_err(|err| AWGError::WaveformParseError(err.to_string()))?;
        let frequency_resolution: u64
            = lines.next()
            .ok_or(AWGError::WaveformParseError(
                "missing frequency resolution".to_string()
            ))?
            .parse::<u64>()
            .map_err(|err| AWGError::WaveformParseError(err.to_string()))?;
        let tones: Vec<i32>
            = lines.next()
            .ok_or(AWGError::WaveformParseError(
                "missing frequency tones".to_string()
            ))?
            .split(',')
            .map(|f| {
                f.parse::<i32>()
                .map_err(|err| AWGError::WaveformParseError(err.to_string()))
            })
            .collect::<AWGResult<Vec<i32>>>()?;
        let phases: Vec<f64>
            = lines.next()
            .ok_or(AWGError::WaveformParseError(
                "missing phases".to_string()
            ))?
            .split(',')
            .map(|ph| {
                ph.parse::<f64>()
                .map_err(|err| AWGError::WaveformParseError(err.to_string()))
            })
            .collect::<AWGResult<Vec<f64>>>()?;
        let amplitudes: Vec<f64>
            = lines.next()
            .ok_or(AWGError::WaveformParseError(
                "missing amplitudes".to_string()
            ))?
            .split(',')
            .map(|a| {
                a.parse::<f64>()
                .map_err(|err| AWGError::WaveformParseError(err.to_string()))
            })
            .collect::<AWGResult<Vec<f64>>>()?;
        let data = Self {
            tones: Some(tones),
            phases: Some(phases),
            amplitudes: Some(amplitudes),
            sampling_rate: Some(sampling_rate),
            frequency_resolution: Some(frequency_resolution),
        };
        Ok(data)
    }

    /// Return `true` if frequency tones have been initialized.
    pub fn has_tones(&self) -> bool { self.tones.is_some() }

    /// Get the frequency tones in hertz, if initialized.
    pub fn get_tones(&self) -> Option<&Vec<i32>> {
        self.tones.as_ref()
    }

    /// Set the frequency tones in hertz.
    pub fn set_tones<'a, I>(&mut self, tones: I) -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a i32>
    {
        let tones: Vec<i32> = tones.into_iter().copied().collect();
        if let Some(phases) = &self.phases {
            (tones.len() == phases.len()).then_some(())
                .ok_or(AWGError::NumTones)?;
        }
        if let Some(amplitudes) = &self.amplitudes {
            (tones.len() == amplitudes.len()).then_some(())
                .ok_or(AWGError::NumTones)?;
        }
        self.tones = Some(tones);
        Ok(self)
    }

    /// Set the frequency tones using a center frequency and a uniform spacing,
    /// both in hertz.
    pub fn set_tones_spaced(&mut self, center: i32, spacing: i32, num: usize)
        -> AWGResult<&mut Self>
    {
        if let Some(phases) = &self.phases {
            (num == phases.len()).then_some(())
                .ok_or(AWGError::NumTones)?;
        }
        if let Some(amplitudes) = &self.amplitudes {
            (num == amplitudes.len()).then_some(())
                .ok_or(AWGError::NumTones)?;
        }
        let f0: i32 = center - spacing * num as i32 / 2;
        let tones: Vec<i32>
            = (0..num as i32).map(|k| f0 + spacing * k).collect();
        self.tones = Some(tones);
        Ok(self)
    }

    /// Return `true` if initial phases have been initialized.
    pub fn has_phases(&self) -> bool { self.phases.is_some() }

    /// Get the initial phases of each tone in radians.
    pub fn get_phases(&self) -> Option<&Vec<f64>> {
        self.phases.as_ref()
    }

    /// Set the initial phases of each tone in radians.
    pub fn set_phases<'a, I>(&mut self, phases: I) -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a f64>
    {
        let phases: Vec<f64>
            = phases.into_iter().copied().map(|ph| ph % TAU).collect();
        if let Some(tones) = &self.tones {
            (phases.len() == tones.len()).then_some(())
                .ok_or(AWGError::NumPhases)?;
        }
        if let Some(amplitudes) = &self.amplitudes {
            (phases.len() == amplitudes.len()).then_some(())
                .ok_or(AWGError::NumPhases)?;
        }
        self.phases = Some(phases);
        Ok(self)
    }

    /// Return `true` if amplitudes have been initialized.
    pub fn has_amplitudes(&self) -> bool { self.amplitudes.is_some() }

    /// Get the amplitudes of each tone.
    pub fn get_amplitudes(&self) -> Option<&Vec<f64>> {
        self.amplitudes.as_ref()
    }

    /// Set the amplitudes of each tone.
    ///
    /// Each amplitude must lie in the range `[0, 2^15 - 1)`.
    pub fn set_amplitudes<'a, I>(&mut self, amplitudes: I)
        -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a f64>
    {
        let amplitudes: Vec<f64>
            = amplitudes.into_iter().copied()
            .map(|a| {
                (0.0..32767.0).contains(&a)
                    .then_some(a)
                    .ok_or(AWGError::InvalidAmplitude(a))
            })
            .collect::<AWGResult<Vec<f64>>>()?;
        if let Some(tones) = &self.tones {
            (amplitudes.len() == tones.len()).then_some(())
                .ok_or(AWGError::NumAmplitudes)?;
        }
        if let Some(phases) = &self.phases {
            (amplitudes.len() == phases.len()).then_some(())
                .ok_or(AWGError::NumAmplitudes)?;
        }
        self.amplitudes = Some(amplitudes);
        Ok(self)
    }

    /// Return `true` if the sampling rate has been initialized.
    pub fn has_sampling_rate(&self) -> bool {
        self.sampling_rate.is_some()
    }

    /// Get the sampling rate in hertz.
    pub fn get_sampling_rate(&self) -> Option<u64> {
        self.sampling_rate
    }

    /// Set the sampling rate in hertz.
    pub fn set_sampling_rate(&mut self, sampling_rate: u64) -> &mut Self {
        self.sampling_rate = Some(sampling_rate);
        self
    }

    /// Return `true` if the frequency resolution has been initialized.
    pub fn has_frequency_resolution(&self) -> bool {
        self.frequency_resolution.is_some()
    }

    /// Get the frequency resolution in hertz.
    pub fn get_frequency_resolution(&self) -> Option<u64> {
        self.frequency_resolution
    }

    /// Set the frequency resolution in hertz.
    pub fn set_frequency_resolution(&mut self, resolution: u64) -> &mut Self {
        self.frequency_resolution = Some(resolution);
        self
    }

    /// Generate an [`ArrayWaveform`] from the current set of parameters if all
    /// have been initialized.
    pub fn to_waveform(&self) -> AWGResult<ArrayWaveform> {
        self.is_init().then_some(())
            .ok_or(AWGError::WaveformUninit)?;
        let mut waveform = ArrayWaveform::new_uninit();
        waveform
            .set_freq_tones(self.tones.as_ref().unwrap())?
            .set_phases(self.phases.as_ref().unwrap())?
            .set_amplitudes(self.amplitudes.as_ref().unwrap())?
            .set_sampling_rate(self.sampling_rate.unwrap())?
            .set_freq_resolution(self.frequency_resolution.unwrap())?;
        Ok(waveform)
    }
}

impl TryFrom<&WaveformParams> for ArrayWaveform {
    type Error = AWGError;

    fn try_from(params: &WaveformParams) -> AWGResult<Self> {
        params.to_waveform()
    }
}

impl TryFrom<WaveformParams> for ArrayWaveform {
    type Error = AWGError;

    fn try_from(params: WaveformParams) -> AWGResult<Self> {
        params.to_waveform()
    }
}

impl fmt::Display for WaveformParams {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "WaveformParams {{")?;

        write!(f, "  tones: [")?;
        if let Some(tones) = &self.tones {
            let n = tones.len();
            for (k, tone) in tones.iter().enumerate() {
                tone.fmt(f)?;
                if k < n - 1 { write!(f, ", ")?; }
            }
        } else {
            write!(f, "<uninit>")?;
        }
        writeln!(f, "],")?;

        write!(f, "  phases: [")?;
        if let Some(phases) = &self.phases {
            let n = phases.len();
            for (k, phase) in phases.iter().enumerate() {
                phase.fmt(f)?;
                if k < n - 1 { write!(f, ", ")?; }
            }
        } else {
            write!(f, "<uninit>")?;
        }
        writeln!(f, "],")?;

        write!(f, "  amplitudes: [")?;
        if let Some(amplitudes) = &self.amplitudes {
            let n = amplitudes.len();
            for (k, amplitude) in amplitudes.iter().enumerate() {
                amplitude.fmt(f)?;
                if k < n - 1 { write!(f, ", ")?; }
            }
        } else {
            write!(f, "<uninit>")?;
        }
        writeln!(f, "],")?;

        write!(f, "  sampling_rate: ")?;
        if let Some(sampling_rate) = self.sampling_rate {
            sampling_rate.fmt(f)?;
        } else {
            write!(f, "<uninit>")?;
        }
        writeln!(f, ",")?;

        write!(f, "  frequency_resolution: ")?;
        if let Some(frequency_resolution) = self.frequency_resolution {
            frequency_resolution.fmt(f)?;
        } else {
            write!(f, "<uninit>")?;
        }
        writeln!(f, ",")?;

        write!(f, "}}")?;
        Ok(())
    }
}


/// Data type for array waveform generation.
pub struct ArrayWaveform {
    data: UniquePtr<ffi::ArrayWaveform>,
}

impl_debug_boring!(ArrayWaveform);

impl Default for ArrayWaveform {
    fn default() -> Self { Self::new_uninit() }
}

impl ArrayWaveform {
    fn pinned(&mut self) -> Pin<&mut ffi::ArrayWaveform> { self.data.pin_mut() }

    /// Create a new, uninitialized waveform.
    ///
    /// # Safety
    /// After this constructor is called, all private member variables are set
    /// to 0 or `nullptr` values. It is the programmer's responsibility to
    /// ensure that all properties and memory addresses are properly
    /// initialized.
    fn new_uninit() -> Self {
        unsafe {
            return Self { data: ffi::new_arraywaveform().unwrap() };
        }
    }

    /// Get a pointer to the associated data buffer.
    pub fn get_data_buffer(&mut self) -> AWGResult<PageAlignedMem> {
        unsafe {
            return ffi::arraywaveform_get_data_buffer(self.pinned())
                .map(|ptr| PageAlignedMem { ptr })
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Get the nunmber of samples in the current waveform.
    pub fn get_data_len(&mut self) -> AWGResult<u64> {
        unsafe {
            return ffi::arraywaveform_get_data_len(self.pinned())
                .map(|len| len as u64)
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Set the sampling rate in hertz.
    fn set_sampling_rate(&mut self, rate: u64) -> AWGResult<&mut Self> {
        unsafe {
            ffi::arraywaveform_set_sampling_rate(self.pinned(), rate)
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Get the sampling rate in hertz.
    pub fn get_sampling_rate(&mut self) -> AWGResult<u64> {
        unsafe {
            return ffi::arraywaveform_get_sampling_rate(self.pinned())
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Set the frequency resolution in hertz.
    fn set_freq_resolution(&mut self, resolution: u64)
        -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::arraywaveform_set_freq_resolution(self.pinned(), resolution)
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Get the frequency resolution in hertz.
    pub fn get_freq_resolution(&mut self) -> AWGResult<u64> {
        unsafe {
            return ffi::arraywaveform_get_freq_resolution(self.pinned())
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Set the constituent tones using a fixed center frequency and uniform
    /// spacing in hertz.
    fn set_freq_tones_spaced(
        &mut self,
        center: i32,
        spacing: i32,
        num_tones: u16,
    ) -> AWGResult<&mut Self>
    {
        unsafe {
            ffi::arraywaveform_set_freq_tones_spaced(
                self.pinned(), center, spacing, num_tones.into())
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Set the constituent tones to arbitrary values in hertz.
    fn set_freq_tones<'a, I>(&mut self, tones: I) -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a i32>
    {
        unsafe {
            let tones_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(tones);
            ffi::arraywaveform_set_freq_tones(
                self.pinned(), tones_cxx.as_ref().unwrap())
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Get a list of contituent tones.
    pub fn get_freq_tones(&mut self) -> AWGResult<Vec<f64>> {
        unsafe {
            return ffi::arraywaveform_get_freq_tones(self.pinned())
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Set the initial phases in radians of all tones.
    fn set_phases<'a, I>(&mut self, phases: I) -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a f64>
    {
        unsafe {
            let phases_cxx: UniquePtr<CxxVector<f64>>
                = collect_cxx_vector(phases);
            ffi::arraywaveform_set_phases(
                self.pinned(), phases_cxx.as_ref().unwrap())
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Get a list of initial phases in radians for each tone.
    pub fn get_phases(&mut self) -> AWGResult<Vec<f64>> {
        unsafe {
            return ffi::arraywaveform_get_phases(self.pinned())
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Set the amplitudes of all tones.
    ///
    /// Each amplitude must be in the range `[0, 2^15 - 1)`
    fn set_amplitudes<'a, I>(&mut self, amplitudes: I)
        -> AWGResult<&mut Self>
    where I: IntoIterator<Item = &'a f64>
    {
        unsafe {
            let amplitudes: Vec<f64>
                = amplitudes.into_iter().copied()
                .map(|a| {
                    (0.0..32767.0).contains(&a)
                        .then_some(a)
                        .ok_or(AWGError::InvalidAmplitude(a))
                })
                .collect::<AWGResult<Vec<f64>>>()?;
            let amplitudes_cxx: UniquePtr<CxxVector<f64>>
                = collect_cxx_vector(&amplitudes);
            ffi::arraywaveform_set_amplitudes(
                self.pinned(), amplitudes_cxx.as_ref().unwrap())
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Get a list of amplitudes for each tone.
    pub fn get_amplitudes(&mut self) -> AWGResult<Vec<f64>> {
        unsafe {
            return ffi::arraywaveform_get_amplitudes(self.pinned())
                .map_err(AWGError::as_waveform_err);
            }
    }

    /// Set phases to 0 and amplitudes to 2000.
    fn set_default_params(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::arraywaveform_set_default_params(self.pinned())
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Save the current waveform parameters to a file in CSV format.
    fn save_params<P>(&mut self, filename: P) -> AWGResult<&mut Self>
    where P: AsRef<Path>
    {
        unsafe {
            let path_osstr: &OsStr = filename.as_ref().as_os_str();
            let path_str: &str
                = path_osstr.to_str()
                .ok_or(AWGError::PathError(path_osstr.to_os_string()))?;
            path_str.ends_with(".csv").then_some(())
                .ok_or(AWGError::WaveformCSV(path_osstr.to_os_string()))?;
            let_cxx_string!(fname = path_str);
            ffi::arraywaveform_save_params(self.pinned(), &fname)
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Load waveform parameters from a file.
    ///
    /// The file must be in CSV format.
    fn load_params<P>(&mut self, filename: P) -> AWGResult<&mut Self>
    where P: AsRef<Path>
    {
        unsafe {
            let path_osstr: &OsStr = filename.as_ref().as_os_str();
            let path_str: &str
                = path_osstr.to_str()
                .ok_or(AWGError::PathError(path_osstr.to_os_string()))?;
            path_str.ends_with(".csv").then_some(())
                .ok_or(AWGError::WaveformCSV(path_osstr.to_os_string()))?;
            let_cxx_string!(fname = path_str);
            ffi::arraywaveform_load_params(self.pinned(), &fname)
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Print the current waveform parameters.
    pub fn print_params(&mut self) -> AWGResult<&mut Self> {
        unsafe {
            ffi::arraywaveform_print_params(self.pinned())
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }

    /// Get the minimum data length that fulfills rounding constraints for the
    /// given sampling rate and frequency resolution (in hertz).
    pub fn get_min_sample_len(
        &mut self,
        sampling_rate: u64,
        frequency_resolution: u64,
    ) -> AWGResult<u64>
    {
        unsafe {
            return ffi::arraywaveform_get_min_sample_len(
                self.pinned(), sampling_rate, frequency_resolution)
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Get a data array length close to the specified runtime tau (in seconds)
    /// that also fulfills rounding constraints for the given sampling rate and
    /// frequency resolution (in hertz).
    pub fn get_sample_len(
        &mut self,
        tau: f64,
        sampling_rate: u64,
        frequency_resolution: u64,
    ) -> AWGResult<u64>
    {
        unsafe {
            return ffi::arraywaveform_get_sample_len(
                self.pinned(), tau, sampling_rate, frequency_resolution)
                .map_err(AWGError::as_waveform_err);
        }
    }

    /// Generate a static frequency waveform from the current set of parameters.
    pub fn get_static_waveform(&mut self) -> AWGResult<WaveformDataPtr> {
        unsafe {
            return ffi::arraywaveform_get_static_waveform(self.pinned())
                .map(|ptr| WaveformDataPtr { ptr })
                .map_err(AWGError::as_waveform_err);
            }
    }

    /// Generate a tricky-trick waveform from the current set of parameters.
    ///
    /// - `sites`: Iterable of array site indices for which the tricky-trick
    /// will be performed
    /// - `df`: Frequency to move by in hertz
    /// - `tau_move`: Moving time in seconds
    /// - `tau_stay`: Wait time in seconds
    pub fn get_trick_waveform<'a, I>(
        &mut self,
        sites: I,
        df: f64,
        tau_move: f64,
        tau_stay: f64,
    ) -> AWGResult<WaveformDataPtr>
    where I: IntoIterator<Item = &'a i32>
    {
        unsafe {
            let sites_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(sites);
            return ffi::arraywaveform_get_trick_waveform(
                self.pinned(),
                sites_cxx.as_ref().unwrap(),
                df,
                tau_move,
                tau_stay,
            )
            .map(|ptr| WaveformDataPtr { ptr })
            .map_err(AWGError::as_waveform_err);
        }
    }

    /// Save the generated waveform to a CSV-formatted file.
    pub fn save_waveform<P>(&mut self, filename: P) -> AWGResult<&mut Self>
    where P: AsRef<Path>
    {
        unsafe {
            let path_osstr: &OsStr = filename.as_ref().as_os_str();
            let path_str: &str
                = path_osstr.to_str()
                .ok_or(AWGError::PathError(path_osstr.to_os_string()))?;
            path_str.ends_with(".csv").then_some(())
                .ok_or(AWGError::WaveformCSV(path_osstr.to_os_string()))?;
            let_cxx_string!(fname = path_str);
            ffi::arraywaveform_save_waveform(self.pinned(), &fname)
                .map_err(AWGError::as_waveform_err)?;
            return Ok(self);
        }
    }
}

