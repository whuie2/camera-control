#[cxx::bridge]
pub(crate) mod ffi {
    #[cfg(target_family = "unix")]
    unsafe extern "C++" {
        include!("awg-cxx/awg-control/Cpp/lib/AWG.h");
        include!("awg-cxx/awg-control/Cpp/lib/waveform.h");
        include!("awg-cxx/include/awg.h");
    }

    #[cfg(target_family = "windows")]
    unsafe extern "C++" {
        include!("awg-cxx\\awg-control\\Cpp\\lib\\AWG.h");
        include!("awg-cxx\\awg-control\\Cpp\\lib\\waveform.h");
        include!("awg-cxx\\include\\awg.h");
    }

    #[allow(clippy::too_many_arguments)]
    unsafe extern "C++" {
        type PageAlignedMem;
        type AWGBox;

        unsafe fn new_awg(idx: i32) -> Result<UniquePtr<AWGBox>>;

        unsafe fn awg_reset(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_get_card_idx(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_get_serial_number(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_get_inst_mem_size(awg: Pin<&mut AWGBox>) -> Result<i64>;
        unsafe fn awg_get_bytes_per_sample(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_get_max_sample_rate(awg: Pin<&mut AWGBox>) -> Result<i64>;
        unsafe fn awg_set_sample_rate(awg: Pin<&mut AWGBox>, sample_rate: i64) -> Result<()>;
        unsafe fn awg_get_sample_rate(awg: Pin<&mut AWGBox>) -> Result<i64>;

        unsafe fn awg_set_active_channels(awg: Pin<&mut AWGBox>, channels: &CxxVector<i32>) -> Result<()>;
        unsafe fn awg_set_channel_amp(awg: Pin<&mut AWGBox>, ch: i32, amp: i32) -> Result<()>;
        unsafe fn awg_set_channel_stop_level(awg: Pin<&mut AWGBox>, ch: i32, stop_level: i32) -> Result<()>;
        unsafe fn awg_toggle_channel_output(awg: Pin<&mut AWGBox>, ch: i32, enable: bool) -> Result<()>;
        unsafe fn awg_set_channel(awg: Pin<&mut AWGBox>, ch: i32, amp: i32, stop_level: i32, enable: bool) -> Result<()>;
        unsafe fn awg_get_channel_count(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_get_channel_activated(awg: Pin<&mut AWGBox>) -> Result<Vec<i32>>;
        unsafe fn awg_set_channel_diff_mode(awg: Pin<&mut AWGBox>, ch_pair: i32, enable: bool) -> Result<()>;
        unsafe fn awg_set_channel_double_mode(awg: Pin<&mut AWGBox>, ch_pair: i32, enable: bool) -> Result<()>;

        unsafe fn awg_write_setup(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_card_run(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_card_stop(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_wait_ready(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_set_timeout(awg: Pin<&mut AWGBox>, timeout_ms: i32) -> Result<()>;
        unsafe fn awg_get_timeout(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_print_card_status(awg: Pin<&mut AWGBox>) -> Result<()>;

        unsafe fn awg_toggle_trigger(awg: Pin<&mut AWGBox>, enable: bool) -> Result<()>;
        unsafe fn awg_force_trigger(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_wait_trigger(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_set_trig_mask_or(awg: Pin<&mut AWGBox>, masks: &CxxVector<i32>) -> Result<()>;
        unsafe fn awg_set_trig_mask_and(awg: Pin<&mut AWGBox>, masks: &CxxVector<i32>) -> Result<()>;
        unsafe fn awg_set_trig_mode(awg: Pin<&mut AWGBox>, ch: i32, mode: i32) -> Result<()>;
        unsafe fn awg_set_trig_term(awg: Pin<&mut AWGBox>, term: i32) -> Result<()>;
        unsafe fn awg_get_trig_term(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_set_trig_coupling(awg: Pin<&mut AWGBox>, ch: i32, coupling: i32) -> Result<()>;
        unsafe fn awg_get_trig_coupling(awg: Pin<&mut AWGBox>, ch: i32) -> Result<i32>;
        unsafe fn awg_set_trig_level(awg: Pin<&mut AWGBox>, ch: i32, level: i32) -> Result<()>;
        unsafe fn awg_get_trig_level(awg: Pin<&mut AWGBox>, ch: i32) -> Result<i32>;
        unsafe fn awg_set_trig_rearm_level(awg: Pin<&mut AWGBox>, ch: i32, level: i32) -> Result<()>;
        unsafe fn awg_get_trig_rearm_level(awg: Pin<&mut AWGBox>, ch: i32) -> Result<i64>;

        unsafe fn awg_init_replay_mode_seq(awg: Pin<&mut AWGBox>, n_segments: i32) -> Result<()>;
        unsafe fn awg_set_seq_mode_step(awg: Pin<&mut AWGBox>, step: u64, segment: u64, next_step: u64, n_loop: u64, condition: i32) -> Result<()>;
        unsafe fn awg_write_seq_mode_segment(awg: Pin<&mut AWGBox>, segment: i32, p_data_buffer: Pin<&mut PageAlignedMem>, size: i32) -> Result<()>;

        unsafe fn awg_prep_data_transfer(awg: Pin<&mut AWGBox>, p_data_buffer: Pin<&mut PageAlignedMem>, buffer_len: u64, buf_type: i32, dir: i32, notify_size: u32, board_mem_offs: u64) -> Result<()>;
        unsafe fn awg_start_data_transfer(awg: Pin<&mut AWGBox>) -> Result<()>;
        unsafe fn awg_stop_data_transfer(awg: Pin<&mut AWGBox>) -> Result<()>;

        unsafe fn awg_set_clock_mode(awg: Pin<&mut AWGBox>, mode: i32) -> Result<()>;
        unsafe fn awg_get_clock_mode(awg: Pin<&mut AWGBox>) -> Result<i32>;
        unsafe fn awg_set_ref_clock_freq(awg: Pin<&mut AWGBox>, frequency: i64) -> Result<()>;
        unsafe fn awg_set_clock_out(awg: Pin<&mut AWGBox>, enable: bool) -> Result<()>;
        unsafe fn awg_get_clock_out_freq(awg: Pin<&mut AWGBox>) -> Result<i64>;

        // unsafe fn awg_get_page_aligned_mem(awg: Pin<&mut AWGBox>, size: u64) -> Result<UniquePtr<PageAlignedMem>>;
        // unsafe fn awg_free_page_aligned_mem(awg: Pin<&mut AWGBox>, p_data_buffer: UniquePtr<PageAlignedMem>, size: u64) -> Result<()>;

        /**********************************************************************/

        type ArrayWaveform;
        type WaveformData;

        unsafe fn new_arraywaveform() -> Result<UniquePtr<ArrayWaveform>>;

        unsafe fn arraywaveform_get_data_buffer(waveform: Pin<&mut ArrayWaveform>) -> Result<UniquePtr<PageAlignedMem>>;
        unsafe fn arraywaveform_get_data_len(waveform: Pin<&mut ArrayWaveform>) -> Result<i64>;

        unsafe fn arraywaveform_set_sampling_rate(waveform: Pin<&mut ArrayWaveform>, rate: u64) -> Result<()>;
        unsafe fn arraywaveform_get_sampling_rate(waveform: Pin<&mut ArrayWaveform>) -> Result<u64>;
        unsafe fn arraywaveform_set_freq_resolution(waveform: Pin<&mut ArrayWaveform>, resolution: u64) -> Result<()>;
        unsafe fn arraywaveform_get_freq_resolution(waveform: Pin<&mut ArrayWaveform>) -> Result<u64>;

        unsafe fn arraywaveform_set_freq_tones_spaced(waveform: Pin<&mut ArrayWaveform>, center: i32, spacing: i32, num_tones: i32) -> Result<()>;
        unsafe fn arraywaveform_set_freq_tones(waveform: Pin<&mut ArrayWaveform>, tones: &CxxVector<i32>) -> Result<()>;
        unsafe fn arraywaveform_get_freq_tones(waveform: Pin<&mut ArrayWaveform>) -> Result<Vec<f64>>;
        unsafe fn arraywaveform_set_phases(waveform: Pin<&mut ArrayWaveform>, phases: &CxxVector<f64>) -> Result<()>;
        unsafe fn arraywaveform_get_phases(waveform: Pin<&mut ArrayWaveform>) -> Result<Vec<f64>>;
        unsafe fn arraywaveform_set_amplitudes(waveform: Pin<&mut ArrayWaveform>, amplitudes: &CxxVector<f64>) -> Result<()>;
        unsafe fn arraywaveform_get_amplitudes(waveform: Pin<&mut ArrayWaveform>) -> Result<Vec<f64>>;
        unsafe fn arraywaveform_set_default_params(waveform: Pin<&mut ArrayWaveform>) -> Result<()>;
        unsafe fn arraywaveform_save_params(waveform: Pin<&mut ArrayWaveform>, filename: &CxxString) -> Result<()>;
        unsafe fn arraywaveform_load_params(waveform: Pin<&mut ArrayWaveform>, filename: &CxxString) -> Result<()>;
        unsafe fn arraywaveform_print_params(waveform: Pin<&mut ArrayWaveform>) -> Result<()>;

        unsafe fn arraywaveform_get_min_sample_len(waveform: Pin<&mut ArrayWaveform>, sampling_rate: u64, frequency_resolution: u64) -> Result<u64>;
        unsafe fn arraywaveform_get_sample_len(waveform: Pin<&mut ArrayWaveform>, tau: f64, sampling_rate: u64, frequency_resolution: u64) -> Result<u64>;

        unsafe fn arraywaveform_get_static_waveform(waveform: Pin<&mut ArrayWaveform>) -> Result<UniquePtr<WaveformData>>;
        unsafe fn arraywaveform_get_trick_waveform(waveform: Pin<&mut ArrayWaveform>, site_index: &CxxVector<i32>, df: f64, tau_move: f64, tau_stay: f64) -> Result<UniquePtr<WaveformData>>;

        unsafe fn arraywaveform_save_waveform(waveform: Pin<&mut ArrayWaveform>, filename: &CxxString) -> Result<()>;

        /**********************************************************************/

        unsafe fn waveformdata_get_mem(data: Pin<&mut WaveformData>) -> Result<UniquePtr<PageAlignedMem>>;
        unsafe fn waveformdata_get_datalen(data: Pin<&mut WaveformData>) -> Result<i64>;
    }
}
