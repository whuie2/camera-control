#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return)]

pub(crate) mod awg_ffi;
pub mod awg;
pub mod prelude;

