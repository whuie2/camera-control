use awg::awg::*;

fn main() {
	let mut awg = AWG::connect(AWGIndex::Zero)
		.expect("couldn't connect");
	awg.print_card_status()
		.expect("couldn't print status");
}
