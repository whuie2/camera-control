fn main() {
    #[cfg(target_family = "unix")]
    {
        println!("cargo:rerun-if-changed=include/awg.h");
        println!("cargo:rerun-if-changed=lib/awg.cpp");
        println!("cargo:rerun-if-changed=lib/awg_ffi.rs");

        println!("cargo:rustc-link-search=/usr/lib64");
        println!("cargo:rustc-link-lib=spcm_linux");

        cxx_build::bridge("lib/awg_ffi.rs")
            .cpp(true)
            .flag("-std=c++20")
            .include("include")
            .include("awg-control/Cpp/lib")
            .include("awg-control/Cpp/lib/driver_header")
            .file("awg-control/Cpp/lib/AWG.cpp")
            .file("lib/awg.cc")
            .compile("awg-cxx");
    }

    #[cfg(target_family = "windows")]
    {
        println!("cargo:rerun-if-changed=include\\awg.h");
        println!("cargo:rerun-if-changed=lib\\awg.cpp");
        println!("cargo:rerun-if-changed=lib\\awg_ffi.rs");

        println!("cargo:rustc-link-search=awg-control\\Cpp\\lib\\driver_header");
        println!("cargo:rustc-link-lib=static:+verbatim=spcm_win64_msvcpp.lib");

        cxx_build::bridge("lib\\awg_ffi.rs")
            .cpp(true)
            // .flag("-std=c++14") // this gets ignored on Windows for some reason
            .include("include")
            .include("awg-control\\Cpp\\lib")
            .include("awg-control\\Cpp\\lib\\driver_header")
            .file("lib\\awg.cc")
            .compile("awg-cxx");
    }
}
