//! Provides construct to live-plot data output from the camera and frame
//! filters.
//!
//! Operation is based on a model where a [`FrameViewer`] is run in the main
//! thread and optionally visualizes the output from a frame process in addition
//! to the direct stream from the camera. These frames are received via
//! [`ToViewer`]s sent over a [`crossbeam`] channel, the receiving end of which
//! the viewer is used to initialize the viewer via a [`ViewerConfig`].

use std::collections::VecDeque;
use crossbeam::channel::{
    Receiver,
    TryRecvError,
};
use eframe::egui::{
    self,
    Key,
    Color32,
    Margin,
    Rounding,
    SidePanel,
    Stroke,
    TopBottomPanel,
    Visuals,
    containers::Frame,
    epaint::Shadow,
    widgets::DragValue,
};
use egui_plotter::EguiBackend;
use ndarray as nd;
use plotters::{
    drawing::IntoDrawingArea,
    prelude::*,
    style::text_anchor::{
        Pos,
        HPos,
        VPos,
    },
};
use thiserror::Error;
use crate::{
    colors::{
        ColorBar,
        ColorMap,
        // ColorMapBound,
        ColorMapBounds,
    },
    // viewer::FrameData,
};

#[derive(Debug, Error)]
pub enum ViewerError {
    #[error("egui error: {0}")]
    EguiError(String),
}
pub type ViewerResult<T> = Result<T, ViewerError>;

const ZERO_MARGIN: Margin
    = Margin {
        left: 0.0,
        right: 0.0,
        top: 0.0,
        bottom: 0.0,
    };

fn zero_margin_frame() -> Frame {
    Frame {
        inner_margin: ZERO_MARGIN,
        outer_margin: ZERO_MARGIN,
        rounding: Rounding { nw: 0.0, ne: 0.0, sw: 0.0, se: 0.0 },
        shadow: Shadow::default(),
        fill: Color32::from_rgb(27, 27, 27),
        stroke: Stroke::default(),
    }
}

fn inner_margin_frame(lr: f32, tb: f32) -> Frame {
    Frame {
        inner_margin: Margin { left: lr, right: lr, top: tb, bottom: tb },
        ..zero_margin_frame()
    }
}

const MASTER_CONTROL_AREA_HEIGHT: f64 = 45.0;
const DRAWING_AREA_WIDTH: f64 = 600.0;
const DRAWING_AREA_HEIGHT: f64 = 540.0;
const CONTROL_AREA_HEIGHT: f64 = 46.0;
const PLOT_OUTER_MARGIN: f64 = 0.0;
const TICK_LABEL_MARGIN: f64 = 30.0;
const NOTICK_LABEL_MARGIN: f64 = 5.0;
const BAR_WIDTH: f64 = 80.0;
const BAR_LABEL_MARGIN: f64 = 60.0;

const BACKGROUND: RGBColor = RGBColor(27, 27, 27);
const FOREGROUND: RGBColor = RGBColor(129, 129, 129);
const TEXT_STYLE: (&str, u32, &RGBColor) = ("sans-serif", 14, &FOREGROUND);
const AXIS_STYLE: ShapeStyle
    = ShapeStyle {
        color: RGBAColor(129, 129, 129, 1.0),
        filled: true,
        stroke_width: 1,
    };
const BOLD_MESH_STYLE: ShapeStyle
    = ShapeStyle {
        color: RGBAColor(50, 50, 50, 1.0),
        filled: true,
        stroke_width: 1,
    };
const LIGHT_MESH_STYLE: ShapeStyle
    = ShapeStyle {
        color: RGBAColor(35, 35, 35, 1.0),
        filled: true,
        stroke_width: 1,
    };
const TICK_MARK_SIZE: f64 = 3.0;

/// Marker trait for basic numeric types that can be ferried around in frame
/// arrays.
pub trait FrameData
where
    nd::OwnedRepr<Self>: nd::RawData,
    Self: Copy,
    Self: Into<f64>,
    Self: PartialOrd,
    Self: std::fmt::Debug,
    Self: std::marker::Send,
{ }

macro_rules! impl_framedata_for {
    ( $t:ty ) => { impl FrameData for $t { } }
}
impl_framedata_for!(u8);
impl_framedata_for!(u16);
impl_framedata_for!(u32);
impl_framedata_for!(i8);
impl_framedata_for!(i16);
impl_framedata_for!(i32);
impl_framedata_for!(f32);
impl_framedata_for!(f64);

/// Holds frame data and colormap settings.
///
/// Builds frame plots that are eventually drawn in the viewer window.
#[derive(Debug)]
pub struct FramePlot<T>
where T: FrameData
{
    frame: Option<nd::Array2<T>>,
    colormap: ColorMap,
    bounds: ColorMapBounds,
}

impl<T> FramePlot<T>
where T: FrameData
{
    /// Create a new [`FramePlot`] with no frame data.
    pub fn new(colorbar: ColorBar) -> Self {
        let (colormap, bounds): (ColorMap, ColorMapBounds) = colorbar;
        return Self {
            frame: None,
            colormap,
            bounds,
        };
    }

    /// Check whether a frame can be drawn.
    pub fn is_initialized(&self) -> bool { self.frame.is_some() }

    /// Get the shape of the current frame, if it exists.
    pub fn get_frame_shape(&self) -> Option<(usize, usize)> {
        return self.frame.as_ref()
            .map(|f| {
                let shape: &[usize] = f.shape();
                (shape[0], shape[1])
            });
    }

    /// Load a new frame to draw, discarding the old one.
    pub fn set_frame(&mut self, frame: nd::Array2<T>) {
        self.frame = Some(frame);
    }

    /// Draw a given view of the currently held frame.
    pub fn draw<A>(&self, backend: A, pan: &[f64; 2], scale: f64)
    where A: IntoDrawingArea
    {
        let root = backend.into_drawing_area();
        root.fill(&BACKGROUND).expect("couldn't fill plotting area");

        if let Some(frame) = &self.frame {
            let frame_shape: &[usize] = frame.shape();
            let max_dim: f64 = frame_shape[0].max(frame_shape[1]) as f64;
            let half_dim: f64 = max_dim / 2.0;
            let center_i: f64 = frame_shape[0] as f64 / 2.0 - 0.5;
            let center_j: f64 = frame_shape[1] as f64 / 2.0 - 0.5;

            let (image_area, bar_area)
                = root.split_horizontally(
                    DRAWING_AREA_WIDTH
                    - PLOT_OUTER_MARGIN
                    - BAR_WIDTH
                    - NOTICK_LABEL_MARGIN
                );

            let mut image_chart = ChartBuilder::on(&image_area)
                .margin(PLOT_OUTER_MARGIN)
                .x_label_area_size(TICK_LABEL_MARGIN)
                .top_x_label_area_size(TICK_LABEL_MARGIN)
                .y_label_area_size(TICK_LABEL_MARGIN)
                .right_y_label_area_size(NOTICK_LABEL_MARGIN)
                .build_cartesian_2d(
                    center_j + pan[1] - half_dim / scale
                        .. center_j + pan[1] + half_dim / scale,
                    center_i + pan[0] + half_dim / scale
                        .. center_i + pan[0] - half_dim / scale,
                )
                .expect("chart build failed for image chart")
                .set_secondary_coord(0..0, 0..0);
            image_chart
                .configure_mesh()
                .axis_style(AXIS_STYLE)
                .bold_line_style(BOLD_MESH_STYLE)
                .light_line_style(LIGHT_MESH_STYLE)
                .x_labels(((max_dim / scale).ln() as usize + 1).max(2))
                .y_labels(((max_dim / scale).ln() as usize + 1).max(2))
                .x_label_formatter(&|x| format!("{:.0}", x))
                .y_label_formatter(&|y| format!("{:.0}", y))
                .set_all_tick_mark_size(TICK_MARK_SIZE)
                .x_label_style(TEXT_STYLE)
                .y_label_style(TEXT_STYLE)
                .draw()
                .expect("axis draw failed for image chart");
            image_chart
                .configure_secondary_axes()
                .axis_style(AXIS_STYLE)
                .x_labels(0)
                .y_labels(0)
                .x_label_formatter(&|_| String::new())
                .y_label_formatter(&|_| String::new())
                .set_all_tick_mark_size(0)
                .draw()
                .expect("secondary axis draw failed for image chart");
            let (min, max): (f64, f64)
                = (
                    self.bounds.0.bound_min(frame),
                    self.bounds.1.bound_max(frame),
                );
            let range: f64 = max - min;
            image_chart
                .draw_series(
                    frame.indexed_iter()
                        .map(|((i, j), x)| {
                            Rectangle::new(
                                [
                                    (j as f64 - 0.5, i as f64 - 0.5),
                                    (j as f64 + 0.5, i as f64 + 0.5),
                                ],
                                self.colormap.c(
                                    (<T as Into<f64>>::into(*x) - min)
                                    / range
                                )
                                .filled(),
                            )
                        })
                )
                .expect("data draw failed for image chart");

            let dbar: f64 = range / 100.0;
            let mut bar_chart = ChartBuilder::on(&bar_area)
                .margin(PLOT_OUTER_MARGIN)
                .x_label_area_size(TICK_LABEL_MARGIN)
                .top_x_label_area_size(TICK_LABEL_MARGIN)
                .y_label_area_size(NOTICK_LABEL_MARGIN)
                .right_y_label_area_size(BAR_LABEL_MARGIN)
                .build_cartesian_2d(0..0, 0..0)
                .expect("chart build failed for colorbar")
                .set_secondary_coord(
                    -0.5 .. 0.5,
                    min - 3.0 * dbar / 4.0 .. max + dbar / 2.0,
                );
            bar_chart
                .configure_mesh()
                .axis_style(AXIS_STYLE)
                .x_labels(0)
                .y_labels(0)
                .x_label_formatter(&|_| String::new())
                .y_label_formatter(&|_| String::new())
                .set_all_tick_mark_size(0)
                .x_desc("")
                .y_desc("")
                .draw()
                .expect("axis draw failed for colorbar");
            bar_chart
                .configure_secondary_axes()
                .axis_style(AXIS_STYLE)
                .x_labels(0)
                .y_labels(((max.ceil() - min.floor()).ln() as usize + 1).max(2))
                .x_label_formatter(&|_| String::new())
                .y_label_formatter(&|y| format!("{:.0}", y))
                .set_all_tick_mark_size(TICK_MARK_SIZE)
                .label_style(TEXT_STYLE)
                .x_desc("")
                .y_desc("")
                .draw()
                .expect("secondary axis draw failed for colorbar");
            bar_chart
                .draw_secondary_series(
                    (0..=100_usize).map(|k| {
                        Rectangle::new(
                            [
                                (
                                    -0.5,
                                    k as f64 / 100.0 * range + min - dbar / 2.0,
                                ),
                                (
                                     0.5,
                                    k as f64 / 100.0 * range + min + dbar / 2.0,
                                ),
                            ],
                            self.colormap.c(k as f64 / 100.0)
                            .filled(),
                        )
                    })
                )
                .expect("data draw failed for colorbar");
        } else {
            let mut chart = ChartBuilder::on(&root)
                .margin(PLOT_OUTER_MARGIN)
                .x_label_area_size(TICK_LABEL_MARGIN)
                .top_x_label_area_size(TICK_LABEL_MARGIN)
                .y_label_area_size(TICK_LABEL_MARGIN)
                .right_y_label_area_size(TICK_LABEL_MARGIN)
                .build_cartesian_2d(-1.0 .. 1.0, -1.0 .. 1.0)
                .expect("chart build failed for image chart")
                .set_secondary_coord(0..0, 0..0);
            chart
                .configure_mesh()
                .axis_style(AXIS_STYLE)
                .bold_line_style(BOLD_MESH_STYLE)
                .light_line_style(LIGHT_MESH_STYLE)
                .x_labels(3)
                .y_labels(3)
                .x_label_formatter(&|x| format!("{:.0}", x))
                .y_label_formatter(&|y| format!("{:.0}", y))
                .set_all_tick_mark_size(TICK_MARK_SIZE)
                .x_label_style(TEXT_STYLE)
                .y_label_style(TEXT_STYLE)
                .draw()
                .expect("axis draw failed for image chart");
            chart
                .configure_secondary_axes()
                .axis_style(AXIS_STYLE)
                .x_labels(0)
                .y_labels(0)
                .x_label_formatter(&|_| String::new())
                .y_label_formatter(&|_| String::new())
                .set_all_tick_mark_size(0)
                .draw()
                .expect("secondary axis draw failed for image chart");
            chart
                .draw_series([
                    Text::new(
                        "Awaiting first acquisition...",
                        (0.0, 0.0),
                        TEXT_STYLE
                            .with_anchor::<RGBColor>(Pos {
                                h_pos: HPos::Center,
                                v_pos: VPos::Center,
                            })
                            .into_text_style(&root)
                    )
                ])
                .expect("data draw failed for image chart");
        }
    }
}

/// Input to a [`FrameViewer`] sent from another thread.
#[derive(Clone, Debug)]
pub enum ToViewer {
    /// Acquisition loop is ready.
    Ready,

    /// No frame.
    Nothing,

    /// Frame obtained directly from the camera.
    Frame(nd::Array2<u16>),

    /// Processed frame.
    ProcFrame(nd::Array2<f64>),

    /// Halt visualization and close the window.
    Stop,
}

/// Internal frame redirection type.
#[derive(Debug)]
enum FrameRedirect {
    Main(nd::Array2<u16>),
    Proc(nd::Array2<f64>),
}

impl FrameRedirect {
    fn is_main(&self) -> bool { matches!(self, Self::Main(_)) }

    fn into_main(self) -> Option<nd::Array2<u16>> {
        return match self {
            Self::Main(f) => Some(f),
            Self::Proc(_) => None,
        };
    }

    fn is_proc(&self) -> bool { matches!(self, Self::Proc(_)) }

    fn into_proc(self) -> Option<nd::Array2<f64>> {
        return match self {
            Self::Main(_) => None,
            Self::Proc(f) => Some(f),
        };
    }
}

/// Initialize a [`FrameViewer`] with these data.
pub struct ViewerConfig {
    /// Message channel to the viewer.
    pub source: Receiver<ToViewer>,

    /// Colormap and bounds to use for the frame plot.
    pub colorbar: ColorBar,

    /// Colormap and bounds to use for the processed-frame plot, if desired.
    pub proc_colorbar: Option<ColorBar>,
}

/// A particular view of a [`FramePlot`]
#[derive(Debug)]
pub struct FramePlotView<T>
where T: FrameData
{
    pub plot: FramePlot<T>,
    pub pan: [f64; 2],
    pub scale: f64,
}

impl<T> FramePlotView<T>
where T: FrameData
{
    /// Load a new frame to draw, discarding the old one.
    pub fn set_frame(&mut self, frame: nd::Array2<T>) {
        self.plot.set_frame(frame);
    }

    /// Draw the view of the currently held frame.
    pub fn draw<A>(&self, backend: A)
    where A: IntoDrawingArea
    {
        self.plot.draw(backend, &self.pan, self.scale);
    }
}

/// Frame data visualizer.
///
/// Call the [`Self::run`] function to spawn a new frame visualization window.
/// Note that this blocks the calling thread and cannot be called from another,
/// so care should be taken to ensure that all supportin channels are properly
/// instantiated and handled beforehand.
#[derive(Debug)]
pub struct FrameViewer {
    /// Message channel to receive instructions from another thread.
    source: Receiver<ToViewer>,

    /// Plot of the current frame from the camera-direct stream.
    main_view: FramePlotView<u16>,

    /// Optional plot of a processed frame.
    proc_view: Option<FramePlotView<f64>>,

    /// Internal queue for redirecting frames received over `source` to the
    /// appropriate viewer.
    proc_redirect: Option<VecDeque<FrameRedirect>>,

    /// Lock the pan/scale controls of the views of both frames together.
    lock_views: bool,

    /// Whether to make the "Abort" button available.
    abort_ready: bool,
}

impl FrameViewer {
    /// For use with the built-in [`eframe::App::run_native`] method.
    fn new(cc: &eframe::CreationContext<'_>, config: ViewerConfig) -> Self {
        let context = &cc.egui_ctx;
        context.tessellation_options_mut(|tess_options| {
            tess_options.feathering = false;
        });
        context.set_visuals(Visuals::dark());

        let ViewerConfig { source, colorbar, proc_colorbar } = config;
        let main_view = FramePlotView {
            plot: FramePlot::new(colorbar),
            pan: [0.0, 0.0],
            scale: 1.0,
        };
        let proc_view: Option<FramePlotView<f64>>
            = proc_colorbar.map(|cbar| {
                FramePlotView {
                    plot: FramePlot::new(cbar),
                    pan: [0.0, 0.0],
                    scale: 1.0,
                }
            });
        let proc_redirect: Option<VecDeque<FrameRedirect>>
            = proc_view.is_some().then_some(VecDeque::new());
        return Self {
            source,
            main_view,
            proc_view,
            proc_redirect,
            lock_views: false,
            abort_ready: false,
        };
    }

    /// Spawn a new frame visualization window.
    pub fn run(config: ViewerConfig) -> ViewerResult<()> {
        let native_options
            = eframe::NativeOptions {
                always_on_top: false, // bool
                maximized: false, // bool
                decorated: true, // bool
                fullscreen: false, // bool
                drag_and_drop_support: false, // bool
                icon_data: None, // Option<IconData>
                initial_window_pos: None, // Option<Pos2>
                initial_window_size: Some(
                    egui::Vec2 {
                        x: if config.proc_colorbar.is_some() {
                            DRAWING_AREA_WIDTH as f32 * 2.0
                        } else {
                            DRAWING_AREA_WIDTH as f32
                        },
                        y:
                            MASTER_CONTROL_AREA_HEIGHT as f32
                            + DRAWING_AREA_HEIGHT as f32
                            + CONTROL_AREA_HEIGHT as f32,
                    }
                ), // Option<Vec2>
                min_window_size: None, // Option<Vec2>
                max_window_size: None, // Option<Vec2>
                resizable: false, // bool
                transparent: false, // bool
                mouse_passthrough: false, // bool
                active: true, // bool
                vsync: true, // bool
                ..eframe::NativeOptions::default()
                // multisampling: // u16
                // depth_buffer: // u8
                // stencil_buffer: // u8
                // hardware_acceleration: // HardwareAcceleration
                // renderer: // Renderer
                // follow_system_theme: // bool
                // default_theme: // Theme
                // run_and_return: // bool
                // event_loop_builder: // Option<EventLoopBuilderHook>
                // shader_version: // Option<ShaderVersion>
                // centered: // bool
                // wgpu_options: // WgpuConfiguration
                // app_id: // Option<String>
            };
        return eframe::run_native(
            "Frame viewer",
            native_options,
            Box::new(|cc| Box::new(Self::new(cc, config))),
        )
        .map_err(|err| ViewerError::EguiError(err.to_string()));
    }

    fn pull_main(
        abort_ready: &mut bool,
        source: &Receiver<ToViewer>,
        main_view: &mut FramePlotView<u16>,
        proc_redirect: &mut Option<VecDeque<FrameRedirect>>,
        frame: &mut eframe::Frame,
    )
    {
        if let Some(redir) = proc_redirect {
            if redir.front().is_some_and(|f| f.is_main()) {
                let frame: nd::Array2<u16>
                    = redir.pop_front().unwrap()
                    .into_main().unwrap();
                main_view.set_frame(frame);
            } else {
                match source.try_recv() {
                    Ok(to_viewer) => match to_viewer {
                        ToViewer::Ready => { *abort_ready = true; },
                        ToViewer::Nothing => { },
                        ToViewer::Frame(frame) => {
                            main_view.set_frame(frame);
                        },
                        ToViewer::ProcFrame(proc_frame) => {
                            redir.push_back(FrameRedirect::Proc(proc_frame));
                        },
                        ToViewer::Stop => { frame.close(); },
                    },
                    Err(TryRecvError::Empty) => { },
                    Err(TryRecvError::Disconnected) => { frame.close(); },
                }
            }
        } else {
            match source.try_recv() {
                Ok(to_viewer) => match to_viewer {
                    ToViewer::Ready => { *abort_ready = true; },
                    ToViewer::Nothing => { },
                    ToViewer::Frame(frame) => {
                        main_view.set_frame(frame);
                    },
                    ToViewer::ProcFrame(_) => { },
                    ToViewer::Stop => { frame.close(); },
                },
                Err(TryRecvError::Empty) => { },
                Err(TryRecvError::Disconnected) => { frame.close(); },
            }
        }
    }

    fn controls_main(main_view: &mut FramePlotView<u16>, ui: &mut egui::Ui) {
        let frame_shape: Option<(usize, usize)>
            = main_view.plot.get_frame_shape();
        let max_dim: f64
            = frame_shape.map(|(h, w)| h.max(w) as f64)
            .unwrap_or(std::f64::consts::E);
        let slide_speed: f64
            = 0.1 * max_dim.ln() / main_view.scale;
        ui.horizontal_centered(|ui| {
            ui.add(
                DragValue::new(&mut main_view.pan[0])
                    .prefix("Pan i:  ")
                    .speed(slide_speed)
                    .clamp_range(
                        frame_shape.map(|(h, _)| {
                            let h = h as f64;
                            let d = (max_dim - h) / 2.0;
                            -(h + d - 1.0) ..= (h + d - 1.0)
                        })
                        .unwrap_or(f64::NEG_INFINITY ..= f64::INFINITY)
                    )
            );
            ui.add(
                DragValue::new(&mut main_view.pan[1])
                    .prefix("Pan j:  ")
                    .speed(slide_speed)
                    .clamp_range(
                        frame_shape.map(|(_, w)| {
                            let w = w as f64;
                            let d = (max_dim - w) / 2.0;
                            -(w + d - 1.0) ..= (w + d - 1.0)
                        })
                        .unwrap_or(f64::NEG_INFINITY ..= f64::INFINITY)
                    )
            );
            ui.add(
                DragValue::new(&mut main_view.scale)
                    .prefix("Scale:  ")
                    .speed(0.05)
                    .clamp_range(0.5 ..= 10.0)
            );
            if ui.button("Reset view").clicked() {
                main_view.pan = [0.0, 0.0];
                main_view.scale = 1.0;
            }
        });
    }

    fn pull_proc(
        abort_ready: &mut bool,
        source: &Receiver<ToViewer>,
        proc_view: &mut FramePlotView<f64>,
        redir: &mut VecDeque<FrameRedirect>,
        frame: &mut eframe::Frame,
    )
    {
        if redir.front().is_some_and(|f| f.is_proc()) {
            let frame: nd::Array2<f64>
                = redir.pop_front().unwrap()
                .into_proc().unwrap();
            proc_view.set_frame(frame);
        } else {
            match source.try_recv() {
                Ok(to_viewer) => match to_viewer {
                    ToViewer::Ready => { *abort_ready = true; },
                    ToViewer::Nothing => { },
                    ToViewer::Frame(main_frame) => {
                        redir.push_back(FrameRedirect::Main(main_frame));
                    },
                    ToViewer::ProcFrame(frame) => {
                        proc_view.set_frame(frame);
                    },
                    ToViewer::Stop => { frame.close(); },
                },
                Err(TryRecvError::Empty) => { },
                Err(TryRecvError::Disconnected) => { frame.close(); },
            }
        }
    }

    fn controls_proc(
        lock_views: bool,
        main_view: &mut FramePlotView<u16>,
        proc_view: &mut FramePlotView<f64>,
        ui: &mut egui::Ui,
    )
    {
        if lock_views {
            proc_view.pan = main_view.pan;
            proc_view.scale = main_view.scale;
        } else {
            let frame_shape: Option<(usize, usize)>
                = proc_view.plot.get_frame_shape();
            let max_dim: f64
                = frame_shape.map(|(h, w)| h.max(w) as f64)
                .unwrap_or(std::f64::consts::E);
            let slide_speed: f64
                = 0.1 * max_dim.ln() / proc_view.scale;
            ui.horizontal_centered(|ui| {
                ui.add(
                    DragValue::new(&mut proc_view.pan[0])
                        .prefix("Pan i:  ")
                        .speed(slide_speed)
                        .clamp_range(
                            frame_shape.map(|(h, _)| {
                                let h = h as f64;
                                let d = (max_dim - h) / 2.0;
                                -(h + d - 1.0) ..= (h + d - 1.0)
                            })
                            .unwrap_or(f64::NEG_INFINITY ..= f64::INFINITY)
                        )
                );
                ui.add(
                    DragValue::new(&mut proc_view.pan[1])
                        .prefix("Pan j:  ")
                        .speed(slide_speed)
                        .clamp_range(
                            frame_shape.map(|(_, w)| {
                                let w = w as f64;
                                let d = (max_dim - w) / 2.0;
                                -(w + d - 1.0) ..= (w + d - 1.0)
                            })
                            .unwrap_or(f64::NEG_INFINITY ..= f64::INFINITY)
                        )
                );
                ui.add(
                    DragValue::new(&mut proc_view.scale)
                        .prefix("Scale:  ")
                        .speed(0.05)
                        .clamp_range(0.5 ..= 10.0)
                );
                if ui.button("Reset view").clicked() {
                    proc_view.pan = [0.0, 0.0];
                    proc_view.scale = 1.0;
                }
            });
        }
    }
}

impl eframe::App for FrameViewer {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        ctx.request_repaint();
        TopBottomPanel::top("master_controls")
            .exact_height(MASTER_CONTROL_AREA_HEIGHT as f32)
            .show_separator_line(true)
            .resizable(false)
            .show(ctx, |ui| {
                ui.input(|input| {
                    if
                        self.abort_ready
                            && input.modifiers.command_only()
                            && input.key_down(Key::Q)
                    {
                        frame.close();
                    }
                });
                ui.horizontal_centered(|ui| {
                    if self.abort_ready && ui.button("Abort").clicked() {
                        crate::andor::cancel_wait()
                            .expect("error cancelling wait for data");
                        frame.close();
                    }
                    if self.proc_view.is_some() {
                        ui.toggle_value(&mut self.lock_views, "Lock views");
                    }
                });
            });
        SidePanel::left("main")
            .exact_width(DRAWING_AREA_WIDTH as f32)
            .frame(zero_margin_frame())
            .show_separator_line(self.proc_view.is_some())
            .resizable(false)
            .show(ctx, |ui| {
                TopBottomPanel::top("main_view")
                    .exact_height(DRAWING_AREA_HEIGHT as f32)
                    .frame(zero_margin_frame())
                    .show_separator_line(false)
                    .resizable(false)
                    .show_inside(ui, |ui| {
                        Self::pull_main(
                            &mut self.abort_ready,
                            &self.source,
                            &mut self.main_view,
                            &mut self.proc_redirect,
                            frame,
                        );
                        let backend = EguiBackend::new(ui);
                        self.main_view.draw(backend);
                    });
                TopBottomPanel::bottom("main_controls")
                    .exact_height(CONTROL_AREA_HEIGHT as f32)
                    .show_separator_line(false)
                    .resizable(false)
                    .show_inside(ui, |ui| {
                        Self::controls_main(&mut self.main_view, ui);
                    });
            });
        if
            let (Some(proc_view), Some(redir))
                = (&mut self.proc_view, &mut self.proc_redirect)
        {
            SidePanel::left("proc")
                .exact_width(DRAWING_AREA_WIDTH as f32)
                .frame(zero_margin_frame())
                .show_separator_line(false)
                .resizable(false)
                .show(ctx, |ui| {
                    TopBottomPanel::top("proc_view")
                        .exact_height(DRAWING_AREA_HEIGHT as f32)
                        .frame(zero_margin_frame())
                        .show_separator_line(false)
                        .resizable(false)
                        .show_inside(ui, |ui| {
                            Self::pull_proc(
                                &mut self.abort_ready,
                                &self.source,
                                proc_view,
                                redir,
                                frame,
                            );
                            let backend = EguiBackend::new(ui);
                            proc_view.draw(backend);
                        });
                    TopBottomPanel::bottom("proc_controls")
                        .exact_height(CONTROL_AREA_HEIGHT as f32)
                        .show_separator_line(false)
                        .resizable(false)
                        .show_inside(ui, |ui| {
                            Self::controls_proc(
                                self.lock_views,
                                &mut self.main_view,
                                proc_view,
                                ui,
                            );
                        });
                });
        }
    }
}

