//! Machinery to drive the command-line interface.

use std::{
    borrow::Cow,
    boxed::Box,
    path::Path,
    sync::{ Arc, Mutex },
};
use crossterm::event::{
    Event as CTEvent,
    KeyEvent,
    KeyEventKind,
    KeyCode,
    KeyModifiers as KeyMod,
};
use reedline::{
    self as reed,
    ReedlineEvent as RLEvent,
};
use thiserror::Error;
use timetagger::prelude::{
    TimeTaggerUltra,
    TaggerError,
};
use crate::{
    actions,
    camera,
    config,
};

#[derive(Error, Debug)]
pub enum CliError {
    #[error("action error:\n\t{0}")]
    ActionError(#[from] actions::ActionError),

    #[error("camera error:\n\t{0}")]
    CameraError(#[from] camera::CameraError),

    #[error("timetagger error:\n\t{0}")]
    TaggerError(#[from] TaggerError),

    #[error("config error:\n\t{0}")]
    ConfigError(#[from] config::ConfigError),
}
pub type CliResult<T> = Result<T, CliError>;

#[derive(Clone)]
struct Prompt;

impl reed::Prompt for Prompt {
    fn render_prompt_left(&self) -> Cow<str> { Cow::from("") }

    fn render_prompt_right(&self) -> Cow<str> { Cow::from("") }

    fn render_prompt_indicator(&self, prompt_mode: reed::PromptEditMode)
        -> Cow<str>
    {
        return match prompt_mode {
            reed::PromptEditMode::Default => Cow::from(">>> "),
            reed::PromptEditMode::Emacs => Cow::from("e>> "),
            reed::PromptEditMode::Vi(vimode) => match vimode {
                reed::PromptViMode::Normal => Cow::from("v|> "),
                reed::PromptViMode::Insert => Cow::from("v>> "),
            },
            reed::PromptEditMode::Custom(_) => Cow::from("c>> "),
        };
    }

    fn render_prompt_multiline_indicator(&self) -> Cow<str> {
        return Cow::from("... ");
    }

    fn render_prompt_history_search_indicator(
        &self,
        _history_search: reed::PromptHistorySearch,
    ) -> Cow<str>
    {
        return Cow::from("history: ");
    }
}

struct EditMode { }

impl EditMode {
    pub fn new() -> Self { Self { } }
}

impl reed::EditMode for EditMode {
    fn parse_event(&mut self, event: reed::ReedlineRawEvent) -> RLEvent {
        return match event.into() {
            CTEvent::Key(
                KeyEvent { code, modifiers, kind, state: _ }
            ) => match (code, modifiers, kind) {
                (KeyCode::Char(c), KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::InsertChar(c.to_ascii_lowercase())
                    ])
                },
                (KeyCode::Char(c), KeyMod::SHIFT, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::InsertChar(c.to_ascii_uppercase())
                    ])
                },
                (KeyCode::Char('c'), KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::CtrlC
                },
                (KeyCode::Char('d'), KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::CtrlD
                },
                (KeyCode::Char('a'), KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::MoveToStart
                    ])
                },
                (KeyCode::Char('e'), KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::MoveToEnd
                    ])
                },
                (KeyCode::Char('r'), KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::SearchHistory
                },
                (KeyCode::Char('l'), KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::ClearScreen
                },
                (KeyCode::Char('l'), keymod, KeyEventKind::Press) => {
                    if keymod == KeyMod::CONTROL | KeyMod::SHIFT {
                        RLEvent::ClearScrollback
                    } else {
                        RLEvent::None
                    }
                },
                (KeyCode::Left, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Left
                },
                (KeyCode::Right, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Right
                },
                (KeyCode::Char('b'), keymod, KeyEventKind::Press) => {
                    if keymod == KeyMod::ALT | KeyMod::SHIFT {
                        RLEvent::Edit(vec![
                            reed::EditCommand::MoveBigWordLeft
                        ])
                    } else if keymod == KeyMod::ALT {
                        RLEvent::Edit(vec![
                            reed::EditCommand::MoveWordLeft
                        ])
                    } else {
                        RLEvent::None
                    }
                },
                (KeyCode::Char('w'), keymod, KeyEventKind::Press) => {
                    if keymod == KeyMod::ALT | KeyMod::SHIFT {
                        RLEvent::Edit(vec![
                            reed::EditCommand::MoveBigWordRightStart
                        ])
                    } else if keymod == KeyMod::ALT {
                        RLEvent::Edit(vec![
                            reed::EditCommand::MoveWordRightStart
                        ])
                    } else {
                        RLEvent::None
                    }
                },
                (KeyCode::Char('e'), keymod, KeyEventKind::Press) => {
                    if keymod == KeyMod::ALT | KeyMod::SHIFT {
                        RLEvent::Edit(vec![
                            reed::EditCommand::MoveBigWordRightEnd
                        ])
                    } else if keymod == KeyMod::ALT {
                        RLEvent::Edit(vec![
                            reed::EditCommand::MoveWordRightEnd
                        ])
                    } else {
                        RLEvent::None
                    }
                },
                (KeyCode::Up, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Up
                },
                (KeyCode::Down, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Down
                },
                (KeyCode::Enter, KeyMod::SHIFT, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::InsertChar('\n')
                    ])
                },
                (KeyCode::Enter, KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::Submit
                },
                (KeyCode::Enter, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Enter
                },
                (KeyCode::Backspace, KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::BackspaceWord
                    ])
                },
                (KeyCode::Backspace, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::Backspace
                    ])
                },
                (KeyCode::Delete, KeyMod::CONTROL, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::DeleteWord
                    ])
                },
                (KeyCode::Delete, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::Delete
                    ])
                },
                (KeyCode::Home, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::MoveToLineStart
                    ])
                },
                (KeyCode::End, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::MoveToLineEnd
                    ])
                },
                (KeyCode::Esc, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Esc
                },
                (KeyCode::Tab, KeyMod::NONE, KeyEventKind::Press) => {
                    RLEvent::Edit(vec![
                        reed::EditCommand::InsertChar('\t'),
                    ])
                },
                _ => RLEvent::None,
            },
            // Event::Mouse(
            //     MouseEvent { kind, column, row, modifiers }
            // ) => match (kind, column, row, modifiers) {
            //     _ => RLEvent::None,
            // },
            CTEvent::Mouse(_) => RLEvent::None,
            CTEvent::Resize(n, m) => RLEvent::Resize(n, m),
            _ => RLEvent::None,
        };
    }

    fn edit_mode(&self) -> reed::PromptEditMode {
        return reed::PromptEditMode::Custom("EM".into());
    }
}

macro_rules! print_err {
    ($err:ident) => {
        println!("Error: {}", $err);
    }
}

macro_rules! reload_config {
    ($cam_arc:ident, $config_path:ident, $config:ident) => {
        $config
            = match config::load_def_config($config_path.as_ref()) {
                Ok(c) => c,
                Err(e) => {
                    print_err!(e);
                    continue;
                },
            };
        match Arc::get_mut(&mut $cam_arc) {
            Some(c) => {
                match c.set_config(&$config) {
                    Ok(_) => { },
                    Err(e) => {
                        print_err!(e);
                        continue;
                    },
                }
            },
            None => {
                println!(
                    "Error: tried to mutate camera instance with existing \
                    references outside the main thread. This shouldn't happen!"
                );
                continue;
            },
        }
    }
}

#[derive(Clone, Debug)]
struct CommandCompleter {
    commands: Vec<String>,
}

impl reed::Completer for CommandCompleter {
    fn complete(&mut self, line: &str, pos: usize) -> Vec<reed::Suggestion> {
        let k: usize = pos.min(line.len());
        return self.commands.iter()
            .filter_map(|cmd| {
                cmd.contains(line)
                    .then(|| {
                        reed::Suggestion {
                            value: cmd.to_string(),
                            description: None,
                            extra: None,
                            span: reed::Span {
                                start: 0,
                                end: k,
                            },
                            append_whitespace: false,
                        }
                    })
            })
            .collect();
    }
}

/// Run the command-line interface.
pub fn run_cli<P>(
    cam: camera::IXon888,
    config_path: P,
    tt: Option<TimeTaggerUltra>,
) -> CliResult<()>
where P: AsRef<Path>
{
    let commands: Vec<String>
        = actions::ACTION_REGISTRY.keys()
        .chain([&"reload-config", &"help", &"quit", &"exit"])
        .map(|s| s.to_string())
        .collect();
    let completer = Box::new(CommandCompleter { commands });
    // let completer
    //     = Box::new(reed::DefaultCompleter::new_with_wordlen(commands, 2));
    let completion_menu = Box::new(
        reed::ColumnarMenu::default().with_name("Available commands"));
    let mut keybindings = reed::default_vi_insert_keybindings();
    keybindings.add_binding(
        reed::KeyModifiers::NONE,
        reed::KeyCode::Tab,
        RLEvent::UntilFound(vec![
            RLEvent::Menu("Available commands".to_string()),
            RLEvent::MenuNext,
        ]),
    );
    let edit_mode = Box::new(
        reed::Vi::new(keybindings, reed::default_vi_normal_keybindings()));
    let mut line_editor
        = reed::Reedline::create()
        .with_completer(completer)
        .with_menu(reed::ReedlineMenu::EngineCompleter(completion_menu))
        .with_edit_mode(edit_mode);
    let prompt = Prompt;
    let mut config: config::Config;
    let mut cam_arc = Arc::new(cam);
    let mut tt_arc: Option<Arc<Mutex<TimeTaggerUltra>>>
        = tt.map(|tagger| Arc::new(Mutex::new(tagger)));
    println!("Andor iXon 888 EMCCD interactive console");
    loop {
        match line_editor.read_line(&prompt) {
            Ok(reed::Signal::Success(s)) => {
                let (cmd, _comment): (String, String)
                    = match s.split_once('#') {
                        Some((e, c)) => (e.to_string(), c.to_string()),
                        None => (s, "".to_string()),
                    };
                match cmd.as_ref() {
                    "help" => {
                        println!(
"Run commands by typing them and pressing <ENTER>.\n\
Config from file '{}' is loaded before each command is executed.\n\
Available commands:",
                            config_path.as_ref().display()
                        );
                        let mut commands: Vec<&&str>
                            = actions::ACTION_REGISTRY.keys().collect();
                        commands.sort();
                        commands.into_iter()
                            .for_each(|cmd| println!("  {}", cmd));
                        println!("  reload-config\n  help\n  quit\n  exit");
                    },

                    "quit" | "exit" => { break; }

                    "reload-config" => {
                        reload_config!(cam_arc, config_path, config);
                    },

                    x => {
                        if let Some((act, args))
                            = x.split(' ')
                            .filter_map(
                                |s| (!s.is_empty()).then(|| s.to_string())
                            )
                            .collect::<Vec<String>>()
                            .split_first()
                        {
                            if let Some(action_f)
                                = actions::ACTION_REGISTRY.get(act)
                            {
                                reload_config!(cam_arc, config_path, config);
                                if
                                    config.a("timetagger.use")?
                                        && tt_arc.is_none()
                                {
                                    println!("Connect to timetagger");
                                    let tt_serial: String
                                        = config.a("timetagger.serial")?;
                                    tt_arc = Some(Arc::new(Mutex::new(
                                        TimeTaggerUltra::connect(&tt_serial)?
                                    )));
                                }

                                let action_res
                                    = action_f(
                                        cam_arc.clone(),
                                        &config,
                                        config.a::<bool>("timetagger.use")?
                                            .then_some(())
                                            .and_then(|_| {
                                                tt_arc.as_ref().cloned()
                                            }),
                                        args,
                                    );
                                match action_res {
                                    Ok(_) => { continue; },
                                    Err(e) => {
                                        print_err!(e);
                                        continue;
                                    },
                                }
                            } else {
                                println!("unknown command '{}'", cmd);
                                continue;
                            }
                        } else {
                            continue;
                        }
                    },

                }
            },
            Ok(reed::Signal::CtrlD) => { break; }
            Ok(reed::Signal::CtrlC) => { continue; }
            x => { println!("Error: {:?}", x); },
        }
    }
    return Ok(());
}

