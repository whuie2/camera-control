//! Provides constructs to specify various kinds of colormaps that can be used
//! for [frame visualization][`crate::viewer`].
//!
//! The main object of interest is [`ColorMap`]. All color maps should implement
//! [`ColorScale`], which describes a transformation from the `[0.0, 1.0]` range
//! to a corresponding range of [`RGBAColor`]s used for plotting.

use std::{
    cmp::Ordering,
};
use ndarray as nd;
use plotters::style::{
    Color,
    RGBAColor,
    HSLColor,
};
use thiserror::Error;
use crate::{
    viewer::FrameData,
};

#[derive(Debug, Error)]
pub enum ColorError {
    #[error("expected color points to be between 0.0 and 1.0 but got {0}")]
    InvalidColorPoint(String),
    #[error("expected at least one color point")]
    EmptyColorPointList,
}
pub type ColorResult<T> = Result<T, ColorError>;

/// Main sum type over different kinds of colormaps.
#[derive(Clone, Debug)]
pub enum ColorMap {
    /// Straight linear grayscale.
    GrayScale(GrayScale),

    /// Linear grayscale, with highlighting for points that fall outside the
    /// `[0.0, 1.0]` range.
    GrayScaleClip(GrayScaleClip),

    /// Linear scale from white to a dark-ish blue-gray that we use in our
    /// papers.
    PaperScale(PaperScale),

    /// Linear scale from black through blue to white.
    Plasma(Plasma),

    /// Linear scale from black through pink, orange, and yellow, to white.
    Vibrant(Vibrant),

    /// Linear scale from Brown through green to white.
    Artsy(Artsy),

    /// Linear scale from blue through gold to white.
    Pix(Pix),

    /// Specify a custom colormap as a list of colors at different points with
    /// linear interpolation in between.
    LinearSegmentedColorMap(LinearSegmentedColorMap),

    /// Specify a custom colormap using a function.
    FuncColorMap(FuncColorMap),
}

impl ColorMap {
    /// Create a new [GrayScale] colormap.
    pub fn new_grayscale() -> Self {
        return Self::GrayScale(GrayScale);
    }

    /// Create a new [GrayScaleClip] colormap with highlighting for clipping points.
    pub fn new_grayscale_clip() -> Self {
        return Self::GrayScaleClip(GrayScaleClip);
    }

    /// Create a new [PaperScale] colormap.
    pub fn new_paperscale() -> Self {
        return Self::PaperScale(PaperScale::new());
    }

    /// Create a new [Plasma] colormap.
    pub fn new_plasma() -> Self {
        return Self::Plasma(Plasma::new());
    }

    /// Create a new [Vibrant] colormap.
    pub fn new_vibrant() -> Self {
        return Self::Vibrant(Vibrant::new());
    }

    /// Create a new [Artsy] colormap.
    pub fn new_artsy() -> Self {
        return Self::Artsy(Artsy::new());
    }

    /// Create a new [Pix] colormap.
    pub fn new_pix() -> Self {
        return Self::Pix(Pix::new());
    }

    /// Create a new linear segmented colormap.
    pub fn new_linear_segmented_colormap(colormap: LinearSegmentedColorMap)
        -> Self
    {
        return Self::LinearSegmentedColorMap(colormap);
    }

    /// Create a new colormap function.
    pub fn new_func_colormap(func: FuncColorMap) -> Self {
        return Self::FuncColorMap(func);
    }

    /// Map a number in the `[0.0, 1.0]` range to a color.
    pub fn c<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>
    {
        return match self {
            Self::GrayScale(g) => g.c(x),
            Self::GrayScaleClip(g) => g.c(x),
            Self::PaperScale(p) => p.c(x),
            Self::Plasma(p) => p.c(x),
            Self::Vibrant(v) => v.c(x),
            Self::Artsy(a) => a.c(x),
            Self::Pix(p) => p.c(x),
            Self::FuncColorMap(f) => f.c(x),
            Self::LinearSegmentedColorMap(l) => l.c(x),
        };
    }
}

/// Represents a mapping from the `[0.0, 1.0]` range to a color for plotting.
pub trait ColorScale {
    /// Convert a number in the `[0.0, 1.0]` range to a color.
    fn cmap<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>;

    /// Alias for [`Self::cmap`].
    fn c<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>
    {
        return self.cmap(x);
    }
}

/// Straight grayscale.
#[derive(Copy, Clone, Debug)]
pub struct GrayScale;

impl Default for GrayScale {
    fn default() -> Self { Self::new() }
}

impl GrayScale {
    pub fn new() -> Self { Self }
}

impl ColorScale for GrayScale {
    fn cmap<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>
    {
        return HSLColor(
            0.0,
            0.0,
            x.into().min(1.0).max(0.0).powi(2)
        ).to_rgba();
    }
}

/// Grayscale with clipping detection: `x > 1.0` is colored red and `x < 0.0` is
/// colored blue.
#[derive(Copy, Clone, Debug)]
pub struct GrayScaleClip;

impl Default for GrayScaleClip {
    fn default() -> Self { Self }
}

impl GrayScaleClip {
    pub fn new() -> Self { Self }
}

impl ColorScale for GrayScaleClip {
    fn cmap<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>
    {
        let y = x.into();
        return if y < 0.0 {
            RGBAColor(12, 64, 185, 1.0)
        } else if y > 1.0 {
            RGBAColor(200, 18, 18, 1.0)
        } else {
            HSLColor(
                0.0,
                0.0,
                y.min(1.0).max(0.0).powi(2)
            ).to_rgba()
        };
    }
}

macro_rules! make_linsegcolormap {
    (
        $doc:literal,
        $name:ident,
        colors: {
            $( $p:literal => ( $r:literal, $g:literal, $b:literal ) ),+ $(,)?
        }
    ) => {
        #[doc = $doc]
        #[derive(Clone, Debug)]
        pub struct $name {
            linseg: LinearSegmentedColorMap,
        }

        impl Default for $name {
            fn default() -> Self { Self::new() }
        }

        impl $name {
            pub fn new() -> Self {
                return Self {
                    linseg: [$(
                        ($p, RGBAColor($r, $g, $b, 1.0))
                    ),+]
                    .into_iter()
                    .collect()
                };
            }
        }

        impl ColorScale for $name {
            fn cmap<T>(&self, x: T) -> RGBAColor
            where T: Into<f64>
            {
                return self.linseg.cmap(x);
            }
        }
    }
}

make_linsegcolormap!(
    "Atom color scale used in our papers.",
    PaperScale,
    colors: {
        0.000 => (255, 255, 255),
        1.000 => ( 50,  80, 129),
    }
);

make_linsegcolormap!(
    "Linear scale black -> blue -> white",
    Plasma,
    colors: {
        0.000 => (  0,   0,   0),
        0.450 => ( 59,  69, 104),
        0.600 => ( 88,  97, 134),
        0.700 => (147, 156, 196),
        1.000 => (248, 248, 248),
    }
);

make_linsegcolormap!(
    "Linear scale black -> blue -> pink -> yellow -> white",
    Vibrant,
    colors: {
        0.000 => ( 16,  16,  16),
        0.050 => (  1,  45,  94),
        0.125 => (  0,  57, 167),
        0.250 => ( 22,  71, 207),
        0.375 => (102,  70, 255),
        0.500 => (188,  39, 255),
        0.600 => (220,  71, 175),
        0.800 => (245, 117,  72),
        0.900 => (241, 158,   0),
        0.950 => (251, 184,   0),
        1.000 => (254, 200,   0),
    }
);

make_linsegcolormap!(
    "Linear scale brown -> green -> white",
    Artsy,
    colors: {
        0.000 => ( 31,   1,   9),
        0.034 => ( 31,   1,  16),
        0.069 => ( 35,   2,  17),
        0.103 => ( 37,   8,  22),
        0.138 => ( 39,  11,  27),
        0.172 => ( 37,  15,  29),
        0.207 => ( 37,  21,  33),
        0.241 => ( 37,  26,  37),
        0.276 => ( 44,  27,  40),
        0.310 => ( 39,  29,  43),
        0.345 => ( 36,  32,  45),
        0.379 => ( 35,  38,  50),
        0.414 => ( 33,  45,  50),
        0.448 => ( 30,  52,  60),
        0.483 => ( 23,  62,  68),
        0.517 => ( 23,  70,  74),
        0.552 => ( 16,  74,  73),
        0.586 => ( 14,  85,  83),
        0.621 => (  0,  99,  95),
        0.655 => (  0, 112, 101),
        0.690 => (  0, 122, 109),
        0.724 => ( 14, 132, 118),
        0.759 => ( 28, 140, 125),
        0.793 => ( 33, 149, 129),
        0.828 => ( 47, 159, 138),
        0.862 => ( 73, 168, 144),
        0.897 => ( 96, 184, 157),
        0.931 => (126, 200, 169),
        0.966 => (154, 214, 180),
        1.000 => (188, 230, 191),
    }
);

make_linsegcolormap!(
    "Linear scale blue -> gold -> white",
    Pix,
    colors: {
        0.000 => ( 13,  43,  69),
        0.143 => ( 22,  51,  77),
        0.286 => ( 84,  78, 104),
        0.429 => (141, 105, 122),
        0.571 => (208, 129,  89),
        0.714 => (255, 170,  94),
        0.857 => (255, 212, 163),
        1.000 => (255, 236, 214),
    }
);

/// Color scale based on a provided function.
#[derive(Copy, Clone, Debug)]
pub struct FuncColorMap {
    f: fn(f64) -> RGBAColor
}

impl FuncColorMap {
    pub fn new(f: fn(f64) -> RGBAColor) -> Self { Self { f } }
}

impl ColorScale for FuncColorMap {
    fn cmap<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>
    {
        return (self.f)(x.into());
    }
}

/// Color scale based on linear interpolation between a given set of points in
/// the `[0.0, 1.0]` range.
///
/// Interpolation is performed element-wise on all four R, G, B, and A channels.
#[derive(Clone, Debug)]
pub struct LinearSegmentedColorMap {
    points: Vec<f64>,
    colors: Vec<RGBAColor>,
}

impl FromIterator<(f64, RGBAColor)> for LinearSegmentedColorMap {
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = (f64, RGBAColor)>
    {
        let (points, colors): (Vec<f64>, Vec<RGBAColor>)
            = iter.into_iter().unzip();
        return Self { points, colors };
    }
}

impl LinearSegmentedColorMap {
    pub fn new(mut color_points: Vec<(f64, RGBAColor)>) -> ColorResult<Self>
    {
        if color_points.is_empty() {
            return Err(ColorError::EmptyColorPointList);
        }
        color_points.iter()
            .map(|(x, _)| {
                (0.0..=1.0).contains(x)
                    .then_some(())
                    .ok_or_else(|| {
                        ColorError::InvalidColorPoint(x.to_string())
                    })
            })
            .collect::<ColorResult<Vec<()>>>()?;
        color_points.sort_by(|(a, _), (b, _)| a.partial_cmp(b).unwrap());
        let (points, colors): (Vec<f64>, Vec<RGBAColor>)
            = color_points.into_iter().unzip();
        return Ok(Self { points, colors });
    }
}

impl ColorScale for LinearSegmentedColorMap {
    fn cmap<T>(&self, x: T) -> RGBAColor
    where T: Into<f64>
    {
        let y = x.into();
        return if y <= *self.points.first().unwrap() {
            *self.colors.first().unwrap()
        } else if y >= *self.points.last().unwrap() {
            *self.colors.last().unwrap()
        } else {
            let ((ya, ca), (yb, cb)): ((f64, RGBAColor), (f64, RGBAColor))
                = {
                    let mut lower: (f64, &RGBAColor)
                        = (
                            *self.points.first().unwrap(),
                            self.colors.first().unwrap(),
                        );
                    let mut upper: (f64, &RGBAColor)
                        = (
                            *self.points.last().unwrap(),
                            self.colors.last().unwrap(),
                        );
                    for (yk, ck)
                        in self.points.iter().zip(self.colors.iter())
                    {
                        let yk_: f64 = *yk;
                        if y < yk_ {
                            upper = (yk_, ck);
                            break;
                        } else {
                            lower = (yk_, ck);
                        }
                    }
                    ((lower.0, *lower.1), (upper.0, *upper.1))
                };
            let Dy: f64 = yb - ya;
            let dy: f64 = y - ya;
            let ca: (f64, f64, f64, f64)
                = (
                    ca.0 as f64,
                    ca.1 as f64,
                    ca.2 as f64,
                    ca.3,
                );
            let cb: (f64, f64, f64, f64)
                = (
                    cb.0 as f64,
                    cb.1 as f64,
                    cb.2 as f64,
                    cb.3,
                );
            RGBAColor(
                ((cb.0 - ca.0) / Dy * dy + ca.0).round() as u8,
                ((cb.1 - ca.1) / Dy * dy + ca.1).round() as u8,
                ((cb.2 - ca.2) / Dy * dy + ca.2).round() as u8,
                (cb.3 - ca.3) / Dy * dy + ca.3,
            )
        };
    }
}

/// Single plotting bound for color scales.
#[derive(Copy, Clone, Debug)]
pub enum ColorMapBound {
    Auto,
    Fixed(f64),
}

impl ColorMapBound {
    /// Get the value of the bound if not `Auto`.
    pub fn get(&self) -> Option<f64> {
        return match self {
            Self::Auto => None,
            Self::Fixed(b) => Some(*b),
        };
    }

    /// Get the value of the bound, defaulting to `def` if `Auto`.
    pub fn get_or(&self, def: f64) -> f64 {
        return match self {
            Self::Auto => def,
            Self::Fixed(b) => *b,
        };
    }

    /// Get the value of the bound, defaulting to the output of `def` if `Auto`.
    pub fn get_or_else<F>(&self, def: F) -> f64
    where F: FnOnce() -> f64
    {
        return match self {
            Self::Auto => def(),
            Self::Fixed(b) => *b,
        };
    }

    /// Return the value of the bound if `self` is `Fixed`, otherwise find the
    /// minimum of an array.
    ///
    /// *Panics if `self` is `Auto` and a minimum does not exist.*
    pub fn bound_min<T, D>(&self, data: &nd::Array<T, D>) -> f64
    where
        T: FrameData,
        D: nd::Dimension,
    {
        return match self {
            Self::Auto => {
                data.iter()
                    .min_by(|l, r| {
                        l.partial_cmp(r)
                            .unwrap_or(Ordering::Less)
                    })
                    .map(|v| (*v).into())
                    .expect("empty or non-finite array")
            },
            Self::Fixed(b) => *b,
        };
    }

    /// Return the value of the bound if `self` is `Fixed`, otherwise find the
    /// maximum of an array.
    ///
    /// *Panics if `self` is `Auto` and a maximum does not exist.*
    pub fn bound_max<T, D>(&self, data: &nd::Array<T, D>) -> f64
    where
        T: FrameData,
        D: nd::Dimension,
    {
        return match self {
            Self::Auto => {
                data.iter()
                    .max_by(|l, r| {
                        l.partial_cmp(r)
                            .unwrap_or(Ordering::Greater)
                    })
                    .map(|v| (*v).into())
                    .expect("empty or non-finite array")
            },
            Self::Fixed(b) => *b,
        };
    }
}

macro_rules! impl_colormapbound_from {
    ( $t:ty ) => {
        impl From<$t> for ColorMapBound {
            fn from(bound: $t) -> Self { Self::Fixed(f64::from(bound)) }
        }
    }
}
impl_colormapbound_from!(u8);
impl_colormapbound_from!(u16);
impl_colormapbound_from!(u32);
impl_colormapbound_from!(i8);
impl_colormapbound_from!(i16);
impl_colormapbound_from!(i32);
impl_colormapbound_from!(f32);
impl_colormapbound_from!(f64);

/// Plotting bounds for color scales.
pub type ColorMapBounds = (ColorMapBound, ColorMapBound);

/// Define a full color bar.
pub type ColorBar = (ColorMap, ColorMapBounds);

