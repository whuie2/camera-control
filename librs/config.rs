//! Provides definitions for loading and verifying values from TOML-formatted
//! config files.

use std::{
    collections::{
        HashMap,
        HashSet,
    },
    fmt,
    fs,
    iter::Peekable,
    ops::Deref,
    path::Path,
};
use once_cell::sync::Lazy;
use toml::{
    Value,
    Table,
};
use serde::Deserialize;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error(
        "config: invalid type for key '{0}':\n\
        \texpected type <{1}> but got value '{2}'"
    )]
    InvalidType(String, String, String),

    #[error(
        "config: invalid value for key '{0}':\n\
        \texpected value to satisfy '{1}' but got '{2}'"
    )]
    InvalidValue(String, String, String),

    #[error("config: encountered incompatible structure")]
    IncompatibleStructure,

    #[error("config: missing key '{0}'")]
    MissingKey(String),

    #[error("config: key path too long at key '{0}'")]
    KeyPathTooLong(String),

    #[error("config: failed to convert type of value {0}")]
    FailedTypeConversion(String),

    #[error("config: couldn't read file {0}")]
    FileRead(String),

    #[error("config: couldn't parse file {0}")]
    FileParse(String),
}
pub type ConfigResult<T> = Result<T, ConfigError>;

/// The default config specification, created from a cached, lazily evaluated
/// call to [`ConfigSpec::default`].
pub static DEF_CONFIG_SPEC: Lazy<ConfigSpec> = Lazy::new(ConfigSpec::default);

/// Config value type specification via the TOML format.
///
/// See [`toml::Value`]
#[derive(Clone, Debug)]
pub enum TypeVer {
    /// [`toml::Value::Boolean`]
    Bool,
    /// [`toml::Value::Integer`]
    Int,
    /// [`toml::Value::Float`]
    Float,
    /// [`toml::Value::Datetime`]
    Datetime,
    /// [`toml::Value::Array`]
    Array,
    /// [`toml::Value::Array`] holding only specified types, optionally in
    /// specific positions.
    TypedArray {
        types: Vec<TypeVer>,
        finite: bool,
    },
    /// [`toml::Value::String`]
    Str,
}

impl TypeVer {
    /// Return `true` if `value` matches the type specification, `false`
    /// otherwise.
    pub fn verify(&self, value: &Value) -> bool {
        return match (self, value) {
            (Self::Bool, Value::Boolean(_)) => true,
            (Self::Int, Value::Integer(_)) => true,
            (Self::Float, Value::Float(_)) => true,
            (Self::Datetime, Value::Datetime(_)) => true,
            (Self::Array, Value::Array(_)) => true,
            (Self::TypedArray { types, finite }, Value::Array(a)) => {
                if *finite {
                    types.len() == a.len()
                        && types.iter().zip(a.iter())
                            .all(|(tyk, ak)| tyk.verify(ak))
                } else {
                    a.iter()
                        .all(|ak| types.iter().any(|tyk| tyk.verify(ak)))
                }
            },
            (Self::Str, Value::String(_)) => true,
            _ => false,
        };
    }

    /// Return `Ok(value)` if `value` matches the type specification, `Err(err)`
    /// otherwise.
    pub fn verify_ok_or(&self, value: Value, err: ConfigError)
        -> ConfigResult<Value>
    {
        return self.verify(&value)
            .then_some(value)
            .ok_or(err);
    }
}

impl fmt::Display for TypeVer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return match self {
            Self::Bool => write!(f, "bool"),
            Self::Int => write!(f, "int"),
            Self::Float => write!(f, "float"),
            Self::Datetime => write!(f, "datetime"),
            Self::Array => write!(f, "array"),
            Self::TypedArray { types, finite } => {
                let n: usize = types.len();
                write!(f, "TypedArray([")?;
                for (k, tyk) in types.iter().enumerate() {
                    tyk.fmt(f)?;
                    if k < n - 1 { write!(f, ", ")?; }
                }
                write!(f, ", finite={})", finite)
            },
            Self::Str => write!(f, "str"),
        };
    }
}

/// Config specification to bound possible values found in a config file.
#[derive(Clone, Debug)]
pub enum ValueVer {
    IntRange {
        min: i64,
        max: i64,
        incl_start: bool,
        incl_end: bool,
    },
    FloatRange {
        min: f64,
        max: f64,
        incl_start: bool,
        incl_end: bool,
    },
    StrCollection(HashSet<String>),
}

impl ValueVer {
    /// Return `true` if `value` matches the specification, `false` otherwise.
    pub fn verify(&self, value: &Value) -> bool {
        return match (self, value) {
            (
                Self::IntRange { min, max, incl_start, incl_end },
                Value::Integer(i),
            ) => {
                let in_start: bool
                    = if *incl_start { *i >= *min } else { *i > *min };
                let in_end: bool
                    = if *incl_end { *i <= *max } else { *i < *max };
                in_start && in_end
            },
            (
                Self::FloatRange { min, max, incl_start, incl_end },
                Value::Float(f),
            ) => {
                let in_start: bool
                    = if *incl_start { *f >= *min } else { *f > *min };
                let in_end: bool
                    = if *incl_end { *f <= *max } else { *f < *max };
                in_start && in_end
            },
            (Self::StrCollection(strs), Value::String(s))
                => { strs.contains(s) },
            _ => false,
        };
    }

    /// Return `Ok(value)` if `value` matches the specification, `Err(err)`
    /// otherwise.
    pub fn verify_ok_or(&self, value: Value, err: ConfigError)
        -> ConfigResult<Value>
    {
        return self.verify(&value)
            .then_some(value)
            .ok_or(err);
    }
}

impl fmt::Display for ValueVer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return match self {
            Self::IntRange { min, max, incl_start, incl_end } => {
                write!(f, "IntRange{}", if *incl_start { "[" } else { "(" })?;
                min.fmt(f)?;
                write!(f, ", ")?;
                max.fmt(f)?;
                write!(f, "{}", if *incl_end { "]" } else { ")" })
            },
            Self::FloatRange { min, max, incl_start, incl_end } => {
                write!(f, "FloatRange{}", if *incl_start { "[" } else { "(" })?;
                min.fmt(f)?;
                write!(f, ", ")?;
                max.fmt(f)?;
                write!(f, "{}", if *incl_end { "]" } else { ")" })
            },
            Self::StrCollection(strs) => {
                let n: usize = strs.len();
                write!(f, "StrCollection{{")?;
                for (k, sk) in strs.iter().enumerate() {
                    sk.fmt(f)?;
                    if k < n - 1 { write!(f, ", ")?; }
                }
                write!(f, "}}")
            },
        };
    }
}

/// Holds either a type or value specification.
#[derive(Clone, Debug)]
pub enum Verifier {
    TypeVer(TypeVer),
    ValueVer(ValueVer),
}

impl fmt::Display for Verifier {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return match self {
            Self::TypeVer(type_ver) => type_ver.fmt(f),
            Self::ValueVer(value_ver) => value_ver.fmt(f),
        };
    }
}

/// An item in a config specification, either a type/value specification for
/// single values or a set of specifications for a sub-table.
#[derive(Clone, Debug)]
pub enum ConfigSpecItem {
    Value(Verifier),
    Table(ConfigSpec),
}

/// Shorthand for creating [`ConfigSpecItem`]s.
#[macro_export]
macro_rules! spec {
    ( Bool ) => { ConfigSpecItem::Value(Verifier::TypeVer(TypeVer::Bool)) };
    ( Int ) => { ConfigSpecItem::Value(Verifier::TypeVer(TypeVer::Int)) };
    ( Float ) => { ConfigSpecItem::Value(Verifier::TypeVer(TypeVer::Float)) };
    ( Datetime ) => { ConfigSpecItem::Value(Verifier::TypeVer(TypeVer::Datetime)) };
    ( Array ) => { ConfigSpecItem::Value(Verifier::TypeVer(TypeVer::Array)) };
    ( TypedArray { [ $( $t:expr ),* $(,)? ], finite: $f:expr } ) => {
        ConfigSpecItem::Value(
            Verifier::TypeVer(
                TypeVer::TypedArray { types: vec![$( $t ),*], finite: $f }
            )
        )
    };
    ( Str ) => { ConfigSpecItem::Value(Verifier::TypeVer(TypeVer::Str)) };
    ( IntRange { ($min:expr, $max:expr) } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::IntRange {
                    min: $min,
                    max: $max,
                    incl_start: false,
                    incl_end: false,
                }
            )
        )
    };
    ( IntRange { ($min:expr, $max:expr)= } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::IntRange {
                    min: $min,
                    max: $max,
                    incl_start: false,
                    incl_end: true,
                }
            )
        )
    };
    ( IntRange { =($min:expr, $max:expr) } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::IntRange {
                    min: $min,
                    max: $max,
                    incl_start: true,
                    incl_end: false,
                }
            )
        )
    };
    ( IntRange { =($min:expr, $max:expr)= } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::IntRange {
                    min: $min,
                    max: $max,
                    incl_start: true,
                    incl_end: true,
                }
            )
        )
    };
    ( FloatRange { ($min:expr, $max:expr) } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::FloatRange {
                    min: $min,
                    max: $max,
                    incl_start: false,
                    incl_end: false,
                }
            )
        )
    };
    ( FloatRange { ($min:expr, $max:expr)= } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::FloatRange {
                    min: $min,
                    max: $max,
                    incl_start: false,
                    incl_end: true,
                }
            )
        )
    };
    ( FloatRange { =($min:expr, $max:expr) } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::FloatRange {
                    min: $min,
                    max: $max,
                    incl_start: true,
                    incl_end: false,
                }
            )
        )
    };
    ( FloatRange { =($min:expr, $max:expr)= } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::FloatRange {
                    min: $min,
                    max: $max,
                    incl_start: true,
                    incl_end: true,
                }
            )
        )
    };
    ( Table { $( $key:literal => $val:expr ),* $(,)? } ) => {
        ConfigSpecItem::Table(
            ConfigSpec::from_iter([
                $(
                    ($key.to_string(), $val)
                ),*
            ])
        )
    };
    ( StrCollection { $( $s:expr ),* $(,)? } ) => {
        ConfigSpecItem::Value(
            Verifier::ValueVer(
                ValueVer::StrCollection(
                    HashSet::from_iter([
                        $( $s.to_string() ),*
                    ])
                )
            )
        )
    };
}

/// Sugared [`std::collections::HashMap`] representing a specification for the
/// values and structure of a TOML-formatted config file.
#[derive(Clone, Debug)]
pub struct ConfigSpec {
    spec: HashMap<String, ConfigSpecItem>
}

impl AsRef<HashMap<String, ConfigSpecItem>> for ConfigSpec {
    fn as_ref(&self) -> &HashMap<String, ConfigSpecItem> { &self.spec }
}

impl From<HashMap<String, ConfigSpecItem>> for ConfigSpec {
    fn from(spec: HashMap<String, ConfigSpecItem>) -> Self { Self { spec } }
}

impl FromIterator<(String, ConfigSpecItem)> for ConfigSpec {
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = (String, ConfigSpecItem)>
    {
        return Self { spec: iter.into_iter().collect() };
    }
}

/// Create a [`ConfigSpec`].
///
/// Expects `str` literals for keys and [`ConfigSpecItem`]s for values. See also
/// [`spec`].
#[macro_export]
macro_rules! config_spec {
    ( $( $key:literal => $val:expr ),* $(,)? ) => {
        ConfigSpec::from_iter([
            $(
                ($key.to_string(), $val)
            ),*
        ])
    }
}

impl Default for ConfigSpec {
    fn default() -> Self {
        return config_spec!(
            "output" => spec!(Table {
                "save" => spec!(Bool),
                "datadir" => spec!(Str),
                "dir" => spec!(Str),
                "name" => spec!(Str),
                "counter" => spec!(Int),
                "show_frames" => spec!(Table {
                    "mode" => spec!(StrCollection {
                        "off",
                        "first",
                        "last",
                        "all",
                    }),
                    "color_scale" => spec!(StrCollection {
                        "gray",
                        "grayclip",
                        "paperscale",
                        "plasma",
                        "vibrant",
                        "artsy",
                        "pix",
                    }),
                    "colorbar_limits" => spec!(TypedArray {
                        [TypeVer::Float, TypeVer::Float],
                        finite: true
                    }),
                }),
            }),
            "camera" => spec!(Table {
                "acquisition_mode" => spec!(StrCollection {
                    "single",
                    "accum",
                    "kinetic",
                    "fast_kinetic",
                    "cont",
                }),
                "roi" => spec!(TypedArray {
                    [TypeVer::Int, TypeVer::Int, TypeVer::Int, TypeVer::Int],
                    finite: true
                }),
                "bin" => spec!(TypedArray {
                    [TypeVer::Int, TypeVer::Int],
                    finite: true
                }),
                "exposure_time_ms" => spec!(FloatRange { =(1e-3, 1e3)= }),
                "cooler" => spec!(Table {
                    "on" => spec!(Bool),
                    "fan_mode" => spec!(StrCollection {
                        "full",
                        "low",
                        "off",
                    }),
                    "temperature_c" => spec!(FloatRange { =(-100.0, 30.0)= }),
                }),
                "amp" => spec!(Table {
                    "em_gain" => spec!(IntRange { =(2, 300)= }),
                    "preamp_gain_idx" => spec!(IntRange { =(0, 1)= }),
                    "oamp" => spec!(IntRange { =(0, 1)= }),
                }),
                "shutter" => spec!(Table {
                    "mode" => spec!(StrCollection {
                        "auto",
                        "open",
                        "closed",
                    }),
                    "ttl_mode" => spec!(IntRange { =(0, 1)= }),
                    "open_time_ms"
                        => spec!(FloatRange { =(27.0, f64::INFINITY) }),
                    "close_time_ms"
                        => spec!(FloatRange { =(27.0, f64::INFINITY) }),
                }),
                "trigger" => spec!(Table {
                    "mode" => spec!(StrCollection {
                        "int",
                        "ext",
                        "ext_start",
                        "ext_exp",
                        "software",
                        "ext_chargeshift",
                    }),
                }),
                "read" => spec!(Table {
                    "mode" => spec!(StrCollection {
                        "fvb",
                        "single_track",
                        "multi_track",
                        "random_track",
                        "image",
                    }),
                    "hsspeed_idx" => spec!(IntRange { =(0, 3)= }),
                    "vsspeed_idx" => spec!(IntRange { =(0, 3)= }),
                }),
                "kinetic" => spec!(Table {
                    "buffer_size" => spec!(IntRange { =(1, i64::MAX)= }),
                    "cycle_time"
                        => spec!(FloatRange { =(0.0, f64::INFINITY) }),
                    "num_acc" => spec!(IntRange { =(1, i64::MAX)= }),
                    "cycle_time_acc"
                        => spec!(FloatRange { =(0.0, f64::INFINITY) }),
                    "num_prescan" => spec!(IntRange { =(0, i64::MAX)= }),
                }),
            }),
            "processing" => spec!(Table {
                "show" => spec!(StrCollection {
                    "off",
                    "first",
                    "last",
                    "all",
                }),
                "color_scale" => spec!(StrCollection {
                    "gray",
                    "grayclip",
                    "paperscale",
                    "plasma",
                    "vibrant",
                    "artsy",
                    "pix",
                }),
                "colorbar_limits" => spec!(TypedArray {
                    [TypeVer::Float, TypeVer::Float],
                    finite: true
                }),
                "count_bias" => spec!(Float),
                "qe" => spec!(FloatRange { =(0.0, 1.0)= }),
                "window_avg" => spec!(Table {
                    "enabled" => spec!(Bool),
                    "image_dim" => spec!(TypedArray {
                        [TypeVer::Int, TypeVer::Int],
                        finite: true
                    }),
                    "window_size" => spec!(IntRange { =(1, i64::MAX)= }),
                    "rolling_avg" => spec!(Bool),
                }),
                "ff_prep" => spec!(Table {
                    "enabled" => spec!(Bool),
                    "image_dim" => spec!(TypedArray {
                        [TypeVer::Int, TypeVer::Int],
                        finite: true
                    }),
                    "n_loop" => spec!(IntRange { =(1, i64::MAX)= }),
                    "mode" => spec!(StrCollection { "bright", "dark" }),
                    "box" => spec!(TypedArray {
                        [
                            TypeVer::Int,
                            TypeVer::Int,
                            TypeVer::Int,
                            TypeVer::Int,
                        ],
                        finite: true
                    }),
                    "threshold" => spec!(FloatRange { =(0.0, f64::INFINITY) }),
                }),
                "ff_series" => spec!(Table {
                    "enabled" => spec!(Bool),
                    "image_dim" => spec!(TypedArray {
                        [TypeVer::Int, TypeVer::Int],
                        finite: true
                    }),
                    "n_loop" => spec!(IntRange { =(1, i64::MAX)= }),
                    "mode" => spec!(StrCollection {
                        "xor",
                        "xnor",
                        "alternate",
                    }),
                    "box" => spec!(TypedArray {
                        [
                            TypeVer::Int,
                            TypeVer::Int,
                            TypeVer::Int,
                            TypeVer::Int,
                        ],
                        finite: true
                    }),
                    "threshold" => spec!(FloatRange { =(0.0, f64::INFINITY) }),
                }),
            }),
            "timetagger" => spec!(Table {
                "use" => spec!(Bool),
                "serial" => spec!(Str),
                "counter_channel" => spec!(Int),
                "start_channel" => spec!(Int),
                "stop_channel" => spec!(Int),
                "binwidth_sec" => spec!(Float),
                "n_bins" => spec!(Int),
            }),
        );
    }
}

impl ConfigSpec {
    pub fn from_spec(spec: HashMap<String, ConfigSpecItem>) -> Self {
        return Self { spec };
    }

    fn verify_ok(&self, value: Value) -> ConfigResult<Value> {
        if let Value::Table(mut tab) = value {
            let mut data = Table::new();
            let mut val: Value;
            let mut valstr: String;
            for (key, spec_item) in self.spec.iter() {
                val = match (spec_item, tab.remove(key)) {
                    (ConfigSpecItem::Table(spec_table), Some(v)) => {
                        spec_table.verify_ok(v)
                    },
                    (ConfigSpecItem::Value(verifier), Some(v)) => {
                        match verifier {
                            Verifier::TypeVer(type_ver) => {
                                valstr = v.to_string();
                                type_ver.verify_ok_or(
                                    v,
                                    ConfigError::InvalidType(
                                        key.clone(),
                                        type_ver.to_string(),
                                        valstr,
                                    )
                                )
                            },
                            Verifier::ValueVer(value_ver) => {
                                valstr = v.to_string();
                                value_ver.verify_ok_or(
                                    v,
                                    ConfigError::InvalidValue(
                                        key.clone(),
                                        value_ver.to_string(),
                                        valstr,
                                    )
                                )
                            },
                        }
                    },
                    _ => Err(ConfigError::MissingKey(key.clone())),
                }?;
                data.insert(key.clone(), val);
            }
            return Ok(Value::Table(data));
        } else {
            return Err(ConfigError::IncompatibleStructure);
        }
    }

    /// Return `Ok` if `table` matches the specification, `Err` otherwise.
    ///
    /// The keys of `table` are filtered to contain only those in the
    /// specification.
    pub fn verify(&self, table: Table) -> ConfigResult<Config> {
        let data: Table
            = self.verify_ok(Value::Table(table))?
            .try_into()
            .unwrap();
        return Ok(Config { data });
    }
}

/// Sugared [`toml::Table`] holding configuration values.
#[derive(Clone, Debug)]
pub struct Config {
    data: Table
}

impl AsRef<Table> for Config {
    fn as_ref(&self) -> &Table { &self.data }
}

impl Config {
    /// Create a new [`Config`] with verification against [`spec`].
    pub fn new(data: Table, spec: &ConfigSpec) -> ConfigResult<Self> {
        return spec.verify(data);
    }

    fn table_get_path<'a, K>(table: &Table, mut keys: Peekable<K>)
        -> Option<&Value>
    where K: Iterator<Item = &'a str>
    {
        return if let Some(key) = keys.next() {
            match (table.get(key), keys.peek()) {
                (Some(Value::Table(tab)), Some(_)) => {
                    Self::table_get_path(tab, keys)
                },
                (x, None) => x,
                (Some(_), Some(_)) => None,
                (None, _) => None,
            }
        } else {
            unreachable!()
        };
    }

    /// Access a key path in `self`, returning `Some` if the complete path
    /// exists, `None` otherwise.
    pub fn get_path<'a, K>(&self, keys: K) -> Option<&Value>
    where K: IntoIterator<Item = &'a str>
    {
        return Self::table_get_path(&self.data, keys.into_iter().peekable());
    }

    /// Access a key path in `self` where the individual keys in the path are
    /// separated by `'.'`. Returns `Some` if the complete path exists, `None`
    /// otherwise.
    pub fn get_path_s(&self, keys: &str) -> Option<&Value> {
        return self.get_path(keys.split('.'));
    }

    /// Access a key path in `self` and attempt to convert its type to `T`,
    /// returning `Some(T)` if the complete path exists and the type is
    /// convertible, `None` otherwise.
    pub fn get_path_into<'a, 'de, K, T>(&self, keys: K) -> Option<T>
    where
        T: Deserialize<'de>,
        K: IntoIterator<Item = &'a str>,
    {
        return match self.get_path(keys) {
            Some(x) => x.clone().try_into().ok(),
            None => None,
        };
    }

    /// Access a key path in `self`, where individual keys in the path are
    /// separated by `'.'`, and attempt to convert its type to `T`, returning
    /// `Some(T)` if the complete path exists and the type is convertible,
    /// `None` otherwise.
    pub fn get_path_s_into<'de, T>(&self, keys: &str) -> Option<T>
    where T: Deserialize<'de>
    {
        return match self.get_path_s(keys) {
            Some(x) => x.clone().try_into().ok(),
            None => None,
        };
    }

    fn table_get_path_ok<'a, K>(table: &Table, mut keys: Peekable<K>)
        -> ConfigResult<&Value>
    where K: Iterator<Item = &'a str>
    {
        return if let Some(key) = keys.next() {
            match (table.get(key), keys.peek()) {
                (Some(Value::Table(tab)), Some(_)) => {
                    Self::table_get_path_ok(tab, keys)
                },
                (x, None) => {
                    x.ok_or_else(|| ConfigError::MissingKey(key.to_string()))
                },
                (Some(_), Some(k))
                    => Err(ConfigError::KeyPathTooLong(k.to_string())),
                (None, _) => Err(ConfigError::MissingKey(key.to_string())),
            }
        } else {
            unreachable!()
        };
    }

    /// Access a key path in `self`, returning `Ok` if the complete path
    /// exists, `Err` otherwise.
    pub fn get_path_ok<'a, K>(&self, keys: K) -> ConfigResult<&Value>
    where K: IntoIterator<Item = &'a str>
    {
        return Self::table_get_path_ok(&self.data, keys.into_iter().peekable());
    }

    /// Access a key path in `self` where the individual keys in the path are
    /// separated by `'.'`. Returns `Ok` if the complete path exists, `Err`
    /// otherwise.
    pub fn get_path_s_ok(&self, keys: &str) -> ConfigResult<&Value> {
        return self.get_path_ok(keys.split('.'));
    }

    /// Access a key path in `self` and attempt to convert its type to `T`,
    /// returning `Ok(T)` if the complete path exists and the type is
    /// convertible, `Err` otherwise.
    pub fn get_path_ok_into<'a, 'de, K, T>(&self, keys: K) -> ConfigResult<T>
    where
        T: Deserialize<'de>,
        K: IntoIterator<Item = &'a str>,
    {
        return self.get_path_ok(keys)
            .and_then(|x| {
                x.clone()
                    .try_into()
                    .map_err(|_| {
                        ConfigError::FailedTypeConversion(x.to_string())
                    })
            });
    }

    /// Access a key path in `self`, where individual keys in the path are
    /// separated by `'.'`, and attempt to convert its type to `T`, returning
    /// `Ok(T)` if the complete path exists and the type is convertible, `Err`
    /// otherwise.
    pub fn get_path_s_ok_into<'de, T>(&self, keys: &str) -> ConfigResult<T>
    where T: Deserialize<'de>
    {
        return self.get_path_s_ok(keys)
            .and_then(|x| {
                x.clone()
                    .try_into()
                    .map_err(|_| {
                        ConfigError::FailedTypeConversion(x.to_string())
                    })
            });
    }

    /// Alias for [`Self::get_path_s_ok_into`].
    pub fn a<'de, T>(&self, keys: &str) -> ConfigResult<T>
    where T: Deserialize<'de>
    {
        return self.get_path_s_ok_into(keys);
    }
}

/// Load a [`Config`] from the given path, optionally verifying against a
/// specification.
fn load_config<P>(infile: P, verify: Option<&ConfigSpec>)
    -> ConfigResult<Config>
where P: AsRef<Path>
{
    let infile_str: String = infile.as_ref().display().to_string();
    let table: Table
        = fs::read_to_string(infile)
        .map_err(|_| ConfigError::FileRead(infile_str.clone()))?
        .parse()
        .map_err(|_| ConfigError::FileParse(infile_str.clone()))?;
    return if let Some(config_spec) = verify {
        config_spec.verify(table)
    } else {
        Ok(Config { data: table })
    };
}

/// Load a [`Config`] from the given path, verifying aginst
/// [`ConfigSpec::default`]
pub fn load_def_config<P>(infile: P) -> ConfigResult<Config>
where P: AsRef<Path>
{
    return load_config(infile, Some(DEF_CONFIG_SPEC.deref()));
}

#[cfg(test)]
mod test {
    use super::*;

    fn create_table_good() -> Table {
        return "[suptab]\nf1 = true\nf2 = \"foo\"\nf3 = [1.0, 1]"
            .parse::<Table>().unwrap();
    }

    fn create_table_bad() -> Table {
        return "[suptab]\nf1 = true\nf2 = \"foo\"\nf3 = [1, 1.0]"
            .parse::<Table>().unwrap();
    }

    fn create_spec() -> ConfigSpec {
        return config_spec!(
            "suptab" => spec!(Table {
                "f1" => spec!(Bool),
                "f2" => spec!(StrCollection { "foo", "bar" }),
                "f3" => spec!(TypedArray {
                    [TypeVer::Float, TypeVer::Int],
                    finite: true
                }),
            }),
        );
    }

    #[test]
    fn verify_config_good() -> ConfigResult<()> {
        let table = create_table_good();
        let spec = create_spec();
        spec.verify(table)?;
        return Ok(());
    }

    #[test]
    #[should_panic]
    fn verify_config_bad() -> () {
        let table = create_table_bad();
        let spec = create_spec();
        spec.verify(table).expect("should fail!");
        return ();
    }

    #[test]
    fn key_path_access() -> ConfigResult<()> {
        let table = create_table_good();
        let spec = create_spec();
        let config = spec.verify(table)?;
        config.get_path_ok(["suptab", "f1"])?;
        config.get_path_s_ok("suptab.f1")?;
        return Ok(());
    }

    #[test]
    #[should_panic]
    fn key_path_access_bad() -> () {
        let table = create_table_good();
        let spec = create_spec();
        let config = spec.verify(table).expect("won't fail");
        config.get_path_ok(["suptab", "f4"]).expect("should fail!");
        return ();
    }

    #[test]
    fn key_path_access_convert() -> ConfigResult<()> {
        let table = create_table_good();
        let spec = create_spec();
        let config = spec.verify(table)?;
        let f1: bool = config.get_path_ok_into(["suptab", "f1"])?;
        assert_eq!(f1, true);
        return Ok(());
    }

    #[test]
    #[should_panic]
    fn key_path_access_convert_bad() -> () {
        let table = create_table_good();
        let spec = create_spec();
        let config = spec.verify(table).expect("won't fail");
        let f1: f64 = config.get_path_ok_into(["suptab", "f1"])
            .expect("should fail!");
        assert_eq!(f1, 1.0);
        return ()
    }
}

