//! Provides constructs to live-plot data output from the camera and frame
//! filters.
//!
//! Operation is based on a model where a [`FrameViewer`] is run in the main
//! thread and optionally visualizes the output from a frame processor in
//! addition to the direct stream from the camera. These frames are received via
//! [`Message`]s sent over a [`crossbeam`] channel, the receiving end of which
//! the viewer is initialized with via [`ViewerConfig`].

use std::{
    time::Duration,
};
use crossbeam::{
    channel::{
        Receiver,
    },
};
use iced::{
    executor,
    alignment::{
        Horizontal,
        Vertical,
    },
    Alignment,
    Application,
    Command,
    widget::{
        Button,
        Column,
        Container,
        Row,
        Space,
        Text,
        Toggler,
    },
    Element,
    Length,
    Subscription,
    Theme,
};
use iced_runtime::{
    command::Action as CmdAction,
    window::Action as WinAction,
};
use ndarray as nd;
use plotters::{
    chart::ChartBuilder,
    element::Rectangle,
    style::{
        Color,
        RGBColor,
        ShapeStyle,
    },
};
use plotters_backend::DrawingBackend;
use plotters_iced::{
    Chart,
    ChartWidget,
};
use crate::colors::{
    ColorBar,
    ColorMap,
    ColorMapBounds,
};

/// Frame viewer window height in pixels.
pub const WINDOW_HEIGHT: u32 = 375;

/// Frame viewer window width in pixels.
pub const WINDOW_WIDTH: u32 = 350;

// Compile-time window size math:
//  - WINDOW_SIZE: average window dimension in pixels
//  - MARGIN: empty border space on all sides of the plot
//  - LABEL_AREA_SIZE: axis label area size in pixels
//  - TEXT_SIZE: size of text placed outside the plot in pixels
//  - TEXT_SPACING: relative positioning of extra-plot text and plot in pixels
//  - CONTAINER_PAD: empty border space from the edges of the window
const WINDOW_SIZE: f32 = (WINDOW_HEIGHT + WINDOW_WIDTH) as f32 / 2.0;
const MARGIN: f32 = WINDOW_SIZE / 25.0;
const LABEL_AREA_SIZE: f32 = WINDOW_SIZE / 100.0;
const TEXT_SIZE: f32 = 15.0;
const TEXT_SPACING: f32 = -TEXT_SIZE;
const CONTAINER_PAD: f32 = WINDOW_SIZE / 100.0;
const CONTROL_ROW_HEIGHT: f32 = 25.0;

/// Marker trait for basic numeric types that can be ferried around in frame
/// arrays.
pub trait FrameData
where
    nd::OwnedRepr<Self>: nd::RawData,
    Self: Copy,
    Self: Into<f64>,
    Self: PartialOrd,
    Self: std::fmt::Debug,
    Self: std::marker::Send,
{ }

macro_rules! impl_framedata_for {
    ( $t:ty ) => { impl FrameData for $t { } }
}
impl_framedata_for!(u8);
impl_framedata_for!(u16);
impl_framedata_for!(u32);
impl_framedata_for!(i8);
impl_framedata_for!(i16);
impl_framedata_for!(i32);
impl_framedata_for!(f32);
impl_framedata_for!(f64);

/// `(height, width)` dimensions of a frame array.
type FrameSize = (usize, usize);

/// `(height, width)`, in pixels, of a frame plot.
type ChartSize = (Option<f32>, Option<f32>);

/// `Text-plot spacing in pixels.
type Spacing = f32;

/// Holds frame data and colormap settings.
///
/// Builds frame plots that are eventually drawn in the viewer window.
#[derive(Debug)]
pub struct FramePlot<T>
where T: FrameData
{
    frame: Option<nd::Array2<T>>,
    colormap: ColorMap,
    bounds: ColorMapBounds,
    sizes: Option<(FrameSize, ChartSize, Spacing)>,
}

impl<T> FramePlot<T>
where T: FrameData
{
    /// Create a new [`FramePlot`].
    pub fn new(
        colormap: ColorMap,
        bounds: ColorMapBounds,
    ) -> Self
    {
        return Self {
            frame: None,
            colormap,
            bounds,
            sizes: None,
        };
    }

    /// Check whether a frame can be drawn.
    pub fn is_initialized(&self) -> bool { self.frame.is_some() }

    /// Load a new frame to draw, discarding the old one.
    ///
    /// This function caches computation of the plot height, width, and top
    /// spacing, updating based on whether the new frame size is the same as
    /// that of the previous frame.
    pub fn set_frame(&mut self, frame: nd::Array2<T>) {
        let frame_shape: &[usize] = frame.shape();
        let compute_sizes: bool
            = if let Some((cached_frame_shape, _, _)) = self.sizes {
                frame_shape[0] != cached_frame_shape.0
                    || frame_shape[1] != cached_frame_shape.1
            } else {
                true
            };
        if compute_sizes {
            let aspect: f32 = frame_shape[0] as f32 / frame_shape[1] as f32;
            let horizontal: bool = aspect < 1.0;
            let (height, width): (Option<f32>, Option<f32>)
                = if horizontal {
                    (
                        Some(
                            (
                                WINDOW_WIDTH as f32
                                - 2.0 * (
                                    MARGIN
                                    + LABEL_AREA_SIZE
                                    + CONTAINER_PAD
                                )
                            ) * aspect
                            + 2.0 * (MARGIN + LABEL_AREA_SIZE)
                        ),
                        None,
                    )
                } else {
                    (
                        None,
                        Some(
                            (
                                WINDOW_HEIGHT as f32
                                - CONTROL_ROW_HEIGHT
                                - (TEXT_SIZE + TEXT_SPACING)
                                - 2.0 * (
                                    MARGIN
                                    + LABEL_AREA_SIZE
                                    + CONTAINER_PAD
                                )
                            ) / aspect
                            + 2.0 * (MARGIN + LABEL_AREA_SIZE)
                        ),
                    )
                };
            let space: f32
                = (
                    WINDOW_HEIGHT as f32
                    - height.map(|h| h + TEXT_SIZE + TEXT_SPACING)
                        .unwrap_or(WINDOW_HEIGHT as f32)
                ) / 2.0
                - TEXT_SPACING;
            self.sizes = Some(
                (
                    (frame_shape[0], frame_shape[1]),
                    (height, width),
                    space,
                )
            );
        }
        self.frame = Some(frame);
    }

    /// Return a GUI [`Element`] containing objects to display in the
    /// [`FrameViewer`] window.
    pub fn view(&self) -> Element<'_, Message> {
        return if self.is_initialized() {
            let (_, (height, width), space) = self.sizes.unwrap();
            let frame = self.frame.as_ref().unwrap();
            Column::new()
                .width(Length::Fill)
                // .width(width.map(Length::Fixed).unwrap_or(Length::Fill))
                .height(Length::Fixed(WINDOW_HEIGHT as f32))
                .align_items(Alignment::Center)
                .spacing(TEXT_SPACING)
                .push(Space::new(Length::Fill, Length::Fixed(space)))
                .push(
                    Text::new(
                        format!(
                            "Range: [{:.1}, {:.1}]",
                            self.bounds.0.bound_min(frame),
                            self.bounds.1.bound_max(frame),
                        )
                    )
                    .size(TEXT_SIZE)
                )
                .push(
                    ChartWidget::new(self)
                        .height(
                            height.map(Length::Fixed)
                                .unwrap_or(Length::Fill)
                        )
                        .width(
                            width.map(Length::Fixed)
                                .unwrap_or(Length::Fill)
                        )
                )
                .into()
        } else {
            Text::new("Awaiting first acquisition...")
                .width(Length::Fixed(WINDOW_WIDTH as f32))
                .height(Length::Fixed(WINDOW_HEIGHT as f32))
                .horizontal_alignment(Horizontal::Center)
                .vertical_alignment(Vertical::Center)
                .size(TEXT_SIZE)
                .into()
        };
    }
}

impl<T> Chart<Message> for FramePlot<T>
where T: FrameData
{
    type State = ();

    fn build_chart<DB>(&self, _state: &Self::State, mut chart: ChartBuilder<DB>)
    where DB: DrawingBackend
    {
        let frame_shape: &[usize]
            = self.frame.as_ref()
            .map(|f| f.shape())
            .unwrap_or(&[10, 10]);
        let mut chart_context = chart
            .margin(MARGIN)
            .x_label_area_size(LABEL_AREA_SIZE)
            .top_x_label_area_size(LABEL_AREA_SIZE)
            .y_label_area_size(LABEL_AREA_SIZE)
            .right_y_label_area_size(LABEL_AREA_SIZE)
            .build_cartesian_2d(
                -0.5..frame_shape[1] as f64 - 0.5,
                (frame_shape[0] as f64 - 0.5)..-0.5,
            )
            .expect("failed to build chart")
            .set_secondary_coord(0..0, 0..0);

        chart_context
            .configure_mesh()
            .axis_style(ShapeStyle {
                color: RGBColor(0, 0, 0).to_rgba(),
                filled: true,
                stroke_width: 1
            })
            .x_label_offset(0)
            .y_label_offset(0)
            .x_label_formatter(&|x| format!("{:.0}", x))
            .y_label_formatter(&|y| format!("{:.0}", y))
            .x_desc("")
            .y_desc("")
            .draw()
            .expect("failed to draw mesh");

        chart_context
            .configure_secondary_axes()
            .axis_style(ShapeStyle {
                color: RGBColor(0, 0, 0).to_rgba(),
                filled: false,
                stroke_width: 1
            })
            .x_label_offset(0)
            .y_label_offset(0)
            .x_label_formatter(&|_x| String::new())
            .y_label_formatter(&|_y| String::new())
            .x_desc("")
            .y_desc("")
            .set_all_tick_mark_size(0)
            .draw()
            .expect("failed to draw secondary mesh");

        if let Some(frame) = &self.frame {
            let (min, max): (f64, f64)
                = (
                    self.bounds.0.bound_min(frame),
                    self.bounds.1.bound_max(frame),
                );
            let range: f64 = max - min;
            chart_context
                .draw_series(
                    frame.indexed_iter()
                        .map(|((i, j), x)| {
                            Rectangle::new(
                                [
                                    (j as f64 - 0.5, i as f64 - 0.5),
                                    (j as f64 + 0.5, i as f64 + 0.5),
                                ],
                                self.colormap
                                    .c(
                                        (<T as Into<f64>>::into(*x) - min)
                                        / range
                                    )
                                    .filled(),
                            )
                        })
                )
                .expect("failed to draw data");
        }
    }
}

/// Use this variable to store the most recently received [`Message`].
static mut MESSAGE_STORAGE: Message = Message::Idle;

/// Use this type as a static interface to a particular `static mut` storage
/// variable local to this module.
struct MessageStorage;

impl MessageStorage {
    /// Set the value of the storage variable.
    fn store(data: Message) { unsafe { MESSAGE_STORAGE = data; } }

    /// Return a mutable reference to the storage variable.
    ///
    /// # Safety
    ///
    /// It's probably a bad idea to work directly with this function. Use
    /// [`Self::store`] instead.
    unsafe fn storage() -> &'static mut Message {
        return &mut MESSAGE_STORAGE;
    }

    /// Retrieve data from the storage variable.
    fn retrieve() -> Message {
        unsafe {
            return std::mem::replace(&mut MESSAGE_STORAGE, Message::Idle);
        }
    }
}

/// Input to a [`FrameViewer`] sent from another thread.
#[derive(Clone, Debug)]
pub enum ToViewer {
    /// No frame.
    Nothing,

    /// Frame obtained directly from the camera.
    Frame(nd::Array2<u16>),

    /// Processed frame.
    ProcFrame(nd::Array2<f64>),

    /// Halt visualization and close the window.
    Stop,
}

/// Message data passed within a [`FrameViewer`].
#[derive(Clone, Debug)]
pub enum Message {
    /// System is idle.
    Idle,

    /// New camera frame to be visualized.
    Frame(nd::Array2<u16>),

    /// New processed frame to be visualized.
    ProcFrame(nd::Array2<f64>),

    /// Toggle view of the proc frame.
    ViewSwitch(bool),

    // /// Relay a sender for transmission to outside the application.
    // Ready(mpsc::UnboundedSender<ToViewer>),

    /// Halt visualization and close the window.
    Stop,
}

impl From<ToViewer> for Message {
    fn from(to_viewer: ToViewer) -> Self {
        return match to_viewer {
            ToViewer::Nothing => Self::Idle,
            ToViewer::Frame(frame) => Self::Frame(frame),
            ToViewer::ProcFrame(frame) => Self::ProcFrame(frame),
            ToViewer::Stop => Self::Stop,
        };
    }
}

/// Initialize a [`FrameViewer`] with these data.
#[derive(Debug)]
pub struct ViewerConfig {
    /// Message channel to the viewer.
    pub source: Receiver<ToViewer>,

    /// Colormap and bounds to use for the frame plot.
    pub colorbar: ColorBar,

    /// Colormap and bounds to use for a processed frame plot.
    ///
    /// Indicates whether to initialize a second plot for processed frames.
    pub proc_colorbar: Option<ColorBar>,
}

impl ViewerConfig {
    /// Convenience function to convert to application settings with preferred
    /// defaults.
    pub fn into_settings(self) -> iced::Settings<Self> {
        return iced::Settings {
            id: None,
            window: iced::window::Settings {
                size: (WINDOW_WIDTH, WINDOW_HEIGHT),
                resizable: false,
                ..Default::default()
            },
            flags: self,
            default_font: iced::Font::default(),
            default_text_size: 15.0,
            // text_multithreading: false,
            antialiasing: false,
            exit_on_close_request: true,
            // try_opengles_first: false,
        };
    }

    /// Convenience function to create preferred application settings for the
    /// viewer.
    pub fn new_settings(
        source: Receiver<ToViewer>,
        colorbar: ColorBar,
        proc_colorbar: Option<ColorBar>,
    ) -> iced::Settings<Self>
    {
        return iced::Settings {
            id: None,
            window: iced::window::Settings {
                size: (WINDOW_WIDTH, WINDOW_HEIGHT),
                resizable: false,
                ..Default::default()
            },
            flags: Self {
                source,
                colorbar,
                proc_colorbar,
            },
            default_font: iced::Font::default(),
            default_text_size: 15.0,
            // text_multithreading: false,
            antialiasing: false,
            exit_on_close_request: true,
            // try_opengles_first: false,
        };
    }
}

/// Frame data visualizer.
///
/// Call the [`Application::run`] method to spawn a new frame visualizer window.
/// Note that this blocks the calling thread and cannot be called from another
/// thread, so care should be taken to ensure that all supporting channels are
/// properly instantiated and handled beforehand.
#[derive(Debug)]
pub struct FrameViewer {
    /// Message channel to receive instructions from another thread.
    source: Receiver<ToViewer>,

    /// Plot of the current frame from the camera-direct stream.
    plot: FramePlot<u16>,

    /// Optional plot of a processed frame.
    proc_plot: Option<FramePlot<f64>>,

    view_proc: bool,
}

impl Application for FrameViewer
{
    type Message = Message;
    type Executor = executor::Default;
    type Flags = ViewerConfig;
    type Theme = Theme;

    fn new(flags: Self::Flags) -> (Self, Command<Self::Message>) {
        let ViewerConfig { source, colorbar, proc_colorbar } = flags;
        return (
            Self {
                source,
                plot: FramePlot::new(colorbar.0, colorbar.1),
                proc_plot: proc_colorbar
                    .map(|(colormap, bounds)| FramePlot::new(colormap, bounds)),
                view_proc: false,
            },
            Command::none(),
        );
    }

    fn title(&self) -> String { "Frame display".to_string() }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        return match message {
            Message::Idle => {
                Command::none()
            },
            Message::Frame(frame) => {
                self.plot.set_frame(frame);
                Command::none()
            },
            Message::ProcFrame(frame) => {
                if let Some(plot) = &mut self.proc_plot {
                    plot.set_frame(frame);
                }
                Command::none()
            },
            Message::ViewSwitch(b) => {
                self.view_proc = b;
                Command::none()
            },
            Message::Stop => {
                crate::andor::cancel_wait().ok();
                Command::single(CmdAction::Window(WinAction::Close))
            },
        };
    }

    fn view(&self) -> Element<'_, Self::Message> {
        // controls
        let quit_button
            = Button::new("Abort")
            .on_press(Message::Stop);
        let view_switch
            = Toggler::new(
                "View frame processor output  ".to_string(),
                self.view_proc,
                |b| if self.proc_plot.is_some() {
                    Message::ViewSwitch(b)
                } else {
                    Message::ViewSwitch(false)
                }
            )
            .text_alignment(Horizontal::Right);
        let controls
            = Row::new()
            .height(Length::Fixed(CONTROL_ROW_HEIGHT))
            .align_items(Alignment::Center)
            .push(quit_button)
            .push(view_switch);

        // main frame view
        let mut col
            = Column::new()
            .width(Length::Fill)
            .height(Length::Fill)
            .align_items(Alignment::Center)
            .push(controls);
        if self.proc_plot.is_some() && self.view_proc {
            col = col.push(self.proc_plot.as_ref().unwrap().view());
        } else {
            col = col.push(self.plot.view());
        }
        return Container::new(col)
            .width(Length::Fill)
            .height(Length::Fill)
            .padding(CONTAINER_PAD)
            .center_x()
            .center_y()
            .into();
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        let message: Self::Message
            = match self.source.try_recv() {
                Ok(msg) => msg.into(),
                Err(crossbeam::channel::TryRecvError::Empty)
                    => Message::Idle,
                Err(crossbeam::channel::TryRecvError::Disconnected)
                    => Message::Stop,
            };
        MessageStorage::store(message);
        return iced::time::every(Duration::from_micros(100))
            .map(|_| MessageStorage::retrieve());
    }

    // fn subscription(&self) -> Subscription<Self::Message> {
    //     struct ViewerId;
    //
    //     // let state_init = State::Starting(self.gen_sources);
    //     return iced::subscription::channel(
    //         std::any::TypeId::of::<ViewerId>(),
    //         100,
    //         move |mut output| async move {
    //             let mut state = State::Starting(1);
    //             loop {
    //                 match &mut state {
    //                     State::Starting(num_sources) => {
    //                         let (tx, rx):
    //                             (
    //                                 mpsc::UnboundedSender<ToViewer>,
    //                                 mpsc::UnboundedReceiver<ToViewer>,
    //                             )
    //                             = mpsc::unbounded();
    //                         for _ in 0..*num_sources {
    //                             output.send(Message::Ready(tx.clone())).await;
    //                         }
    //                         state = State::Working(rx);
    //                     },
    //                     State::Working(rx) => {
    //                         let msg: Message
    //                             = match rx.select_next_some().await {
    //                                 ToViewer::Nothing => Message::Idle,
    //                                 ToViewer::Frame(f) => Message::Frame(f),
    //                                 ToViewer::ProcFrame(f) => Message::ProcFrame(f),
    //                                 ToViewer::Stop => Message::Stop,
    //                             };
    //                         output.send(msg).await;
    //                     },
    //                 }
    //             }
    //         }
    //     );
    // }
}

// /// Represents the state of the viewer, communicated to itself.
// #[derive(Debug)]
// pub enum State {
//     Starting,
//     Working(mpsc::UnboundedReceiver<Message>),
// }

