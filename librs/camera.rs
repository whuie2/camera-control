//! Provides the main interface to the camera, [`IXon888`].

use std::{
    sync::Arc,
    thread,
    time,
};
use ndarray::{
    self as nd,
};
use rand::{
    prelude as rnd,
    Rng,
};
use thiserror::Error;
use crate::{
    print_flush,
    // println_flush,
    andor,
    config::{
        self,
        Config,
    },
};

#[derive(Debug, Error)]
pub enum CameraError {
    #[error("camera is not connected")]
    NotConnected,

    #[error("bad ROI coordinates: ending coordinate must be > starting coordinate")]
    BadROI,

    #[error("bad binning: bin size must evenly divide ROI size")]
    BadBinning,

    #[error("Andor SDK error: {0}")]
    AndorError(#[from] andor::AndorError),

    #[error("config error: {0}")]
    ConfigError(#[from] config::ConfigError),
}
pub type CameraResult<T> = Result<T, CameraError>;

pub trait PrettyString {
    const IND: &'static str;

    fn pretty_string(&self, indent_level: usize, indent_first: bool) -> String;

    fn pretty_string_inline(&self, indent_level: usize) -> String {
        return self.pretty_string(indent_level, false);
    }
}

macro_rules! impl_pretty_string_fn {
    (
        $fmt_name:literal,
        {
            $( $field:expr => $fmt_field:literal ),+
            $(,)?
        },
        $fmt_close:literal,
        $indent_level:ident,
        $indent_first:ident
        $(,)?
    ) => {
        return [
            format!(
                $fmt_name,
                if $indent_first {
                    Self::IND.repeat($indent_level)
                } else {
                    "".to_string()
                },
            ),
            $(
                format!(
                    $fmt_field,
                    Self::IND.repeat($indent_level + 1),
                    $field,
                )
            ),+,
            format!(
                $fmt_close,
                Self::IND.repeat($indent_level),
            ),
        ].join("\n");
    }
}

/// General system information:
/// - Camera serial number
/// - Device driver version
/// - SDK version
/// - [Hardware versions][`andor::HardwareVersions`]
/// - [Software versions][`andor::SoftwareVersions`]
#[derive(Clone, Debug)]
pub struct SystemInfo {
    pub serial_number: usize,
    pub driver_version: String,
    pub sdk_version: String,
    pub hardware: andor::HardwareVersions,
    pub software: andor::SoftwareVersions,
}

impl PrettyString for SystemInfo {
    const IND: &'static str = "  ";

    fn pretty_string(&self, indent_level: usize, indent_first: bool) -> String {
        impl_pretty_string_fn!(
            "{}SystemInfo {{",
            {
                self.serial_number => "{}serial_number: {}",
                self.driver_version => "{}driver_version: {}",
                self.sdk_version => "{}sdk_version: {}",
                self.hardware.pretty_string_inline(indent_level + 1) => "{}hardware: {}",
                self.software.pretty_string_inline(indent_level + 1) => "{}software: {}",
            },
            "{}}}",
            indent_level,
            indent_first,
        );
    }
}

impl PrettyString for andor::HardwareVersions {
    const IND: &'static str = "  ";

    fn pretty_string(&self, indent_level: usize, indent_first: bool) -> String {
        impl_pretty_string_fn!(
            "{}HardwareVersions {{",
            {
                self.pcb => "{}pcb: {}",
                self.flex => "{}flex: {}",
                self.firm_ver => "{}firm_ver: {}",
                self.firm_build => "{}firm_build: {}",
            },
            "{}}}",
            indent_level,
            indent_first,
        );
    }
}

impl PrettyString for andor::SoftwareVersions {
    const IND: &'static str = "  ";

    fn pretty_string(&self, indent_level: usize, indent_first: bool) -> String {
        impl_pretty_string_fn!(
            "{}SoftwareVersions {{",
            {
                self.eprom => "{}eprom: {}",
                self.cof => "{}cof: {}",
                self.driver_ver => "{}driver_ver: {}",
                self.driver_rev => "{}driver_rev: {}",
                self.dll_ver => "{}dll_ver: {}",
                self.dll_rev => "{}dll_rev: {}",
            },
            "{}}}",
            indent_level,
            indent_first,
        );
    }
}

impl PrettyString for andor::CameraInformation {
    const IND: &'static str = "  ";

    fn pretty_string(&self, indent_level: usize, indent_first: bool) -> String {
        impl_pretty_string_fn!(
            "{}CameraInformation {{",
            {
                self.camera_present => "{}camera_present: {}",
                self.dlls_loaded => "{}dlls_loaded: {}",
                self.camera_initialized => "{}camera_initialized: {}",
            },
            "{}}}",
            indent_level,
            indent_first,
        );
    }
}

impl PrettyString for andor::AcquisitionTimings {
    const IND: &'static str = "  ";

    fn pretty_string(&self, indent_level: usize, indent_first: bool) -> String {
        impl_pretty_string_fn!(
            "{}AcquisitionTimings {{",
            {
                self.exposure => "{}exposure: {}",
                self.accumulate => "{}accumulate: {}",
                self.kinetic => "{}kinetic: {}",
            },
            "{}}}",
            indent_level,
            indent_first,
        );
    }
}

impl PrettyString for (f32, andor::TemperatureStatus) {
    const IND: &'static str = "  ";

    fn pretty_string(
        &self,
        _indent_level: usize,
        _indent_first: bool,
    ) -> String
    {
        return format!("{}C; {:?}", self.0, self.1);
    }
}

/// Disconnected interface to the camera.
#[derive(Debug)]
pub struct IXon888DC;

impl Default for IXon888DC {
    fn default() -> Self { Self }
}

impl IXon888DC {
    /// Create a new, disconnected camera handle.
    pub fn new() -> Self { Self }

    /// Connect to the physical device. Only one such connection can exist at
    /// once.
    pub fn connect(self, verbose: bool) -> CameraResult<IXon888> {
        print_flush!("[camera] initialize ... ");
        andor::initialize("/usr/local/etc/andor")?;
        println!("done");

        print_flush!("[camera] get handle ... ");
        let handle: andor::CameraHandle
            = andor::get_camera_handle(0)?;
        println!("done");

        print_flush!("[camera] get capabilities ... ");
        let capabilities: andor::Capabilities
            = andor::get_capabilities()?;
        println!("done");

        return Ok(IXon888 {
            handle,
            capabilities,
            image_shape: (1024, 1024),
            binning: (1, 1),
            oamp_mode: andor::OutputAmpMode::EM,
            preamp_gain_idx: 0,
            hs_speed_idx: 0,
            vs_speed_idx: 0,
            verbose,
            dummy: false,
            dummy_print: false,
        });
    }

    /// Create a 'connection' to a dummy device. All available actions are
    /// simulated, including read-only staling.
    pub fn connect_dummy(self, dummy_print: bool) -> CameraResult<IXon888> {
        if dummy_print {
            println!("[dummy] connecting to simulated dummy camera");
        }
        if dummy_print {
            println!("[dummy] initialize");
        }
        let handle = andor::CameraHandle(0);
        let capabilities = andor::Capabilities {
            acquisition_modes: std::collections::HashSet::new(),
            read_modes: std::collections::HashSet::new(),
            ft_read_modes: std::collections::HashSet::new(),
            trigger_modes: std::collections::HashSet::new(),
            camera_type: andor::CameraType::IXon,
            pixel_modes: std::collections::HashSet::new(),
            get_functions: std::collections::HashSet::new(),
            set_functions: std::collections::HashSet::new(),
            features: std::collections::HashSet::new(),
            features2: std::collections::HashSet::new(),
            pci_card: andor::PCISpeed(0.0),
            em_gain_modes: std::collections::HashSet::new(),
        };
        return Ok(IXon888 {
            handle,
            capabilities,
            image_shape: (1024, 1024),
            binning: (1, 1),
            oamp_mode: andor::OutputAmpMode::EM,
            preamp_gain_idx: 0,
            hs_speed_idx: 0,
            vs_speed_idx: 0,
            verbose: false,
            dummy: true,
            dummy_print,
        });
    }
}

/// Main interface to the camera.
///
/// Does not automatically connect upon instantiation, and assumes that this
/// camera is only one connected to the computer.
///
/// Not all functions in [`andor`] are implemented.
#[derive(Debug)]
pub struct IXon888 {
    handle: andor::CameraHandle,
    capabilities: andor::Capabilities,
    image_shape: (usize, usize),
    binning: (usize, usize),
    oamp_mode: andor::OutputAmpMode,
    preamp_gain_idx: usize,
    hs_speed_idx: usize,
    vs_speed_idx: usize,
    verbose: bool,
    dummy: bool,
    dummy_print: bool,
}

impl Drop for IXon888 {
    fn drop(&mut self) {
        self.disconnect();
    }
}

#[allow(clippy::collapsible_else_if)]
impl IXon888 {
    pub const GET_PROPS: &'static [&'static str] = &[
        "system-info",
        // "connection-info", // throws a USB error for some reason
        "status",
        "acquisition-timings",
        "exposure-time",
        "cooler",
        "temperature",
        "em-gain",
        "preamp-gain",
        "number-preamp-gain-modes",
        "hs-speed",
        "number-hs-speed-modes",
        "vs-speed",
        "number-vs-speed-modes",
    ];

    /// Create a new camera handle.
    ///
    /// Does not automatically connect to the physical camera.
    #[allow(clippy::new_ret_no_self)]
    pub fn new() -> IXon888DC {
        return IXon888DC;
    }

    /// Close the connection to the camera.
    pub fn disconnect(&mut self) -> IXon888DC {
        if !self.dummy {
            if self.verbose {
                println!("[camera] disconnect");
            }
            self.stop_acquisition().ok();
            self.cancel_wait().ok();
            andor::shutdown().ok();
        } else {
            if self.dummy_print {
                println!("[dummy] disconnect");
            }
        }
        return IXon888DC;
    }

    /// Create an [`Arc`] of `self`.
    pub fn as_arc(self) -> Arc<Self> { Arc::new(self) }

    /// Get the current camera status.
    pub fn get_status(&self) -> CameraResult<andor::CameraStatus> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get camera status");
            }
            return Ok(andor::get_status()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get camera status");
            }
            return Ok(andor::CameraStatus::Idle);
        }
    }

    pub fn is_idle(&self) -> CameraResult<bool> {
        return self.get_status()
            .map(|s| matches!(s, andor::CameraStatus::Idle));
    }

    pub fn is_acquiring(&self) -> CameraResult<bool> {
        return self.get_status()
            .map(|s| matches!(s, andor::CameraStatus::Acquiring));
    }

    /// Set camera paramters according to a [`Config`]. Assumes the config has
    /// already been validated.
    pub fn set_config(&mut self, config: &Config) -> CameraResult<&mut Self> {
        // [camera]
        let acquisition_mode: andor::AcquisitionMode
            = match config.a::<String>("camera.acquisition_mode")?.as_ref() {
                "single" => andor::AcquisitionMode::Single,
                "accum" => andor::AcquisitionMode::Accumulate,
                "kinetic" => andor::AcquisitionMode::Kinetics,
                "fast_kinetic" => andor::AcquisitionMode::FastKinetics,
                "cont" => andor::AcquisitionMode::Video,
                _ => unreachable!(),
            };
        self.set_acquisition_mode(acquisition_mode)?;
        let (roi_x, roi_y): ([usize; 2], [usize; 2])
            = {
                let v: Vec<i64> = config.a("camera.roi")?;
                (
                    [v[0] as usize, v[1] as usize],
                    [v[2] as usize, v[3] as usize],
                )
            };
        let binning: [usize; 2]
            = {
                let v: Vec<i64> = config.a("camera.bin")?;
                [v[0] as usize, v[1] as usize]
            };
        if roi_x[1] <= roi_x[0] || roi_y[1] <= roi_y[0] {
            return Err(CameraError::BadROI);
        }
        let roi_w: usize = roi_x[1] - roi_x[0];
        let roi_h: usize = roi_y[1] - roi_y[0];
        if roi_w % binning[0] != 0 || roi_h % binning[1] != 0 {
            return Err(CameraError::BadBinning);
        }
        self.set_image(&roi_x, &roi_y, &binning)?;
        let exposure_time: f32 // have to convert this to seconds
            = config.a::<f32>("camera.exposure_time_ms")? * 1e-3;
        self.set_exposure_time(exposure_time)?;

        // [camera.cooler]
        let cooler_mode: andor::CoolerMode
            = if config.a("camera.cooler.on")? {
                andor::CoolerMode::On
            } else {
                andor::CoolerMode::Off
            };
        self.set_cooler(cooler_mode)?;
        let fan_mode: andor::FanMode
            = match config.a::<String>("camera.cooler.fan_mode")?.as_ref() {
                "full" => andor::FanMode::Full,
                "low" => andor::FanMode::Low,
                "off" => andor::FanMode::Off,
                _ => unreachable!(),
            };
        self.set_fan_mode(fan_mode)?;
        let temperature: f32 = config.a("camera.cooler.temperature_c")?;
        self.set_temperature(temperature)?;

        // [camera.amp]
        let em_gain: u32 = config.a::<i64>("camera.amp.em_gain")? as u32;
        self.set_em_gain(em_gain)?;
        let preamp_gain_idx: usize
            = config.a::<i64>("camera.amp.preamp_gain_idx")? as usize;
        self.set_preamp_gain(preamp_gain_idx)?;
        // self.preamp_gain_idx = preamp_gain_idx;
        let oamp_mode: andor::OutputAmpMode
            = match config.a::<i64>("camera.amp.oamp")? {
                0 => andor::OutputAmpMode::EM,
                1 => andor::OutputAmpMode::Normal,
                _ => unreachable!(),
            };
        self.set_oamp_mode(oamp_mode)?;
        // self.oamp_mode = oamp_mode;

        // [camera.shutter]
        let shutter_mode: andor::ShutterMode
            = match config.a::<String>("camera.shutter.mode")?.as_ref() {
                "auto" => andor::ShutterMode::Auto,
                "open" => andor::ShutterMode::Open,
                "closed" => andor::ShutterMode::Closed,
                "fvb" => andor::ShutterMode::FVB,
                "any" => andor::ShutterMode::Any,
                _ => unreachable!(),
            };
        let shutter_trigger_mode: andor::ShutterTriggerMode
            = match config.a::<i64>("camera.shutter.ttl_mode")? {
                0 => andor::ShutterTriggerMode::Low,
                1 => andor::ShutterTriggerMode::High,
                _ => unreachable!(),
            };
        let shutter_open_time: u32
            = config.a::<f64>("camera.shutter.open_time_ms")?
            .round() as u32;
        let shutter_close_time: u32
            = config.a::<f64>("camera.shutter.close_time_ms")?
            .round() as u32;
        self.set_shutter(
            shutter_trigger_mode,
            shutter_mode,
            shutter_open_time,
            shutter_close_time,
        )?;

        // [camera.trigger]
        let trigger_mode: andor::TriggerMode
            = match config.a::<String>("camera.trigger.mode")?.as_ref() {
                "int" => andor::TriggerMode::Internal,
                "ext" => andor::TriggerMode::External,
                "ext_start" => andor::TriggerMode::ExternalStart,
                "ext_exp" => andor::TriggerMode::ExternalExposure,
                "ext_fvbem" => andor::TriggerMode::ExternalFVBEM,
                "software" => andor::TriggerMode::Software,
                "ext_chargeshift"
                    => andor::TriggerMode::ExternalChargeShifting,
                _ => unreachable!(),
            };
        self.set_trigger_mode(trigger_mode)?;

        // [camera.read]
        let read_mode: andor::ReadMode
            = match config.a::<String>("camera.read.mode")?.as_ref() {
                "image" => andor::ReadMode::FullImage,
                "subimage" => andor::ReadMode::SubImage,
                "single_track" => andor::ReadMode::SingleTrack,
                "fvb" => andor::ReadMode::FVB,
                "multi_track" => andor::ReadMode::MultiTrack,
                "random_track" => andor::ReadMode::RandomTrack,
                _ => unreachable!(),
            };
        self.set_read_mode(read_mode)?;
        let hs_speed_idx: usize
            = config.a::<i64>("camera.read.hsspeed_idx")? as usize;
        self.set_hs_speed(hs_speed_idx)?; // should be set after oamp mode
        // self.hs_speed_idx = hs_speed_idx;
        let vs_speed_idx: usize
            = config.a::<i64>("camera.read.vsspeed_idx")? as usize;
        self.set_vs_speed(vs_speed_idx)?;
        // self.vs_speed_idx = vs_speed_idx;

        // [camera.kinetic]
        let kinetic_buffer_size: usize
            = config.a::<i64>("camera.kinetic.buffer_size")? as usize;
        self.set_number_kinetic_scans(kinetic_buffer_size)?;
        let kinetic_cycle_time: f32
            = config.a("camera.kinetic.cycle_time")?;
        self.set_kinetic_cycle_time(kinetic_cycle_time)?;
        let num_accum: usize
            = config.a::<i64>("camera.kinetic.num_acc")? as usize;
        self.set_number_accumulations(num_accum)?;
        let accum_cycle_time: f32
            = config.a("camera.kinetic.cycle_time_acc")?;
        self.set_accumulation_cycle_time(accum_cycle_time)?;
        // let num_prescan: usize
        //     = config.a::<i64>("camera.kinetic.num_prescan")? as usize;
        // self.set_number_prescans(num_prescan)?;

        return Ok(self);
    }

    /// Get device connectivity/initialization info.
    pub fn get_connection_info(&self)
        -> CameraResult<andor::CameraInformation>
    {
        if !self.dummy {
            return Ok(andor::get_camera_information(0)?);
        } else {
            return Ok(andor::CameraInformation {
                camera_present: true,
                dlls_loaded: true,
                camera_initialized: true,
            });
        }
    }

    /// Get general system information:
    /// - Camera serial number
    /// - Device driver version
    /// - SDK version
    /// - [Hardware versions][`andor::HardwareVersions`]
    /// - [Software versions][`andor::SoftwareVersions`]
    pub fn get_system_info(&self) -> CameraResult<SystemInfo> {
        if !self.dummy {
            return Ok(
                SystemInfo {
                    serial_number: andor::get_camera_serial_number()?,
                    driver_version: andor::get_version_info(
                        andor::VersionInfo::DeviceDriver
                    )?,
                    sdk_version: andor::get_version_info(
                        andor::VersionInfo::SDK
                    )?,
                    hardware: andor::get_hardware_versions()?,
                    software: andor::get_software_versions()?,
                }
            );
        } else {
            return Ok(
                SystemInfo {
                    serial_number: 0,
                    driver_version: "".to_string(),
                    sdk_version: "".to_string(),
                    hardware: andor::HardwareVersions {
                        pcb: 0,
                        flex: 0,
                        firm_ver: 0,
                        firm_build: 0,
                    },
                    software: andor::SoftwareVersions {
                        eprom: 0,
                        cof: 0,
                        driver_ver: 0,
                        driver_rev: 0,
                        dll_ver: 0,
                        dll_rev: 0,
                    },
                }
            );
        }
    }

    /// Set the Frame Transfer mode.
    pub fn set_frame_transfer_mode(&mut self, mode: andor::FrameTransferMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set frame transfer mode to {:?}", mode);
            }
            andor::set_frame_transfer_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set frame transfer mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Get the state of the cooler.
    pub fn get_cooler(&self) -> CameraResult<andor::CoolerMode> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get cooler mode");
            }
            return Ok(andor::get_cooler_mode()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get cooler mode");
            }
            return Ok(andor::CoolerMode::On);
        }
    }

    /// Turn the cooler on/off.
    pub fn set_cooler(&mut self, mode: andor::CoolerMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set cooler mode to {:?}", mode);
            }
            andor::set_cooler_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set cooler mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Set the fan mode.
    pub fn set_fan_mode(&mut self, mode: andor::FanMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set fan mode to {:?}", mode);
            }
            andor::set_fan_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set fan mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Get the detector temperature in degrees Celsius and temperature control
    /// status.
    pub fn get_temperature(&self)
        -> CameraResult<(f32, andor::TemperatureStatus)>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get temperature");
            }
            return Ok(andor::get_temperature_f()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get temperature");
            }
            return Ok((0.0, andor::TemperatureStatus::Stabilized));
        }
    }

    /// Set the detector temperature setpoint in degrees Celsius.
    pub fn set_temperature(&mut self, temp: f32) -> CameraResult<&mut Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set temperature to {}", temp);
            }
            andor::set_temperature(temp.round() as i32)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set temperature to {}", temp);
            }
        }
        return Ok(self);
    }

    /// Get acquisition timings in seconds.
    pub fn get_acquisition_timings(&self)
        -> CameraResult<andor::AcquisitionTimings>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get acquisition timings");
            }
            return Ok(andor::get_acquisition_timings()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get acquisition timings");
            }
            return Ok(
                andor::AcquisitionTimings {
                    exposure: 0.0,
                    accumulate: 0.0,
                    kinetic: 0.0,
                }
            );
        }
    }

    /// Get the exposure time in seconds.
    pub fn get_exposure_time(&self) -> CameraResult<f32> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get exposure time");
            }
            return Ok(andor::get_acquisition_timings().map(|at| at.exposure)?);
        } else {
            if self.dummy_print {
                println!("[dummy] get exposure time");
            }
            return Ok(0.0);
        }
    }

    /// Set the exposure time in seconds.
    pub fn set_exposure_time(&mut self, time: f32) -> CameraResult<&mut Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set exposure time to {}", time);
            }
            andor::set_exposure_time(time)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set exposure time to {}", time);
            }
        }
        return Ok(self);
    }

    /// Set the acquisition mode.
    pub fn set_acquisition_mode(&mut self, mode: andor::AcquisitionMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set acquisition mode to {:?}", mode);
            }
            andor::set_acquisition_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set acquisition mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Set the image ROI and binning. Pixel coordinates start at 0.
    pub fn set_image(
        &mut self,
        x: &[usize; 2],
        y: &[usize; 2],
        bin: &[usize; 2],
    ) -> CameraResult<&mut Self>
    {
        if x[1] <= x[0] || y[1] <= y[0] {
            return Err(CameraError::BadROI);
        }
        let w: usize = x[1] - x[0];
        let h: usize = y[1] - y[0];
        if w % bin[0] != 0 || h % bin[1] != 0 {
            return Err(CameraError::BadBinning);
        }
        if !self.dummy {
            if self.verbose {
                println!(
                    "[camera] set image size/binning to {:?}/{:?}",
                    (h, w),
                    (bin[1], bin[0]),
                );
            }
            return andor::set_image(&[x[0], x[1] - 1], &[y[0], y[1] - 1], bin)
                .map(|_| {
                    self.image_shape = (h, w);
                    self.binning = (bin[1], bin[0]);
                    self
                })
                .map_err(|err| err.into());
        } else {
            self.image_shape = (h, w);
            self.binning = (bin[1], bin[0]);
            if self.dummy_print {
                println!(
                    "[dummy] set image size/binning to {:?}/{:?}",
                    (h, w),
                    (bin[1], bin[0]),
                );
            }
            return Ok(self);
        }
    }

    /// Set the kinetic cycle time, in seconds, to the nearest valid value not
    /// less than `time`.
    pub fn set_kinetic_cycle_time(&mut self, time: f32)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set kinetic cycle time to {}", time);
            }
            andor::set_kinetic_cycle_time(time)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set kinetic cycle time to {}", time);
            }
        }
        return Ok(self);
    }

    /// Set the number of scans to be performed before data can be retrieved.
    ///
    /// Will only take effect if the acquisition mode is Kinetic Series.
    pub fn set_number_prescans(&mut self, num_scans: usize)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set number prescans to {}", num_scans);
            }
            andor::set_number_prescans(num_scans)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set number prescans to {}", num_scans);
            }
        }
        return Ok(self);
    }

    /// Set the accumulation cycle time, in seconds, to the nearest valid value
    /// not less than `time`.
    pub fn set_accumulation_cycle_time(&mut self, time: f32)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set accumulation cycle time to {}", time);
            }
            andor::set_accumulation_cycle_time(time)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set accumulation cycle time to {}", time);
            }
        }
        return Ok(self);
    }

    /// Set the number of scans that can be accumulated in memory.
    ///
    /// Will only take effect if the acquisition mode is either Accumulate or
    /// Kinetic Series.
    pub fn set_number_accumulations(&mut self, num: usize)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set number accumulations to {}", num);
            }
            andor::set_number_accumulations(num)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set number accumulations to {}", num);
            }
        }
        return Ok(self);
    }

    /// Set the number of kinetic scans (possibly accumulated scans) to be taken
    /// during a single acquisition sequence.
    ///
    /// Will only take effect if the acquisition mode is Kinetic Series.
    pub fn set_number_kinetic_scans(&mut self, num_scans: usize)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set number kinetics to {}", num_scans);
            }
            andor::set_number_kinetics(num_scans)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set number kinetics to {}", num_scans);
            }
        }
        return Ok(self);
    }

    /// Set the output amplifier mode.
    ///
    /// If the current horizontal shift speed is not available for the given
    /// amplifier mode, then it will default to the maximum shift speed that is.
    pub fn set_oamp_mode(&mut self, mode: andor::OutputAmpMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set oamp mode to {:?}", mode);
            }
            andor::set_output_amplifier_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set oamp mode to {:?}", mode);
            }
        }
        self.oamp_mode = mode;
        return Ok(self);
    }

    /// Get the current EM gain.
    pub fn get_em_gain(&self) -> CameraResult<u32> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get em gain");
            }
            return Ok(andor::get_emccd_gain()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get em gain");
            }
            return Ok(0);
        }
    }

    /// Set the EM gain.
    pub fn set_em_gain(&mut self, gain: u32) -> CameraResult<&mut Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set em gain to {}", gain);
            }
            andor::set_emccd_gain(gain)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set em gain to {}", gain);
            }
        }
        return Ok(self);
    }

    /// Set the EM gain mode.
    pub fn set_em_gain_mode(&mut self, mode: andor::EMGainMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set em gain mode to {:?}", mode);
            }
            andor::set_em_gain_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set em gain mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Set the EM advanced gain mode.
    pub fn set_em_advanced_gain_mode(&mut self, mode: andor::EMAdvancedGainMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set em advanced gain mode to {:?}", mode);
            }
            andor::set_em_advanced_gain(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set em advanced gain mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Get the number of available preamp gain settings.
    pub fn get_number_preamp_gain_modes(&self) -> CameraResult<usize> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get number preamp gain modes");
            }
            return Ok(andor::get_number_preamp_gain_modes()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get number preamp gain modes");
            }
            return Ok(0);
        }
    }

    /// Get the gain factor for the current preamp gain setting.
    pub fn get_preamp_gain(&self) -> CameraResult<f32> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get preamp gain");
            }
            return Ok(andor::get_preamp_gain(self.preamp_gain_idx)?);
        } else {
            if self.dummy_print {
                println!("[dummy] get preamp gain");
            }
            return Ok(0.0);
        }
    }

    /// Set the preamp gain to the `index`-th allowed value.
    pub fn set_preamp_gain(&mut self, index: usize) -> CameraResult<&mut Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set preamp gain index to {}", index);
            }
            andor::set_preamp_gain(index)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set preamp gain index to {}", index);
            }
        }
        self.preamp_gain_idx = index;
        return Ok(self);
    }

    /// Set the read mode.
    pub fn set_read_mode(&mut self, mode: andor::ReadMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set read mode to {:?}", mode);
            }
            andor::set_read_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set read mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Set the trigger mode.
    pub fn set_trigger_mode(&mut self, mode: andor::TriggerMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set trigger mode to {:?}", mode);
            }
            andor::set_trigger_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set trigger mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Set the trigger inversion mode.
    pub fn set_trigger_invert_mode(&mut self, mode: andor::TriggerInvertMode)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set trigger invert mode to {:?}", mode);
            }
            andor::set_trigger_invert_mode(mode)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set trigger invert mode to {:?}", mode);
            }
        }
        return Ok(self);
    }

    /// Get the number of allowed horizontal shift speed settings.
    pub fn get_number_hs_speed_modes(&self) -> CameraResult<usize> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get number hs speed modes");
            }
            return Ok(andor::get_number_hs_speeds(0, self.oamp_mode)?);
        } else {
            if self.dummy_print {
                println!("[dummy] get number hs speed modes");
            }
            return Ok(0);
        }
    }

    /// Get the speed, in megahertz, of the current horizontal shift speed
    /// setting.
    pub fn get_hs_speed(&self) -> CameraResult<f32> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get hs speed");
            }
            return Ok(
                andor::get_hs_speed(0, self.oamp_mode, self.hs_speed_idx)?
            );
        } else {
            if self.dummy_print {
                println!("[dummy] get hs speed");
            }
            return Ok(0.0);
        }
    }

    /// Set the horizontal shift speed to the `index`-th allowed value.
    pub fn set_hs_speed(&mut self, index: usize) -> CameraResult<&mut Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set hs speed index to {}", index);
            }
            andor::set_hs_speed(self.oamp_mode, index)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set hs speed index to {}", index);
            }
        }
        self.hs_speed_idx = index;
        return Ok(self);
    }

    /// Get the number of allowed vertical shift speed settings.
    pub fn get_number_vs_speed_modes(&self) -> CameraResult<usize> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get number vs speeds");
            }
            return Ok(andor::get_number_vs_speeds()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get number vs speeds");
            }
            return Ok(0);
        }
    }

    /// Get the speed, in microseconds per shift, of the current vertical shift
    /// speed setting.
    pub fn get_vs_speed(&self) -> CameraResult<f32> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get vs speed");
            }
            return Ok(andor::get_vs_speed(self.vs_speed_idx)?);
        } else {
            if self.dummy_print {
                println!("[dummy] get vs speed");
            }
            return Ok(0.0);
        }
    }

    /// Set the vertical shift speed to the `index`-th allowed value.
    pub fn set_vs_speed(&mut self, index: usize) -> CameraResult<&mut Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set vs speed index to {}", index);
            }
            andor::set_vs_speed(index)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set vs speed index to {}", index);
            }
        }
        self.vs_speed_idx = index;
        return Ok(self);
    }

    /// Set the vertical clock voltage.
    pub fn set_vs_amplitude(&mut self, amp: andor::VSAmplitude)
        -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] set vs amplitude to {:?}", amp);
            }
            andor::set_vs_amplitude(amp)?;
        } else {
            if self.dummy_print {
                println!("[dummy] set vs amplitude to {:?}", amp);
            }
        }
        return Ok(self);
    }

    /// Set shutter parameters, including opening and closing times in
    /// milliseconds.
    pub fn set_shutter(
        &mut self,
        trigger_mode: andor::ShutterTriggerMode,
        mode: andor::ShutterMode,
        open: u32,
        close: u32,
    ) -> CameraResult<&mut Self>
    {
        if !self.dummy {
            if self.verbose {
                println!(
                    "[camera] set shutter parameters to {:?}/{:?}/{}/{}",
                    trigger_mode,
                    mode,
                    open,
                    close,
                );
            }
            andor::set_shutter_parameters(trigger_mode, mode, open, close)?;
        } else {
            if self.dummy_print {
                println!(
                    "[dummy] set shutter parameters to {:?}/{:?}/{}/{}",
                    trigger_mode,
                    mode,
                    open,
                    close,
                );
            }
        }
        return Ok(self);
    }

    /// Start acquisition without blocking.
    ///
    /// See also [`Self::wait_for_acquisition`].
    pub fn start_acquisition(&self) -> CameraResult<&Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] start acquisition");
            }
            andor::start_acquisition()?;
        } else {
            if self.dummy_print {
                println!("[dummy] start acquisition");
            }
        }
        return Ok(self);
    }

    /// Stop the current acquisition if one is active.
    pub fn stop_acquisition(&self) -> CameraResult<&Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] stop acquisition");
            }
            andor::abort_acquisition()?;
        } else {
            if self.dummy_print {
                println!("[dummy] stop acquisition");
            }
        }
        return Ok(self);
    }

    /// Get the number of scans of accumulations completed in the current
    /// acquisition. Can be called at any point in the acquisition.
    ///
    /// If called while not currently acquiring, return info on the last
    /// acquisition.
    pub fn get_acquisition_progress(&self)
        -> CameraResult<andor::AcquisitionProgress>
    {
        if !self.dummy {
            if self.verbose {
                println!("[camera] get acquisition progress");
            }
            return Ok(andor::get_acquisition_progress()?);
        } else {
            if self.dummy_print {
                println!("[dummy] get acquisition progress");
            }
            return Ok(
                andor::AcquisitionProgress {
                    accumulations: 0,
                    kinetic_scans: 0,
                }
            );
        }
    }

    /// Read the newest image from the internal buffer.
    pub fn read_newest_image(&self) -> CameraResult<nd::Array2<u16>> {
        let image_shape: (usize, usize)
            = (
                self.image_shape.0 / self.binning.0,
                self.image_shape.1 / self.binning.1,
            );
        if !self.dummy {
            if self.verbose {
                println!("[camera] read newest image");
            }
            return Ok(andor::get_acquired_data_16(image_shape)?);
        } else {
            if self.dummy_print {
                println!("[dummy] read newest image");
            }
            let mut rng = rnd::thread_rng();
            return Ok(
                nd::Array2::from_shape_fn(
                    image_shape,
                    |_| rng.gen_range(0..100)
                )
            );
        }
    }

    /// Read the oldest image from the internal buffer.
    pub fn read_oldest_image(&self) -> CameraResult<nd::Array2<u16>> {
        let image_shape: (usize, usize)
            = (
                self.image_shape.0 / self.binning.0,
                self.image_shape.1 / self.binning.1,
            );
        if !self.dummy {
            if self.verbose {
                println!("[camera] read oldest image");
            }
            return Ok(andor::get_oldest_image_16(image_shape)?);
        } else {
            if self.dummy_print {
                println!("[dummy] read oldest image");
            }
            let mut rng = rnd::thread_rng();
            return Ok(
                nd::Array2::from_shape_fn(
                    image_shape,
                    |_| rng.gen_range(0..100)
                )
            );
        }
    }

    /// Read a series of images from the internal buffer as a 3D array.
    ///
    /// Indices of the first and last valid images in the array are returned as
    /// well.
    pub fn read_image_series(&self, first: usize, num_images: usize)
        -> CameraResult<(nd::Array3<u16>, usize, usize)>
    {
        let image_shape: (usize, usize)
            = (
                self.image_shape.0 / self.binning.0,
                self.image_shape.1 / self.binning.1,
            );
        if !self.dummy {
            if self.verbose {
                println!("[camera] read image series");
            }
            return Ok(
                andor::get_image_series_16(first, num_images, image_shape)?
            );
        } else {
            if self.dummy_print {
                println!("[dummy] read image series");
            }
            let mut rng = rnd::thread_rng();
            let images: Vec<nd::Array2<u16>>
                = (0..num_images).map(|_| {
                    nd::Array2::from_shape_fn(
                        image_shape,
                        |_| rng.gen_range(0..100)
                    )
                })
                .collect();
            let images: nd::Array3<u16>
                = nd::stack(
                    nd::Axis(0),
                    &images.iter()
                        .map(|img| img.view())
                        .collect::<Vec<nd::ArrayView2<u16>>>(),
                )
                .expect("[dummy] stacking error in read_image_series");
            return Ok((images, 0, num_images - 1));
        }
    }

    /// Wait for an acquisition event from the camera, with optional timeout in
    /// milliseconds.
    pub fn wait_for_acquisition(&self, timeout: Option<u32>)
        -> CameraResult<Wait>
    {
        if !self.dummy {

            let res: andor::AndorResult<()>
                = if let Some(t) = timeout {
                    andor::wait_for_acquisition_by_handle_timeout(
                        self.handle,
                        t,
                    )
                } else {
                    andor::wait_for_acquisition_by_handle(self.handle)
                };
            return match res {
                Ok(()) => Ok(Wait::AcquiredData),
                Err(andor::AndorError::Code(andor::AndorReturn::Success))
                    => Ok(Wait::AcquiredData),
                Err(andor::AndorError::Code(andor::AndorReturn::NoNewData))
                    => Ok(Wait::Canceled),
                Err(andor_error) => Err(andor_error.into()),
            };
        } else {
            thread::sleep(time::Duration::from_millis(1000));
            unsafe {
                return match DUMMY_WAIT {
                    Wait::AcquiredData => Ok(Wait::AcquiredData),
                    Wait::Canceled => {
                        DUMMY_WAIT = Wait::AcquiredData;
                        Ok(Wait::Canceled)
                    },
                };
            }
        }
    }

    /// Wait for an acquisition event from the camera before reading the oldest
    /// image from the internal buffer, with optional timeout in milliseconds.
    pub fn wait_for_read_oldest(&self, timeout: Option<u32>)
        -> CameraResult<WaitData>
    {
        return match self.wait_for_acquisition(timeout)? {
            Wait::AcquiredData => {
                let image: nd::Array2<u16> = self.read_oldest_image()?;
                Ok(WaitData::AcquiredData(image))
            },
            Wait::Canceled => Ok(WaitData::Canceled),
        };
    }

    /// Wait for an acquisition event from the camera before reading the newest
    /// image from the internal buffer, with optional timeout in milliseconds.
    pub fn wait_for_read_newest(&self, timeout: Option<u32>)
        -> CameraResult<WaitData>
    {
        return match self.wait_for_acquisition(timeout)? {
            Wait::AcquiredData => {
                let image: nd::Array2<u16> = self.read_newest_image()?;
                Ok(WaitData::AcquiredData(image))
            },
            Wait::Canceled => Ok(WaitData::Canceled),
        };
    }

    /// Cancel a wait for an acquisition.
    pub fn cancel_wait(&self) -> CameraResult<&Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] cancel wait");
            }
            andor::cancel_wait()?;
        } else {
            if self.dummy_print {
                println!("[dummy] cancel wait");
            }
            unsafe {
                DUMMY_WAIT = Wait::Canceled;
            }
        }
        return Ok(self);
    }

    /// Send an event to the camera to start an acquisition when in Software
    /// Trigger Mode.
    pub fn send_software_trigger(&self) -> CameraResult<&Self> {
        if !self.dummy {
            if self.verbose {
                println!("[camera] send software trigger");
            }
            andor::send_software_trigger()?;
        } else {
            if self.dummy_print {
                println!("[dummy] send software trigger");
            }
        }
        return Ok(self);
    }
}

static mut DUMMY_WAIT: Wait = Wait::AcquiredData;

/// Termination of a call to [`IXon888::wait_for_acquisition`].
#[derive(Copy, Clone, Debug)]
pub enum Wait {
    AcquiredData,
    Canceled,
}

/// Termination of a call to [`IXon888::wait_for_read_oldest`] or
/// [`IXon888::wait_for_read_newest`].
#[derive(Clone, Debug)]
pub enum WaitData {
    AcquiredData(nd::Array2<u16>),
    Canceled,
}

