//! Provides the [`WindowAvg`] [`FrameProcessor`], which records a stack of
//! frames and reports an average of the stack.

use ndarray::{
    self as nd,
    s,
};
use crate::{
    config::Config,
    frame_processing::{
        FrameProcessor,
        FrameProcResult,
    },
};

/// Required [`Config`] keys:
/// - `processing.window_avg.window_size`
/// - `processing.window_avg.rolling_avg`
/// - `processing.window_avg.image_dim`
pub struct WindowAvg {
    window_size: usize,
    current_idx: usize,
    rolling_avg: bool,
    frames: nd::Array3<f64>,
}

impl FrameProcessor for WindowAvg {
    fn init(config: &Config) -> FrameProcResult<Self> {
        let window_size: usize
            = config.a::<i64>("processing.window_avg.window_size")? as usize;
        let current_idx: usize = 0;
        let rolling_avg: bool = config.a("processing.window_avg.rolling_avg")?;
        let image_dim: Vec<usize>
            = config.a::<Vec<i64>>("processing.window_avg.image_dim")?
            .into_iter()
            .map(|x| x as usize)
            .collect();
        let frames: nd::Array3<f64>
            = nd::Array::zeros((window_size, image_dim[1], image_dim[0]));

        return Ok(Self {
            window_size,
            current_idx,
            rolling_avg,
            frames,
        });
    }

    fn doit(&mut self, frame: &nd::Array2<u16>) -> Option<nd::Array2<f64>> {
        let f: nd::Array2<f64> = frame.mapv(f64::from);
        if f.shape() != &self.frames.shape()[1..3] {
            panic!(
                "window average: array dimensions are wrong:\n\
                frame: {:?}\nstack: {:?}",
                f.shape(),
                self.frames.shape(),
            );
        }
        f.move_into(self.frames.slice_mut(s![self.current_idx, .., ..]));
        return if self.rolling_avg || self.current_idx == self.window_size - 1 {
            self.current_idx = (self.current_idx + 1) % self.window_size;
            Some(self.frames.mean_axis(nd::Axis(0)).unwrap())
        } else {
            self.current_idx = (self.current_idx + 1) % self.window_size;
            None
        };
    }

    fn deinit(self) { }
}

