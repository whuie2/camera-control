//! Provides the [`FFPrep`] and [`FFSeries`] [`FrameProcessor`]s, which perform
//! state preparation via an Arduino [`SerialPort`].

use ndarray::{
    self as nd,
    s,
};
use serial2::SerialPort;
use crate::{
    config::Config,
    frame_processing::{
        FrameProcessor,
        FrameProcResult,
    },
};

fn e_per_count(readout_rate: f64, preamp_gain: f64) -> f64 {
    return if readout_rate == 1.0 {
        if preamp_gain == 1.0 { 16.1 } else { 3.89 }
    } else if readout_rate == 10.0 {
        if preamp_gain == 1.0 { 16.0 } else { 4.01 }
    } else if readout_rate == 20.0 {
        if preamp_gain == 1.0 { 16.9 } else { 4.63 }
    } else if readout_rate == 30.0 {
        if preamp_gain == 1.0 { 18.0 } else { 5.70 }
    } else {
        3.89
    };
}

#[derive(Copy, Clone, Debug)]
enum FFPrepMode {
    Bright,
    Dark,
}

impl From<String> for FFPrepMode {
    fn from(s: String) -> Self {
        return match s.as_ref() {
            "bright" => Self::Bright,
            "dark" => Self::Dark,
            _ => Self::Bright,
        };
    }
}

/// Required [`Config`] keys:
/// - `camera.read.hsspeed_idx`
/// - `camera.amp.preamp_gain_idx`
/// - `camera.amp.em_gain`
/// - `processing.count_bias`
/// - `processing.qe`
/// - `processing.ff_prep.n_loop`
/// - `processing.ff_prep.mode`
/// - `processing.ff_prep.box`
/// - `processing.ff_prep.threshold`
pub struct FFPrep {
    to_photons_params: (f64, f64),
    current_idx: usize,
    n_loop: usize,
    mode: FFPrepMode,
    roi: Vec<usize>,
    threshold: f64,
    arduino: SerialPort,
}

impl FrameProcessor for FFPrep {
    fn init(config: &Config) -> FrameProcResult<Self> {
        let hs_rate: f64
            = match
                config.get_path_s_ok_into::<i64>("camera.read.hsspeed_idx")?
            {
                0 => 30.0,
                1 => 20.0,
                2 => 10.0,
                3 => 1.0,
                _ => 1.0,
            };
        let preamp_gain: f64
            = match
                config.get_path_s_ok_into::<i64>("camera.amp.preamp_gain_idx")?
            {
                0 => 1.0,
                1 => 2.0,
                _ => 1.0,
            };
        let em_gain: f64
            = config.get_path_s_ok_into::<i64>("camera.amp.em_gain")? as f64;
        let count_bias: f64
            = config.get_path_s_ok_into("processing.count_bias")?;
        let qe: f64 = config.get_path_s_ok_into("processing.qe")?;
        let e_per_count: f64 = e_per_count(hs_rate, preamp_gain);
        let to_photons_params: (f64, f64)
            = (e_per_count * em_gain * qe, count_bias);

        let current_idx: usize = 0;
        let n_loop: usize
            = config.get_path_s_ok_into::<i64>("processing.ff_prep.n_loop")?
            as usize;
        let mode: FFPrepMode
            = config.get_path_s_ok_into::<String>("processing.ff_prep.mode")?
            .into();
        let roi: Vec<usize>
            = config.get_path_s_ok_into::<Vec<i64>>("processing.ff_prep.box")?
            .into_iter()
            .map(|x| x as usize)
            .collect();
        let threshold: f64
            = config.get_path_s_ok_into("processing.ff_prep.threshold")?;
        
        let arduino = SerialPort::open("COM4", 2e6 as u32)?;
        arduino.write(b"1100\n")?;

        return Ok(Self {
            to_photons_params,
            current_idx,
            n_loop,
            mode,
            roi,
            threshold,
            arduino,
        });
    }

    fn doit(&mut self, frame: &nd::Array2<u16>) -> Option<nd::Array2<f64>> {
        let roi: nd::Array2<f64>
            = frame.slice(s![
                self.roi[1]..self.roi[1] + self.roi[3],
                self.roi[0]..self.roi[0] + self.roi[2],
            ])
            .mapv(f64::from);
        let photons: f64
            = roi.mapv(|x| {
                (x - self.to_photons_params.1)
                    / self.to_photons_params.0
            })
            .sum();
        let is_bright = photons >= self.threshold;

        let do_pulse: bool = match self.mode {
            FFPrepMode::Bright => !is_bright || self.current_idx != 0,
            FFPrepMode::Dark => is_bright || self.current_idx != 0,
        };

        self.arduino.write(if do_pulse { b"1100\n" } else { b"1111\n" })
            .expect("ff_prep: communication with arduino failed mid-sequence");
        println!("{} : {:4.0} => {}", self.current_idx, photons, do_pulse);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
        self.current_idx = (self.current_idx + 1) % self.n_loop;
        return Some(roi);
    }

    fn deinit(self) { }
}

#[derive(Copy, Clone, Debug)]
enum FFSeriesMode {
    XOR,
    XNOR,
    Alternate,
}

impl From<String> for FFSeriesMode {
    fn from(s: String) -> Self {
        return match s.as_ref() {
            "xor" => Self::XOR,
            "xnor" => Self::XNOR,
            "alternate" => Self::Alternate,
            _ => Self::XOR,
        };
    }
}

/// Required [`Config`] keys:
/// - `camera.read.hsspeed_idx`
/// - `camera.amp.preamp_gain_idx`
/// - `camera.amp.em_gain`
/// - `processing.count_bias`
/// - `processing.qe`
/// - `processing.ff_series.n_loop`
/// - `processing.ff_series.mode`
/// - `processing.ff_series.box`
/// - `processing.ff_series.threshold`
pub struct FFSeries {
    to_photons_params: (f64, f64),
    current_idx: usize,
    n_loop: usize,
    mode: FFSeriesMode,
    roi: Vec<usize>,
    threshold: f64,
    init_state: bool,
    arduino: SerialPort,
}

impl FrameProcessor for FFSeries {
    fn init(config: &Config) -> FrameProcResult<Self> {
        let hs_rate: f64
            = match
                config.get_path_s_ok_into::<i64>("camera.read.hsspeed_idx")?
            {
                0 => 30.0,
                1 => 20.0,
                2 => 10.0,
                3 => 1.0,
                _ => 1.0,
            };
        let preamp_gain: f64
            = match
                config.get_path_s_ok_into::<i64>("camera.amp.preamp_gain_idx")?
            {
                0 => 1.0,
                1 => 2.0,
                _ => 1.0,
            };
        let em_gain: f64
            = config.get_path_s_ok_into::<i64>("camera.amp.em_gain")? as f64;
        let count_bias: f64
            = config.get_path_s_ok_into("processing.count_bias")?;
        let qe: f64 = config.get_path_s_ok_into("processing.qe")?;
        let e_per_count: f64 = e_per_count(hs_rate, preamp_gain);
        let to_photons_params: (f64, f64)
            = (e_per_count * em_gain * qe, count_bias);

        let current_idx: usize = 0;
        let n_loop: usize
            = config.get_path_s_ok_into::<i64>("processing.ff_series.n_loop")?
            as usize;
        let mode: FFSeriesMode
            = config.get_path_s_ok_into::<String>("processing.ff_series.mode")?
            .into();
        let roi: Vec<usize>
            = config.get_path_s_ok_into::<Vec<i64>>("processing.ff_series.box")?
            .into_iter()
            .map(|x| x as usize)
            .collect();
        let threshold: f64
            = config.get_path_s_ok_into("processing.ff_series.threshold")?;
        let init_state: bool = true;
        
        let arduino = SerialPort::open("COM4", 2e6 as u32)?;
        arduino.write(b"1100\n")?;

        return Ok(Self {
            to_photons_params,
            current_idx,
            n_loop,
            mode,
            roi,
            threshold,
            init_state,
            arduino,
        });
    }

    fn doit(&mut self, frame: &nd::Array2<u16>) -> Option<nd::Array2<f64>> {
        let roi: nd::Array2<f64>
            = frame.slice(s![
                self.roi[1]..self.roi[1] + self.roi[3],
                self.roi[0]..self.roi[0] + self.roi[2],
            ])
            .mapv(f64::from);
        let photons: f64
            = roi.mapv(|x| {
                (x - self.to_photons_params.1)
                    / self.to_photons_params.0
            })
            .sum();
        let is_bright = photons >= self.threshold;

        let do_pulse: bool = if self.current_idx == 0 {
            self.init_state = is_bright;
            true
        } else if self.current_idx == self.n_loop - 1 {
            true
        } else {
            match (self.mode, self.current_idx % 4) {
                (_, 0) | (_, 2) => true,
                (FFSeriesMode::XOR, 1) | (FFSeriesMode::XOR, 3) => {
                    self.init_state ^ is_bright
                },
                (FFSeriesMode::XNOR, 1) | (FFSeriesMode::XNOR, 3) => {
                    !(self.init_state ^ is_bright)
                },
                (FFSeriesMode::Alternate, 1) => {
                    !(self.init_state ^ is_bright)
                },
                (FFSeriesMode::Alternate, 3) => {
                    self.init_state ^ is_bright
                },
                _ => true,
            }
        };

        self.arduino.write(if do_pulse { b"1100\n" } else { b"1111\n" })
            .expect("ff_series: communication with arduino failed mid-sequence");
        println!("{} : {:4.0} => {}", self.current_idx, photons, do_pulse);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
        self.current_idx = (self.current_idx + 1) % self.n_loop;
        return Some(roi);
    }

    fn deinit(self) { }
}

