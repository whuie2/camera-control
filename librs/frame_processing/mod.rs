//! Provides constructs and a registry for frame processors.

use ndarray::{
    self as nd,
};
use phf::phf_map;
use thiserror::Error;
use crate::config::{
    Config,
    ConfigError,
};
pub mod window_avg;
pub mod feedforward;

#[derive(Error, Debug)]
pub enum FrameProcError {
    #[error("frame_processing: error processing config:\n{0}")]
    ConfigError(#[from] ConfigError),
    #[error("frame_processing: error communicating with serial port:\n{0}")]
    SerialError(#[from] std::io::Error),
}
pub type FrameProcResult<T> = Result<T, FrameProcError>;

/// Describes the general behavior of a frame processor.
pub trait FrameProcessor: Sized {
    /// Initialize the processor with input obtained from an external config.
    fn init(config: &Config) -> FrameProcResult<Self>;

    /// Process a frame, optionally returning a result.
    fn doit(&mut self, frame: &nd::Array2<u16>) -> Option<nd::Array2<f64>>;

    /// De-initialize `self`.
    fn deinit(self);
}

/// Selector for available frame processors.
#[derive(Copy, Clone, Debug, Hash)]
pub enum FrameProcSel {
    WindowAvg,
    FFPrep,
    FFSeries,
}

pub static LABEL_SEL: phf::Map<&'static str, FrameProcSel>
    = phf_map!{
        "window_avg" => FrameProcSel::WindowAvg,
        "ff_prep" => FrameProcSel::FFPrep,
        "ff_series" => FrameProcSel::FFSeries,
    };

/// Sum type over all available frame processors, implementing a top-level
/// interface for [`FrameProcessor`] methods.
pub enum FrameProc {
    WindowAvg(window_avg::WindowAvg),
    FFPrep(feedforward::FFPrep),
    FFSeries(feedforward::FFSeries),
}

impl FrameProc {
    /// Create a new [`window_avg::WindowAvg`].
    pub fn new_window_avg(config: &Config) -> FrameProcResult<Self> {
        return Ok(Self::WindowAvg(window_avg::WindowAvg::init(config)?));
    }

    /// Create a new [`feedforward::FFPrep`].
    pub fn new_ff_prep(config: &Config) -> FrameProcResult<Self> {
        return Ok(Self::FFPrep(feedforward::FFPrep::init(config)?));
    }

    /// Create a new [`feedforward::FFSeries`].
    pub fn new_ff_series(config: &Config) -> FrameProcResult<Self> {
        return Ok(Self::FFSeries(feedforward::FFSeries::init(config)?));
    }

    /// Initialize a processor with values obtained from an external config.
    pub fn init(proc_sel: FrameProcSel, config: &Config)
        -> FrameProcResult<Self>
    {
        return match proc_sel {
            FrameProcSel::WindowAvg => Self::new_window_avg(config),
            FrameProcSel::FFPrep => Self::new_ff_prep(config),
            FrameProcSel::FFSeries => Self::new_ff_series(config),
        };
    }

    /// Process a frame, optionally returning a result.
    pub fn doit(&mut self, frame: &nd::Array2<u16>) -> Option<nd::Array2<f64>> {
        return match self {
            Self::WindowAvg(window_avg) => window_avg.doit(frame),
            Self::FFPrep(ff_prep) => ff_prep.doit(frame),
            Self::FFSeries(ff_series) => ff_series.doit(frame),
        };
    }

    /// De-initialize `self`.
    pub fn deinit(self) { }
}

