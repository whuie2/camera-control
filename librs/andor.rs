#![allow(clippy::unnecessary_cast)]

//! Rust wrapper around functions and data provided by [`andor_sdk2`].

use std::{
    collections::HashSet,
    ffi::CStr,
    fmt,
    hash::Hash,
    io::Cursor,
    os::raw::{
        c_int, // probably i32
        c_short, // probably i16
        c_ushort, // probably u16
        c_uint, // probably u32
        c_long,
        c_ulong, // u32 on Windows, probably u64 on Linux
        c_ulonglong, // u64
        c_char, // i8 or u8
        c_uchar, // probably u8
    },
    path::{
        Path,
        PathBuf,
    },
};
use andor_sdk2::*;
use byteorder::ReadBytesExt;
use ndarray::{
    self as nd,
};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum AndorError {
    #[error("andor code: {0}")]
    Code(AndorReturn),

    #[error("UTF-8 conversion error")]
    UTF8Error,

    #[error("IRIG byte read error: {0}")]
    IRIGByteRead(#[from] std::io::Error),

    #[error("invalid acquisition mode {0:?}")]
    InvalidAcquisitionMode(AcquisitionMode),

    // #[error("invalid read mode {0:?}")]
    // InvalidReadMode(ReadMode),

    #[error("invalid trigger mode {0:?}")]
    InvalidTriggerMode(TriggerMode),

    #[error("invalid baseline offset: got {0}, but must be a multiple of 100 between -1000 and +1000")]
    InvalidBaselineOffset(i32),

    #[error("invalid number of charge shift repeats: got {0}, but must be a multiple of 2")]
    InvalidChargeShiftRepeats(usize),

    #[error("invalid DDG gate step resolution: got {0}, but must be netween 25 and 25e12 picoseconds")]
    InvalidDDGGateStepResolution(f64),
}
pub type AndorResult<T> = Result<T, AndorError>;

/// Return codes from the SDK functions.
#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum AndorReturn {
    ErrorCodes = 20001,
    Success = 20002,
    VXDNotInstalled = 20003,
    ErrorScan = 20004,
    ErrorChecksum = 20005,
    ErrorFileLoad = 20006,
    UnknownFunction = 20007,
    ErrorVXDInit = 20008,
    ErrorAddress = 20009,
    ErrorPageLock = 20010,
    ErrorPageUnlock = 20011,
    ErrorBoardTest = 20012,
    ErrorAck = 20013,
    ErrorUpFIFO = 20014,
    ErrorPattern = 20015,
    AcquisitionErrors = 20017,
    AcqBuffer = 20018,
    AcqDownFIFOFull = 20019,
    ProcUnknownInstruction = 20020,
    IllegalOpCode = 20021,
    KineticTimeNotMet = 20022,
    AccumTimeNotMet = 20023,
    NoNewData = 20024,
    KernMemError = 20025,
    SpoolError = 20026,
    SpoolSetupError = 20027,
    FileSizeLimitError = 20028,
    ErrorFileSave = 20029,
    TemperatureCodes = 20033,
    TemperatureOff = 20034,
    TemperatureNotStabilized = 20035,
    TemperatureStabilized = 20036,
    TemperatureNotReached = 20037,
    TemperatureOutRange = 20038,
    TemperatureNotSupported = 20039,
    TemperatureDrift = 20040,
    GeneralErrors = 20049,
    InvalidAux = 20050,
    COFNotLoaded = 20051,
    FPGAProg = 20052,
    FlexError = 20053,
    GPIBError = 20054,
    EEPRomVersionError = 20055,
    DataType = 20064,
    DriverErrors = 20065,
    P1Invalid = 20066,
    P2Invalid = 20067,
    P3Invalid = 20068,
    P4Invalid = 20069,
    INIError = 20070,
    COFError = 20071,
    Acquiring = 20072,
    Idle = 20073,
    TempCycle = 20074,
    NotInitialized = 20075,
    P5Invalid = 20076,
    P6Invalid = 20077,
    InvalidMode = 20078,
    InvalidFilter = 20079,
    I2CErrors = 20080,
    I2CDevNotFount = 20081,
    I2CTimeout = 20082,
    P7Invalid = 20083,
    P8Invalid = 20084,
    P9Invalid = 20085,
    P10Invalid = 20086,
    P11Invalid = 20087,
    USBError = 20089,
    IOCError = 20090,
    VRMVersionError = 20091,
    GateStepError = 20092,
    USBInterruptEndpointError = 20093,
    RandomTrackError = 20094,
    InvalidTriggerMode = 20095,
    LoadFirmwareError = 20096,
    DivideByZeroError = 20097,
    InvalidRingExposures = 20098,
    BinningError = 20099,
    InvalidAmplifier = 20100,
    InvalidCountConvertMode = 20101,
    USBInterruptEndpointTimeout = 20102,
    ErrorNoCamera = 20990,
    NotSupported = 20991,
    NotAvailable = 20992,
    ErrorMap = 20115,
    ErrorUnmap = 20116,
    ErrorMDL = 20117,
    ErrorUnMDL = 20118,
    ErrorBuffSize = 20119,
    ErrorNoHandle = 20121,
    GatingNotAvailable = 20130,
    FPGAVoltageError = 20131,
    OWCmdFail = 20150,
    OWMemoryBadAddr = 20151,
    OWCmdNotAvailable = 20152,
    OWNoSlaves = 20153,
    OWNotInitialized = 20154,
    OWErrorSlaveNum = 20155,
    MSTimingsError = 20156,
    OANullError = 20173,
    OAParsedTDError = 20174,
    OADTDValidateError = 20175,
    OAFileAccessError = 20176,
    OAFileDoesNotExist = 20177,
    OAXMLInvalidOrNotFoundError = 20178,
    OAPresetFileNotLoaded = 20179,
    OAUserFileNotLoaded = 20180,
    OAPresetAndUserFileNotLoaded = 20181,
    OAInvalidFile = 20182,
    OAFileHasBeenModified = 20183,
    OABufferNull = 20184,
    OAInvalidStringLength = 20185,
    OAInvalidCharsInName = 20186,
    OAInvalidNaming = 20187,
    OAGetCameraError = 20188,
    OAModeAlreadyExists = 20189,
    OAStringsNotEqual = 20190,
    OANoUserData = 20191,
    OAValueNotSupported = 20192,
    OAModeDoesNotExist = 20193,
    OACameraNotSupported = 20194,
    OAFailedToGetMode = 20195,
    OACameraNotAvailable = 20196,
    ProcessingFailed = 20211,
    Unknown(u32) = 0,
}

impl From<c_uint> for AndorReturn {
    fn from(ret: c_uint) -> Self {
        return match ret {
            20001 => Self::ErrorCodes,
            20002 => Self::Success,
            20003 => Self::VXDNotInstalled,
            20004 => Self::ErrorScan,
            20005 => Self::ErrorChecksum,
            20006 => Self::ErrorFileLoad,
            20007 => Self::UnknownFunction,
            20008 => Self::ErrorVXDInit,
            20009 => Self::ErrorAddress,
            20010 => Self::ErrorPageLock,
            20011 => Self::ErrorPageUnlock,
            20012 => Self::ErrorBoardTest,
            20013 => Self::ErrorAck,
            20014 => Self::ErrorUpFIFO,
            20015 => Self::ErrorPattern,
            20017 => Self::AcquisitionErrors,
            20018 => Self::AcqBuffer,
            20019 => Self::AcqDownFIFOFull,
            20020 => Self::ProcUnknownInstruction,
            20021 => Self::IllegalOpCode,
            20022 => Self::KineticTimeNotMet,
            20023 => Self::AccumTimeNotMet,
            20024 => Self::NoNewData,
            20025 => Self::KernMemError,
            20026 => Self::SpoolError,
            20027 => Self::SpoolSetupError,
            20028 => Self::FileSizeLimitError,
            20029 => Self::ErrorFileSave,
            20033 => Self::TemperatureCodes,
            20034 => Self::TemperatureOff,
            20035 => Self::TemperatureNotStabilized,
            20036 => Self::TemperatureStabilized,
            20037 => Self::TemperatureNotReached,
            20038 => Self::TemperatureOutRange,
            20039 => Self::TemperatureNotSupported,
            20040 => Self::TemperatureDrift,
            20049 => Self::GeneralErrors,
            20050 => Self::InvalidAux,
            20051 => Self::COFNotLoaded,
            20052 => Self::FPGAProg,
            20053 => Self::FlexError,
            20054 => Self::GPIBError,
            20055 => Self::EEPRomVersionError,
            20064 => Self::DataType,
            20065 => Self::DriverErrors,
            20066 => Self::P1Invalid,
            20067 => Self::P2Invalid,
            20068 => Self::P3Invalid,
            20069 => Self::P4Invalid,
            20070 => Self::INIError,
            20071 => Self::COFError,
            20072 => Self::Acquiring,
            20073 => Self::Idle,
            20074 => Self::TempCycle,
            20075 => Self::NotInitialized,
            20076 => Self::P5Invalid,
            20077 => Self::P6Invalid,
            20078 => Self::InvalidMode,
            20079 => Self::InvalidFilter,
            20080 => Self::I2CErrors,
            20081 => Self::I2CDevNotFount,
            20082 => Self::I2CTimeout,
            20083 => Self::P7Invalid,
            20084 => Self::P8Invalid,
            20085 => Self::P9Invalid,
            20086 => Self::P10Invalid,
            20087 => Self::P11Invalid,
            20089 => Self::USBError,
            20090 => Self::IOCError,
            20091 => Self::VRMVersionError,
            20092 => Self::GateStepError,
            20093 => Self::USBInterruptEndpointError,
            20094 => Self::RandomTrackError,
            20095 => Self::InvalidTriggerMode,
            20096 => Self::LoadFirmwareError,
            20097 => Self::DivideByZeroError,
            20098 => Self::InvalidRingExposures,
            20099 => Self::BinningError,
            20100 => Self::InvalidAmplifier,
            20101 => Self::InvalidCountConvertMode,
            20102 => Self::USBInterruptEndpointTimeout,
            20990 => Self::ErrorNoCamera,
            20991 => Self::NotSupported,
            20992 => Self::NotAvailable,
            20115 => Self::ErrorMap,
            20116 => Self::ErrorUnmap,
            20117 => Self::ErrorMDL,
            20118 => Self::ErrorUnMDL,
            20119 => Self::ErrorBuffSize,
            20121 => Self::ErrorNoHandle,
            20130 => Self::GatingNotAvailable,
            20131 => Self::FPGAVoltageError,
            20150 => Self::OWCmdFail,
            20151 => Self::OWMemoryBadAddr,
            20152 => Self::OWCmdNotAvailable,
            20153 => Self::OWNoSlaves,
            20154 => Self::OWNotInitialized,
            20155 => Self::OWErrorSlaveNum,
            20156 => Self::MSTimingsError,
            20173 => Self::OANullError,
            20174 => Self::OAParsedTDError,
            20175 => Self::OADTDValidateError,
            20176 => Self::OAFileAccessError,
            20177 => Self::OAFileDoesNotExist,
            20178 => Self::OAXMLInvalidOrNotFoundError,
            20179 => Self::OAPresetFileNotLoaded,
            20180 => Self::OAUserFileNotLoaded,
            20181 => Self::OAPresetAndUserFileNotLoaded,
            20182 => Self::OAInvalidFile,
            20183 => Self::OAFileHasBeenModified,
            20184 => Self::OABufferNull,
            20185 => Self::OAInvalidStringLength,
            20186 => Self::OAInvalidCharsInName,
            20187 => Self::OAInvalidNaming,
            20188 => Self::OAGetCameraError,
            20189 => Self::OAModeAlreadyExists,
            20190 => Self::OAStringsNotEqual,
            20191 => Self::OANoUserData,
            20192 => Self::OAValueNotSupported,
            20193 => Self::OAModeDoesNotExist,
            20194 => Self::OACameraNotSupported,
            20195 => Self::OAFailedToGetMode,
            20196 => Self::OACameraNotAvailable,
            20211 => Self::ProcessingFailed,
            n => Self::Unknown(n),
        };
    }
}

macro_rules! code_to_ok {
    ( $expr:expr ) => {
        match $expr.into() {
            AndorReturn::Success => Ok(()),
            x => Err(AndorError::Code(x)),
        }
    };
    ( $expr:expr => ok: { $( $ok:expr ),* $(,)? } ) => {
        match $expr.into() {
            $(
                $ok => Ok(()),
            )*
            x => Err(AndorError::Code(x)),
        }
    };
}

impl fmt::Display for AndorReturn {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return match self {
            Self::ErrorCodes => write!(f, "ErrorCodes"),
            Self::Success => write!(f, "Success"),
            Self::VXDNotInstalled => write!(f, "VXDNotInstalled"),
            Self::ErrorScan => write!(f, "ErrorScan"),
            Self::ErrorChecksum => write!(f, "ErrorChecksum"),
            Self::ErrorFileLoad => write!(f, "ErrorFileLoad"),
            Self::UnknownFunction => write!(f, "UnknownFunction"),
            Self::ErrorVXDInit => write!(f, "ErrorVXDInit"),
            Self::ErrorAddress => write!(f, "ErrorAddress"),
            Self::ErrorPageLock => write!(f, "ErrorPageLock"),
            Self::ErrorPageUnlock => write!(f, "ErrorPageUnlock"),
            Self::ErrorBoardTest => write!(f, "ErrorBoardTest"),
            Self::ErrorAck => write!(f, "ErrorAck"),
            Self::ErrorUpFIFO => write!(f, "ErrorUpFIFO"),
            Self::ErrorPattern => write!(f, "ErrorPattern"),
            Self::AcquisitionErrors => write!(f, "AcquisitionErrors"),
            Self::AcqBuffer => write!(f, "AcqBuffer"),
            Self::AcqDownFIFOFull => write!(f, "AcqDownFIFOFull"),
            Self::ProcUnknownInstruction => write!(f, "ProcUnknownInstruction"),
            Self::IllegalOpCode => write!(f, "IllegalOpCode"),
            Self::KineticTimeNotMet => write!(f, "KineticTimeNotMet"),
            Self::AccumTimeNotMet => write!(f, "AccumTimeNotMet"),
            Self::NoNewData => write!(f, "NoNewData"),
            Self::KernMemError => write!(f, "KernMemError"),
            Self::SpoolError => write!(f, "SpoolError"),
            Self::SpoolSetupError => write!(f, "SpoolSetupError"),
            Self::FileSizeLimitError => write!(f, "FileSizeLimitError"),
            Self::ErrorFileSave => write!(f, "ErrorFileSave"),
            Self::TemperatureCodes => write!(f, "TemperatureCodes"),
            Self::TemperatureOff => write!(f, "TemperatureOff"),
            Self::TemperatureNotStabilized => write!(f, "TemperatureNotStabilized"),
            Self::TemperatureStabilized => write!(f, "TemperatureStabilized"),
            Self::TemperatureNotReached => write!(f, "TemperatureNotReached"),
            Self::TemperatureOutRange => write!(f, "TemperatureOutRange"),
            Self::TemperatureNotSupported => write!(f, "TemperatureNotSupported"),
            Self::TemperatureDrift => write!(f, "TemperatureDrift"),
            Self::GeneralErrors => write!(f, "GeneralErrors"),
            Self::InvalidAux => write!(f, "InvalidAux"),
            Self::COFNotLoaded => write!(f, "COFNotLoaded"),
            Self::FPGAProg => write!(f, "FPGAProg"),
            Self::FlexError => write!(f, "FlexError"),
            Self::GPIBError => write!(f, "GPIBError"),
            Self::EEPRomVersionError => write!(f, "EEPRomVersionError"),
            Self::DataType => write!(f, "DataType"),
            Self::DriverErrors => write!(f, "DriverErrors"),
            Self::P1Invalid => write!(f, "P1Invalid"),
            Self::P2Invalid => write!(f, "P2Invalid"),
            Self::P3Invalid => write!(f, "P3Invalid"),
            Self::P4Invalid => write!(f, "P4Invalid"),
            Self::INIError => write!(f, "INIError"),
            Self::COFError => write!(f, "COFError"),
            Self::Acquiring => write!(f, "Acquiring"),
            Self::Idle => write!(f, "Idle"),
            Self::TempCycle => write!(f, "TempCycle"),
            Self::NotInitialized => write!(f, "NotInitialized"),
            Self::P5Invalid => write!(f, "P5Invalid"),
            Self::P6Invalid => write!(f, "P6Invalid"),
            Self::InvalidMode => write!(f, "InvalidMode"),
            Self::InvalidFilter => write!(f, "InvalidFilter"),
            Self::I2CErrors => write!(f, "I2CErrors"),
            Self::I2CDevNotFount => write!(f, "I2CDevNotFount"),
            Self::I2CTimeout => write!(f, "I2CTimeout"),
            Self::P7Invalid => write!(f, "P7Invalid"),
            Self::P8Invalid => write!(f, "P8Invalid"),
            Self::P9Invalid => write!(f, "P9Invalid"),
            Self::P10Invalid => write!(f, "P10Invalid"),
            Self::P11Invalid => write!(f, "P11Invalid"),
            Self::USBError => write!(f, "USBError"),
            Self::IOCError => write!(f, "IOCError"),
            Self::VRMVersionError => write!(f, "VRMVersionError"),
            Self::GateStepError => write!(f, "GateStepError"),
            Self::USBInterruptEndpointError => write!(f, "USBInterruptEndpointError"),
            Self::RandomTrackError => write!(f, "RandomTrackError"),
            Self::InvalidTriggerMode => write!(f, "InvalidTriggerMode"),
            Self::LoadFirmwareError => write!(f, "LoadFirmwareError"),
            Self::DivideByZeroError => write!(f, "DivideByZeroError"),
            Self::InvalidRingExposures => write!(f, "InvalidRingExposures"),
            Self::BinningError => write!(f, "BinningError"),
            Self::InvalidAmplifier => write!(f, "InvalidAmplifier"),
            Self::InvalidCountConvertMode => write!(f, "InvalidCountConvertMode"),
            Self::USBInterruptEndpointTimeout => write!(f, "USBInterruptEndpointTimeout"),
            Self::ErrorNoCamera => write!(f, "ErrorNoCamera"),
            Self::NotSupported => write!(f, "NotSupported"),
            Self::NotAvailable => write!(f, "NotAvailable"),
            Self::ErrorMap => write!(f, "ErrorMap"),
            Self::ErrorUnmap => write!(f, "ErrorUnmap"),
            Self::ErrorMDL => write!(f, "ErrorMDL"),
            Self::ErrorUnMDL => write!(f, "ErrorUnMDL"),
            Self::ErrorBuffSize => write!(f, "ErrorBuffSize"),
            Self::ErrorNoHandle => write!(f, "ErrorNoHandle"),
            Self::GatingNotAvailable => write!(f, "GatingNotAvailable"),
            Self::FPGAVoltageError => write!(f, "FPGAVoltageError"),
            Self::OWCmdFail => write!(f, "OWCmdFail"),
            Self::OWMemoryBadAddr => write!(f, "OWMemoryBadAddr"),
            Self::OWCmdNotAvailable => write!(f, "OWCmdNotAvailable"),
            Self::OWNoSlaves => write!(f, "OWNoSlaves"),
            Self::OWNotInitialized => write!(f, "OWNotInitialized"),
            Self::OWErrorSlaveNum => write!(f, "OWErrorSlaveNum"),
            Self::MSTimingsError => write!(f, "MSTimingsError"),
            Self::OANullError => write!(f, "OANullError"),
            Self::OAParsedTDError => write!(f, "OAParsedTDError"),
            Self::OADTDValidateError => write!(f, "OADTDValidateError"),
            Self::OAFileAccessError => write!(f, "OAFileAccessError"),
            Self::OAFileDoesNotExist => write!(f, "OAFileDoesNotExist"),
            Self::OAXMLInvalidOrNotFoundError => write!(f, "OAXMLInvalidOrNotFoundError"),
            Self::OAPresetFileNotLoaded => write!(f, "OAPresetFileNotLoaded"),
            Self::OAUserFileNotLoaded => write!(f, "OAUserFileNotLoaded"),
            Self::OAPresetAndUserFileNotLoaded => write!(f, "OAPresetAndUserFileNotLoaded"),
            Self::OAInvalidFile => write!(f, "OAInvalidFile"),
            Self::OAFileHasBeenModified => write!(f, "OAFileHasBeenModified"),
            Self::OABufferNull => write!(f, "OABufferNull"),
            Self::OAInvalidStringLength => write!(f, "OAInvalidStringLength"),
            Self::OAInvalidCharsInName => write!(f, "OAInvalidCharsInName"),
            Self::OAInvalidNaming => write!(f, "OAInvalidNaming"),
            Self::OAGetCameraError => write!(f, "OAGetCameraError"),
            Self::OAModeAlreadyExists => write!(f, "OAModeAlreadyExists"),
            Self::OAStringsNotEqual => write!(f, "OAStringsNotEqual"),
            Self::OANoUserData => write!(f, "OANoUserData"),
            Self::OAValueNotSupported => write!(f, "OAValueNotSupported"),
            Self::OAModeDoesNotExist => write!(f, "OAModeDoesNotExist"),
            Self::OACameraNotSupported => write!(f, "OACameraNotSupported"),
            Self::OAFailedToGetMode => write!(f, "OAFailedToGetMode"),
            Self::OACameraNotAvailable => write!(f, "OACameraNotAvailable"),
            Self::ProcessingFailed => write!(f, "ProcessingFailed"),
            Self::Unknown(n) => write!(f, "Unknown: {}", n),
        };
    }
}

/// Aborts the current acquisition if one is active.
///
/// Returns an `Err` if the system is not currently acquiring.
pub fn abort_acquisition() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(AbortAcquisition());
    }
}

/// On/off cooler mode, used for [`set_cooler_mode`].
#[derive(Copy, Clone, Debug)]
pub enum CoolerMode {
    Off = 0,
    On = 1,
}

/// Turn the cooler on/off.
pub fn set_cooler_mode(mode: CoolerMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(
            match mode {
                CoolerMode::On => CoolerON(),
                CoolerMode::Off => CoolerOFF(),
            }
        );
    }
}

/// Restarts a sleeping thread within the [`wait_for_acquisition`] function.
///
/// The thread will return from [`wait_for_acquisition`] with a non-success
/// code.
pub fn cancel_wait() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(CancelWait());
    }
}

/// On/off keep-clean mode, used for [`set_keep_clean_mode`].
#[derive(Copy, Clone, Debug)]
pub enum KeepCleanMode {
    Off = 0,
    On = 1,
}

/// Turn on/off keep-clean cycles between acquisitions.
pub fn set_keep_clean_mode(mode: KeepCleanMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(EnableKeepCleans(mode as c_int));
    }
}

/// On/off sensor compensation mode, used for [`set_sensor_compensation`]
#[derive(Copy, Clone, Debug)]
pub enum SensorCompensationMode {
    Off = 0,
    On = 1,
}

/// Turn on/off sensor compensation mode.
pub fn set_sensor_compensation(mode: SensorCompensationMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(EnableSensorCompensation(mode as c_int));
    }
}

/// Deallocate any memory used to store previously acquired data.
pub fn free_internal_memory() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(FreeInternalMemory());
    }
}

/// Get data from the last acquisition as a 2D array of `i32`s.
pub fn get_acquired_data(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<i32>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<c_int>
            = nd::Array::zeros(image_shape);
        code_to_ok!(
            GetAcquiredData(data.as_mut_ptr(), size as c_uint)
        )?;
        return Ok(data.mapv(|x| x as i32));
    }
}

/// Get data from the last acquisition as a 2D array of `u16`s.
pub fn get_acquired_data_16(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<u16>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<c_ushort>
            = nd::Array::zeros(image_shape);
        code_to_ok!(
            GetAcquiredData16(data.as_mut_ptr(), size as c_uint)
        )?;
        return Ok(data.mapv(|x| x as u16));
    }
}

/// Get data from the last acquisition as a 2D array of `f32`s.
pub fn get_acquired_data_float(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<f32>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<f32>
            = nd::Array::zeros(image_shape);
        code_to_ok!(
            GetAcquiredFloatData(data.as_mut_ptr(), size as c_uint)
        )?;
        return Ok(data);
    }
}

/// Number of accumulations in a scan and number of scans in a series completed
/// in the current acquisition.
#[derive(Copy, Clone, Debug)]
pub struct AcquisitionProgress {
    pub accumulations: usize,
    pub kinetic_scans: usize,
}

/// Get the number of scans of accumulations completed in the current
/// acquisition. Can be called at any point during the acquisition.
pub fn get_acquisition_progress() -> AndorResult<AcquisitionProgress> {
    unsafe {
        let mut accumulations: c_int = 0;
        let mut kinetic_scans: c_int = 0;
        code_to_ok!(
            GetAcquisitionProgress(
                &mut accumulations,
                &mut kinetic_scans,
            )
        )?;
        let acc_prog = AcquisitionProgress {
            accumulations: accumulations as usize,
            kinetic_scans: kinetic_scans as usize,
        };
        return Ok(acc_prog);
    }
}

/// Data struct to hold timings, in seconds, related to data acquisition.
#[derive(Copy, Clone, Debug)]
pub struct AcquisitionTimings {
    pub exposure: f32,
    pub accumulate: f32,
    pub kinetic: f32,
}

/// Get the current acquisition timing information.
///
/// Should be used after all acquisition settings have been set using
/// [`set_exposure_time`], [`set_kinetic_cycle_time`], [`set_read_mode`], etc.
/// The values returned are the actual times used in following acquisitions.
pub fn get_acquisition_timings() -> AndorResult<AcquisitionTimings> {
    unsafe {
        let mut exposure: f32 = 0.0;
        let mut accumulate: f32 = 0.0;
        let mut kinetic: f32 = 0.0;
        code_to_ok!(
            GetAcquisitionTimings(
                &mut exposure,
                &mut accumulate,
                &mut kinetic,
            )
        )?;
        let acc_timings = AcquisitionTimings {
            exposure,
            accumulate,
            kinetic,
        };
        return Ok(acc_timings);
    }
}

/// Get a description of the camera's `index`-th amplifier.
///
/// Descriptions are at most 21 characters long.
pub fn get_amp_description(index: usize) -> AndorResult<String> {
    unsafe {
        // maximum number of characters needed for an amp description is 21
        // let mut s: Vec<c_char> = Vec::with_capacity(21);
        let mut s: [c_char; 21] = [0; 21];
        code_to_ok!(GetAmpDesc(index as c_int, s.as_mut_ptr(), 21))?;
        let s: String
            = CStr::from_ptr(s.as_ptr())
            .to_str()
            .map_err(|_| AndorError::UTF8Error)?
            .to_string();
        return Ok(s.trim().to_string());
    }
}

/// Get the maximum available horizontal shift speed for the `index`-th
/// amplifier.
pub fn get_amp_max_speed(index: usize) -> AndorResult<f32> {
    unsafe {
        let mut speed: f32 = 0.0;
        code_to_ok!(GetAmpMaxSpeed(index as c_int, &mut speed))?;
        return Ok(speed);
    }
}

/// Get the number of Andor cameras currently installed.
///
/// Can be called before any camera has been initialized.
pub fn get_available_cameras() -> AndorResult<usize> {
    unsafe {
        let mut n: c_int = 0;
        code_to_ok!(GetAvailableCameras(&mut n))?;
        return Ok(n as usize);
    }
}

/// Current status of the baseline clamp functionality.
#[derive(Copy, Clone, Debug)]
pub enum BaselineClampMode {
    Off = 0,
    On = 1,
}

/// Get the current status of the baseline clamp functionality.
pub fn get_baseline_clamp_mode() -> AndorResult<BaselineClampMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetBaselineClamp(&mut mode))?;
        let mode: BaselineClampMode
            = match mode {
                0 => BaselineClampMode::Off,
                _ => BaselineClampMode::On,
            };
        return Ok(mode);
    }
}

/// Get the size in bits of the dynamic range for any available AD channel.
pub fn get_bit_depth(channel: usize) -> AndorResult<usize> {
    unsafe {
        let mut depth: c_int = 0;
        code_to_ok!(GetBitDepth(channel as c_int, &mut depth))?;
        return Ok(depth as usize);
    }
}

/// Holds the identifier used by the Andor library to refer to a specific
/// camera. This is generally different from the camera's index.
#[derive(Copy, Clone, Debug)]
pub struct CameraHandle(pub i32);

/// Get the handle used by the Andor library to refer to the `index`-th camera.
pub fn get_camera_handle(index: usize) -> AndorResult<CameraHandle> {
    unsafe {
        let mut handle: c_int = 0;
        code_to_ok!(GetCameraHandle(index as c_int, &mut handle))?;
        return Ok(CameraHandle(handle as i32));
    }
}

/// Data struct to hold connection/initialization information about a specific
/// camera.
#[derive(Copy, Clone, Debug)]
pub struct CameraInformation {
    pub camera_present: bool,
    pub dlls_loaded: bool,
    pub camera_initialized: bool,
}

/// Get connection/initialization information about the `index`-th camera.
pub fn get_camera_information(index: usize) -> AndorResult<CameraInformation> {
    unsafe {
        let mut info: c_int = 0;
        code_to_ok!(GetCameraInformation(index as c_int, &mut info))?;
        let camera_present: bool = (info >> 1) % 2 == 1;
        let dlls_loaded: bool = (info >> 2) % 2 == 1;
        let camera_initialized: bool = (info >> 3) % 2 == 1;
        let info = CameraInformation {
            camera_present,
            dlls_loaded,
            camera_initialized,
        };
        return Ok(info);
    }
}

/// Get the current camera's serial number.
pub fn get_camera_serial_number() -> AndorResult<usize> {
    unsafe {
        let mut serial: c_int = 0;
        code_to_ok!(GetCameraSerialNumber(&mut serial))?;
        return Ok(serial as usize);
    }
}

/// Data struct to hold information about a camera's capabilities.
#[derive(Clone, Debug)]
pub struct Capabilities {
    /// Available data acquisition modes.
    pub acquisition_modes: HashSet<AcquisitionCapabilities>,
    /// Available readout modes.
    pub read_modes: HashSet<ReadMode>,
    /// Available Frame Transfer readout modes.
    pub ft_read_modes: HashSet<FTReadMode>,
    /// Available trigger modes.
    pub trigger_modes: HashSet<TriggerCapabilities>,
    /// Camera model.
    pub camera_type: CameraType,
    /// Available pixel modes.
    pub pixel_modes: HashSet<PixelMode>,
    /// Available getter functions.
    pub get_functions: HashSet<GetFunction>,
    /// Available setter functions.
    pub set_functions: HashSet<SetFunction>,
    /// Available features.
    pub features: HashSet<Feature>,
    /// Available (extended) features.
    pub features2: HashSet<Feature2>,
    /// Maximum speed in Hz the PCI controller is capable of.
    pub pci_card: PCISpeed,
    /// Available EM gain modes.
    pub em_gain_modes: HashSet<EMGainMode>,
}

/// Acquisition capabilities.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum AcquisitionCapabilities {
    Single = 0,
    Video = 1,
    Accumulate = 2,
    Kinetic = 3,
    FrameTransfer = 4,
    FastKinetics = 5,
    Overlap = 6,
}

impl AcquisitionCapabilities {
    fn decode(n: c_uint) -> HashSet<AcquisitionCapabilities> {
        let mut ret: HashSet<AcquisitionCapabilities> = HashSet::new();
        for mode in [
            AcquisitionCapabilities::Single,
            AcquisitionCapabilities::Video,
            AcquisitionCapabilities::Accumulate,
            AcquisitionCapabilities::Kinetic,
            AcquisitionCapabilities::FrameTransfer,
            AcquisitionCapabilities::FastKinetics,
            AcquisitionCapabilities::Overlap,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Available readout modes.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum ReadMode {
    FullImage = 0,
    SubImage = 1,
    SingleTrack = 2,
    FVB = 3,
    MultiTrack = 4,
    RandomTrack = 5,
}

impl ReadMode {
    fn decode(n: c_uint) -> HashSet<ReadMode> {
        let mut ret: HashSet<ReadMode> = HashSet::new();
        for mode in [
            ReadMode::FullImage,
            ReadMode::SubImage,
            ReadMode::SingleTrack,
            ReadMode::FVB,
            ReadMode::MultiTrack,
            ReadMode::RandomTrack,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Available Frame Transfer readout modes.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum FTReadMode {
    FullImage = 0,
    SubImage = 1,
    SingleTrack = 2,
    FVB = 3,
    MultiTrack = 4,
    RandomTrack = 5,
}

impl FTReadMode {
    fn decode(n: c_uint) -> HashSet<FTReadMode> {
        let mut ret: HashSet<FTReadMode> = HashSet::new();
        for mode in [
            FTReadMode::FullImage,
            FTReadMode::SubImage,
            FTReadMode::SingleTrack,
            FTReadMode::FVB,
            FTReadMode::MultiTrack,
            FTReadMode::RandomTrack,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Trigger capabilities.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum TriggerCapabilities {
    Internal = 0,
    External = 1,
    ExternalFVBEM = 2,
    Continuous = 3,
    ExternalStart = 4,
    ExternalExposure = 5,
    Inverted = 6,
    ExternalChargeShifting = 7,
}

impl TriggerCapabilities {
    fn decode(n: c_uint) -> HashSet<TriggerCapabilities> {
        let mut ret: HashSet<TriggerCapabilities> = HashSet::new();
        for mode in [
            TriggerCapabilities::Internal,
            TriggerCapabilities::External,
            TriggerCapabilities::ExternalFVBEM,
            TriggerCapabilities::Continuous,
            TriggerCapabilities::ExternalStart,
            TriggerCapabilities::ExternalExposure,
            TriggerCapabilities::Inverted,
            TriggerCapabilities::ExternalChargeShifting,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Camera model.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
#[repr(u32)]
pub enum CameraType {
    PDA = 0,
    IXon = 1,
    ICCD = 2,
    EMCCD = 3,
    CCD = 4,
    IStar = 5,
    Video = 6,
    IDus = 7,
    Newton = 8,
    Surcam = 9,
    USBICCD = 10,
    Luca = 11,
    Reserved = 12,
    IKon = 13,
    InGaAs = 14,
    IVac = 15,
    Clara = 17,
    USBIStar = 18,
    IXonUltra = 21,
    IVacCCD = 23,
    IKonXL = 28,
    IStarSCMOS = 30,
    IKonLR = 31,
    Unknown(u32) = 32,
}

impl From<c_uint> for CameraType {
    fn from(x: c_uint) -> Self {
        return match x {
            0 => Self::PDA,
            1 => Self::IXon,
            2 => Self::ICCD,
            3 => Self::EMCCD,
            4 => Self::CCD,
            5 => Self::IStar,
            6 => Self::Video,
            7 => Self::IDus,
            8 => Self::Newton,
            9 => Self::Surcam,
            10 => Self::USBICCD,
            11 => Self::Luca,
            12 => Self::Reserved,
            13 => Self::IKon,
            14 => Self::InGaAs,
            15 => Self::IVac,
            17 => Self::Clara,
            18 => Self::USBIStar,
            21 => Self::IXonUltra,
            23 => Self::IVacCCD,
            28 => Self::IKonXL,
            30 => Self::IStarSCMOS,
            31 => Self::IKonLR,
            n => Self::Unknown(n),
        };
    }
}

/// Available pixel modes.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum PixelMode {
    U8Bit = 0,
    U14Bit = 1,
    U16Bit = 2,
    U32Bit = 3,
    Mono = 4,
    RGB = 5,
    CMY = 6,
}

impl PixelMode {
    fn decode(n: c_uint) -> HashSet<PixelMode> {
        let mut ret: HashSet<PixelMode> = HashSet::new();
        for mode in [
            PixelMode::U8Bit,
            PixelMode::U14Bit,
            PixelMode::U16Bit,
            PixelMode::U32Bit,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        match n >> 16 {
            0 => { ret.insert(PixelMode::Mono); },
            1 => { ret.insert(PixelMode::RGB); },
            2 => { ret.insert(PixelMode::CMY); },
            _ => { },
        }
        return ret;
    }
}

/// Available setter functions.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum SetFunction {
    VReadout = 0, // SetVSSpeed
    HReadout = 1, // SetHSSpeed
    Temperature = 2, // SetTemperature
    MCPGain = 3, // SetMCPGain
    EMCCDGain = 4, // SetEMCCDGain
    BaselineClamp = 5, // SetBaselineClamp
    VSAmplitude = 6, // SetVSAmplitude
    HighCapacity = 7, // SetHighCapacity
    BaselineOffset = 8, // SetBaselineOffset
    PreampGain = 9, // SetPreAmpGain
    CropMode = 10, // SetCropMode or SetIsolatedCropMode
    DMAParameters = 11, // SetDMAParameters
    HorizontalBin = 12, // set horizontal binning for the relative read mode
    MultiTrackHRange = 13, // SetMultiTrackHRange
    RandomTrackNoGaps = 14, // SetRandomTracks
    EMAdvanced = 15, // SetEMAdvanced
    GateMode = 16, // SetGateMode
    DDGTimes = 17, // SetDDGGateTime
    IOC = 18, // SetDDGIOC
    Intelligate = 19, // SetDDGIntelligate
    InsertionDelay = 20, // SetDDGInsertionDelay
    GateStep = 21, // SetDDGGateStep
    TriggerTermination = 22, // SetExternalTriggerTermination
    ExtendedNIR = 23, // SetHSSpeed
    SpoolThreadCount = 24, // SetSpoolThreadCount
    RegisterPack = 25, // SetReadoutRegisterPacking
    Prescans = 26, // SetNumberPrescans
    GateWidthStep = 27, // SetDDGWidthStepCoefficients
    ExtendedCropMode = 28, // SetIsolatedCropModeEx
    SuperKinetics = 29, // SetFastKineticsStorageMode
    TimeScan = 30, // SetFastKineticsTimeScanMode
    CropModeType = 31, // SetIsolatedCropModeType
}

impl SetFunction {
    fn decode(n: c_uint) -> HashSet<SetFunction> {
        let mut ret: HashSet<SetFunction> = HashSet::new();
        for mode in [
            SetFunction::VReadout,
            SetFunction::HReadout,
            SetFunction::Temperature,
            SetFunction::MCPGain,
            SetFunction::EMCCDGain,
            SetFunction::BaselineClamp,
            SetFunction::VSAmplitude,
            SetFunction::HighCapacity,
            SetFunction::BaselineOffset,
            SetFunction::PreampGain,
            SetFunction::CropMode,
            SetFunction::DMAParameters,
            SetFunction::HorizontalBin,
            SetFunction::MultiTrackHRange,
            SetFunction::RandomTrackNoGaps,
            SetFunction::EMAdvanced,
            SetFunction::GateMode,
            SetFunction::DDGTimes,
            SetFunction::IOC,
            SetFunction::Intelligate,
            SetFunction::InsertionDelay,
            SetFunction::GateStep,
            SetFunction::TriggerTermination,
            SetFunction::ExtendedNIR,
            SetFunction::SpoolThreadCount,
            SetFunction::RegisterPack,
            SetFunction::Prescans,
            SetFunction::GateWidthStep,
            SetFunction::ExtendedCropMode,
            SetFunction::SuperKinetics,
            SetFunction::TimeScan,
            SetFunction::CropModeType,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Available getter functions.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum GetFunction {
    Temperature = 0, // GetTemperature
    TemperatureRange = 2, // GetTemperatureRange
    DetectorSize = 3, // GetDetector
    MCPGain = 4, // [reserved]
    EMCCDGain = 5, // GetEMCCDGain
    GateMode = 7, // GetGateMode
    DDGTimes = 8, // GetDDGGateTime
    IOC = 9, // GetDDGIOC
    Intelligate = 10, // GetDDGIntelligate
    InsertionDelay = 11, // GetDDGInsertionDelay
    PhosphorStatus = 13, // GetPhosphorStatus
    BaselineClamp = 15, // GetBaselineClamp
}

impl GetFunction {
    fn decode(n: c_uint) -> HashSet<GetFunction> {
        let mut ret: HashSet<GetFunction> = HashSet::new();
        for mode in [
            GetFunction::Temperature,
            GetFunction::TemperatureRange,
            GetFunction::DetectorSize,
            GetFunction::MCPGain,
            GetFunction::EMCCDGain,
            GetFunction::GateMode,
            GetFunction::DDGTimes,
            GetFunction::IOC,
            GetFunction::Intelligate,
            GetFunction::InsertionDelay,
            GetFunction::PhosphorStatus,
            GetFunction::BaselineClamp,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Available features.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Feature {
    Polling = 0, // current acquisition status via GetStatus
    Events = 1, // accepts Windows Events via SetDriverEvent
    Spooling = 2, // spool to disk via SetSpool
    Shutter = 3, // shutter settings via SetShutter
    ShutterEx = 4, // shutter settings via SetShutterEx
    ExternalI2C = 5, // camera has a dedicated external I2C bus
    SaturationEvent = 6, // sensor saturation via SetSaturationEvent
    FanControl = 7, // fan settings via SetFanMode
    MidFanControl = 8, // low fan mode via SetFanMode
    TempDuringAcquisition = 9, // temperature during acquisition via GetTemperature
    KeepCleanControl = 10, // can turn off keep cleans between scans
    DDGLite = 11, // [reserved]
    FTExternalExposure = 12, // can combine Frame Transfer and External Exposure
    KineticExternalExposure = 13, // External Exposure is available with Kinetic acquisition
    DACControl = 14, // [reserved]
    Metadata = 15, // metadata via SetMetaData
    IOControl = 16, // configurable IO's via SetIOLevel
    PhotonCounting = 17, // photon counting via SetPhotonCounting
    CountConvert = 18, // Count Convert via SetCountConvertMode
    DualMode = 19, // dual exposure mode via SetDualExposureMode
    OptAcquire = 20, // OptAcquire features via OA_Initialize
    RealTimeSpurNoiseFilter = 21, // real-time noise filtering via Filter_SetMode
    PostProcSpurNoiseFilter = 22, // post-processing noise filtering via PostProcessNoiseFilter
    DualPreampGain = 23, // [reserved]
    DefectCorrection = 24, // [reserved]
    StartOfExposureEvent = 25, // start-of-exposure events via SetDriverEvent
    EndOfExposureEvent = 26, // end-of-exposure events via SetDriverEvent
    CameraLink = 27, // Cameralink controllable via SetCameraLinkMode
    FIFOFullEvent = 28, // FIFO-full events via SetDriverEvent
    SensorPortConfig = 29, // multiple sensor port outputs via SetSensorPortMode
    SensorCompensation = 30, // on-campera sensor compensation via EnableSensorCompensation
    IRIGSupport = 31, // external IRIG device via SetIRIGModulation
}

impl Feature {
    fn decode(n: c_uint) -> HashSet<Feature> {
        let mut ret: HashSet<Feature> = HashSet::new();
        for mode in [
            Feature::Polling,
            Feature::Events,
            Feature::Spooling,
            Feature::Shutter,
            Feature::ShutterEx,
            Feature::ExternalI2C,
            Feature::SaturationEvent,
            Feature::FanControl,
            Feature::MidFanControl,
            Feature::TempDuringAcquisition,
            Feature::KeepCleanControl,
            Feature::DDGLite,
            Feature::FTExternalExposure,
            Feature::KineticExternalExposure,
            Feature::DACControl,
            Feature::Metadata,
            Feature::IOControl,
            Feature::PhotonCounting,
            Feature::CountConvert,
            Feature::DualMode,
            Feature::OptAcquire,
            Feature::RealTimeSpurNoiseFilter,
            Feature::PostProcSpurNoiseFilter,
            Feature::DualPreampGain,
            Feature::DefectCorrection,
            Feature::StartOfExposureEvent,
            Feature::EndOfExposureEvent,
            Feature::CameraLink,
            Feature::FIFOFullEvent,
            Feature::SensorPortConfig,
            Feature::SensorCompensation,
            Feature::IRIGSupport,
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Available (extended) features.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Feature2 {
    ESDEvent = 0, // detection of ESD events via SetESDEvent
}

impl Feature2 {
    fn decode(n: c_uint) -> HashSet<Feature2> {
        let mut ret: HashSet<Feature2> = HashSet::new();
        for mode in [
            Feature2::ESDEvent
        ].into_iter()
        {
            if (n >> mode as c_uint) % 2 == 1 {
                ret.insert(mode);
            }
        }
        return ret;
    }
}

/// Maximum speed in Hz the PCI controller is capable of.
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct PCISpeed(pub f32);

/// Available EM gain modes.
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum EMGainMode {
    U8Bit = 0, // 8-bit DAC settable
    U12Bit = 1, // 12-bit DAC settable
    Linear12 = 2, // linear gain scale (12-bit DAC internally)
    Real12 = 3, // real EM gain scale (12-bit DAC internally)
}

impl EMGainMode {
    fn decode(n: c_uint) -> HashSet<EMGainMode> {
        let mut ret: HashSet<EMGainMode> = HashSet::new();
        for cap in [
            EMGainMode::U8Bit,
            EMGainMode::U12Bit,
            EMGainMode::Linear12,
            EMGainMode::Real12,
        ].into_iter()
        {
            if (n >> cap as c_uint) % 2 == 1 {
                ret.insert(cap);
            }
        }
        return ret;
    }
}

/// Get a list of all available camera getters, setters, features, modes, etc.
pub fn get_capabilities() -> AndorResult<Capabilities> {
    unsafe {
        let mut caps = ANDORCAPS {
            ulSize: std::mem::size_of::<ANDORCAPS>() as c_uint,
            ulAcqModes: 0,
            ulReadModes: 0,
            ulTriggerModes: 0,
            ulCameraType: 0,
            ulPixelMode: 0,
            ulSetFunctions: 0,
            ulGetFunctions: 0,
            ulFeatures: 0,
            ulPCICard: 0,
            ulEMGainCapability: 0,
            ulFTReadModes: 0,
            ulFeatures2: 0,
        };
        code_to_ok!(GetCapabilities(&mut caps))?;
        let caps = Capabilities {
            acquisition_modes: AcquisitionCapabilities::decode(caps.ulAcqModes),
            read_modes: ReadMode::decode(caps.ulReadModes),
            ft_read_modes: FTReadMode::decode(caps.ulFTReadModes),
            trigger_modes: TriggerCapabilities::decode(caps.ulTriggerModes),
            camera_type: caps.ulCameraType.into(),
            pixel_modes: PixelMode::decode(caps.ulPixelMode),
            get_functions: GetFunction::decode(caps.ulGetFunctions),
            set_functions: SetFunction::decode(caps.ulSetFunctions),
            features: Feature::decode(caps.ulFeatures),
            features2: Feature2::decode(caps.ulFeatures2),
            pci_card: PCISpeed(caps.ulPCICard as f32),
            em_gain_modes: EMGainMode::decode(caps.ulEMGainCapability),
        };
        return Ok(caps);
    }
}

/// Get the type of PCI controller card currently installed. Not applicable for
/// USB systems.
///
/// Card model names are at most 10 characters long.
pub fn get_controller_card_model() -> AndorResult<String> {
    unsafe {
        // maximum number of characters needed is 10
        // let mut s: Vec<c_char> = Vec::with_capacity(10);
        let mut s: [c_char; 10] = [0; 10];
        code_to_ok!(GetControllerCardModel(s.as_mut_ptr()))?;
        let s: String
            = CStr::from_ptr(s.as_ptr())
            .to_str()
            .map_err(|_| AndorError::UTF8Error)?
            .to_string();
        return Ok(s.trim().to_string());
    }
}

/// Get the `(min, max)` wavelength range available for Count Convert mode.
pub fn get_count_convert_wavelength_range() -> AndorResult<(f32, f32)> {
    unsafe {
        let mut min: f32 = 0.0;
        let mut max: f32 = 0.0;
        code_to_ok!(GetCountConvertWavelengthRange(&mut min, &mut max))?;
        return Ok((min, max));
    }
}

/// Get the handle of the current camera.
pub fn get_current_camera() -> AndorResult<CameraHandle> {
    unsafe {
        let mut handle: c_int = 0;
        code_to_ok!(GetCurrentCamera(&mut handle))?;
        return Ok(CameraHandle(handle as i32));
    }
}

/// Get the currently active preamp gain index and a string with its
/// description.
///
/// Descriptions are at most 30 characters long.
pub fn get_current_preamp_gain() -> AndorResult<(usize, String)> {
    unsafe {
        let mut preamp_idx: c_int = 0;
        // maximum number of characters needed is 30
        // let mut s: Vec<c_char> = Vec::with_capacity(30);
        let mut s: [c_char; 30] = [0; 30];
        code_to_ok!(
            GetCurrentPreAmpGain(&mut preamp_idx, s.as_mut_ptr(), 30)
        )?;
        let s: String
            = CStr::from_ptr(s.as_ptr())
            .to_str()
            .map_err(|_| AndorError::UTF8Error)?
            .to_string();
        return Ok((preamp_idx as usize, s));
    }
}

/// State of an external output (DDG) channel.
#[derive(Copy, Clone, Debug)]
pub enum ExternalOutputMode {
    Off = 0,
    On = 1,
}

/// Get the current state of the `index`-th external output (DDG) channel.
pub fn get_ddg_external_output_mode(index: usize)
    -> AndorResult<ExternalOutputMode>
{
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(GetDDGExternalOutputEnabled(index as c_uint, &mut mode))?;
        let mode: ExternalOutputMode
            = match mode {
                0 => ExternalOutputMode::Off,
                _ => ExternalOutputMode::On,
            };
        return Ok(mode);
    }
}

/// Polarity of an external output (DDG) channel.
#[derive(Copy, Clone, Debug)]
pub enum ExternalOutputPolarity {
    Positive = 0,
    Negative = 1,
}

/// Get the current polarity of the `index`-th external output (DDG) channel.
pub fn get_ddg_external_output_polarity(index: usize)
    -> AndorResult<ExternalOutputPolarity>
{
    unsafe {
        let mut pol: c_uint = 0;
        code_to_ok!(GetDDGExternalOutputPolarity(index as c_uint, &mut pol))?;
        let polarity: ExternalOutputPolarity
            = match pol {
                0 => ExternalOutputPolarity::Positive,
                _ => ExternalOutputPolarity::Negative,
            };
        return Ok(polarity);
    }
}

/// Gate step tracking mode of an external output (DDG) channel.
#[derive(Copy, Clone, Debug)]
pub enum ExternalOutputStepMode {
    Off = 0,
    On = 1,
}

/// Get the current state of output step tracking applied to the gater on the
/// `index`-th external output (DDG) channel.
pub fn get_ddg_external_output_step_mode(index: usize)
    -> AndorResult<ExternalOutputStepMode>
{
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(
            GetDDGExternalOutputStepEnabled(index as c_uint, &mut mode)
        )?;
        let mode: ExternalOutputStepMode
            = match mode {
                0 => ExternalOutputStepMode::Off,
                _ => ExternalOutputStepMode::On,
            };
        return Ok(mode);
    }
}

/// Get the actual `(delay, width)` timings, in picoseconds, of the `index`-th
/// external output (DDG) channel.
pub fn get_ddg_external_output_time(index: usize) -> AndorResult<(u64, u64)> {
    unsafe {
        let mut delay: c_ulonglong = 0;
        let mut width: c_ulonglong = 0;
        code_to_ok!(
            GetDDGExternalOutputTime(index as c_uint, &mut delay, &mut width)
        )?;
        return Ok((delay as u64, width as u64));
    }
}

/// Get the TTL gate width corresponding to a particular optical gate width,
/// both in picoseconds.
pub fn get_ddg_ttl_gate_width(optical_width: u64) -> AndorResult<u64> {
    unsafe {
        let mut ttl_width: c_ulonglong = 0;
        code_to_ok!(
            GetDDGTTLGateWidth(optical_width as c_ulonglong, &mut ttl_width)
        )?;
        return Ok(ttl_width as u64);
    }
}

/// Get the actual `(delay, width)` gate timings, in picoseconds, of a USB
/// iStar.
pub fn get_ddg_gate_time() -> AndorResult<(u64, u64)> {
    unsafe {
        let mut delay: c_ulonglong = 0;
        let mut width: c_ulonglong = 0;
        code_to_ok!(GetDDGGateTime(&mut delay, &mut width))?;
        return Ok((delay as u64, width as u64));
    }
}

/// State of the insertion delay mode on a DDG-enabled device.
#[derive(Copy, Clone, Debug)]
pub enum InsertionDelayMode {
    Normal = 0,
    UltraFast = 1,
}

/// Get the insertion delay mode of a DDG-enabled device.
pub fn get_ddg_insertion_delay() -> AndorResult<InsertionDelayMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetDDGInsertionDelay(&mut mode))?;
        let mode: InsertionDelayMode
            = match mode {
                0 => InsertionDelayMode::Normal,
                _ => InsertionDelayMode::UltraFast,
            };
        return Ok(mode);
    }
}

/// State of Intelligate functionality.
#[derive(Copy, Clone, Debug)]
pub enum IntelligateMode {
    Off = 0,
    On = 1,
}

/// Get the current state of Intelligate functionality.
pub fn get_ddg_intelligate_mode() -> AndorResult<IntelligateMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetDDGIntelligate(&mut mode))?;
        let mode: IntelligateMode
            = match mode {
                0 => IntelligateMode::Off,
                _ => IntelligateMode::On,
            };
        return Ok(mode);
    }
}

/// State of the integrate on chip (IOC) functionality.
#[derive(Copy, Clone, Debug)]
pub enum IOCMode {
    Off = 0,
    On = 1,
}

/// Get the current state of the integrate on chip (IOC) functionality.
pub fn get_ddg_ioc_mode() -> AndorResult<IOCMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetDDGIOC(&mut mode))?;
        let mode: IOCMode
            = match mode {
                0 => IOCMode::Off,
                _ => IOCMode::On,
            };
        return Ok(mode);
    }
}

/// Get the actual frequency, in Hz, with which integrate on chip (IOC) pulses
/// will be triggered.
///
/// Should only be called once all other relevant conditions have been defined.
pub fn get_ddg_ioc_frequency() -> AndorResult<f64> {
    unsafe {
        let mut freq: f64 = 0.0;
        code_to_ok!(GetDDGIOCFrequency(&mut freq))?;
        return Ok(freq);
    }
}

/// Get the actual number of integrate on chip (IOC) pulses that will be
/// triggered.
///
/// Should only be called once all other relevant conditions have been defined.
pub fn get_ddg_ioc_number() -> AndorResult<u64> {
    unsafe {
        let mut num: c_ulong = 0;
        code_to_ok!(GetDDGIOCNumber(&mut num))?;
        return Ok(num as u64);
    }
}

/// Get the number of integrate on chip (IOC) pulses that were requested.
pub fn get_ddg_ioc_number_requested() -> AndorResult<u64> {
    unsafe {
        let mut num: c_uint = 0;
        code_to_ok!(GetDDGIOCNumberRequested(&mut num))?;
        return Ok(num as u64);
    }
}

/// Get the actual integrate on chip (IOC) period that will be triggered.
///
/// Should only be called once all other relevant conditions have been defined.
pub fn get_ddg_ioc_period() -> AndorResult<u64> {
    unsafe {
        let mut period: c_ulonglong = 0;
        code_to_ok!(GetDDGIOCPeriod(&mut period))?;
        return Ok(period as u64);
    }
}

/// Get the number integrate on chip (IOC) pulses that will be triggered
/// within the given exposure time, readout mode, acquisition mode, and IOC
/// frequency.
///
/// Should only be called once all other relevant conditions have been defined.
pub fn get_ddg_ioc_pulses() -> AndorResult<u64> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetDDGIOCPulses(&mut num))?;
        return Ok(num as u64);
    }
}

/// Integrate on chip (IOC) trigger mode.
#[derive(Copy, Clone, Debug)]
pub enum IOCTriggerMode {
    FirePulse = 0,
    External = 1,
}

/// Get the active integrate on chip (IOC) trigger mode.
pub fn get_ddg_ioc_trigger_mode() -> AndorResult<IOCTriggerMode> {
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(GetDDGIOCTrigger(&mut mode))?;
        let mode: IOCTriggerMode
            = match mode {
                0 => IOCTriggerMode::FirePulse,
                _ => IOCTriggerMode::External,
            };
        return Ok(mode);
    }
}

/// Optical gate status.
#[derive(Copy, Clone, Debug)]
pub enum OpticalGateMode {
    Off = 0,
    On = 1,
}

/// Get whether optical gate widths are being used.
pub fn get_ddg_optical_gate_mode() -> AndorResult<OpticalGateMode> {
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(GetDDGOpticalWidthEnabled(&mut mode))?;
        let mode: OpticalGateMode
            = match mode {
                0 => OpticalGateMode::Off,
                _ => OpticalGateMode::On,
            };
        return Ok(mode);
    }
}

/// Attempts to find the temporal location of a laser pulse in a user-defined
/// region (in picoseconds) with a given minimum gate pulse width as a
/// resolution scale. Return the `(delay, width)` estimation of the temporal
/// location of the pulse.
pub fn get_ddg_pulse(region: f64, resolution: f64) -> AndorResult<(f64, f64)> {
    unsafe {
        let mut delay: f64 = 0.0;
        let mut width: f64 = 0.0;
        code_to_ok!(GetDDGPulse(region, resolution, &mut delay, &mut width))?;
        return Ok((delay, width));
    }
}

/// DDG gate step mode.
#[derive(Copy, Clone, Debug)]
pub enum GateStepMode {
    Constant = 0,
    Exponential = 1,
    Logarithmic = 2,
    Linear = 3,
}

/// Get the `(p1, p2)` gate step coefficients for a particular gate step mode.
pub fn get_ddg_step_coefficients(mode: GateStepMode)
    -> AndorResult<(f64, f64)>
{
    unsafe {
        let mut p1: f64 = 0.0;
        let mut p2: f64 = 0.0;
        code_to_ok!(GetDDGStepCoefficients(mode as c_uint, &mut p1, &mut p2))?;
        return Ok((p1, p2));
    }
}

/// DDG gate width step mode.
#[derive(Copy, Clone, Debug)]
pub enum GateWidthStepMode {
    Constant = 0,
    Exponential = 1,
    Logarithmic = 2,
    Linear = 3,
}

/// Get the `(p1, p2)` gate width step coefficients for a particular gate width
/// step mode.
pub fn get_ddg_width_step_coefficients(mode: GateWidthStepMode)
    -> AndorResult<(f64, f64)>
{
    unsafe {
        let mut p1: f64 = 0.0;
        let mut p2: f64 = 0.0;
        code_to_ok!(
            GetDDGWidthStepCoefficients(mode as c_uint, &mut p1, &mut p2)
        )?;
        return Ok((p1, p2));
    }
}

/// Get the current gate step mode if activated, otherwise `None`.
pub fn get_ddg_step_mode() -> AndorResult<Option<GateStepMode>> {
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(GetDDGStepMode(&mut mode))?;
        let mode: Option<GateStepMode>
            = match mode {
                0 => Some(GateStepMode::Constant),
                1 => Some(GateStepMode::Exponential),
                2 => Some(GateStepMode::Logarithmic),
                3 => Some(GateStepMode::Linear),
                _ => None,
            };
        return Ok(mode);
    }
}

/// Get the current gate width step mode if activated, otherwise `None`.
pub fn get_ddg_width_step_mode() -> AndorResult<Option<GateWidthStepMode>> {
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(GetDDGWidthStepMode(&mut mode))?;
        let mode: Option<GateWidthStepMode>
            = match mode {
                0 => Some(GateWidthStepMode::Constant),
                1 => Some(GateWidthStepMode::Exponential),
                2 => Some(GateWidthStepMode::Logarithmic),
                3 => Some(GateWidthStepMode::Linear),
                _ => None,
            };
        return Ok(mode);
    }
}

/// Get the `(x, y)` size, in pixels, of the detector, where `x` is the
/// direction parallel to the readout register.
pub fn get_detector_size() -> AndorResult<(usize, usize)> {
    unsafe {
        let mut x: c_int = 0;
        let mut y: c_int = 0;
        code_to_ok!(GetDetector(&mut x, &mut y))?;
        return Ok((x as usize, y as usize));
    }
}

/// EM advanced gain setting.
#[derive(Copy, Clone, Debug)]
pub enum EMAdvancedGainMode {
    Off = 0,
    On = 1,
}

/// Get the current EM advanced gain setting.
pub fn get_em_advanced_gain() -> AndorResult<EMAdvancedGainMode> {
    unsafe {
        let mut em: c_int = 0;
        code_to_ok!(GetEMAdvanced(&mut em))?;
        let em: EMAdvancedGainMode
            = match em {
                0 => EMAdvancedGainMode::Off,
                _ => EMAdvancedGainMode::On,
            };
        return Ok(em);
    }
}

/// Get the current EM gain setting. The exact meaning of the returned value
/// depends on the current EM gain mode.
///
/// See also [`get_capabilities`] and [`EMGainMode`].
pub fn get_emccd_gain() -> AndorResult<u32> {
    unsafe {
        let mut emgain: c_int = 0;
        code_to_ok!(GetEMCCDGain(&mut emgain))?;
        return Ok(emgain as u32);
    }
}

/// Get the `(min, max)` EM gain bounds for the current EM gain mode and
/// temperature of the sensor.
///
/// See also [`get_capabilities`] and [`EMGainMode`]
pub fn get_em_gain_range() -> AndorResult<(u32, u32)> {
    unsafe {
        let mut min: c_int = 0;
        let mut max: c_int = 0;
        code_to_ok!(GetEMGainRange(&mut min, &mut max))?;
        return Ok((min as u32, max as u32));
    }
}

/// External trigger termination mode.
#[derive(Copy, Clone, Debug)]
pub enum ExternalTriggerTerminationMode {
    FiftyOhm = 0,
    HighZ = 1,
}

/// Get the current external trigger termination mode.
pub fn get_external_trigger_termination_mode()
    -> AndorResult<ExternalTriggerTerminationMode>
{
    unsafe {
        let mut mode: c_uint = 0;
        code_to_ok!(GetExternalTriggerTermination(&mut mode))?;
        let mode: ExternalTriggerTerminationMode
            = match mode {
                0 => ExternalTriggerTerminationMode::FiftyOhm,
                _ => ExternalTriggerTerminationMode::HighZ,
            };
        return Ok(mode);
    }
}

/// Get the index and speed (in microseconds per shift) of the fastest vertical
/// shift mode that does not require the vertical clock voltage to be adjusted.
pub fn get_fastest_recommended_vs_speed() -> AndorResult<(usize, f32)> {
    unsafe {
        let mut index: c_int = 0;
        let mut speed: f32 = 0.0;
        code_to_ok!(GetFastestRecommendedVSSpeed(&mut index, &mut speed))?;
        return Ok((index as usize, speed));
    }
}

/// Cosmic ray-filtering mode.
#[derive(Copy, Clone, Debug)]
pub enum CosmicRayFilterMode {
    Off = 0,
    On = 1,
}

/// Get the current cosmic ray-filtering mode.
pub fn get_cosmic_ray_filter_mode() -> AndorResult<CosmicRayFilterMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetFilterMode(&mut mode))?;
        let mode: CosmicRayFilterMode
            = match mode {
                0 => CosmicRayFilterMode::Off,
                _ => CosmicRayFilterMode::On,
            };
        return Ok(mode);
    }
}

/// Get the current exposure time, in seconds, for a Fast Kinetics acquisition.
///
/// Should be used only after all acquisition settings have been set.
pub fn get_fk_exposure_time() -> AndorResult<f32> {
    unsafe {
        let mut time: f32 = 0.0;
        code_to_ok!(GetFKExposureTime(&mut time))?;
        return Ok(time);
    }
}

/// Get the actual speed (in microseconds per shift) of the `index`-th Fast
/// Kinetics vertical shift speed mode.
///
/// See also [`get_number_fk_vs_speeds`].
pub fn get_fk_vshift_speed(index: usize) -> AndorResult<f32> {
    unsafe {
        let mut speed: f32 = 0.0;
        code_to_ok!(GetFKVShiftSpeedF(index as c_int, &mut speed))?;
        return Ok(speed);
    }
}

/// Front-end cooler status.
#[derive(Copy, Clone, Debug)]
pub enum FrontEndCoolerStatus {
    Normal = 0,
    Overheated = 1,
}

/// Get the current front-end cooler status.
pub fn get_front_end_cooler_status() -> AndorResult<FrontEndCoolerStatus> {
    unsafe {
        let mut status: c_int = 0;
        code_to_ok!(GetFrontEndStatus(&mut status))?;
        let status: FrontEndCoolerStatus
            = match status {
                0 => FrontEndCoolerStatus::Normal,
                _ => FrontEndCoolerStatus::Overheated,
            };
        return Ok(status);
    }
}

/// Photocathode gating mode.
#[derive(Copy, Clone, Debug)]
pub enum PhotocathodeGateMode {
    /// Gate input is AND'd with fire pulse.
    ANDFire = 0,
    /// Controlled from fire pulse only.
    Fire = 1,
    /// Controlled from SMB pulse only.
    SMB = 2,
    /// Gating ON continuously.
    On = 3,
    /// Gating OFF continuously.
    Off = 4,
    /// Control using DDG.
    DDG = 5,
}

/// Get the current photocathode gating mode.
pub fn get_photocathode_gate_mode() -> AndorResult<PhotocathodeGateMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetGateMode(&mut mode))?;
        let mode: PhotocathodeGateMode
            = match mode {
                0 => PhotocathodeGateMode::ANDFire,
                1 => PhotocathodeGateMode::Fire,
                2 => PhotocathodeGateMode::SMB,
                3 => PhotocathodeGateMode::On,
                4 => PhotocathodeGateMode::Off,
                _ => PhotocathodeGateMode::DDG,
            };
        return Ok(mode);
    }
}

/// Hardware version numbers.
#[derive(Copy, Clone, Debug)]
pub struct HardwareVersions {
    /// Plug-in card version
    pub pcb: u32,
    /// Flex 10K file version
    pub flex: u32,
    /// Firmware version
    pub firm_ver: u32,
    /// Firmware build number
    pub firm_build: u32,
}

/// Get hardware version numbers for the plug-in card version, the Flex 10K file
/// version, the camera firmware version, and the camera firmware build number.
pub fn get_hardware_versions() -> AndorResult<HardwareVersions> {
    unsafe {
        let mut pcb: c_uint = 0;
        let mut decode: c_uint = 0;
        let mut firm_ver: c_uint = 0;
        let mut firm_build: c_uint = 0;
        code_to_ok!(
            GetHardwareVersion(
                &mut pcb,
                &mut decode,
                &mut 0, // dummy input for some weird reason
                &mut 0, // dummy input for some weird reason
                &mut firm_ver,
                &mut firm_build
            )
        )?;
        return Ok(
            HardwareVersions {
                pcb: pcb as u32,
                flex: decode as u32,
                firm_ver: firm_ver as u32,
                firm_build: firm_build as u32,
            }
        );
    }
}

/// Output amplification type
#[derive(Copy, Clone, Debug)]
pub enum OutputAmpMode {
    EM = 0,
    Normal = 1,
}

/// Get the actual speed (in megahertz) of the `index`-th horizontal shift speed
/// mode for given AD channel and output amplification type.
///
/// See also [`get_number_hs_speeds`].
pub fn get_hs_speed(channel: usize, oamp_mode: OutputAmpMode, index: usize)
    -> AndorResult<f32>
{
    unsafe {
        let mut speed: f32 = 0.0;
        code_to_ok!(
            GetHSSpeed(
                channel as c_int,
                oamp_mode as c_int,
                index as c_int,
                &mut speed,
            )
        )?;
        return Ok(speed);
    }
}

/// High voltage flag from a USB iStar intensifier.
#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum HighVoltageFlag {
    Abnormal = 0,
    Normal(i32) = 1,
}

/// Retrieve a high voltage flag from a USB iStar intensifier.
pub fn get_high_voltage_flag() -> AndorResult<HighVoltageFlag> {
    unsafe {
        let mut flag: c_int = 0;
        code_to_ok!(GetHVflag(&mut flag))?;
        let flag: HighVoltageFlag
            = match flag {
                0 => HighVoltageFlag::Abnormal,
                n => HighVoltageFlag::Normal(n as i32),
            };
        return Ok(flag);
    }
}

/// Whether images are flipped in either direction.
#[derive(Copy, Clone, Debug)]
pub struct ImageFlipMode {
    pub h: bool,
    pub v: bool,
}

/// Get whether flipping is performed along either direction.
pub fn get_image_flip_mode() -> AndorResult<ImageFlipMode> {
    unsafe {
        let mut h: c_int = 0;
        let mut v: c_int = 0;
        code_to_ok!(GetImageFlip(&mut h, &mut v))?;
        let h: bool = !matches!(h, 0);
        let v: bool = !matches!(v, 0);
        return Ok(ImageFlipMode { h, v });
    }
}

/// Whether images are rotated in either direction.
#[derive(Copy, Clone, Debug)]
pub enum ImageRotateMode {
    NoRotation = 0,
    Clockwise = 1,
    AntiClockwise = 2,
}

/// Get whether images are rotated.
pub fn get_image_rotate_mode() -> AndorResult<ImageRotateMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(GetImageRotate(&mut mode))?;
        let mode: ImageRotateMode
            = match mode {
                0 => ImageRotateMode::NoRotation,
                1 => ImageRotateMode::Clockwise,
                _ => ImageRotateMode::AntiClockwise,
            };
        return Ok(mode);
    }
}

/// Get data from a series of images in the index range `[first, first +
/// num_images]` from the circular buffer as a 3D array of `i32`s. Indices of
/// the first and last valid images in the array are returned alongside the
/// data.
pub fn get_image_series(
    first: usize,
    num_images: usize,
    image_shape: (usize, usize),
) -> AndorResult<(nd::Array3<i32>, usize, usize)>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array3<c_int>
            = nd::Array::zeros((num_images, image_shape.0, image_shape.1));
        let mut idx0: c_int = 0;
        let mut idx1: c_int = 1;
        code_to_ok!(
            GetImages(
                first as c_int,
                (first + num_images) as c_int,
                data.as_mut_ptr(),
                size as c_uint,
                &mut idx0,
                &mut idx1,
            )
        )?;
        return Ok((data.mapv(|x| x as i32), idx0 as usize, idx1 as usize));
    }
}

/// Get data from a series of images in the index range `[first, first +
/// num_images]` from the circular buffer as a 3D array of `u16`s. Indices of
/// the first and last valid images in the array are returned alongside the
/// data.
pub fn get_image_series_16(
    first: usize,
    num_images: usize,
    image_shape: (usize, usize),
) -> AndorResult<(nd::Array3<u16>, usize, usize)>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array3<c_ushort>
            = nd::Array::zeros((num_images, image_shape.0, image_shape.1));
        let mut idx0: c_int = 0;
        let mut idx1: c_int = 0;
        code_to_ok!(
            GetImages16(
                first as c_int,
                (first + num_images) as c_int,
                data.as_mut_ptr(),
                size as c_uint,
                &mut idx0,
                &mut idx1,
            )
        )?;
        return Ok((data.mapv(|x| x as u16), idx0 as usize, idx1 as usize));
    }
}

/// Get the maximum number of images that can be transferred during a single
/// DMA transaction.
pub fn get_images_per_dma() -> AndorResult<usize> {
    unsafe {
        let mut num: c_uint = 0;
        code_to_ok!(GetImagesPerDMA(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the time, in seconds, to perform a keep clean cycle.
///
/// Should only be used after all acquisition parameters have been set.
pub fn get_keep_clean_time() -> AndorResult<f32> {
    unsafe {
        let mut time: f32 = 0.0;
        code_to_ok!(GetKeepCleanTime(&mut time))?;
        return Ok(time);
    }
}

/// Binnning mode.
#[derive(Copy, Clone, Debug)]
pub enum BinningMode {
    Horizontal = 0,
    Vertical = 1,
}

/// Get the maximum binning allowed in a direction for a given readout mode.
pub fn get_maximum_binning(read_mode: ReadMode, hv: BinningMode)
    -> AndorResult<usize>
{
    unsafe {
        let _read_mode: c_int
            = match read_mode {
                ReadMode::FullImage => 4,
                ReadMode::SubImage => 4,
                ReadMode::SingleTrack => 3,
                ReadMode::FVB => 0,
                ReadMode::MultiTrack => 1,
                ReadMode::RandomTrack => 2,
            };
        let mut bin_size: c_int = 0;
        code_to_ok!(GetMaximumBinning(_read_mode, hv as c_int, &mut bin_size))?;
        return Ok(bin_size as usize);
    }
}

/// Get the maximum exposure time in seconds.
pub fn get_maximum_exposure_time() -> AndorResult<f32> {
    unsafe {
        let mut time: f32 = 0.0;
        code_to_ok!(GetMaximumExposure(&mut time))?;
        return Ok(time);
    }
}

/// Get the maximum number of ring exposures.
///
/// Typically this is 16 for PCI systems and 15 for USB systems.
pub fn get_maximum_number_ring_exposures() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetMaximumNumberRingExposureTimes(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the current set value for MCP gain.
pub fn get_mcp_gain() -> AndorResult<u32> {
    unsafe {
        let mut gain: c_int = 0;
        code_to_ok!(GetMCPGain(&mut gain))?;
        return Ok(gain as u32);
    }
}

/// Get the `(min, max)` range of allowable MCP gain values.
pub fn get_mcp_gain_range() -> AndorResult<(u32, u32)> {
    unsafe {
        let mut min: c_int = 0;
        let mut max: c_int = 0;
        code_to_ok!(GetMCPGainRange(&mut min, &mut max))?;
        return Ok((min as u32, max as u32));
    }
}

/// Get the current Micro Channel Plate voltage in volts.
///
/// Normal values are between 600 and 1100 volts.
pub fn get_mcp_voltage() -> AndorResult<u32> {
    unsafe {
        let mut voltage: c_int = 0;
        code_to_ok!(GetMCPVoltage(&mut voltage))?;
        return Ok(voltage as u32);
    }
}

/// Get the minimum number of pixels that can be read out at each exposure.
pub fn get_minimum_image_length() -> AndorResult<usize> {
    unsafe {
        let mut length: c_int = 0;
        code_to_ok!(GetMinimumImageLength(&mut length))?;
        return Ok(length as usize);
    }
}

/// Get the most recently acquired image in any acquisition mode as a 2D array
/// of `i32`s.
///
/// The given array shape must be exactly the same as the expected image.
pub fn get_most_recent_image(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<i32>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<c_int>
            = nd::Array::zeros(image_shape);
        code_to_ok!(
            GetMostRecentImage(data.as_mut_ptr(), size as c_uint)
        )?;
        return Ok(data.mapv(|x| x as i32));
    }
}

/// Get the most recently acquired image in any acquisition mode as a 2D
/// array of `u16`s.
///
/// The given array shape must be exactly the same as the expected image.
pub fn get_most_recent_image_16(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<u16>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<c_ushort>
            = nd::Array::zeros(image_shape);
        code_to_ok!(
            GetMostRecentImage16(data.as_mut_ptr(), size as c_uint)
        )?;
        return Ok(data.mapv(|x| x as u16));
    }
}

/// A particular point in time, as measured by the camera.
#[derive(Copy, Clone, Debug)]
pub struct CameraTime {
    pub year: u16,
    pub month: u16,
    pub weekday: u16,
    pub day: u16,
    pub hour: u16,
    pub minute: u16,
    pub second: u16,
    pub millisecond: u16,
}

impl From<SYSTEMTIME> for CameraTime {
    fn from(systime: SYSTEMTIME) -> Self {
        return Self {
            year: systime.wYear as u16,
            month: systime.wMonth as u16,
            weekday: systime.wDayOfWeek as u16,
            day: systime.wDay as u16,
            hour: systime.wHour as u16,
            minute: systime.wMinute as u16,
            second: systime.wSecond as u16,
            millisecond: systime.wMilliseconds as u16,
        };
    }
}

/// Get the time of the initial frame and the time in milliseconds of the
/// `index`-th frame after the initial frame.
pub fn get_metadata_info(index: usize) -> AndorResult<(CameraTime, f32)> {
    unsafe {
        let mut systime = SYSTEMTIME {
            wYear: 0,
            wMonth: 0,
            wDayOfWeek: 0,
            wDay: 0,
            wHour: 0,
            wMinute: 0,
            wSecond: 0,
            wMilliseconds: 0,
        };
        let mut time_since: f32 = 0.0;
        code_to_ok!(
            GetMetaDataInfo(&mut systime, &mut time_since, index as c_uint)
        )?;
        return Ok((systime.into(), time_since));
    }
}

/// Get the nnumber of available analog-digital converter channels.
pub fn get_number_adc() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberADChannels(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of available output amplifiers.
pub fn get_number_oamp() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberAmp(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the indices `(first, last)` of the first and last available images in
/// the circular buffer.
pub fn get_available_image_idx() -> AndorResult<(usize, usize)> {
    unsafe {
        let mut first: c_int = 0;
        let mut last: c_int = 0;
        code_to_ok!(GetNumberAvailableImages(&mut first, &mut last))?;
        return Ok((first as usize, last as usize));
    }
}

/// Get the number of available external outputs.
pub fn get_number_ddg_external_outputs() -> AndorResult<usize> {
    unsafe {
        let mut num: c_uint = 0;
        code_to_ok!(GetNumberDDGExternalOutputs(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of available Fast Kinetics vertical shift speeds.
pub fn get_number_fk_vs_speeds() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberFKVShiftSpeeds(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of available horizontal shift speeds for the `index`-th
/// channel and output amplification type.
pub fn get_number_hs_speeds(index: usize, oamp_mode: OutputAmpMode)
    -> AndorResult<usize>
{
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(
            GetNumberHSSpeeds(index as c_int, oamp_mode as c_int, &mut num)
        )?;
        return Ok(num as usize);
    }
}

/// Get IRIG data for the `index`-th frame, comprising:
/// - IRIG time frame as received from the external IRIG device; will be the
///   most recent full time frame available when the start exposure event
///   occurred
/// - the bit position in the current IRIG time frame when the start exposure
///   event occurred
/// - the number of 10-nanosecond clock periods that have occurred since the
///   start of the current bit period and the start exposure event occurred
pub fn get_irig_data(index: usize) -> AndorResult<(u128, u8, u32)> {
    unsafe {
        let mut buf: [c_uchar; 16] = [0; 16];
        code_to_ok!(GetIRIGData(buf.as_mut_ptr(), index as c_uint))?;
        let mut data: u128
            = Cursor::new(buf).read_u128::<byteorder::BigEndian>()?;
        let frame_time: u128 = data % (1_u128 << 100);
        data >>= 100;
        let bit_pos: u8 = (data % (1_u128 << 8)) as u8;
        data >>= 8;
        let periods: u32 = (data % (1_u128 << 20)) as u32;
        return Ok((frame_time, bit_pos, periods));
    }
}

/// Get the indices `(first, last)` of the first and last new (i.e. yet to be
/// retrieved) images in the circular buffer.
pub fn get_new_image_idx() -> AndorResult<(usize, usize)> {
    unsafe {
        let mut first: c_int = 0;
        let mut last: c_int = 0;
        code_to_ok!(GetNumberNewImages(&mut first, &mut last))?;
        return Ok((first as usize, last as usize));
    }
}

/// Get the number of available photon counting divisions.
///
/// See also [`set_photon_counting_mode`] and
/// [`set_photon_counting_thresholds`].
pub fn get_number_photon_counting_divisions() -> AndorResult<usize> {
    unsafe {
        let mut num: c_uint = 0;
        code_to_ok!(GetNumberPhotonCountingDivisions(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of available preamp gain modes.
pub fn get_number_preamp_gain_modes() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberPreAmpGains(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of exposures currently in the ring.
pub fn get_number_ring_exposures() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberRingExposureTimes(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of configurable IO channels.
///
/// See also [`get_io_direction`], [`get_io_level`], [`set_io_direction`], and
/// [`set_io_level`].
pub fn get_number_io_channels() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberIO(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of available vertical clock voltage amplitudes.
pub fn get_number_vs_amplitudes() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberVSAmplitudes(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the number of available vertical shift speeds.
pub fn get_number_vs_speeds() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetNumberVSSpeeds(&mut num))?;
        return Ok(num as usize);
    }
}

/// Get the oldest image in the circular buffer as a 2D array of `i32`s.
///
/// The given array shape must be exactly the same as the expected image.
pub fn get_oldest_image(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<i32>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<c_int>
            = nd::Array::zeros(image_shape);
        code_to_ok!(GetOldestImage(data.as_mut_ptr(), size as c_uint))?;
        return Ok(data.mapv(|x| x as i32));
    }
}

/// Get the oldest image in the circular buffer as a 2D array of `u16`s.
///
/// The given array shape must be exactly the same as the expected image.
pub fn get_oldest_image_16(image_shape: (usize, usize))
    -> AndorResult<nd::Array2<u16>>
{
    unsafe {
        let size: usize = image_shape.0 * image_shape.1;
        let mut data: nd::Array2<c_ushort>
            = nd::Array::zeros(image_shape);
        code_to_ok!(GetOldestImage16(data.as_mut_ptr(), size as c_uint))?;
        return Ok(data.mapv(|x| x as u16));
    }
}

/// Current status of the phosphor.
#[derive(Copy, Clone, Debug)]
pub enum PhosphorStatus {
    Saturated = 0,
    Normal = 1,
}

/// Get the current status of the phosphor.
pub fn get_phosphor_status() -> AndorResult<PhosphorStatus> {
    unsafe {
        let mut status: c_int = 0;
        code_to_ok!(GetPhosphorStatus(&mut status))?;
        let status: PhosphorStatus
            = match status {
                0 => PhosphorStatus::Saturated,
                _ => PhosphorStatus::Normal,
            };
        return Ok(status);
    }
}

/// Get the `(x, y)` dimensions, in microns, of each pixel.
pub fn get_pixel_size() -> AndorResult<(f32, f32)> {
    unsafe {
        let mut x: f32 = 0.0;
        let mut y: f32 = 0.0;
        code_to_ok!(GetPixelSize(&mut x, &mut y))?;
        return Ok((x, y));
    }
}

/// Get the gain factor for the `index`-th preamp gain mode.
///
/// See also [`get_number_preamp_gain_modes`].
pub fn get_preamp_gain(index: usize) -> AndorResult<f32> {
    unsafe {
        let mut gain: f32 = 0.0;
        code_to_ok!(GetPreAmpGain(index as c_int, &mut gain))?;
        return Ok(gain);
    }
}

/// Get a description of the `index`-th preamp gain mode.
///
/// Descriptions are at most 30 characters long.
///
/// See also [`get_number_preamp_gain_modes`].
pub fn get_preamp_gain_text(index: usize) -> AndorResult<String> {
    unsafe {
        // maximum number of characters needed for an amp description is 30
        // let mut s: Vec<c_char> = Vec::with_capacity(30);
        let mut s: [c_char; 30] = [0; 30];
        code_to_ok!(GetPreAmpGainText(index as c_int, s.as_mut_ptr(), 30))?;
        let s: String
            = CStr::from_ptr(s.as_ptr())
            .to_str()
            .map_err(|_| AndorError::UTF8Error)?
            .to_string();
        return Ok(s.trim().to_string());
    }
}

/// Get the `(odd, even)`-frame exposure times in dual exposure mode.
///
/// This mode is only available for certain sensors in video, external trigger,
/// and full image mode.
pub fn get_dual_exposure_times() -> AndorResult<(f32, f32)> {
    unsafe {
        let mut odd: f32 = 0.0;
        let mut even: f32 = 0.0;
        code_to_ok!(GetDualExposureTimes(&mut odd, &mut even))?;
        return Ok((odd, even));
    }
}

/// Get the quantum efficiency, in the `[0.0, 1.0]` range, for a particular head
/// model and wavelength.
pub fn get_qe(sensor: &str, wavelength: f32) -> AndorResult<f32> {
    unsafe {
        let mut sensor_c: Vec<c_char>
            = Into::<Vec<u8>>::into(sensor)
            .into_iter()
            .map(|c| c as c_char)
            .collect();
        let mut qe: f32 = 0.0;
        code_to_ok!(GetQE(sensor_c.as_mut_ptr(), wavelength, 0, &mut qe))?;
        return Ok(qe / 100.0);
    }
}

/// Get the total time, in seconds, to read data from the sensor.
///
/// Should only be used after all acquisition settings have been set.
pub fn get_readout_time() -> AndorResult<f32> {
    unsafe {
        let mut time: f32 = 0.0;
        code_to_ok!(GetReadOutTime(&mut time))?;
        return Ok(time);
    }
}

/// Get a 1D array of image times, in nanoseconds, relative to the `first`-th
/// frame.
pub fn get_relative_image_times(first: usize, num_images: usize)
    -> AndorResult<nd::Array1<u64>>
{
    unsafe {
        let last: usize = first + num_images;
        let mut data: nd::Array1<c_ulonglong>
            = nd::Array::zeros(num_images);
        code_to_ok!(
            GetRelativeImageTimes(
                first as c_uint,
                last as c_uint,
                data.as_mut_ptr(),
                num_images as c_uint,
            )
        )?;
        return Ok(data.mapv(|x| x as u64));
    }
}

/// Get the `(min, max)` exposures guaranteed by the camera when in Ring
/// Exposure mode.
pub fn get_ring_exposure_range() -> AndorResult<(f32, f32)> {
    unsafe {
        let mut min: f32 = 0.0;
        let mut max: f32 = 0.0;
        code_to_ok!(GetRingExposureRange(&mut min, &mut max))?;
        return Ok((min, max));
    }
}

/// Get the sensitivity for a given channel/output amplifier mode/channel speed
/// index/preamp gain index.
pub fn get_sensitivity(
    channel: usize,
    hsspeed_index: usize,
    oamp_mode: OutputAmpMode,
    preamp_index: usize,
) -> AndorResult<f32>
{
    unsafe {
        let mut sensitivity: f32 = 0.0;
        code_to_ok!(
            GetSensitivity(
                channel as c_int,
                hsspeed_index as c_int,
                oamp_mode as c_int,
                preamp_index as c_int,
                &mut sensitivity,
            )
        )?;
        return Ok(sensitivity);
    }
}

/// Get the minimum shutter `(open, close)` times, in milliseconds.
pub fn get_shutter_min_times() -> AndorResult<(u32, u32)> {
    unsafe {
        let mut open: c_int = 0;
        let mut close: c_int = 0;
        code_to_ok!(GetShutterMinTimes(&mut close, &mut open))?;
        return Ok((open as u32, close as u32));
    }
}

/// Get the maximum number of images the circular buffer can hold.
pub fn get_circular_buffer_size() -> AndorResult<usize> {
    unsafe {
        let mut size: c_int = 0;
        code_to_ok!(GetSizeOfCircularBuffer(&mut size))?;
        return Ok(size as usize);
    }
}

/// Software version numbers.
#[derive(Copy, Clone, Debug)]
pub struct SoftwareVersions {
    /// EPROM version
    pub eprom: u32,
    /// COF file version
    pub cof: u32,
    /// Driver version number
    pub driver_ver: u32,
    /// Driver revision number
    pub driver_rev: u32,
    /// DLL version number
    pub dll_ver: u32,
    /// DLL revision number
    pub dll_rev: u32,
}

/// Get software version numbers for EPROM version, COF file version, driver
/// revision number, driver version, DLL revision number, and DLL version.
pub fn get_software_versions() -> AndorResult<SoftwareVersions> {
    unsafe {
        let mut eprom: c_uint = 0;
        let mut cof: c_uint = 0;
        let mut driver_rev: c_uint = 0;
        let mut driver_ver: c_uint = 0;
        let mut dll_rev: c_uint = 0;
        let mut dll_ver: c_uint = 0;
        code_to_ok!(
            GetSoftwareVersion(
                &mut eprom,
                &mut cof,
                &mut driver_rev,
                &mut driver_ver,
                &mut dll_rev,
                &mut dll_ver,
            )
        )?;
        return Ok(
            SoftwareVersions {
                eprom: eprom as u32,
                cof: cof as u32,
                driver_ver: driver_ver as u32,
                driver_rev: driver_rev as u32,
                dll_ver: dll_ver as u32,
                dll_rev: dll_rev as u32,
            }
        );
    }
}

/// Current camera status.
#[derive(Copy, Clone, Debug)]
#[repr(u32)]
pub enum CameraStatus {
    /// Awaiting instructions
    Idle = 0,
    /// Executing temperature cycle
    TempCycle = 1,
    /// Acquisition in progress
    Acquiring = 2,
    /// Unable to meet Accumulate cycle time
    AccumTimeNotMet = 3,
    /// Unable to meet Kinetic cycle time
    KineticTimeNotMet = 4,
    /// Unable to communicate with card
    ErrorAck = 5,
    /// Computer unable to read the data via the ISA slot at the required rate.
    AcqBuffer = 6,
    /// Computer unable to read data fast enough to stop camera memory from
    /// filling to maximum capacity
    AcqDownFIFOFull = 7,
    /// Overflow of the spool buffer
    SpoolError = 8,
    /// Unknown
    Unknown(u32),
}

/// Get the current camera status.
///
/// Acquisition is automatically aborted if the returned is one of:
/// - `AccumTimeNotMet`
/// - `KineticTimeNotMet`
/// - `ErrorAck`
/// - `AcqBuffer`
/// - `AcqDownFIFOFull`
pub fn get_status() -> AndorResult<CameraStatus> {
    unsafe {
        let mut status: c_int = 0;
        code_to_ok!(GetStatus(&mut status))?;
        let status: CameraStatus
            = match status as c_uint {
                DRV_IDLE => CameraStatus::Idle,
                DRV_TEMPCYCLE => CameraStatus::TempCycle,
                DRV_ACQUIRING => CameraStatus::Acquiring,
                DRV_ACCUM_TIME_NOT_MET => CameraStatus::AccumTimeNotMet,
                DRV_KINETIC_TIME_NOT_MET => CameraStatus::KineticTimeNotMet,
                DRV_ERROR_ACK => CameraStatus::ErrorAck,
                DRV_ACQ_BUFFER => CameraStatus::AcqBuffer,
                DRV_ACQ_DOWNFIFO_FULL => CameraStatus::AcqDownFIFOFull,
                DRV_SPOOLERROR => CameraStatus::SpoolError,
                n => CameraStatus::Unknown(n as u32),
            };
        return Ok(status);
    }
}

/// TEC overheat status.
#[derive(Copy, Clone, Debug)]
pub enum TECStatus {
    Normal = 0,
    Overheated = 1,
}

/// Get the current TEC status.
pub fn get_tec_status() -> AndorResult<TECStatus> {
    unsafe {
        let mut status: c_int = 0;
        code_to_ok!(GetTECStatus(&mut status))?;
        let status: TECStatus
            = match status {
                0 => TECStatus::Normal,
                _ => TECStatus::Overheated,
            };
        return Ok(status);
    }
}

/// Temperature control status.
#[derive(Copy, Clone, Debug)]
pub enum TemperatureStatus {
    /// Temperature control is off
    Off = 0,
    /// Temperature has stabilized at the set point
    Stabilized = 1,
    /// Temperature has not reached the set point
    NotReached = 2,
    /// Temperature had stabilized, but has since drifted
    Drift = 3,
    /// Temperature reached the set point, but has not stabilized
    NotStabilized = 4,
}

/// Get the current temperature to the nearest degree Celsius as well as the
/// status of temperature control.
pub fn get_temperature() -> AndorResult<(i32, TemperatureStatus)> {
    unsafe {
        let mut temp: c_int = 0;
        let code = GetTemperature(&mut temp);
        return match code {
            DRV_TEMP_OFF => Ok(TemperatureStatus::Off),
            DRV_TEMP_STABILIZED => Ok(TemperatureStatus::Stabilized),
            DRV_TEMP_NOT_REACHED => Ok(TemperatureStatus::NotReached),
            DRV_TEMP_DRIFT => Ok(TemperatureStatus::Drift),
            DRV_TEMP_NOT_STABILIZED => Ok(TemperatureStatus::NotStabilized),
            x => Err(AndorError::Code(x.into())),
        }.map(|status| (temp as i32, status));
    }
}

/// Get the current temperature in degrees Celsius as well as the status of
/// temperature control.
pub fn get_temperature_f() -> AndorResult<(f32, TemperatureStatus)> {
    unsafe {
        let mut temp: f32 = 0.0;
        let code = GetTemperatureF(&mut temp);
        return match code {
            DRV_TEMP_OFF => Ok(TemperatureStatus::Off),
            DRV_TEMP_STABILIZED => Ok(TemperatureStatus::Stabilized),
            DRV_TEMP_NOT_REACHED => Ok(TemperatureStatus::NotReached),
            DRV_TEMP_DRIFT => Ok(TemperatureStatus::Drift),
            DRV_TEMP_NOT_STABILIZED => Ok(TemperatureStatus::NotStabilized),
            x => Err(AndorError::Code(x.into())),
        }.map(|status| (temp, status));
    }
}

/// Get the `(min, max)` range of temperatures in degrees Celsius to which
/// the sensor can be cooled.
pub fn get_temperature_range() -> AndorResult<(i32, i32)> {
    unsafe {
        let mut min: c_int = 0;
        let mut max: c_int = 0;
        code_to_ok!(GetTemperatureRange(&mut min, &mut max))?;
        return Ok((min as i32, max as i32));
    }
}

/// Get the number of decimal places to which the sensor temperature can be
/// measured.
pub fn get_temperature_precision() -> AndorResult<usize> {
    unsafe {
        let mut prec: c_int = 0;
        code_to_ok!(GetTemperaturePrecision(&mut prec))?;
        return Ok(prec as usize);
    }
}

/// Get the total number of images acquired since the start of the current
/// acquisition period.
///
/// If the camera is idle, return the number of images taken during the last
/// acquisition period.
pub fn get_total_images_acquired() -> AndorResult<usize> {
    unsafe {
        let mut num: c_int = 0;
        code_to_ok!(GetTotalNumberImagesAcquired(&mut num))?;
        return Ok(num as usize);
    }
}

/// Direction of a particular IO channel.
#[derive(Copy, Clone, Debug)]
pub enum IODirection {
    Output = 0,
    Input = 1,
}

/// Get the current state of the `index`-th IO channel.
///
/// See also [`get_number_io_channels`].
pub fn get_io_direction(index: usize) -> AndorResult<IODirection> {
    unsafe {
        let mut dir: c_int = 0;
        code_to_ok!(GetIODirection(index as c_int, &mut dir))?;
        let dir: IODirection
            = match dir {
                0 => IODirection::Output,
                _ => IODirection::Input,
            };
        return Ok(dir);
    }
}

/// Level of a particular IO channel.
#[derive(Copy, Clone, Debug)]
pub enum IOLevel {
    Low = 0,
    High = 1,
}

/// Get the current level of the `index`-th IO channel.
///
/// See also [`get_number_io_channels`].
pub fn get_io_level(index: usize) -> AndorResult<IOLevel> {
    unsafe {
        let mut level: c_int = 0;
        code_to_ok!(GetIOLevel(index as c_int, &mut level))?;
        let level: IOLevel
            = match level {
                0 => IOLevel::Low,
                _ => IOLevel::High,
            };
        return Ok(level);
    }
}

/// Details of the active USB system.
#[derive(Copy, Clone, Debug)]
pub struct USBDetails {
    pub vendor_id: u16,
    pub product_id: u16,
    pub firmware_ver: u16,
    pub spec_number: u16,
}

/// Get details for the active USB system.
pub fn get_usb_device_details() -> AndorResult<USBDetails> {
    unsafe {
        let mut vendor_id: c_ushort = 0;
        let mut product_id: c_ushort = 0;
        let mut firmware_ver: c_ushort = 0;
        let mut spec_number: c_ushort = 0;
        code_to_ok!(
            GetUSBDeviceDetails(
                &mut vendor_id,
                &mut product_id,
                &mut firmware_ver,
                &mut spec_number,
            )
        )?;
        return Ok(
            USBDetails {
                vendor_id: vendor_id as u16,
                product_id: product_id as u16,
                firmware_ver: firmware_ver as u16,
                spec_number: spec_number as u16,
            }
        );
    }
}

/// Kind of version info to request from [`get_version_info`].
#[derive(Copy, Clone, Debug)]
pub enum VersionInfo {
    SDK = 1073741824,
    DeviceDriver = 1073741825,
}

/// Get version information about either the SDK or the device driver.
pub fn get_version_info(info_kind: VersionInfo) -> AndorResult<String> {
    unsafe {
        // don't know how many characters are needed
        let mut s: [c_char; 128] = [0; 128];
        #[cfg(target_family = "unix")]
        {
            code_to_ok!(GetVersionInfo(info_kind as c_uint, s.as_mut_ptr(), 128))?;
        }
        #[cfg(target_family = "windows")]
        {
            code_to_ok!(GetVersionInfo(info_kind as c_int, s.as_mut_ptr(), 128))?;
        }
        let s: String
            = CStr::from_ptr(s.as_ptr())
            .to_str()
            .map_err(|_| AndorError::UTF8Error)?
            .to_string();
        return Ok(s.trim().to_string());
    }
}

/// Get a string description of a vertical clock amplitude setting.
///
/// See also [`get_number_vs_amplitudes`].
pub fn get_vs_amplitude_string(amp: VSAmplitude) -> AndorResult<String> {
    unsafe {
        // maximum 6 characters needed
        let mut s: [c_char; 6] = [0; 6];
        code_to_ok!(GetVSAmplitudeString(amp as c_int, s.as_mut_ptr()))?;
        let s: String
            = CStr::from_ptr(s.as_ptr())
            .to_str()
            .map_err(|_| AndorError::UTF8Error)?
            .to_string();
        return Ok(s.trim().to_string());
    }
}

/// Get the value of a vertical clock amplitude setting.
///
/// See also [`get_number_vs_amplitudes`].
pub fn get_vs_amplitude_value(amp: VSAmplitude) -> AndorResult<u32> {
    unsafe {
        let mut val: c_int = 0;
        code_to_ok!(GetVSAmplitudeValue(amp as c_int, &mut val))?;
        return Ok(val as u32);
    }
}

/// Get the speed, in microseconds per shift, of the `index`-th vertical
/// shift speed setting.
///
/// See also [`get_number_vs_speeds`].
pub fn get_vs_speed(index: usize) -> AndorResult<f32> {
    unsafe {
        let mut speed: f32 = 0.0;
        code_to_ok!(GetVSSpeed(index as c_int, &mut speed))?;
        return Ok(speed);
    }
}

/// Read at most `size` bytes over GPIB or until a byte is received with the EOI
/// line asserted from the device at `address` on the `id`-th board interface.
pub fn gpib_receive(id: u32, address: u16, size: usize)
    -> AndorResult<Vec<u8>>
{
    unsafe {
        let mut text: Vec<c_char> = Vec::with_capacity(size);
        text.fill(0);
        code_to_ok!(
            GPIBReceive(
                id as c_int,
                address as c_short,
                text.as_mut_ptr(),
                size as c_int,
            )
        )?;
        return Ok(text.into_iter().map(|x| x as u8).collect());
    }
}

/// Send `interface clear` over GPIB to the device at `address` on the `id`-th
/// board before writing `text` as bytes over GPIB. Newline and an EOI assertion
/// are automatically added afterward.
pub fn gpib_send(id: u32, address: u16, text: &[u8]) -> AndorResult<()> {
    unsafe {
        let mut text_c: Vec<c_char>
            = text.iter().map(|x| (*x) as c_char).collect();
        return code_to_ok!(
            GPIBSend(id as c_int, address as c_short, text_c.as_mut_ptr())
        );
    }
}

/// Read `size` bytes from the I2C device at `address`.
pub fn i2c_burst_read(address: u8, size: usize) -> AndorResult<Vec<u8>> {
    unsafe {
        let mut buf: Vec<c_uchar> = Vec::with_capacity(size);
        buf.fill(0);
        code_to_ok!(
            I2CBurstRead(address as c_uchar, size as c_int, buf.as_mut_ptr())
        )?;
        return Ok(buf.into_iter().map(|x| x as u8).collect());
    }
}

/// Write `text` as bytes to the I2C device at `address`.
pub fn i2c_burst_write(address: u8, text: &[u8]) -> AndorResult<()> {
    unsafe {
        let mut text_c: Vec<c_uchar>
            = text.iter().map(|x| (*x) as c_uchar).collect();
        return code_to_ok!(
            I2CBurstWrite(
                address as c_uchar,
                text_c.len() as c_int,
                text_c.as_mut_ptr(),
            )
        );
    }
}

/// Read a single byte from the `id`-th I2C device, at `address`.
pub fn i2c_read(id: u8, address: u8) -> AndorResult<u8> {
    unsafe {
        let mut data: c_uchar = 0;
        code_to_ok!(I2CRead(id as c_uchar, address as c_uchar, &mut data))?;
        return Ok(data as u8);
    }
}

/// Reset the I2C bus.
pub fn i2c_reset() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(I2CReset());
    }
}

/// Write a single byte to the `id`-th I2C device, at `address`.
pub fn i2c_write(id: u8, address: u8, data: u8) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(
            I2CWrite(id as c_uchar, address as c_uchar, data as c_uchar)
        );
    }
}

/// Auxiliary input port number on the Andor plug-in card.
#[derive(Copy, Clone, Debug)]
pub enum AuxPort {
    P1 = 1,
    P2 = 2,
    P3 = 3,
    P4 = 4,
}

/// State of the TTL auxiliary input port on the Andor plug-in card.
#[derive(Copy, Clone, Debug)]
pub enum AuxPortState {
    Low = 0,
    High = 1,
}

/// Get the state of the `port`-th TTL auxiliary input port on the Andor
/// plug-in card.
pub fn get_aux_port_state(port: AuxPort) -> AndorResult<AuxPortState> {
    unsafe {
        let mut state: c_int = 0;
        code_to_ok!(InAuxPort(port as c_int, &mut state))?;
        let state: AuxPortState
            = match state {
                0 => AuxPortState::Low,
                _ => AuxPortState::High,
            };
        return Ok(state);
    }
}

pub trait ToCCharArray {
    fn to_c_char_array(&self) -> AndorResult<Vec<c_char>>;
    fn to_c_uchar_array(&self) -> AndorResult<Vec<c_uchar>>;
}

impl ToCCharArray for &Path {
    fn to_c_char_array(&self) -> AndorResult<Vec<c_char>> {
        return Ok(
            self.to_str()
            .ok_or_else(|| AndorError::UTF8Error)?
            .chars()
            .map(|c| c as c_char)
            .collect()
        );
    }

    fn to_c_uchar_array(&self) -> AndorResult<Vec<c_uchar>> {
        return Ok(
            self.to_str()
            .ok_or_else(|| AndorError::UTF8Error)?
            .chars()
            .map(|c| c as c_uchar)
            .collect()
        );
    }
}

impl ToCCharArray for PathBuf {
    fn to_c_char_array(&self) -> AndorResult<Vec<c_char>> {
        return Ok(
            self.to_str()
            .ok_or_else(|| AndorError::UTF8Error)?
            .chars()
            .map(|c| c as c_char)
            .collect()
        );
    }

    fn to_c_uchar_array(&self) -> AndorResult<Vec<c_uchar>> {
        return Ok(
            self.to_str()
            .ok_or_else(|| AndorError::UTF8Error)?
            .chars()
            .map(|c| c as c_uchar)
            .collect()
        );
    }
}

impl ToCCharArray for &str {
    fn to_c_char_array(&self) -> AndorResult<Vec<c_char>> {
        return Ok(
            self.chars()
            .map(|c| c as c_char)
            .collect()
        );
    }

    fn to_c_uchar_array(&self) -> AndorResult<Vec<c_uchar>> {
        return Ok(
            self.chars()
            .map(|c| c as c_uchar)
            .collect()
        );
    }
}

/// Initialize the Andor SDK system. Some systems require access to a
/// `detector.ini`-formatted file, whose directory should be provided.
pub fn initialize<P>(path: P) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut dir: Vec<c_char> = path.as_ref().to_c_char_array()?;
        return code_to_ok!(Initialize(dir.as_mut_ptr()));
    }
}

/// Check whether the hardware and current settings permit the use of the
/// `index`-th amplifier.
pub fn check_amplifier(index: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(IsAmplifierAvailable(index as c_int));
    }
}

/// Get the current status of the cooler.
pub fn get_cooler_mode() -> AndorResult<CoolerMode> {
    unsafe {
        let mut mode: c_int = 0;
        code_to_ok!(IsCoolerOn(&mut mode))?;
        let mode: CoolerMode
            = match mode {
                0 => CoolerMode::Off,
                _ => CoolerMode::On,
            };
        return Ok(mode);
    }
}

/// Available Count Convert modes.
#[derive(Copy, Clone, Debug)]
pub enum CountConvertMode {
    Counts = 0,
    Electrons = 1,
    Photons = 2,
}

/// Check whether the hardware and current settings permit the use of the
/// given Count Convert mode.
pub fn check_count_convert_mode(mode: CountConvertMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(IsCountConvertModeAvailable(mode as c_int));
    }
}

/// Check whether an iXon camera has a mechanical shutter installed.
pub fn check_mechanical_shutter_installed() -> AndorResult<bool> {
    unsafe {
        let mut has_shutter: c_int = 0;
        code_to_ok!(IsInternalMechanicalShutter(&mut has_shutter))?;
        let has_shutter: bool = !matches!(has_shutter, 0);
        return Ok(has_shutter);
    }
}

/// Check whether the preamp index is available for given AD
/// channel/amplifier/horizontal shift speed index.
///
/// See also [`get_number_preamp_gain_modes`].
pub fn check_preamp_gain_index(
    channel: usize,
    amp_index: usize,
    hsspeed_index: usize,
    preamp_index: usize,
) -> AndorResult<bool>
{
    unsafe {
        let mut status: c_int = 0;
        code_to_ok!(
            IsPreAmpGainAvailable(
                channel as c_int,
                amp_index as c_int,
                hsspeed_index as c_int,
                preamp_index as c_int,
                &mut status,
            )
        )?;
        let status: bool = !matches!(status, 0);
        return Ok(status);
    }
}

/// Check whether the `index`-th amplifier flips the left-right axis.
pub fn check_amp_flips_readout(index: usize) -> AndorResult<bool> {
    unsafe {
        let mut flipped: c_int = 0;
        code_to_ok!(IsReadoutFlippedByAmplifier(index as c_int, &mut flipped))?;
        let flipped: bool = !matches!(flipped, 0);
        return Ok(flipped);
    }
}

/// Check whether the `index`-th trigger mode is available.
pub fn check_trigger_mode(index: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(IsTriggerModeAvailable(index as c_int));
    }
}

/// Set the state of the `port`-th TTL auxiliary output port on the Andor
/// plug-in card.
pub fn set_aux_port_state(port: AuxPort, state: AuxPortState)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(OutAuxPort(port as c_int, state as c_int));
    }
}

/// Reads the current acquisition setup and allocates and configures any memory
/// that will be used during the acquisition.
///
/// Calling this function is not strictly required, since this will be called by
/// [`start_acquisition`] anyway, but for long kinetic series acquisitions
/// calling this function first will reduce delay after a [`start_acquisition`]
/// call.
pub fn prepare_acquisition() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(PrepareAcquisition());
    }
}

/// Save the data from the last acquisition as a bitmap (`.bmp`) file to `path`.
///
/// Also requires a path to a `.pal` palette file, which describes colors to use
/// in the bitmap, containing 256 lines of ASCII text with each comprising three
/// numbers separated by spaces indicating RGB colors in a linear scale between
/// `ymin` and `ymax`.
///
/// If the last acquisition was in Kinetic Series mode, each image will be saved
/// in a separate file with a number appended to the filename.
pub fn save_as_bmp<P>(path: P, palette: P, ymin: u32, ymax: u32)
    -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        let palfile: Vec<c_char> = palette.as_ref().to_c_char_array()?;
        return code_to_ok!(
            SaveAsBmp(
                outfile.as_ptr(),
                palfile.as_ptr(),
                ymin as c_int,
                ymax as c_int,
            )
        );
    }
}

/// Names a x-axis type and unit for data saved to a `.sif` file.
#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug)]
pub enum SifDataUnit {
    /// Data measured in pixels
    Pixel,
    /// Data measured as a wavelength in nanometers
    Wavelength_NM,
    /// Data measured as a wavelength in microns
    Wavelength_UM,
    /// Data measured as a wavelength in inverse-centimeters
    Wavelength_CM1,
    /// Data measured as a wavelength in electronvolts
    Wavelength_EV,
    /// Data measured as a Raman shift in inverse-centimeters
    RamanShift_CM1,
    /// Data measured as a position in microns
    Position_UM,
    /// Data measured as a position in millimeters
    Position_MM,
    /// Data measured as a position in centimeters
    Position_CM,
    /// Data measured as a position in micro-inches
    Position_UIN,
    /// Data measured as a position in inches
    Position_IN,
    /// Data measured as a time in milliseconds
    Time_MS,
    /// Data measured as a time in seconds
    Time_S,
}

trait UnpackSifDataUnit {
    fn unpack(&self) -> (c_int, c_int);
}

impl UnpackSifDataUnit for SifDataUnit {
    fn unpack(&self) -> (c_int, c_int) {
        return match self {
            Self::Pixel => (1, 1),
            Self::Wavelength_NM => (2, 1),
            Self::Wavelength_UM => (2, 2),
            Self::Wavelength_CM1 => (2, 3),
            Self::Wavelength_EV => (2, 4),
            Self::RamanShift_CM1 => (3, 1),
            Self::Position_UM => (4, 1),
            Self::Position_MM => (4, 2),
            Self::Position_CM => (4, 3),
            Self::Position_UIN => (4, 4),
            Self::Position_IN => (4, 5),
            Self::Time_MS => (5, 1),
            Self::Time_S => (5, 2),
        };
    }
}

/// Save the data from the last acquisition as a standard interchange format
/// (`.sif`) file to `path`.
///
/// `coeff` defines the coefficients of a third-degree polynomial, starting with
/// the constant term. `rayleigh` is the Rayleigh wavelength required for a
/// Raman shift calculation.
pub fn save_as_calibrated_sif<P>(
    path: P,
    x_data_unit: SifDataUnit,
    coeff: [f32; 4],
    rayleigh: f32,
) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        let (d1, d2): (c_int, c_int) = x_data_unit.unpack();
        let mut _coeff = coeff;
        return code_to_ok!(
            SaveAsCalibratedSif(
                outfile.as_mut_ptr(),
                d1,
                d2,
                _coeff.as_mut_ptr(),
                rayleigh,
            )
        );
    }
}

/// Save the data from the last acquisition as a standard interchange format
/// (`.sif`) file to `path` with comments.
pub fn save_as_commented_sif<P>(path: P, comment: &str) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        let mut cmt: Vec<c_char> = comment.to_c_char_array()?;
        return code_to_ok!(
            SaveAsCommentedSif(
                outfile.as_mut_ptr(),
                cmt.as_mut_ptr(),
            )
        );
    }
}

/// Save the data from the last acquisition as a European synchotron radiation
/// facility data format (EDF) file to `path`.
///
/// Kinetic series scans can be `pack`ed into a single file or saved as multiple
/// files.
pub fn save_as_edf<P>(path: P, pack: bool) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        return code_to_ok!(
            SaveAsEDF(
                outfile.as_mut_ptr(),
                (!pack) as c_int,
            )
        );
    }
}

/// Data format for a `.fits` file.
#[derive(Copy, Clone, Debug)]
pub enum FitsFormat {
    /// Unsigned 16-bit integer
    U16 = 0,
    /// Unsigned 32-bit integer
    U32 = 1,
    /// Signed 16-bit integer
    I16 = 2,
    /// Signed 32-bit integer
    I32 = 3,
    /// 32-bit floating point
    F32 = 4,
}

/// Save the data from the last acquisition as a flexible image transport system
/// (`.fits`) file to `path`.
pub fn save_as_fits<P>(path: P, format: FitsFormat) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        return code_to_ok!(
            SaveAsFITS(
                outfile.as_mut_ptr(),
                format as c_int,
            )
        );
    }
}

/// Data format for a raw data file.
#[derive(Copy, Clone, Debug)]
pub enum RawFormat {
    /// Signed 16-bit integer
    I16 = 1,
    /// Signed 32-bit integer
    I32 = 2,
    /// 32-bit floating point
    F32 = 3,
}

/// Save the data from the last acquisition as a raw data file to `path`.
pub fn save_as_raw<P>(path: P, format: RawFormat) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        return code_to_ok!(
            SaveAsRaw(
                outfile.as_mut_ptr(),
                format as c_int,
            )
        );
    }
}

/// Save the data from the last acquisition as a standard interchange format
/// (`.sif`) file to `path`.
///
/// See also [`save_as_calibrated_sif`], [`save_as_commented_sif`], and
/// [`set_sif_comment`].
pub fn save_as_sif<P>(path: P) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        return code_to_ok!(SaveAsSif(outfile.as_mut_ptr()));
    }
}

/// Data format for a `.tiff` file.
#[derive(Copy, Clone, Debug)]
pub enum TiffFormat {
    /// Unsigned 16-bit integer
    U16 = 0,
    /// Unsigned 32-bit integer
    U32 = 1,
    /// Color mode
    Color = 2,
}

/// Save the data from the last acquisition as a tagged image file format
/// (`.tiff`) file to `path`.
///
/// Also requires a path to a `.pal` palette file, which describes colors to use
/// in the tiff, containing 256 lines of ASCII text with each comprising three
/// numbers separated by spaces indicating RGB colors in a linear color scale.
///
/// `index` chooses which scan to save in a kinetic series, starting at zero. If
/// the acquisition is in any other mode, this should be set to 0.
pub fn save_as_tiff<P>(
    path: P,
    palette: P,
    index: usize,
    format: TiffFormat,
) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        let mut palfile: Vec<c_char> = palette.as_ref().to_c_char_array()?;
        return code_to_ok!(
            SaveAsTiff(
                outfile.as_mut_ptr(),
                palfile.as_mut_ptr(),
                (index + 1) as c_int,
                format as c_int,
            )
        );
    }
}

/// Save the data from the last acquisition as a tagged image file format
/// (`.tiff`) file to `path` with optional data rescaling.
///
/// Also requires a path to a `.pal` palette file, which describes colors to use
/// in the tiff, containing 256 lines of ASCII text with each comprising three
/// numbers separated by spaces indicating RGB colors in a linear color scale.
///
/// `index` chooses which scan to save in a kinetic series, starting at zero. If
/// the acquisition is in any other mode, this should be set to 0.
///
/// Optionally perform linear `rescale`ing to the available range.
pub fn save_as_tiff_ex<P>(
    path: P,
    palette: P,
    index: usize,
    format: TiffFormat,
    rescale: bool,
) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let mut outfile: Vec<c_char> = path.as_ref().to_c_char_array()?;
        let mut palfile: Vec<c_char> = palette.as_ref().to_c_char_array()?;
        return code_to_ok!(
            SaveAsTiffEx(
                outfile.as_mut_ptr(),
                palfile.as_mut_ptr(),
                (index + 1) as c_int,
                format as c_int,
                (!rescale) as c_int,
            )
        );
    }
}

/// Send an event to the camera to start acquisition when in Software Trigger
/// mode.
///
/// See also [`check_trigger_mode`].
pub fn send_software_trigger() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SendSoftwareTrigger());
    }
}

/// Set the accumulation cycle time to the nearest valid value not less than the
/// given value.
///
/// See also [`get_acquisition_timings`].
pub fn set_accumulation_cycle_time(time: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetAccumulationCycleTime(time));
    }
}

/// Acquisition mode.
#[derive(Copy, Clone, Debug)]
pub enum AcquisitionMode {
    Single = 1,
    Accumulate = 2,
    Kinetics = 3,
    FastKinetics = 4,
    Video = 5,
}

/// Set the acquisition mode.
pub fn set_acquisition_mode(mode: AcquisitionMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetAcquisitionMode(mode as c_int));
    }
}

/// Sensor port mode used to acquire image data.
#[derive(Copy, Clone, Debug)]
pub enum SensorPortMode {
    /// Acquire image data via the selected single sensor port
    ///
    /// See also [`set_sensor_port`]
    SinglePort = 0,
    /// Acquire image data simultaneously from all available ports
    AllPorts = 1,
}

/// Set the sensor port mode used to acquire image data.
pub fn set_sensor_port_mode(mode: SensorPortMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetSensorPortMode(mode as c_int));
    }
}

/// Sensor port used to acquire image data when in "single" mode.
#[derive(Copy, Clone, Debug)]
pub enum SensorPort {
    BottomLeft = 0,
    BottomRight = 1,
    TopLeft = 2,
    TopRight = 3,
}

/// Set the sensor port used to acquire image data when in "single" mode.
pub fn set_sensor_port(port: SensorPort) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SelectSensorPort(port as c_int));
    }
}

/// Set the AD channel.
///
/// See also [`get_number_adc`]
pub fn set_ad_channel(channel: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetADChannel(channel as c_int));
    }
}

/// iCam trigger functionality mode.
#[derive(Copy, Clone, Debug)]
pub enum ICamMode {
    Off = 0,
    On = 1,
}

/// Set the iCam trigger functionality mode.
pub fn set_advanced_trigger_mode(mode: ICamMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetAdvancedTriggerModeState(mode as c_int));
    }
}

/// Set the state of the baseline clamp functionality.
pub fn set_baseline_clamp_mode(mode: BaselineClampMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetBaselineClamp(mode as c_int));
    }
}

/// Set the baseline offset in counts.
///
/// Must be a multiple of 100 between -1000 and +1000.
pub fn set_baseline_offset(offset: i32) -> AndorResult<()> {
    unsafe {
        if !((-1000_i32..1000).contains(&offset) && offset % 100 == 0) {
            return Err(AndorError::InvalidBaselineOffset(offset));
        }
        return code_to_ok!(SetBaselineOffset(offset as c_int));
    }
}

/// Set the size, in bits, of the dynamic range for the current shift speed
/// (typically 16 or 18).
pub fn set_bits_per_pixel(size: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetBitsPerPixel(size as c_int));
    }
}

/// Camera link mode.
#[derive(Copy, Clone, Debug)]
pub enum CameraLinkMode {
    Off = 0,
    On = 1,
}

/// Set the camera link mode.
///
/// If set on "on", all acquired data will be streamed through the camera link
/// interface.
pub fn set_camera_link_mode(mode: CameraLinkMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetCameraLinkMode(mode as c_int));
    }
}

/// Masking for certain types of acquisition events.
#[derive(Copy, Clone, Debug)]
pub enum EventMaskingMode {
    Off = 3,
    FirePulseDown = 2,
    FirePulseUp = 1,
    Both = 0,
}

/// Set masking for certain types of acquisition events.
///
/// Turning this on may be useful to prevent missed events if many are likely to
/// occur close together.
pub fn set_event_masking_mode(mode: EventMaskingMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetCameraStatusEnable(mode as c_ulong));
    }
}

/// Set the number of rows to shift charge and the number of times to do it for
/// each frame of data.
///
/// The number of repeats must be a multiple of 2.
pub fn set_charge_shifting(rows: usize, repeats: usize) -> AndorResult<()> {
    unsafe {
        if repeats % 2 != 0 {
            return Err(AndorError::InvalidChargeShiftRepeats(repeats));
        }
        return code_to_ok!(
            SetChargeShifting(rows as c_uint, repeats as c_uint)
        );
    }
}

/// On/off cooler mode while the camera is off, used for
/// [`set_standby_cooler_mode`].
#[derive(Copy, Clone, Debug)]
pub enum StandbyCoolerMode {
    Off = 0,
    On = 1,
}

/// Set the on/off cooler mode while the camera is off.
pub fn set_standby_cooler_mode(mode: StandbyCoolerMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetCoolerMode(mode as c_int));
    }
}

/// Set the Count Convert mode.
pub fn set_count_convert_mode(mode: CountConvertMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetCountConvertMode(mode as c_int));
    }
}

/// Set the wavelength to use for Count Convert mode.
pub fn set_count_convert_wavelength(wavelength: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetCountConvertWavelength(wavelength));
    }
}

/// Sensor cropping mode.
#[derive(Copy, Clone, Debug)]
#[repr(usize)]
pub enum CropMode {
    Off = 0,
    On(usize) = 1,
}

/// Set the cropping on the sensor. This can be useful in increasing the frame
/// rate.
///
/// The number of rows used is `height`, counted from the bottom of the sensor.
/// *Note: It is important that no light falls on the excluded region, otherwise
/// acquired data may be corrupted.
///
/// Only available on Newton cameras.
pub fn set_crop_mode(mode: CropMode) -> AndorResult<()> {
    unsafe {
        let (active, height): (c_int, c_int)
            = match mode {
                CropMode::Off => (0, 0),
                CropMode::On(h) => (1, h as c_int),
            };
        return code_to_ok!(SetCropMode(active, height, 0));
    }
}

/// Set the current camera. All following function calls will be applied to only
/// this camera.
pub fn set_current_camera(handle: CameraHandle) -> AndorResult<()> {
    unsafe {
        let CameraHandle(handle_c) = handle;
        return code_to_ok!(SetCurrentCamera(handle_c as c_int));
    }
}

/// Set the horizontal binning value used when in Random Track readout mode.
pub fn set_random_track_hbin(bin: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetCustomTrackHBin(bin as c_int));
    }
}

/// DAC output voltage port on a Clara camera.
#[derive(Copy, Clone, Debug)]
pub enum ClaraDACPort {
    P1 = 1,
    P2 = 2,
}

/// Set the output voltage from one of the two 16-bit DAC outputs of a Clara
/// camera.
///
/// The `resolution` can be set from 2- to 16-bit in steps of 2.
pub fn set_dac_output(port: ClaraDACPort, resolution: usize, value: i32)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetDACOutput(port as c_int, resolution as c_int, value as c_int)
        );
    }
}

/// DAC output voltage range on a Clara camera.
#[derive(Copy, Clone, Debug)]
pub enum ClaraDACRange {
    /// 5-volt DAC range
    Five = 1,
    /// 10-volt DAC range
    Ten = 2,
}

/// Set the DAC output voltage range on a Clara camera for the current output
/// port.
pub fn set_dac_output_range(range: ClaraDACRange) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDACOutputScale(range as c_int));
    }
}

/// State of an external output (DDG) channel.
#[derive(Copy, Clone, Debug)]
pub enum ExternalOutputState {
    Off = 0,
    On = 1,
}

/// Set the state of the `index`-th external output (DDG) channel.
pub fn set_ddg_external_output(index: usize, state: ExternalOutputState)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetDDGExternalOutputEnabled(index as c_uint, state as c_uint)
        );
    }
}

/// Set the polarity of the `index`-th external output (DDG) channel.
pub fn set_ddg_external_output_polarity(
    index: usize,
    polarity: ExternalOutputPolarity
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetDDGExternalOutputPolarity(
                index as c_uint,
                polarity as c_uint,
            )
        );
    }
}

/// Set the state of the output step tracking applied to the gater on the
/// `index`-th external output (DDG) channel.
pub fn set_ddg_external_output_step_mode(
    index: usize,
    mode: ExternalOutputStepMode,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetDDGExternalOutputStepEnabled(
                index as c_uint,
                mode as c_uint,
            )
        );
    }
}

/// Set the `(delay, width)` timings, in picoseconds, of the `index`-th external
/// output (DDG) channel.
pub fn set_ddg_external_output_time(index: usize, delay: u64, width: u64)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetDDGExternalOutputTime(
                index as c_uint,
                delay as c_ulonglong,
                width as c_ulonglong,
            )
        );
    }
}

/// Set the gate step resolution, in picoseconds, for a kinetic series.
///
/// The resolution must be between 25 picoseconds and 25 seconds.
pub fn set_ddg_gate_step_resolution(resolution: f64) -> AndorResult<()> {
    unsafe {
        if !(25.0..25.0e12).contains(&resolution) {
            return Err(AndorError::InvalidDDGGateStepResolution(resolution));
        }
        return code_to_ok!(SetDDGGateStep(resolution));
    }
}

/// Set the `(delay, width)` external output (DDG) gate timings, in picoseconds,
/// for a USB iStar.
pub fn set_ddg_gate_time(delay: u64, width: u64) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(
            SetDDGGateTime(delay as c_ulonglong, width as c_ulonglong)
        );
    }
}

/// Set the insertion delay mode of a DDG-enabled device.
pub fn set_ddg_insertion_delay(mode: InsertionDelayMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGInsertionDelay(mode as c_int));
    }
}

/// Set Intelligate functionality.
pub fn set_ddg_intelligate_mode(mode: IntelligateMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGIntelligate(mode as c_int));
    }
}

/// Set integrate on chip (IOC) functionality.
pub fn set_ddg_ioc_mode(mode: IOCMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGIOC(mode as c_int));
    }
}

/// Set the frequency, in Hz, with which integrate on chip (IOC) pulses will be
/// triggered.
///
/// Should be limited to 5000 Hz when intelligate is on and 50000 Hz otherwise.
/// *Note: to limit the number of function calls, this frequency value is not
/// checked -- setting frequencies beyond these recommended values could damage
/// your device!*
///
/// The recommended order for setting IOC-related parameters is:
/// 1. set IOC frequency ([`set_ddg_ioc_frequency`])
/// 1. turn IOC on ([`set_ddg_ioc_mode`])
/// 1. set number of IOC pulses ([`set_ddg_ioc_number`])
pub fn set_ddg_ioc_frequency(frequency: f64) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGIOCFrequency(frequency));
    }
}

/// Set the number of integrate on chip (IOC) that will be triggered.
pub fn set_ddg_ioc_number(num: u64) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGIOCNumber(num as c_ulong));
    }
}

/// Set the integrate on chip (IOC) period that will be triggered.
pub fn set_ddg_ioc_period(period: u64) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGIOCPeriod(period as c_ulonglong));
    }
}

/// Set the active integrate on chip (IOC) trigger mode.
pub fn set_ddg_ioc_trigger_mode(mode: IOCTriggerMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGIOCTrigger(mode as c_uint));
    }
}

/// External output (DDG) optical gate width mode.
#[derive(Copy, Clone, Debug)]
pub enum ExternalOutputOpticalGateMode {
    Off = 0,
    On = 1,
}

/// Set the external output (DDG) optical gate width mode.
pub fn set_ddg_optical_gate_mode(mode: ExternalOutputOpticalGateMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetDDGOpticalWidthEnabled(mode as c_uint));
    }
}

/// Set the coefficients used in a kinetic series with gate step mode active.
///
/// The accessible gate width resolutions are between 25 picoseconds and 25
/// seconds for a PCI iStar and between 10 picoseconds and 10 seconds for a USB
/// iStar.
///
/// The coefficients `p1` and `p2` describe series
/// - `p1 * (n - 1)` (constant)
/// - `p1 * exp(p2 * n)` (exponential)
/// - `p1 * log(p2 * n)` (logarithmic)
/// - `p1 + p2 * n` (linear)
/// depending on the value of `mode`, with `n = 1, 2, ...` as the kinetic series
/// index.
pub fn set_ddg_step_coefficients(mode: GateStepMode, p1: f64, p2: f64)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetDDGStepCoefficients(mode as c_uint, p1, p2));
    }
}

/// Set the external output (DDG) gate step mode.
///
/// Gate step mode can be turned off by passing `None`.
pub fn set_ddg_step_mode(mode: Option<GateStepMode>) -> AndorResult<()> {
    unsafe {
        let mode_c: c_uint
            = match mode {
                Some(m) => m as c_uint,
                None => 100,
            };
        return code_to_ok!(SetDDGStepMode(mode_c));
    }
}

/// Set the coefficients used in a kinetic series with gate width step mode
/// active.
///
/// The accessible gate width resolutions are between 25 picoseconds and 25
/// seconds for a PCI iStar and between 10 picoseconds and 10 seconds for a USB
/// iStar.
///
/// The coefficients `p1` and `p2` describe series
/// - `p1 * (n - 1)` (constant)
/// - `p1 * exp(p2 * n)` (exponential)
/// - `p1 * log(p2 * n)` (logarithmic)
/// - `p1 + p2 * n` (linear)
/// depending on the value of `mode`, with `n = 1, 2, ...` as the kinetic series
/// index.
pub fn set_ddg_width_step_coefficients(
    mode: GateWidthStepMode,
    p1: f64,
    p2: f64,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetDDGWidthStepCoefficients(mode as c_uint, p1, p2));
    }
}

/// Set the external output (DDG) gate width step mode.
///
/// Gate width step mode can be turned off by passing `None`.
pub fn set_ddg_width_step_mode(mode: Option<GateWidthStepMode>)
    -> AndorResult<()>
{
    unsafe {
        let mode_c: c_uint
            = match mode {
                Some(m) => m as c_uint,
                None => 100,
            };
        return code_to_ok!(SetDDGWidthStepMode(mode_c));
    }
}

/// Set the `(t0, t1, t2)` gate pulse times corresponding to the output delay in
/// nanoseconds, the gate delay in picoseconds, and the pulse width in
/// picoseconds, respectively.
///
/// `t0` has a resolution of 16 nanoseconds while `t1` and `t2` have a
/// resolution of 25 picoseconds.
pub fn set_ddg_times(t0: f64, t1: f64, t2: f64) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDDGTimes(t0, t1, t2));
    }
}

/// Trigger mode of the internal delay generator for DDG output.
#[derive(Copy, Clone, Debug)]
pub enum ExternalOutputTriggerMode {
    Internal = 0,
    External = 1,
}

/// Set the trigger mode of the internal delay generator.
pub fn set_ddg_trigger_mode(mode: ExternalOutputTriggerMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetDDGTriggerMode(mode as c_int));
    }
}

/// DDG variable gate step mode.
#[derive(Copy, Clone, Debug)]
pub enum GateVariableStepMode {
    Exponential = 1,
    Logarithmic = 2,
    Linear = 3,
}

/// Set the coefficients used in a kinetic series with gate width step mode
/// active.
///
/// The accessible gate width resolutions are between 25 picoseconds and 25
/// seconds for a PCI iStar and between 10 picoseconds and 10 seconds for a USB
/// iStar.
///
/// The coefficients `p1` and `p2` describe series
/// - `p1 * exp(p2 * n)` (exponential)
/// - `p1 * log(p2 * n)` (logarithmic)
/// - `p1 + p2 * n` (linear)
/// depending on the value of `mode`, with `n = 1, 2, ...` as the kinetic series
/// index.
pub fn set_ddg_variable_gate_step_coefficients(
    mode: GateVariableStepMode,
    p1: f64,
    p2: f64,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetDDGVariableGateStep(mode as c_int, p1, p2));
    }
}

/// Set parameters to control the delay generator through a GPIB card installed
/// in your computer.
pub fn set_delay_generator(board: usize, address: u16, generator_type: usize)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetDelayGenerator(
                board as c_int,
                address as c_short,
                generator_type as c_int,
            )
        );
    }
}

/// Set parameters to control the DMA buffer.
///
/// The DMA buffer is normally used to hold acquired images for a short time
/// before notifying the SDK that new data is available in order to avoid
/// overwhelming the operating system with interrupt events.
///
/// The maximum number of images that can be held is a function of
/// 1. the size of the DMA buffer (normally set to the size of one full-frame
///    image upon install)
/// 1. the minimum `time`, in seconds, required to occur between interrupt
///    signals (set by default to 0.03)
/// 1. an overriding maximum number of images `num_images` which, if set to 0,
///    is removed
pub fn set_dma_parameters(num_images: usize, time: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDMAParameters(num_images as c_int, time));
    }
}

/// Set the EM advanced gain setting.
pub fn set_em_advanced_gain(mode: EMAdvancedGainMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetEMAdvanced(mode as c_int));
    }
}

/// Set the EM gain. The valid range for the gain depends on the current EM gain
/// mode.
///
/// See also [`set_em_gain_mode`] and [`get_em_gain_range`].
pub fn set_emccd_gain(gain: u32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetEMCCDGain(gain as c_int));
    }
}

/// Set the EM gain mode.
pub fn set_em_gain_mode(mode: EMGainMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetEMGainMode(mode as c_int));
    }
}

/// Set the exposure time, in seconds, to the nearest value not less than this
/// value.
///
/// For Classics, if the current acquisition mode is Single Track, Multi Track,
/// or Image, then this will actually set the shutter time.
///
/// See also [`get_acquisition_timings`].
pub fn set_exposure_time(time: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetExposureTime(time));
    }
}

/// Set the external trigger termination mode.
pub fn set_external_trigger_termination_mode(
    mode: ExternalTriggerTerminationMode,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetExternalTriggerTermination(mode as c_uint));
    }
}

/// Fan mode.
///
/// It is generally not recommended to set this to anything other than `Full`.
#[derive(Copy, Clone, Debug)]
pub enum FanMode {
    Full = 0,
    Low = 1,
    Off = 2,
}

/// Set the fan mode.
///
/// If cooling is turned on, the fan should be turned off only for short periods
/// of time.
pub fn set_fan_mode(mode: FanMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetFanMode(mode as c_int));
    }
}

/// Fast external trigger mode.
#[derive(Copy, Clone, Debug)]
pub enum FastExternalTriggerMode {
    Off = 0,
    On = 1,
}

/// Set the fast external trigger mode.
///
/// If set to `On`, the system will not wait for the completion of a Keep Clean
/// cycle before accepting the next trigger. This setting will only have an
/// effect if external triggering has been turned on.
///
/// See also [`set_trigger_mode`].
pub fn set_fast_external_trigger_mode(mode: FastExternalTriggerMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetFastExtTrigger(mode as c_int));
    }
}

/// Fast Kinetics binning mode.
#[derive(Copy, Clone, Debug)]
pub enum FKBinningMode {
    FVB = 0,
    Image = 4,
}

/// Set parameters related to Fast Kinetics acquisition.
///
/// - `height`: sub-area height in rows
/// - `series_length`: number in series
/// - `exposure_time`: exposure time in seconds
/// - `binning_mode`: binning mode -- either Full Vertical Binning or Image
/// - `hbin`: horizontal binning
/// - `vbin`: vertical binning (only for Image mode)
pub fn set_fast_kinetics(
    height: usize,
    series_length: usize,
    exposure_time: f32,
    mode: FKBinningMode,
    hbin: usize,
    vbin: usize,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetFastKinetics(
                height as c_int,
                series_length as c_int,
                exposure_time,
                mode as c_int,
                hbin as c_int,
                vbin as c_int,
            )
        );
    }
}

/// Set parameters related to Fast Kinetics acquisition.
///
/// - `height`: sub-area height in rows
/// - `offset`: first row to be used, starting from the bottom at 0
/// - `series_length`: number in series
/// - `exposure_time`: exposure time in seconds
/// - `binning_mode`: binning mode -- either Full Vertical Binning or Image
/// - `hbin`: horizontal binning
/// - `vbin`: vertical binning (only for Image mode)
pub fn set_fast_kinetics_ex(
    height: usize,
    offset: usize,
    series_length: usize,
    exposure_time: f32,
    mode: FKBinningMode,
    hbin: usize,
    vbin: usize,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetFastKineticsEx(
                height as c_int,
                series_length as c_int,
                exposure_time,
                mode as c_int,
                hbin as c_int,
                vbin as c_int,
                offset as c_int,
            )
        );
    }
}

/// Fast Kinetics frame storage mode.
#[derive(Copy, Clone, Debug)]
pub enum FKStorageMode {
    BinInReadout = 0,
    BinInStorage = 1,
}

/// Set the frame storage mode when in Fast Kinetics mode.
///
/// This amounts to setting whether vertical binning is performed in the readout
/// register or in the storage area. When binning in the storage area, the
/// offset cannot be adjusted from the bottom of the sensor and the maximum
/// signal level will be reduced.
pub fn set_fast_kinetics_storage_mode(mode: FKStorageMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetFastKineticsStorageMode(mode as c_int));
    }
}

/// Fast Kinetics time scan mode.
#[derive(Copy, Clone, Debug)]
pub enum FKTimeScanMode {
    Off = 0,
    Accumulate = 1,
    Series = 2,
}

/// Set the time scan mode when in Fast Kinetics mode.
///
/// When triggered, the camera starts shifting collected data until
/// `exposed_rows` of data are collected. There is no dwell time between shifts.
/// After the data is collected the entire area (*sensor width* \*
/// `exposed_rows`) is read out and made available. This is repeated
/// `series_length` times. The data can be accumulated or presented as a series.
pub fn set_fast_kinetics_time_scan_mode(
    exposed_rows: usize,
    series_length: usize,
    mode: FKTimeScanMode,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetFastKineticsTimeScanMode(
                exposed_rows as c_int,
                series_length as c_int,
                mode as c_int,
            )
        );
    }
}

/// Set the cosmic ray filtering mode.
///
/// If the filter mode is on, consecutive scans in an accumulation will be
/// compared and any cosmic ray-like features that are only present in one scan
/// will be replaced with a scaled version of the corresponding pixel value in
/// the correct scan.
pub fn set_cosmic_ray_filter_mode(mode: CosmicRayFilterMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetFilterMode((mode as c_int) * 2));
    }
}

/// Set the vertical shift speed to the `index`-th speed allowed by the system.
///
/// See also [`get_number_fk_vs_speeds`].
pub fn set_fk_vshift_speed(index: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetFKVShiftSpeed(index as c_int));
    }
}

/// Frame Transfer mode.
#[derive(Copy, Clone, Debug)]
pub enum FrameTransferMode {
    Off = 0,
    On = 1,
}

/// Set the Frame Transfer mode.
pub fn set_frame_transfer_mode(mode: FrameTransferMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetFrameTransferMode(mode as c_int));
    }
}

/// Set the horizontal bin size used in Full Vertical Binning mode.
pub fn set_fvb_hbin(bin_size: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetFVBHBin(bin_size as c_int));
    }
}

/// Set gater parameters for an ICCD system.
///
/// Parameters:
/// - `delay` in nanoseconds between the T0 and C outputs on the SRS box
/// - `width` of the gate in nanoseconds
/// - `step` by which the gate position is moved in time after each scan in a
///   kinetic series
pub fn set_gate_parameters(delay: f32, width: f32, step: f32)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetGate(delay.abs(), width.abs(), step.abs()));
    }
}

/// Set the photocathode gate mode.
pub fn set_photocathode_gate_mode(mode: PhotocathodeGateMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetGateMode(mode as c_int));
    }
}

/// Switch between high sensitivity and high capacity.
#[derive(Copy, Clone, Debug)]
pub enum SensitivityCapacitySwitch {
    HighSensitivity = 0,
    HighCapcity = 1,
}

/// Set the swtch between high sensitivity mode and high capacity mode.
///
/// When in high capacity mode, the output amplifier is switched to a mode of
/// operation that reduces the responsitivity, thus allowing larger charge
/// packets during binning operations to be read.
pub fn set_sensitivity_capacity_switch(mode: SensitivityCapacitySwitch)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetHighCapacity(mode as c_int));
    }
}

/// Set the horizontal shift speed to the `index`-th setting along with the
/// output amplitifer mode.
///
/// See also [`get_hs_speed`] and [`get_number_hs_speeds`].
pub fn set_hs_speed(mode: OutputAmpMode, index: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetHSSpeed(mode as c_int, index as c_int));
    }
}

/// Set the region of interest and binning. Pixel coordinates start at 0.
///
/// - `x`: `[xmin, xmax]` (inclusive)
/// - `y`: `[ymin, ymax]` (inclusive)
/// - `bin`: `[hbin, vbin]`
///
/// For iDus, it is recommended to set horizontal binning to 1.
pub fn set_image(x: &[usize; 2], y: &[usize; 2], bin: &[usize; 2])
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetImage(
                bin[0] as c_int,
                bin[1] as c_int,
                x[0] as c_int + 1,
                x[1] as c_int + 1,
                y[0] as c_int + 1,
                y[1] as c_int + 1,
            )
        );
    }
}

/// Set whether flipping is performed along either direction.
pub fn set_image_flip_mode(mode: ImageFlipMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetImageFlip(mode.h as c_int, mode.v as c_int));
    }
}

/// Set whether images are rotated.
pub fn set_image_rotate_mode(mode: ImageRotateMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetImageRotate(mode as c_int));
    }
}

/// Isolated crop mode.
#[derive(Copy, Clone, Debug)]
#[repr(usize)]
pub enum IsolatedCropMode {
    Off = 0,
    On {
        /// `[width, height]`
        size: [usize; 2],
        /// `[hbin, vbin]`
        bin: [usize; 2],
    } = 1,
}

/// Set the isolated crop mode to exclude some rows or columns in order to
/// increase frame rate.
///
/// iXon, Newton, and iKon cameras can operate in either Full Vertical Binning
/// or Image readout modes with isolated crop mode active.
///
/// Note: it is important that no light falls on the excluded region, otherwise
/// the acquired data will be corrupted. For iDus, it is recommended to set
/// horizontal binning to 1.
pub fn set_isolated_crop_mode(mode: IsolatedCropMode) -> AndorResult<()> {
    unsafe {
        return match mode {
            IsolatedCropMode::Off => code_to_ok!(
                SetIsolatedCropMode(0, 1, 1, 1, 1)
            ),
            IsolatedCropMode::On { size, bin } => code_to_ok!(
                SetIsolatedCropMode(
                    1,
                    size[1] as c_int,
                    size[0] as c_int,
                    bin[1] as c_int,
                    bin[0] as c_int,
                )
            ),
        };
    }
}

/// Extended isolated crop mode. Pixel coordinates start at 0.
#[derive(Copy, Clone, Debug)]
#[repr(usize)]
pub enum IsolatedCropModeEx {
    Off = 0,
    On {
        /// `[left, bottom]`
        min_corner: [usize; 2],
        /// `[width, height]`
        size: [usize; 2],
        /// `[hbin, vbin]`
        bin: [usize; 2],
    },
}

/// Set the isolated crop mode to exclude some rows or columns in order to
/// increase frame rate. Currently onlu available on iXon Ultra, and can only be
/// used in Image readout mode with the EM output amplifier.
///
/// The following are tables of optimal settings for various ROI sizes:
///
/// For the iXon Ultra 897:
///
/// | ROI     | Left   | Right  | Bottom | Top    |
/// |:--------|:-------|:-------|:-------|:-------|
/// | 32x32   | 240    | 271    | 239    | 270    |
/// | 64x64   | 218    | 281    | 223    | 286    |
/// | 96x96   | 208    | 303    | 207    | 302    |
/// | 128x128 | 188    | 315    | 191    | 318    |
/// | 192x192 | 156    | 347    | 159    | 350    |
/// | 256x256 | 122    | 377    | 127    | 382    |
/// | 496x4   | 7      | 502    | 253    | 256    |
/// | 496x8   | 7      | 502    | 251    | 258    |
/// | 496x16  | 7      | 502    | 248    | 261    |
///
/// For the iXon Ultra 888:
///
/// | ROI     | Left   | Right  | Bottom | Top    |
/// |:--------|:-------|:-------|:-------|:-------|
/// | 32x32   | 486    | 517    | 495    | 526    |
/// | 64x64   | 475    | 538    | 479    | 542    |
/// | 128x128 | 432    | 559    | 447    | 574    |
/// | 256x256 | 368    | 623    | 383    | 638    |
/// | 512x512 | 240    | 751    | 255    | 766    |
/// | 1024x4  | 0      | 1023   | 509    | 512    |
/// | 1024x8  | 0      | 1023   | 507    | 514    |
/// | 1024x16 | 0      | 1023   | 503    | 518    |
/// | 1024x32 | 0      | 1023   | 495    | 526    |
pub fn set_isolated_crop_mode_ex(mode: IsolatedCropModeEx) -> AndorResult<()> {
    unsafe {
        return match mode {
            IsolatedCropModeEx::Off => code_to_ok!(
                SetIsolatedCropModeEx(0, 1, 1, 1, 1, 1, 1)
            ),
            IsolatedCropModeEx::On { min_corner, size, bin } => code_to_ok!(
                SetIsolatedCropModeEx(
                    1,
                    size[1] as c_int,
                    size[0] as c_int,
                    bin[1] as c_int,
                    bin[0] as c_int,
                    min_corner[0] as c_int + 1,
                    min_corner[1] as c_int + 1,
                )
            ),
        };
    }
}

/// Isolated crop mode type.
#[derive(Copy, Clone, Debug)]
pub enum IsolatedCropModeType {
    HighSpeed = 0,
    LowLatency = 1,
}

/// Set the isolate crop mode type.
///
/// In High Speed mode (default), multiple frames can be stored in the storage
/// area before they are read out. In Low Latency mode, each cropped frame is
/// read out as the data is acquired.
pub fn set_isolated_crop_mode_type(ty: IsolatedCropModeType)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetIsolatedCropModeType(ty as c_int));
    }
}

/// Set the kinetic cycle time, in seconds, to the nearest valid value not less
/// than `time`.
///
/// See also [`get_acquisition_timings`].
pub fn set_kinetic_cycle_time(time: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetKineticCycleTime(time));
    }
}

/// Set the voltage across the microchannel plate (MCP).
///
/// See also [`get_mcp_gain`] and [`get_mcp_gain_range`].
pub fn set_mcp_gain(gain: u32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetMCPGain(gain as c_int));
    }
}

/// MCP gating mode.
#[derive(Copy, Clone, Debug)]
pub enum MCPGatingMode {
    Off = 0,
    On = 1,
}

/// Set the MCP gating mode.
pub fn set_mcp_gating_mode(mode: MCPGatingMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetMCPGating(mode as c_int));
    }
}

/// Metadata mode.
#[derive(Copy, Clone, Debug)]
pub enum MetadataMode {
    Off = 0,
    On = 1,
}

/// Set the metadata option.
pub fn set_metadata_mode(mode: MetadataMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetMetaData(mode as c_int));
    }
}

/// Set parameters multi-track readout, with confirmation from the camera in the
/// form of a tuple `(bottom, gap)` giving the location of the bottom-most pixel
/// row of the first track and the number of rows between each track.
///
/// Tracks will be spread evenly over the detector. Validation of the parameters
/// is carried out in the following order:
/// 1. Number of tracks (at least 1),
/// 1. Track height (at least 1),
/// 1. Offset.
pub fn set_multitrack_parameters(
    num_tracks: usize,
    height: usize,
    offset: usize,
) -> AndorResult<(usize, usize)>
{
    unsafe {
        let mut bottom: c_int = 0;
        let mut gap: c_int = 0;
        code_to_ok!(
            SetMultiTrack(
                num_tracks as c_int,
                height as c_int,
                offset as c_int,
                &mut bottom,
                &mut gap,
            )
        )?;
        return Ok((bottom as usize, gap as usize));
    }
}

/// Set the horizontal binning when in multi-track readout mode.
///
/// The bin size must evenly divide the multitrack range. For iDus, a bin size
/// of 1 is recommended.
///
/// See also [`set_multitrack_hrange`].
pub fn set_multitrack_hbin(hbin: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetMultiTrackHBin(hbin as c_int));
    }
}

/// Set the horizontal range when in multi-track readout mode.
pub fn set_multitrack_hrange(start: usize, len: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(
            SetMultiTrackHRange(
                start as c_int,
                (start + len) as c_int
            )
        );
    }
}

/// Set the number of scans accumulated in memory.
///
/// Will only take effect if the acquisition mode is either Accumulate or
/// Kinetic Series.
pub fn set_number_accumulations(num_scans: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetNumberAccumulations(num_scans as c_int));
    }
}

/// Set the number of scans (possibly accumulated scans) to be taken during a
/// single acquisition sequence.
///
/// Will only take effect if the acquisition mode is Kinetic Series.
pub fn set_number_kinetics(num_scans: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetNumberKinetics(num_scans as c_int));
    }
}

/// Set the number of scans acquired before data is to be retrieved.
///
/// Will only take effect if the acquisition mode is Kinetic Series.
pub fn set_number_prescans(num_scans: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetNumberPrescans(num_scans as c_int));
    }
}

/// Set the output amplifier mode to use when reading data from the head of
/// systems that are able to use a second output amplifier.
///
/// If the current horizontal shift speed is not available for the given
/// amplifier mode, then it will default to the maximum shift speed that is.
pub fn set_output_amplifier_mode(mode: OutputAmpMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetOutputAmplifier(mode as c_int));
    }
}

/// Overlap readout mode.
#[derive(Copy, Clone, Debug)]
pub enum OverlapMode {
    Off = 0,
    On = 1,
}

/// Set whether an acquisition will read out in Overlap mode.
///
/// If the acquisition is in Single or Fast Kinetics mode, this will have no
/// effect.
pub fn set_overlap_mode(mode: OverlapMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetOverlapMode(mode as c_int));
    }
}

/// Exposure event emission mode.
#[derive(Copy, Clone, Debug)]
pub enum ExposureEventMode {
    Off = 0,
    On = 1,
}

/// Set whether events will be emitted at the start and end of camera exposures.
///
/// Only supported by cameras with a CCI23 card.
///
/// See also [`set_event_masking_mode`].
pub fn set_exposure_event_mode(mode: ExposureEventMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetPCIMode(1, mode as c_int));
    }
}

/// Photon counting mode.
#[derive(Copy, Clone, Debug)]
pub enum PhotonCountingMode {
    Off = 0,
    On = 1,
}

/// Set the photon counting mode.
pub fn set_photon_counting_mode(mode: PhotonCountingMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetPhotonCounting(mode as c_int));
    }
}

/// Set the threshold(s) to be used in photon counting mode.
pub fn set_photon_counting_thresholds<'a, I>(thresholds: I) -> AndorResult<()>
where I: IntoIterator<Item = &'a i32>
{
    unsafe {
        let mut thresholds_c: Vec<c_int>
            = thresholds.into_iter()
            .map(|x| (*x) as c_int)
            .collect();
        return code_to_ok!(
            SetPhotonCountingDivisions(
                thresholds_c.len() as c_uint,
                thresholds_c.as_mut_ptr(),
            )
        );
    }
}

/// Set the minimum and maximum threshold values for use in photon counting
/// mode.
pub fn set_photon_counting_threshold_range(min: i32, max: i32)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetPhotonCountingThreshold(min as c_int, max as c_int)
        );
    }
}

/// Set the preamp gain to the `index`-th allowed value.
///
/// See also [`get_number_preamp_gain_modes`] and [`get_preamp_gain`].
pub fn set_preamp_gain(index: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetPreAmpGain(index as c_int));
    }
}

/// Set the exposure times, in seconds, used for `odd`- and `even`-numbered
/// frames.
///
/// This mode is only available in Video, External Trigger, and Full Image
/// modes.
pub fn set_dual_exposure_times(odd: f32, even: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDualExposureTimes(odd, even));
    }
}

/// Dual exposure mode.
#[derive(Copy, Clone, Debug)]
pub enum DualExposureMode {
    Off = 0,
    On = 1,
}

/// Set the dual exposure mode, which controls the option to acquired 2 frames
/// for each external trigger.
///
/// This mode is only available in Video, External Trigger, and Full Image
/// modes.
///
/// See also [`set_dual_exposure_times`].
pub fn set_dual_exposure_mode(mode: DualExposureMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetDualExposureMode(mode as c_int));
    }
}

/// Set the `(bottom, top)` pixel row numbers for a series of tracks.
///
/// Tracks are validated by the camera to ensure that they are in ascending
/// order on the sensor and do not overlap. Some cameras need to have at least 1
/// row between each track. iXon+ and the USB cameras allow tracks with no gaps
/// between them. Vertical binning is set to be the height of each track.
///
/// See also [`set_random_track_hbin`].
pub fn set_random_tracks<'a, I>(tracks: I) -> AndorResult<()>
where I: IntoIterator<Item = &'a (usize, usize)>
{
    unsafe {
        let mut tracks_c: Vec<c_int>
            = tracks.into_iter()
            .flat_map(|(b, t)| [(*b) as c_int, (*t) as c_int].into_iter())
            .collect();
        return code_to_ok!(
            SetRandomTracks(
                tracks_c.len() as c_int,
                tracks_c.as_mut_ptr(),
            )
        );
    }
}

/// Set the `(bottom, top, hbin, ybin)` and `(left, right)` settings for a
/// series of tracks.
///
/// The positions of the tracks are validated by the camera to be in ascenting
/// order on the sensor and do not overlap. For iXon the range for left/right
/// values is from 8 to the CCD width. For iDus, the range is from 257 to the
/// CCD width.
pub fn set_custom_tracks<'a, I>(tracks: I, lr: (usize, usize))
    -> AndorResult<()>
where I: IntoIterator<Item = &'a (usize, usize, usize, usize)>
{
    unsafe {
        let mut tracks_c: Vec<c_int>
            = tracks.into_iter()
            .flat_map(|(b, t, hb, vb)| {
                [
                    (*b) as c_int,
                    (*t) as c_int,
                    lr.0 as c_int,
                    lr.1 as c_int,
                    (*hb) as c_int,
                    (*vb) as c_int,
                ].into_iter()
            })
            .collect();
        return code_to_ok!(
            SetComplexImage(
                tracks_c.len() as c_int,
                tracks_c.as_mut_ptr(),
            )
        );
    }
}

/// Set the data readout mode.
pub fn set_read_mode(mode: ReadMode) -> AndorResult<()> {
    unsafe {
        let mode_c: c_int
            = match mode {
                ReadMode::FullImage => 4,
                ReadMode::SubImage => 4,
                ReadMode::SingleTrack => 3,
                ReadMode::FVB => 0,
                ReadMode::MultiTrack => 1,
                ReadMode::RandomTrack => 2,
            };
        return code_to_ok!(SetReadMode(mode_c));
    }
}

/// Readout register packing mode.
#[derive(Copy, Clone, Debug)]
pub enum ReadoutRegisterPackingMode {
    Off = 0,
    On = 1,
}

/// Set whether data is packed into the readout register to improve frame rates
/// for subimages.
///
/// Only available for iXon+ and iXon3.
pub fn set_readout_register_packing_mode(mode: ReadoutRegisterPackingMode)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetReadoutRegisterPacking(mode as c_uint));
    }
}

/// Set the exposure times, in seconds, for ring exposures.
///
/// Each acquisition will use the next exposure in the ring, looping around to
/// the start again when the end is reached.
///
/// See also [`get_maximum_number_ring_exposures`].
pub fn set_ring_exposure_times<'a, I>(times: I) -> AndorResult<()>
where I: IntoIterator<Item = &'a f32>
{
    unsafe {
        let mut times_c: Vec<f32>
            = times.into_iter()
            .copied()
            .collect();
        return code_to_ok!(
            SetRingExposureTimes(
                times_c.len() as c_int,
                times_c.as_mut_ptr(),
            )
        );
    }
}

/// Shutter trigger mode.
#[derive(Copy, Clone, Debug)]
pub enum ShutterTriggerMode {
    /// Trigger on LOW
    Low = 0,
    /// Trigger on HIGH
    High = 1,
}

/// Shutter mode.
#[derive(Copy, Clone, Debug)]
pub enum ShutterMode {
    /// Fully auto
    Auto = 0,
    /// Permanently open
    Open = 1,
    /// Permanently closed
    Closed = 2,
    /// Open for FVB series
    FVB = 3,
    /// Open for any series
    Any = 4,
}

/// Set shutter parameters, including opening and closing times in milliseconds.
///
/// See also [`get_shutter_min_times`].
pub fn set_shutter_parameters(
    trigger_mode: ShutterTriggerMode,
    mode: ShutterMode,
    open: u32,
    close: u32,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetShutter(
                trigger_mode as c_int,
                mode as c_int,
                close as c_int,
                open as c_int,
            )
        );
    }
}

/// Set shutter parameters, including opening and closing times in milliseconds,
/// for both the internal and external shutters.
///
/// Note: For cameras capable of controlling the internal and external shutters
/// independently, you *must* use this function instead of
/// [`set_shutter_parameters`].
///
/// See also [`get_shutter_min_times`].
pub fn set_shutter_parameters_ex(
    trigger_mode: ShutterTriggerMode,
    mode: ShutterMode,
    mode_ex: ShutterMode,
    open: u32,
    close: u32,
) -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(
            SetShutterEx(
                trigger_mode as c_int,
                mode as c_int,
                close as c_int,
                open as c_int,
                mode_ex as c_int,
            )
        );
    }
}

/// Set the user text added to any `.sif` files created with [`save_as_sif`].
///
/// The stored comment can be cleared by passing an empty string.
pub fn set_sif_comment(comment: &str) -> AndorResult<()> {
    unsafe {
        let mut cmt: Vec<c_char> = comment.to_c_char_array()?;
        return code_to_ok!(SetSifComment(cmt.as_mut_ptr()));
    }
}

/// Set the center row and height of the single track in Single Track mode.
pub fn set_single_track(center: usize, height: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetSingleTrack(center as c_int, height as c_int));
    }
}

/// Set the horizontal binning in Single Track mode.
pub fn set_single_Track_hbin(hbin: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetSingleTrackHBin(hbin as c_int));
    }
}

/// Spooling mode.
#[derive(Clone, Debug)]
#[repr(usize)]
pub enum SpoolMode<P>
where P: AsRef<Path>
{
    Off = 0,
    On {
        /// Data format
        format: SpoolFormat,
        /// Filename stem; may also contain the path to the directory to which
        /// files will be written
        path: P,
        /// Circular buffer size; typically 10
        bufsize: usize,
    } = 1,
}

/// Spooling format.
#[derive(Copy, Clone, Debug)]
pub enum SpoolFormat {
    /// Sequence of 32-bit integers
    U32 = 0,
    /// Sequence of 32-bit integers if multiple accumulations are included in
    /// each scan, otherwise 16-bit integers.
    U32AccumU16 = 1,
    /// Sequence of 16-bit integers
    U16 = 2,
    /// Multiple-directory structure with multiple images per file and multiple
    /// files per directory. Data type depends on whether multiple accumulations
    /// are included in each scan, same as `U32AccumU16`
    MultiFileDir = 3,
    /// Spool to RAM disk
    RAMDisk = 4,
    /// Spool to 16-bit FITS
    U16FITS = 5,
    /// Spool to Andor SIF
    SIF = 6,
    /// Spool to 16-bit TIFF
    U16TIFF = 7,
    /// `MultiFileDir`, but with compression
    MultiFileDirCompressed = 8,
}

/// Set the spooling mode.
pub fn set_spool_mode<P>(mode: SpoolMode<P>) -> AndorResult<()>
where P: AsRef<Path>
{
    unsafe {
        let (active, format_c, mut path_c, buffer_size):
            (c_int, c_int, Vec<c_char>, c_int)
            = match mode {
                SpoolMode::Off => (0, 0, vec![], 10),
                SpoolMode::On { format, path, bufsize } => (
                    1,
                    format as c_int,
                    path.as_ref().to_c_char_array()?,
                    bufsize as c_int,
                ),
            };
        return code_to_ok!(
            SetSpool(active, format_c, path_c.as_mut_ptr(), buffer_size)
        );
    }
}

/// Set the number of parallel threads used for writing data to the disk when
/// spooling is enabled.
///
/// Note: Using more threads will not necessarily improve writing speeds; this
/// will in general depend on the kind of disk being written to as well as the
/// threading capabilities of the computer's CPU.
pub fn set_spool_thread_count(num_threads: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetSpoolThreadCount(num_threads as c_int));
    }
}

/// Set the detector temperature setpoint in degrees Celsius.
pub fn set_temperature(temp: i32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetTemperature(temp as c_int));
    }
}

/// Trigger mode.
#[derive(Copy, Clone, Debug)]
pub enum TriggerMode {
    Internal = 0,
    External = 1,
    ExternalStart = 6,
    ExternalExposure = 7,
    ExternalFVBEM = 9,
    Software = 10,
    ExternalChargeShifting = 12,
}

/// Set the trigger mode.
pub fn set_trigger_mode(mode: TriggerMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetTriggerMode(mode as c_int));
    }
}

/// Trigger inversion mode.
#[derive(Copy, Clone, Debug)]
pub enum TriggerInvertMode {
    Rising = 0,
    Falling = 1,
}

/// Set whether acquisition will be triggered on a rising or falling edge.
pub fn set_trigger_invert_mode(mode: TriggerInvertMode) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetTriggerInvert(mode as c_int));
    }
}

/// Get the `(min, max)` range of trigger levels, in volts.
pub fn get_trigger_level_range() -> AndorResult<(f32, f32)> {
    unsafe {
        let mut min: f32 = 0.0;
        let mut max: f32 = 0.0;
        code_to_ok!(GetTriggerLevelRange(&mut min, &mut max))?;
        return Ok((min, max));
    }
}

/// Set the trigger level in volts.
///
/// See also [`get_trigger_level_range`].
pub fn set_trigger_level(level: f32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetTriggerLevel(level));
    }
}

/// Set the state of the `index`-th IO channel.
pub fn set_io_direction(index: usize, direction: IODirection)
    -> AndorResult<()>
{
    unsafe {
        return code_to_ok!(SetIODirection(index as c_int, direction as c_int));
    }
}

/// Set the level for the `index`-th IO channel.
pub fn set_io_level(index: usize, level: IOLevel) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetIOLevel(index as c_int, level as c_int));
    }
}

/// Vertical clock voltage amplitude.
#[derive(Copy, Clone, Debug)]
pub enum VSAmplitude {
    Normal = 0,
    Plus1 = 1,
    Plus2 = 2,
    Plus3 = 3,
    Plus4 = 4,
}

/// Set the vertical clock voltage.
pub fn set_vs_amplitude(amp: VSAmplitude) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetVSAmplitude(amp as c_int));
    }
}

/// Set the vertical shift speed to the `index`-th setting.
pub fn set_vs_speed(index: usize) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(SetVSSpeed(index as c_int));
    }
}

/// Issue a shut-down command to the Andor system.
///
/// For Classic and iCCD systems, the temperature of the detector should be
/// above -20C before shutting down the system. When dynamically loading a DLL
/// which is statically linked to the SDK library, this function *must* be
/// called before unloading.
pub fn shutdown() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(ShutDown());
    }
}

/// Start an acquisition.
///
/// See also [`get_status`].
pub fn start_acquisition() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(StartAcquisition());
    }
}

/// Update the gate timings and external output (DDG) timings during an
/// externally triggered kinetic series.
///
/// Only available when integrate on chip (IOC) is active and gate step is not
/// being used.
pub fn update_ddg_timings() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(UpdateDDGTimings());
    }
}

/// Block the calling thread until an acquisition event is received from the
/// camera.
///
/// This function is a lightweight alternative to continuously polling with
/// [`get_status`].
pub fn wait_for_acquisition() -> AndorResult<()> {
    unsafe {
        return code_to_ok!(WaitForAcquisition());
    }
}

/// Block the calling thread until an acquisition event is received from a
/// specific camera.
///
/// This function is a lightweight alternative to continuously polling with
/// [`get_status`].
pub fn wait_for_acquisition_by_handle(handle: CameraHandle) -> AndorResult<()> {
    unsafe {
        let CameraHandle(h) = handle;
        return code_to_ok!(WaitForAcquisitionByHandle(h as c_int));
    }
}

/// Block the calling thread until an acquisition event is received from a
/// specific camera. If no event is received within `timeout` milliseconds,
/// return `Err(AndorError::Code(AndorReturn::NoNewData))`.
///
/// This function is a lightweight alternative to continuously polling with
/// [`get_status`].
pub fn wait_for_acquisition_by_handle_timeout(
    handle: CameraHandle,
    timeout: u32,
) -> AndorResult<()>
{
    unsafe {
        let CameraHandle(h) = handle;
        return code_to_ok!(
            WaitForAcquisitionByHandleTimeOut(h as c_long, timeout as c_int)
        );
    }
}

/// Block the calling thread until an acqusition event is received from the
/// camera. If no event is received within `timeout` milliseconds, return
/// `Err(AndorError::Code(AndorReturn::NoNewData))`.
///
/// This function is a lightweight alternative to continuously polling with
/// [`get_status`].
pub fn wait_for_acquisition_timeout(timeout: u32) -> AndorResult<()> {
    unsafe {
        return code_to_ok!(WaitForAcquisitionTimeOut(timeout as c_int));
    }
}

/******************************************************************************/

// extern "C" {
//     pub fn DemosaicImage(
//         grey: *mut ::std::os::raw::c_ushort,
//         red: *mut ::std::os::raw::c_ushort,
//         green: *mut ::std::os::raw::c_ushort,
//         blue: *mut ::std::os::raw::c_ushort,
//         info: *mut ColorDemosaicInfo,
//     ) -> ::std::os::raw::c_uint;
// }
// extern "C" {
//     pub fn GetCameraEventStatus(camStatus: *mut ::std::os::raw::c_uint) -> ::std::os::raw::c_uint;
// }
// extern "C" {
//     pub fn GetESDEventStatus(camStatus: *mut ::std::os::raw::c_uint) -> ::std::os::raw::c_uint;
// }
// extern "C" {
//     pub fn SetSizeOfCircularBufferMegaBytes(
//         sizeMB: ::std::os::raw::c_uint,
//     ) -> ::std::os::raw::c_uint;
// }
// extern "C" {
//     pub fn SelectDualSensorPort(port: ::std::os::raw::c_int) -> ::std::os::raw::c_uint;
// }

// /// Return the type of CCD.
// pub fn get_head_model() -> AndorResult<String> {
//     unsafe {
//         todo!()
//     }
// }
// extern "C" {
//     pub fn GetHeadModel(name: *mut ::std::os::raw::c_char) -> ::std::os::raw::c_uint;
// }
