#![allow(dead_code, non_snake_case, non_upper_case_globals)]
#![allow(clippy::needless_return, clippy::upper_case_acronyms)]

pub mod actions;
pub mod andor;
pub mod camera;
pub mod frame_processing;
pub mod viewer;
pub mod colors;
pub mod config;
pub mod cli;
pub mod utils;
pub mod nd_utils;

