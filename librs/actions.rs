//! Definition of actions that can be run from the CLI.
//!
//! Available actions are listed in [`ACTION_REGISTRY`], which is accessible at
//! runtime via the `help` command.

use std::{
    path::PathBuf,
    sync::{ Arc, Mutex },
    thread,
};
use crossbeam::{
    channel::{
        Receiver,
        Sender,
        unbounded,
    },
};
use ndarray as nd;
use ndarray_npy::write_npy;
use phf::{
    self,
    phf_map,
};
use thiserror::Error;
use timetagger::prelude::{ self as tagger, ClickCounter };
use toml::Value;
use crate::{
    mkdir,
    print_flush,
    andor,
    camera::{
        self,
        PrettyString,
    },
    colors,
    config,
    viewer,
    frame_processing as fproc,
};

#[derive(Error, Debug)]
pub enum ActionError {
    #[error("generic error: {0}")]
    GenericError(String),

    #[error("camera is not connected!")]
    CameraNotConnected,

    #[error("frame processor channel closed prematurely")]
    ProcessorChannelClosed,

    #[error("frame viewer channel closed prematurely")]
    ViewerChannelClosed,

    #[error("viewer error: {0}")]
    ViewerError(#[from] viewer::ViewerError),

    #[error("couldn't stack frames")]
    FrameStackError,

    #[error("camera error: {0}")]
    CameraError(#[from] camera::CameraError),

    #[error("timetagger error: {0}")]
    TaggerError(#[from] tagger::TaggerError),

    #[error("timetagger mutability lock has been violated")]
    TaggerMutabilityLock,

    #[error("timetagger counter mutability lock has been violated")]
    TaggerCounterMutabilityLock,

    #[error("config error: {0}")]
    ConfigError(#[from] config::ConfigError),

    #[error("frame processor error: {0}")]
    FrameProcError(#[from] fproc::FrameProcError),

    // #[error("viewer error: {0}")]
    // ViewerError(#[from] iced::Error),

    #[error("no more than one frame processor can be enabled")]
    TooManyFrameProcs,

    #[error("IO error: {0}")]
    IOError(#[from] std::io::Error),

    #[error("data writing error: {0}")]
    DataWriteError(#[from] ndarray_npy::WriteNpyError),
}
pub type ActionResult<T> = Result<T, ActionError>;

macro_rules! jointhrough {
    ($join_handle:ident) => {
        match $join_handle.join() {
            Ok(x) => x,
            Err(e) => std::panic::resume_unwind(e),
        }
    }
}

pub type ActionFn
    = fn(
        Arc<camera::IXon888>,
        &config::Config,
        Option<Arc<Mutex<tagger::TimeTaggerUltra>>>,
        &[String],
    ) -> ActionResult<()>;

pub static ACTION_REGISTRY: phf::Map<&'static str, ActionFn>
    = phf_map!{
        "acquire" => acquire,
        "camera-info" => camera_info,
        "temperature" => temperature,
        "temperature-monitor" => temperature_monitor,
        // "get-capabilities" => get_capabilities,
    };



pub fn camera_info(
    cam: Arc<camera::IXon888>,
    _config: &config::Config,
    _tt: Option<Arc<Mutex<tagger::TimeTaggerUltra>>>,
    args: &[String],
) -> ActionResult<()>
{
    if let Some((prop, rem)) = args.split_first() {
        match prop.as_ref() {
            "system-info" => {
                println!(
                    "  system info: {}",
                    cam.get_system_info()?.pretty_string(1, false)
                );
            },
            "connection-info" => {
                println!(
                    "  connection info: {}",
                    cam.get_connection_info()?.pretty_string(1, false)
                );
            },
            "status" => {
                println!(
                    "  status: {:?}",
                    cam.get_status()?
                );
            },
            "acquisition-timings" => {
                println!(
                    "  acquisition timings: {}",
                    cam.get_acquisition_timings()?.pretty_string(1, false)
                );
            },
            "exposure-time" => {
                println!(
                    "  exposure time: {:?}",
                    cam.get_exposure_time()?
                );
            },
            "cooler" => {
                println!(
                    "  cooler: {:?}",
                    cam.get_cooler()?
                );
            },
            "temperature" => {
                println!(
                    "  temperature: {}",
                    cam.get_temperature()?.pretty_string(1, false)
                );
            },
            "em-gain" => {
                println!(
                    "  em gain: {:?}",
                    cam.get_em_gain()?
                );
            },
            "preamp-gain" => {
                println!(
                    "  preamp gain: {:?}",
                    cam.get_preamp_gain()?
                );
            },
            "number-preamp-gain-modes" => {
                println!(
                    "  number preamp gain modes: {:?}",
                    cam.get_number_preamp_gain_modes()?
                );
            },
            "hs-speed" => {
                println!(
                    "  hs speed: {:?}",
                    cam.get_hs_speed()?
                );
            },
            "number-hs-speed-modes" => {
                println!(
                    "  number hs speed modes: {:?}",
                    cam.get_number_hs_speed_modes()?
                );
            },
            "vs-speed" => {
                println!(
                    "  vs speed: {:?}",
                    cam.get_vs_speed()?
                );
            },
            "number-vs-speed-modes" => {
                println!(
                    "  number vs speed modes: {:?}",
                    cam.get_number_vs_speed_modes()?
                );
            },
            x => {
                println!(
                    "unknown property '{}'",
                    x
                );
            },
        }
        if rem.is_empty() {
            return Ok(());
        } else {
            return camera_info(cam, _config, _tt, rem);
        }
    } else {
        return camera_info(
            cam,
            _config,
            _tt,
            &camera::IXon888::GET_PROPS.iter()
                .map(|prop| prop.to_string())
                .collect::<Vec<String>>(),
        );
    }
}

pub fn temperature(
    cam: Arc<camera::IXon888>,
    _config: &config::Config,
    _tt: Option<Arc<Mutex<tagger::TimeTaggerUltra>>>,
    _args: &[String],
) -> ActionResult<()>
{
    let (temperature, status): (f32, andor::TemperatureStatus)
        = cam.get_temperature()?;
    println!("Temperature: {:+.1}C; {:?} ", temperature, status);
    return Ok(())
}

pub fn temperature_monitor(
    cam: Arc<camera::IXon888>,
    _config: &config::Config,
    _tt: Option<Arc<Mutex<tagger::TimeTaggerUltra>>>,
    _args: &[String],
) -> ActionResult<()>
{
    struct Stop;
    let (tx, rx) = unbounded::<Stop>();
    println!("Press ENTER to stop:");
    let slave = std::thread::spawn(move || -> ActionResult<()> {
        let mut line: String = "".to_string();
        loop {
            if rx.try_recv().is_ok() {
                println!();
                break;
            }
            let (temperature, status): (f32, andor::TemperatureStatus)
                = cam.get_temperature()?;
            print_flush!("\r{}", " ".repeat(line.len()));
            line = format!("  Temperature: {:+.1}C; {:?} ", temperature, status);
            print_flush!("\r{}", line);
            std::thread::sleep(std::time::Duration::from_millis(1000));
        }
        Ok(())
    });

    let mut line = String::new();
    std::io::stdin().read_line(&mut line)?;
    tx.send(Stop).expect("termination channel closed unexpectedly");
    return jointhrough!(slave);
}

macro_rules! get_colormap {
    ($config:ident, $key:expr) => {
        match $config.a::<String>($key)?.as_ref() {
            "gray" => colors::ColorMap::new_grayscale(),
            "grayclip" => colors::ColorMap::new_grayscale_clip(),
            "paperscale" => colors::ColorMap::new_paperscale(),
            "plasma" => colors::ColorMap::new_plasma(),
            "vibrant" => colors::ColorMap::new_vibrant(),
            "artsy" => colors::ColorMap::new_artsy(),
            "pix" => colors::ColorMap::new_pix(),
            "atomwl" => colors::ColorMap::new_atom_wl(),
            _ => colors::ColorMap::new_grayscale(),
        }
    }
}

macro_rules! get_homog_vec {
    ($config:ident, $key:expr, $t:ty) => {
        $config.a::<Vec<Value>>($key)?
            .into_iter()
            .map(|v| {
                v.clone()
                    .try_into::<$t>()
                    .map_err(|_| {
                        config::ConfigError::FailedTypeConversion(
                            v.to_string()
                        )
                    })
            })
            .collect::<config::ConfigResult<Vec<$t>>>()
    }
}

macro_rules! convert_cmap_bound {
    ($val:expr) => {
        if $val < 0.0 {
            colors::ColorMapBound::Auto
        } else {
            colors::ColorMapBound::Fixed($val)
        }
    }
}

pub fn acquire(
    cam: Arc<camera::IXon888>,
    config: &config::Config,
    mut tt: Option<Arc<Mutex<tagger::TimeTaggerUltra>>>,
    _args: &[String]
) -> ActionResult<()>
{
    // channel to carry acquisition-abort signals
    let (tx_acquisition, rx_acquisition) = unbounded::<AbortAcquisition>();

    // channel to carry (processed or raw) frames to the viewer, conditioned on
    // config key `output.show_frames`
    let (tx_viewer, rx_viewer):
        (Option<Sender<viewer::ToViewer>>, Option<Receiver<viewer::ToViewer>>)
        = ["all", "first", "last"] // TODO
        .contains(&config.a::<String>("output.show_frames.mode")?.as_ref())
        .then(unbounded)
        .map_or((None, None), |(tx, rx)| (Some(tx), Some(rx)));
    // and associated color bar
    let colorbar: Option<colors::ColorBar>
        = if tx_viewer.is_some() {
            let scale: colors::ColorMap
                = get_colormap!(config, "output.show_frames.color_scale");
            let bounds: Vec<f64>
                = get_homog_vec!(
                    config,
                    "output.show_frames.colorbar_limits",
                    f64
                )?;
            let min: colors::ColorMapBound = convert_cmap_bound!(bounds[0]);
            let max: colors::ColorMapBound = convert_cmap_bound!(bounds[1]);
            Some((scale, (min, max)))
        } else {
            None
        };

    /* frame processor stuff: */
    // check that no more than one processor has `enabled` == true
    let proc_enables: Vec<fproc::FrameProcSel>
        = fproc::LABEL_SEL.entries()
        .filter_map(|(label, sel)| {
            config.a::<bool>(format!("processing.{}.enabled", label).as_str())
                .ok()
                .and_then(|enabled| enabled.then_some(*sel))
        })
        .collect();
    if proc_enables.len() > 1 {
        return Err(ActionError::TooManyFrameProcs);
    }
    // if one is enabled, create a channel to pass raw frames and initialize the
    // processor
    #[allow(clippy::type_complexity)]
    let (tx_processor, rx_processor, frame_proc):
        (
            Option<Sender<nd::Array2<u16>>>,
            Option<Receiver<nd::Array2<u16>>>,
            Option<fproc::FrameProcResult<fproc::FrameProc>>,
        )
        = proc_enables.first()
        .map_or(
            (None, None, None),
            |proc_sel| {
                let (tx, rx) = unbounded::<nd::Array2<u16>>();
                let proc = fproc::FrameProc::init(*proc_sel, config);
                (Some(tx), Some(rx), Some(proc))
            }
        );
    let frame_proc: Option<fproc::FrameProc> = frame_proc.transpose()?;

    // create another sender for the viewer channel to show processed frames,
    // conditioned on a frame processor being enabled and config key
    // `processing.show`
    let tx_viewer_proc: Option<Sender<viewer::ToViewer>>
        = (
            frame_proc.is_some()
            && ["all", "first", "last"] // TODO
                .contains(&config.a::<String>("processing.show")?.as_ref())
        )
        .then_some(tx_viewer.as_ref())
        .map_or_else(|| None, |tx| tx.cloned());
    // along with associated color bar
    let colorbar_proc: Option<colors::ColorBar>
        = if tx_viewer_proc.is_some() {
            let scale: colors::ColorMap
                = get_colormap!(config, "processing.color_scale");
            let bounds: Vec<f64>
                = get_homog_vec!(config, "processing.colorbar_limits", f64)?;
            let min: colors::ColorMapBound = convert_cmap_bound!(bounds[0]);
            let max: colors::ColorMapBound = convert_cmap_bound!(bounds[1]);
            Some((scale, (min, max)))
        } else {
            None
        };

    let outpath: PathBuf
        = PathBuf::from(config.a::<String>("output.datadir")?)
        .join(config.a::<String>("output.dir")?);
    let mut outfile: Option<PathBuf> = None;
    let mut outfile_tt: Option<PathBuf> = None;
    let mut counter: usize
        = config.a::<i64>("output.counter")?.unsigned_abs() as usize;
    if config.a::<bool>("output.save")? {
        if !outpath.is_dir() {
            mkdir!(outpath);
        }
        let out_name: String = config.a::<String>("output.name")?;
        let mut test_out: PathBuf
            = outpath.join(format!("{}_{:03}.npy", out_name, counter));
        while test_out.is_file() {
            counter += 1;
            test_out
                = outpath.join(format!("{}_{:03}.npy", out_name, counter));
        }
        outfile = Some(test_out.clone());
        let mut file_name = test_out.file_stem().unwrap().to_owned();
        file_name.push(format!("_tt.npy"));
        outfile_tt = Some(
            // test_out.with_file_name(
            //     format!("{:?}_tt.npy", test_out.file_stem().unwrap().to_str().unwrap())
            // )
            test_out.with_file_name(file_name)
        );
        println!("Save data to file '{}'", outfile.as_ref().unwrap().display());
    }

    // run threads / viewer

    // frames are relatively small arrays, but running the frame processor in
    // its own thread requires duplicating frame data into a channel; we may
    // want to switch to just running everything in the acquisition thread
    let thread_processor
        = frame_proc.map(|proc| {
            thread::spawn(move || {
                processing(
                    proc,
                    rx_processor.unwrap(), // unwrap guaranteed by the someness of frame_proc
                    tx_viewer_proc,
                )
            })
        });
    let cam_acquisition = cam.clone();

    let mut gated_channel: Option<tagger::GatedChannel>
        = if let Some(tt_arc) = &mut tt {
            let counter_ch: i32 = config.a("timetagger.counter_channel")?;
            let start_ch: i32 = config.a("timetagger.start_channel")?;
            let stop_ch: i32 = config.a("timetagger.stop_channel")?;
            Some(
                tt_arc.lock()
                    .map_err(|_| ActionError::TaggerMutabilityLock)?
                    .new_gated_channel(
                        counter_ch,
                        start_ch,
                        stop_ch,
                        tagger::GatedChannelInitialState::Closed,
                    )?
            )
        } else {
            None
        };
    let counter: Option<Arc<tagger::Counter>>
        = if let (Some(tt_arc), Some(gated)) = (&mut tt, &mut gated_channel) {
            let binwidth: i64
                = (config.a::<f64>("timetagger.binwidth_sec")? * 1e12) as i64;
            let n_bins: i32 = config.a::<i32>("timetagger.n_bins")?;
            Some(Arc::new(
                tt_arc.lock()
                    .map_err(|_| ActionError::TaggerMutabilityLock)?
                    .new_counter(
                        [&gated.get_channel()?],
                        binwidth,
                        n_bins,
                    )?
            ))
        } else {
            None
        };

    let thread_acquisition
        = thread::spawn(move || {
            acquisition(
                cam_acquisition,
                counter,
                rx_acquisition,
                tx_viewer,
                tx_processor,
            )
        });
    if let Some(rx) = rx_viewer {
        println!("Run acquisition through frame viewer; click `Abort` to stop");
        viewer::FrameViewer::run(
            viewer::ViewerConfig {
                source: rx,
                colorbar: colorbar.unwrap(), // unwrap guaranteed by the someness of rx_viewer
                proc_colorbar: colorbar_proc,
            }
        )?;
    } else {
        println!("Run acquisition without display; press ENTER to stop");
        let mut _line = String::new();
        std::io::stdin().read_line(&mut _line)?;
    }
    cam.cancel_wait().ok();
    tx_acquisition.try_send(AbortAcquisition).ok();

    // wrap up and save data
    let (frame_stack, count_list) = jointhrough!(thread_acquisition)?;
    if let Some(f) = outfile {
        if let FrameStack::Stack(frames) = frame_stack {
            write_npy(f.clone(), &frames)?;
            println!("Data saved to file '{}'", f.display());
        } else {
            println!("Frame stack is empty; no file output produced");
        }
    }
    if let Some(f) = outfile_tt {
        if let CountList::List(tt_counts) = count_list {
            write_npy(f.clone(), &tt_counts)?;
            println!("Timetagger data saved to file '{}'", f.display());
        } else {
            println!("Timetagger counts list is empty; no file output produced");
        }
    }

    if let Some(thread_handle) = thread_processor {
        // TODO: the frame processor is returned from this thread; it's possible
        // to recover some kind of accumulated data here
        jointhrough!(thread_handle)?;
    }

    println!("done\n");

    return Ok(());
}

/// Acquisition abort signal.
pub struct AbortAcquisition;

/// A series of recorded frames.
#[derive(Clone, Debug)]
pub enum FrameStack {
    Stack(nd::Array3<u16>),
    Empty,
}

impl TryFrom<Vec<nd::Array2<u16>>> for FrameStack {
    type Error = ActionError;

    fn try_from(frames: Vec<nd::Array2<u16>>) -> ActionResult<Self> {
        return if frames.is_empty() {
            Ok(Self::Empty)
        } else {
            let frames: nd::Array3<u16>
                = nd::stack(
                    nd::Axis(0),
                    &frames.iter()
                        .map(|f| f.view())
                        .collect::<Vec<nd::ArrayView2<u16>>>(),
                )
                .map_err(|_| ActionError::FrameStackError)?;
            Ok(Self::Stack(frames))
        };
    }
}

/// A series of recorded timetagger counts.
#[derive(Clone, Debug)]
pub enum CountList {
    List(nd::Array1<u64>),
    Empty,
}

impl From<Vec<u64>> for CountList {
    fn from(counts: Vec<u64>) -> Self {
        return if counts.is_empty() {
            Self::Empty
        } else {
            Self::List(counts.into())
        };
    }
}

fn counter_do<F>(counter_opt: &mut Option<Arc<tagger::Counter>>, action: F)
    -> ActionResult<Option<()>>
where F: FnMut(&mut tagger::Counter) -> ActionResult<()>
{
    return counter_opt.as_mut()
        .map(|counter_arc| {
            Arc::get_mut(counter_arc)
                .ok_or(ActionError::TaggerCounterMutabilityLock)
                .and_then(action)
        })
        .transpose();
}

/// Continually reads acquisition data from `cam` and distributes frames across
/// channels for the frame viewer and frame processor.
///
/// Acquisition is terminated upon receipt of [`AbortAcquisition`] on
/// `rx_acquisition`, at which point frames are collected into a single array
/// (whose zero-th axis indexes the frame number) and returned. Note that
/// [`camera::IXon888::wait_for_acquisition`] is called, so some variant of
/// [`crate::andor::cancel_wait`] *must* be called beforehand, otherwise this
/// function will not terminate.
pub fn acquisition(
    cam: Arc<camera::IXon888>,
    mut counter: Option<Arc<tagger::Counter>>,
    rx_acquisition: Receiver<AbortAcquisition>,
    tx_viewer: Option<Sender<viewer::ToViewer>>,
    tx_processor: Option<Sender<nd::Array2<u16>>>,
) -> ActionResult<(FrameStack, CountList)>
{
    let mut frames: Vec<nd::Array2<u16>> = Vec::new();
    let mut tt_counts: Vec<u64> = Vec::new();
    counter_do(&mut counter, |c| { c.start()?; Ok(()) })?;
    cam.start_acquisition()?;
    if let Some(tx) = tx_viewer.as_ref() {
        tx.try_send(viewer::ToViewer::Ready)
            .map_err(|_| ActionError::ViewerChannelClosed)?;
    }
    println!("[thread] acquisition loop is ready");
    loop {
        match cam.wait_for_read_oldest(None) {
            Ok(camera::WaitData::AcquiredData(frame)) => {
                frames.push(frame);
                if let Some(tx) = tx_processor.as_ref() {
                    tx.try_send(frames.last().cloned().unwrap())
                        .map_err(|_| ActionError::ProcessorChannelClosed)?;
                }
                if let Some(tx) = tx_viewer.as_ref() {
                    tx.try_send(
                        viewer::ToViewer::Frame(frames.last().cloned().unwrap())
                    )
                    .map_err(|_| ActionError::ViewerChannelClosed)?;
                }
                counter_do(
                    &mut counter,
                    |c| {
                        tt_counts.push(c.get_data_total_counts()?[0]);
                        Ok(())
                    },
                )?;
            },
            Ok(camera::WaitData::Canceled) => {
                println!("[camera] received cancel wait signal");
                break;
            },
            Err(err) => {
                println!("[camera] error waiting for acquisition: {}", err);
                break;
            },
        }
        if rx_acquisition.try_recv().is_ok() {
            println!("[thread] acquisition received abort signal");
            break;
        }
    }
    cam.stop_acquisition()?;
    counter_do(&mut counter, |c| { c.stop()?; Ok(()) })?;
    let frame_stack: FrameStack = frames.try_into()?;
    let count_list: CountList = tt_counts.into();
    return Ok((frame_stack, count_list));
}

/// Runs the real-time frame processing loop, feeding received frames into
/// `proc` and sending the results back to the viewer.
///
/// Terminates when `rx_processor` is closed.
pub fn processing(
    mut proc: fproc::FrameProc,
    rx_processor: Receiver<nd::Array2<u16>>,
    tx_viewer: Option<Sender<viewer::ToViewer>>,
) -> ActionResult<fproc::FrameProc>
{
    let mut proc_output: Option<nd::Array2<f64>>;
    while let Ok(frame) = rx_processor.recv() {
        proc_output = proc.doit(&frame);
        if let Some(proc_frame) = proc_output {
            if let Some(tx) = &tx_viewer {
                tx.try_send(viewer::ToViewer::ProcFrame(proc_frame))
                    .map_err(|_| ActionError::ViewerChannelClosed)?;
            }
        }
    }
    // println!("terminate frame processing thread");
    return Ok(proc);
}

