use timetagger::prelude::*;

fn main() {
    let devices: Vec<String> = TimeTaggerUltra::scan_devices();
    println!("{:?}", devices);
    let mut tt = TimeTaggerUltra::connect(&devices[0])
        .expect("couldn't connect");
    println!("connected!");
    Counter::new(&mut tt, &[1], 1000000, 1000)
        .expect("couldn't make a counter");
}
