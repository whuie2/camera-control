#pragma once
#include "rust/cxx.h"

rust::Vec<rust::String> scan_devices();

class TimeTaggerBox {
public:
    TimeTaggerBox(const std::string &serial);
    ~TimeTaggerBox();

    TimeTagger *as_ptr();
private:
    TimeTagger *tagger;
};

std::unique_ptr<TimeTaggerBox> create_timetagger(const std::string &);

int64_t get_input_delay(TimeTaggerBox &, int32_t);
void set_input_delay(TimeTaggerBox &, int32_t, int64_t);
int64_t get_delay_hardware(TimeTaggerBox &, int32_t);
void set_delay_hardware(TimeTaggerBox &, int32_t, int64_t);
int64_t get_delay_software(TimeTaggerBox &, int32_t);
void set_delay_software(TimeTaggerBox &, int32_t, int64_t);
void reset(TimeTaggerBox &);
double get_trigger_level(TimeTaggerBox &, int32_t);
void set_trigger_level(TimeTaggerBox &, int32_t, double);
int32_t get_hardware_buffer_size(TimeTaggerBox &);
void set_hardware_buffer_size(TimeTaggerBox &, int32_t);
rust::String get_serial(TimeTaggerBox &);
rust::String get_model(TimeTaggerBox &);
rust::Vec<double> get_dac_range(TimeTaggerBox &);
rust::Vec<int32_t> get_channel_list(TimeTaggerBox &);
void disable_leds(TimeTaggerBox &, bool);

std::unique_ptr<Coincidences> new_coincidences(TimeTaggerBox &, const std::vector<int32_t> &, const std::vector<uint32_t> &, int64_t, int32_t);
/* inherited from IteratorBase */
void coincidences_start(Coincidences &);
void coincidences_start_for(Coincidences &, int64_t, bool);
bool coincidences_wait_until_finished(Coincidences &, int64_t);
void coincidences_stop(Coincidences &);
void coincidences_clear(Coincidences &);
bool coincidences_is_running(Coincidences &);
int64_t coincidences_get_capture_duration(Coincidences &);
rust::String coincidences_get_configuration(Coincidences &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> coincidences_get_channels(Coincidences &);
void coincidences_set_coincidence_window(Coincidences &, int64_t);

std::unique_ptr<Combiner> new_combiner(TimeTaggerBox &, const std::vector<int32_t> &);
/* inherited from IteratorBase */
void combiner_start(Combiner &);
void combiner_start_for(Combiner &, int64_t, bool);
bool combiner_wait_until_finished(Combiner &, int64_t);
void combiner_stop(Combiner &);
void combiner_clear(Combiner &);
bool combiner_is_running(Combiner &);
int64_t combiner_get_capture_duration(Combiner &);
rust::String combiner_get_configuration(Combiner &);
/* * * * * * * * * * * * * * * */
rust::Vec<int64_t> combiner_get_channel_counts(Combiner &);
int32_t combiner_get_channel(Combiner &);

std::unique_ptr<Correlation> new_correlation(TimeTaggerBox &, int32_t, int32_t, int64_t, int32_t);
/* inherited from IteratorBase */
void correlation_start(Correlation &);
void correlation_start_for(Correlation &, int64_t, bool);
bool correlation_wait_until_finished(Correlation &, int64_t);
void correlation_stop(Correlation &);
void correlation_clear(Correlation &);
bool correlation_is_running(Correlation &);
int64_t correlation_get_capture_duration(Correlation &);
rust::String correlation_get_configuration(Correlation &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> correlation_get_data(Correlation &);
rust::Vec<double> correlation_get_data_normalized(Correlation &);
rust::Vec<int64_t> correlation_get_index(Correlation &);

std::unique_ptr<CountBetweenMarkers> new_count_between_markers(TimeTaggerBox &, int32_t, int32_t, int32_t, int32_t);
/* inherited from IteratorBase */
void count_between_markers_start(CountBetweenMarkers &);
void count_between_markers_start_for(CountBetweenMarkers &, int64_t, bool);
bool count_between_markers_wait_until_finished(CountBetweenMarkers &, int64_t);
void count_between_markers_stop(CountBetweenMarkers &);
void count_between_markers_clear(CountBetweenMarkers &);
bool count_between_markers_is_running(CountBetweenMarkers &);
int64_t count_between_markers_get_capture_duration(CountBetweenMarkers &);
rust::String count_between_markers_get_configuration(CountBetweenMarkers &);
/* * * * * * * * * * * * * * * */
bool count_between_markers_ready(CountBetweenMarkers &);
rust::Vec<int32_t> count_between_markers_get_data(CountBetweenMarkers &);
rust::Vec<int64_t> count_between_markers_get_bin_widths(CountBetweenMarkers &);
rust::Vec<int64_t> count_between_markers_get_index(CountBetweenMarkers &);

std::unique_ptr<Counter> new_counter(TimeTaggerBox &, const std::vector<int32_t> &, int64_t, int32_t);
/* inherited from IteratorBase */
void counter_start(Counter &);
void counter_start_for(Counter &, int64_t, bool);
bool counter_wait_until_finished(Counter &, int64_t);
void counter_stop(Counter &);
void counter_clear(Counter &);
bool counter_is_running(Counter &);
int64_t counter_get_capture_duration(Counter &);
rust::String counter_get_configuration(Counter &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> counter_get_data(Counter &, bool);
rust::Vec<uint64_t> counter_get_data_total_counts(Counter &);
rust::Vec<int64_t> counter_get_index(Counter &);
std::unique_ptr<CounterData> counter_get_data_object(Counter &, bool);

std::unique_ptr<Countrate> new_countrate(TimeTaggerBox &, const std::vector<int32_t> &);
/* inherited from IteratorBase */
void countrate_start(Countrate &);
void countrate_start_for(Countrate &, int64_t, bool);
bool countrate_wait_until_finished(Countrate &, int64_t);
void countrate_stop(Countrate &);
void countrate_clear(Countrate &);
bool countrate_is_running(Countrate &);
int64_t countrate_get_capture_duration(Countrate &);
rust::String countrate_get_configuration(Countrate &);
/* * * * * * * * * * * * * * * */
rust::Vec<double> countrate_get_data(Countrate &);
rust::Vec<int64_t> countrate_get_counts_total(Countrate &);

std::unique_ptr<DelayedChannel> new_delayed_channel(TimeTaggerBox &, int32_t, int64_t);
void delayed_channel_start(DelayedChannel &);
void delayed_channel_start_for(DelayedChannel &, int64_t, bool);
bool delayed_channel_wait_until_finished(DelayedChannel &, int64_t);
void delayed_channel_stop(DelayedChannel &);
void delayed_channel_clear(DelayedChannel &);
bool delayed_channel_is_running(DelayedChannel &);
int64_t delayed_channel_get_capture_duration(DelayedChannel &);
rust::String delayed_channel_get_configuration(DelayedChannel &);
/* * * * * * * * * * * * * * * */
int32_t delayed_channel_get_channel(DelayedChannel &);
void delayed_channel_set_delay(DelayedChannel &, int64_t);

std::unique_ptr<FrequencyMultiplier> new_frequency_multiplier(TimeTaggerBox &, int32_t, int32_t);
/* inherited from IteratorBase */
void frequency_multiplier_start(FrequencyMultiplier &);
void frequency_multiplier_start_for(FrequencyMultiplier &, int64_t, bool);
bool frequency_multiplier_wait_until_finished(FrequencyMultiplier &, int64_t);
void frequency_multiplier_stop(FrequencyMultiplier &);
void frequency_multiplier_clear(FrequencyMultiplier &);
bool frequency_multiplier_is_running(FrequencyMultiplier &);
int64_t frequency_multiplier_get_capture_duration(FrequencyMultiplier &);
rust::String frequency_multiplier_get_configuration(FrequencyMultiplier &);
/* * * * * * * * * * * * * * * */
int32_t frequency_multiplier_get_channel(FrequencyMultiplier &);
int32_t frequency_multiplier_get_multiplier(FrequencyMultiplier &);

std::unique_ptr<GatedChannel> new_gated_channel(TimeTaggerBox &, int32_t, int32_t, int32_t, uint32_t);
/* inherited from IteratorBase */
void gated_channel_start(GatedChannel &);
void gated_channel_start_for(GatedChannel &, int64_t, bool);
bool gated_channel_wait_until_finished(GatedChannel &, int64_t);
void gated_channel_stop(GatedChannel &);
void gated_channel_clear(GatedChannel &);
bool gated_channel_is_running(GatedChannel &);
int64_t gated_channel_get_capture_duration(GatedChannel &);
rust::String gated_channel_get_configuration(GatedChannel &);
/* * * * * * * * * * * * * * * */
int32_t gated_channel_get_channel(GatedChannel &);

std::unique_ptr<Histogram> new_histogram(TimeTaggerBox &, int32_t, int32_t, int64_t, int32_t);
/* inherited from IteratorBase */
void histogram_start(Histogram &);
void histogram_start_for(Histogram &, int64_t, bool);
bool histogram_wait_until_finished(Histogram &, int64_t);
void histogram_stop(Histogram &);
void histogram_clear(Histogram &);
bool histogram_is_running(Histogram &);
int64_t histogram_get_capture_duration(Histogram &);
rust::String histogram_get_configuration(Histogram &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> histogram_get_data(Histogram &);
rust::Vec<int64_t> histogram_get_index(Histogram &);

std::unique_ptr<Histogram2D> new_histogram_2d(TimeTaggerBox &, int32_t, int32_t, int32_t, int64_t, int64_t, int32_t, int32_t);
/* inherited from IteratorBase */
void histogram_2d_start(Histogram2D &);
void histogram_2d_start_for(Histogram2D &, int64_t, bool);
bool histogram_2d_wait_until_finished(Histogram2D &, int64_t);
void histogram_2d_stop(Histogram2D &);
void histogram_2d_clear(Histogram2D &);
bool histogram_2d_is_running(Histogram2D &);
int64_t histogram_2d_get_capture_duration(Histogram2D &);
rust::String histogram_2d_get_configuration(Histogram2D &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> histogram_2d_get_data(Histogram2D &);
rust::Vec<int64_t> histogram_2d_get_index(Histogram2D &);
rust::Vec<int64_t> histogram_2d_get_index_1(Histogram2D &);
rust::Vec<int64_t> histogram_2d_get_index_2(Histogram2D &);

std::unique_ptr<HistogramND> new_histogram_nd(TimeTaggerBox &, int32_t, const std::vector<int32_t> &, const std::vector<int64_t> &, const std::vector<int32_t> &);
/* inherited from IteratorBase */
void histogram_nd_start(HistogramND &);
void histogram_nd_start_for(HistogramND &, int64_t, bool);
bool histogram_nd_wait_until_finished(HistogramND &, int64_t);
void histogram_nd_stop(HistogramND &);
void histogram_nd_clear(HistogramND &);
bool histogram_nd_is_running(HistogramND &);
int64_t histogram_nd_get_capture_duration(HistogramND &);
rust::String histogram_nd_get_configuration(HistogramND &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> histogram_nd_get_data(HistogramND &);
rust::Vec<int64_t> histogram_nd_get_index(HistogramND &, int32_t);

std::unique_ptr<HistogramLogBins> new_histogram_log_bins(TimeTaggerBox &, int32_t, int32_t, double, double, int32_t);
/* inherited from IteratorBase */
void histogram_log_bins_start(HistogramLogBins &);
void histogram_log_bins_start_for(HistogramLogBins &, int64_t, bool);
bool histogram_log_bins_wait_until_finished(HistogramLogBins &, int64_t);
void histogram_log_bins_stop(HistogramLogBins &);
void histogram_log_bins_clear(HistogramLogBins &);
bool histogram_log_bins_is_running(HistogramLogBins &);
int64_t histogram_log_bins_get_capture_duration(HistogramLogBins &);
rust::String histogram_log_bins_get_configuration(HistogramLogBins &);
/* * * * * * * * * * * * * * * */
rust::Vec<uint64_t> histogram_log_bins_get_data(HistogramLogBins &);
rust::Vec<double> histogram_log_bins_get_data_normalized_g2(HistogramLogBins &);
rust::Vec<int64_t> histogram_log_bins_get_bin_edges(HistogramLogBins &);

std::unique_ptr<StartStop> new_start_stop(TimeTaggerBox &, int32_t, int32_t, int64_t);
/* inherited from IteratorBase */
void start_stop_start(StartStop &);
void start_stop_start_for(StartStop &, int64_t, bool);
bool start_stop_wait_until_finished(StartStop &, int64_t);
void start_stop_stop(StartStop &);
void start_stop_clear(StartStop &);
bool start_stop_is_running(StartStop &);
int64_t start_stop_get_capture_duration(StartStop &);
rust::String start_stop_get_configuration(StartStop &);
/* * * * * * * * * * * * * * * */
rust::Vec<int64_t> start_stop_get_data(StartStop &);

std::unique_ptr<TimeDifferences> new_time_differences(TimeTaggerBox &, int32_t, int32_t, int32_t, int32_t, int64_t, int32_t, int32_t);
/* inherited from IteratorBase */
void time_differences_start(TimeDifferences &);
void time_differences_start_for(TimeDifferences &, int64_t, bool);
bool time_differences_wait_until_finished(TimeDifferences &, int64_t);
void time_differences_stop(TimeDifferences &);
void time_differences_clear(TimeDifferences &);
bool time_differences_is_running(TimeDifferences &);
int64_t time_differences_get_capture_duration(TimeDifferences &);
rust::String time_differences_get_configuration(TimeDifferences &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> time_differences_get_data(TimeDifferences &);
rust::Vec<int64_t> time_differences_get_index(TimeDifferences &);
void time_differences_set_max_counts(TimeDifferences &, uint64_t);
uint64_t time_differences_get_counts(TimeDifferences &);
int32_t time_differences_get_histogram_index(TimeDifferences &);
bool time_differences_ready(TimeDifferences &);

std::unique_ptr<TimeDifferencesND> new_time_differences_nd(TimeTaggerBox &, int32_t, int32_t, const std::vector<int32_t> &, const std::vector<int32_t> &, const std::vector<int32_t> &, int64_t, int32_t);
/* inherited from IteratorBase */
void time_differences_nd_start(TimeDifferencesND &);
void time_differences_nd_start_for(TimeDifferencesND &, int64_t, bool);
bool time_differences_nd_wait_until_finished(TimeDifferencesND &, int64_t);
void time_differences_nd_stop(TimeDifferencesND &);
void time_differences_nd_clear(TimeDifferencesND &);
bool time_differences_nd_is_running(TimeDifferencesND &);
int64_t time_differences_nd_get_capture_duration(TimeDifferencesND &);
rust::String time_differences_nd_get_configuration(TimeDifferencesND &);
/* * * * * * * * * * * * * * * */
rust::Vec<int32_t> time_differences_nd_get_data(TimeDifferencesND &);
rust::Vec<int64_t> time_differences_nd_get_index(TimeDifferencesND &);

std::unique_ptr<TriggerOnCountrate> new_trigger_on_countrate(TimeTaggerBox &, int32_t, double, double, int64_t);
/* inherited from IteratorBase */
void trigger_on_countrate_start(TriggerOnCountrate &);
void trigger_on_countrate_start_for(TriggerOnCountrate &, int64_t, bool);
bool trigger_on_countrate_wait_until_finished(TriggerOnCountrate &, int64_t);
void trigger_on_countrate_stop(TriggerOnCountrate &);
void trigger_on_countrate_clear(TriggerOnCountrate &);
bool trigger_on_countrate_is_running(TriggerOnCountrate &);
int64_t trigger_on_countrate_get_capture_duration(TriggerOnCountrate &);
rust::String trigger_on_countrate_get_configuration(TriggerOnCountrate &);
/* * * * * * * * * * * * * * * */
int32_t trigger_on_countrate_get_channel_above(TriggerOnCountrate &);
int32_t trigger_on_countrate_get_channel_below(TriggerOnCountrate &);
rust::Vec<int32_t> trigger_on_countrate_get_channels(TriggerOnCountrate &);
bool trigger_on_countrate_is_above(TriggerOnCountrate &);
bool trigger_on_countrate_is_below(TriggerOnCountrate &);
double trigger_on_countrate_get_current_countrate(TriggerOnCountrate &);
bool trigger_on_countrate_inject_current_state(TriggerOnCountrate &);

rust::Vec<int32_t> counter_data_get_data(CounterData &);
rust::Vec<uint64_t> counter_data_get_data_total_counts(CounterData &);
rust::Vec<int64_t> counter_data_get_index(CounterData &);
rust::Vec<int64_t> counter_data_get_time(CounterData &);
rust::Vec<signed char> counter_data_get_overflow_mask(CounterData &);
rust::Vec<int32_t> counter_data_get_channels(CounterData &);
uint32_t counter_data_get_size(CounterData &);
uint32_t counter_data_get_dropped_bins(CounterData &);
bool counter_data_get_overflow(CounterData &);

