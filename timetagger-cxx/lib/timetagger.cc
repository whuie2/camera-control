#if _Windows
    #include "C:\Program Files\Swabian Instruments\Time Tagger\driver\include\TimeTagger.h"
    #include "C:\Program Files\Swabian Instruments\Time Tagger\driver\include\Iterators.h"
#elif _WIN32
    #include "C:\Program Files\Swabian Instruments\Time Tagger\driver\include\TimeTagger.h"
    #include "C:\Program Files\Swabian Instruments\Time Tagger\driver\include\Iterators.h"
#elif _WIN64
    #include "C:\Program Files\Swabian Instruments\Time Tagger\driver\include\TimeTagger.h"
    #include "C:\Program Files\Swabian Instruments\Time Tagger\driver\include\Iterators.h"
#else
    #include "/usr/include/timetagger/TimeTagger.h"
    #include "/usr/include/timetagger/Iterators.h"
#endif
#include "rust/cxx.h"
#include "timetagger-cxx/include/timetagger.h"

/* dumb conversion functions */

rust::String to_string(std::string &s) {
    return rust::String(s);
}

rust::Vec<rust::String> to_string_vec(std::vector<std::string> &v) {
    rust::Vec<rust::String> out;
    for (std::string &s : v)
        out.push_back(to_string(s));
    return out;
}

rust::Vec<signed char> to_i8_vec(std::vector<signed char> &v) {
    rust::Vec<signed char> out;
    for (signed char c : v)
        out.push_back(c);
    return out;
}

rust::Vec<int32_t> to_i32_vec(std::vector<int32_t> &v) {
    rust::Vec<int32_t> out;
    for (int32_t i : v)
        out.push_back(i);
    return out;
}

rust::Vec<int64_t> to_i64_vec(std::vector<int64_t> &v) {
    rust::Vec<int64_t> out;
    for (int64_t i : v)
        out.push_back(i);
    return out;
}

rust::Vec<int64_t> to_i64_vec_timestamp(std::vector<timestamp_t> &v) {
    rust::Vec<int64_t> out;
    for (timestamp_t i: v)
        out.push_back(i);
    return out;
}

rust::Vec<uint64_t> to_u64_vec(std::vector<uint64_t> &v) {
    rust::Vec<uint64_t> out;
    for (uint64_t u : v)
        out.push_back(u);
    return out;
}

rust::Vec<double> to_f64_vec(std::vector<double> &v) {
    rust::Vec<double> out;
    for (double f : v)
        out.push_back(f);
    return out;
}

/* actual ffi functions */

rust::Vec<rust::String> scan_devices() {
    std::vector<std::string> devices = scanTimeTagger();
    return to_string_vec(devices);
}

/* TimeTagger */

TimeTaggerBox::TimeTaggerBox(const std::string &serial) {
    this->tagger = createTimeTagger(serial);
}

TimeTaggerBox::~TimeTaggerBox() {
    freeTimeTagger(this->tagger);
}

TimeTagger *TimeTaggerBox::as_ptr() {
    return this->tagger;
}

std::unique_ptr<TimeTaggerBox> create_timetagger(const std::string &serial) {
    std::unique_ptr<TimeTaggerBox>
        tagger_ptr_unique(new TimeTaggerBox(serial));
    return tagger_ptr_unique;
}

int64_t get_input_delay(TimeTaggerBox &tagger, int32_t channel) {
    return tagger.as_ptr()->getInputDelay(channel);
}

void set_input_delay(TimeTaggerBox &tagger, int32_t channel, int64_t delay) {
    tagger.as_ptr()->setInputDelay(channel, delay);
}

int64_t get_delay_hardware(TimeTaggerBox &tagger, int32_t channel) {
    return tagger.as_ptr()->getDelayHardware(channel);
}

void set_delay_hardware(TimeTaggerBox &tagger, int32_t channel, int64_t delay) {
    tagger.as_ptr()->setDelayHardware(channel, delay);
}

int64_t get_delay_software(TimeTaggerBox &tagger, int32_t channel) {
    return tagger.as_ptr()->getDelaySoftware(channel);
}

void set_delay_software(TimeTaggerBox &tagger, int32_t channel, int64_t delay) {
    tagger.as_ptr()->setDelaySoftware(channel, delay);
}

void reset(TimeTaggerBox &tagger) {
    tagger.as_ptr()->reset();
}

double get_trigger_level(TimeTaggerBox &tagger, int32_t channel) {
    return tagger.as_ptr()->getTriggerLevel(channel);
}

void set_trigger_level(TimeTaggerBox &tagger, int32_t channel, double voltage) {
    tagger.as_ptr()->setTriggerLevel(channel, voltage);
}

int32_t get_hardware_buffer_size(TimeTaggerBox &tagger) {
    return tagger.as_ptr()->getHardwareBufferSize();
}

void set_hardware_buffer_size(TimeTaggerBox &tagger, int32_t size) {
    tagger.as_ptr()->setHardwareBufferSize(size);
}

rust::String get_serial(TimeTaggerBox &tagger) {
    std::string serial = tagger.as_ptr()->getSerial();
    return to_string(serial);
}

rust::String get_model(TimeTaggerBox &tagger) {
    std::string model = tagger.as_ptr()->getModel();
    return to_string(model);
}

rust::Vec<double> get_dac_range(TimeTaggerBox &tagger) {
    std::vector<double> range = tagger.as_ptr()->getDACRange();
    return to_f64_vec(range);
}

rust::Vec<int32_t> get_channel_list(TimeTaggerBox &tagger) {
    std::vector<int32_t> channels = tagger.as_ptr()->getChannelList();
    return to_i32_vec(channels);
}

void disable_leds(TimeTaggerBox &tagger, bool disable) {
    tagger.as_ptr()->disableLEDs(disable);
}

/* Coincidences */

std::unique_ptr<Coincidences> new_coincidences(
    TimeTaggerBox &tagger,
    const std::vector<int32_t> &channels,
    const std::vector<uint32_t> &groupings,
    int64_t window,
    int32_t timestamp
) {
    std::vector<std::vector<int32_t>> channel_groups;
    std::vector<int32_t> group;
    uint32_t offs = 0;
    for (uint32_t groupsize : groupings) {
        group = std::vector<int32_t>(
            channels.begin() + offs,
            channels.begin() + offs + groupsize - 1
        );
        channel_groups.push_back(group);
        offs += groupsize;
    }
    CoincidenceTimestamp timestamp_obj;
    switch (timestamp) {
        case 0:
            timestamp_obj = CoincidenceTimestamp::Last;
            break;
        case 1:
            timestamp_obj = CoincidenceTimestamp::Average;
            break;
        case 2:
            timestamp_obj = CoincidenceTimestamp::First;
            break;
        case 3:
            timestamp_obj = CoincidenceTimestamp::ListedFirst;
            break;
        default:
            timestamp_obj = CoincidenceTimestamp::Last;
            break;
    }
    std::unique_ptr<Coincidences>
        coincidences_ptr_unique(
            new Coincidences(
                tagger.as_ptr(), channel_groups, window, timestamp_obj));
    return coincidences_ptr_unique;
}

void coincidences_start(Coincidences &coincidences) {
    coincidences.start();
}

void coincidences_start_for(
    Coincidences &coincidences,
    int64_t duration,
    bool clear
) {
    coincidences.startFor(duration, clear);
}

bool coincidences_wait_until_finished(
    Coincidences &coincidences,
    int64_t timeout
) {
    return coincidences.waitUntilFinished(timeout);
}

void coincidences_stop(Coincidences &coincidences) {
    coincidences.stop();
}

void coincidences_clear(Coincidences &coincidences) {
    coincidences.clear();
}

bool coincidences_is_running(Coincidences &coincidences) {
    return coincidences.isRunning();
}

int64_t coincidences_get_capture_duration(Coincidences &coincidences) {
    return coincidences.getCaptureDuration();
}

rust::String coincidences_get_configuration(Coincidences &coincidences) {
    std::string config = coincidences.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> coincidences_get_channels(Coincidences &coincidences) {
    std::vector<int32_t> channels = coincidences.getChannels();
    return to_i32_vec(channels);
}

void coincidences_set_coincidence_window(
    Coincidences &coincidences,
    int64_t window
) {
    coincidences.setCoincidenceWindow(window);
}

/* Combiner */

std::unique_ptr<Combiner> new_combiner(
    TimeTaggerBox &tagger,
    const std::vector<int32_t> &channels
) {
    std::unique_ptr<Combiner>
        combiner_ptr_unique(
            new Combiner(tagger.as_ptr(), channels));
    return combiner_ptr_unique;
}

void combiner_start(Combiner &combiner) {
    combiner.start();
}

void combiner_start_for(Combiner &combiner, int64_t duration, bool clear) {
    combiner.startFor(duration, clear);
}

bool combiner_wait_until_finished(Combiner &combiner, int64_t timeout) {
    return combiner.waitUntilFinished(timeout);
}

void combiner_stop(Combiner &combiner) {
    combiner.stop();
}

void combiner_clear(Combiner &combiner) {
    combiner.clear();
}

bool combiner_is_running(Combiner &combiner) {
    return combiner.isRunning();
}

int64_t combiner_get_capture_duration(Combiner &combiner) {
    return combiner.getCaptureDuration();
}

rust::String combiner_get_configuration(Combiner &combiner) {
    std::string config = combiner.getConfiguration();
    return to_string(config);
}

rust::Vec<int64_t> combiner_get_channel_counts(Combiner &combiner) {
    std::vector<int64_t> data;
    combiner.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec(data);
}

int32_t combiner_get_channel(Combiner &combiner) {
    return combiner.getChannel();
}

/* Correlation */

std::unique_ptr<Correlation> new_correlation(
    TimeTaggerBox &tagger,
    int32_t channel_1,
    int32_t channel_2,
    int64_t binwidth,
    int32_t n_bins
) {
    std::unique_ptr<Correlation>
        correlation_ptr_unique(
            new Correlation(
                tagger.as_ptr(), channel_1, channel_2, binwidth, n_bins));
    return correlation_ptr_unique;
}

void correlation_start(Correlation &correlation) {
    correlation.start();
}

void correlation_start_for(
    Correlation &correlation,
    int64_t duration,
    bool clear
) {
    correlation.startFor(duration, clear);
}

bool correlation_wait_until_finished(
    Correlation &correlation,
    int64_t timeout
) {
    return correlation.waitUntilFinished(timeout);
}

void correlation_stop(Correlation &correlation) {
    correlation.stop();
}

void correlation_clear(Correlation &correlation) {
    correlation.clear();
}

bool correlation_is_running(Correlation &correlation) {
    return correlation.isRunning();
}

int64_t correlation_get_capture_duration(Correlation &correlation) {
    return correlation.getCaptureDuration();
}

rust::String correlation_get_configuration(Correlation &correlation) {
    std::string config = correlation.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> correlation_get_data(Correlation &correlation) {
    std::vector<int32_t> data;
    correlation.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<double> correlation_get_data_normalized(Correlation &correlation) {
    std::vector<double> data;
    correlation.getDataNormalized([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_f64_vec(data);
}

rust::Vec<int64_t> correlation_get_index(Correlation &correlation) {
    std::vector<timestamp_t> data;
    correlation.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* CountBetweenMarkers */

std::unique_ptr<CountBetweenMarkers> new_count_between_markers(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    int32_t stop_ch,
    int32_t n_values
) {
    std::unique_ptr<CountBetweenMarkers>
        count_between_markers_ptr_unique(
            new CountBetweenMarkers(
                tagger.as_ptr(), signal_ch, start_ch, stop_ch, n_values));
    return count_between_markers_ptr_unique;
}

void count_between_markers_start(CountBetweenMarkers &count_between_markers) {
    count_between_markers.start();
}

void count_between_markers_start_for(
    CountBetweenMarkers &count_between_markers,
    int64_t duration,
    bool clear
) {
    count_between_markers.startFor(duration, clear);
}

bool count_between_markers_wait_until_finished(
    CountBetweenMarkers &count_between_markers,
    int64_t timeout
) {
    return count_between_markers.waitUntilFinished(timeout);
}

void count_between_markers_stop(CountBetweenMarkers &count_between_markers) {
    count_between_markers.stop();
}

void count_between_markers_clear(CountBetweenMarkers &count_between_markers) {
    count_between_markers.clear();
}

bool count_between_markers_is_running(
    CountBetweenMarkers &count_between_markers
) {
    return count_between_markers.isRunning();
}

int64_t count_between_markers_get_capture_duration(
    CountBetweenMarkers &count_between_markers
) {
    return count_between_markers.getCaptureDuration();
}

rust::String count_between_markers_get_configuration(
    CountBetweenMarkers &count_between_markers
) {
    std::string config = count_between_markers.getConfiguration();
    return to_string(config);
}

bool count_between_markers_ready(CountBetweenMarkers &count_between_markers) {
    return count_between_markers.ready();
}

rust::Vec<int32_t> count_between_markers_get_data(
    CountBetweenMarkers &count_between_markers
) {
    std::vector<int32_t> data;
    count_between_markers.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<int64_t> count_between_markers_get_bin_widths(
    CountBetweenMarkers &count_between_markers
) {
    std::vector<timestamp_t> data;
    count_between_markers.getBinWidths([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

rust::Vec<int64_t> count_between_markers_get_index(
    CountBetweenMarkers &count_between_markers
) {
    std::vector<timestamp_t> data;
    count_between_markers.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* Counter */

std::unique_ptr<Counter> new_counter(
    TimeTaggerBox &tagger,
    const std::vector<int32_t> &channels,
    int64_t binwidth,
    int32_t n_bins
) {
    std::unique_ptr<Counter>
        counter_ptr_unique(
            new Counter(tagger.as_ptr(), channels, binwidth, n_bins));
    return counter_ptr_unique;
}

void counter_start(Counter &counter) {
    counter.start();
}

void counter_start_for(Counter &counter, int64_t duration, bool clear) {
    counter.startFor(duration, clear);
}

bool counter_wait_until_finished(Counter &counter, int64_t timeout) {
    return counter.waitUntilFinished(timeout);
}

void counter_stop(Counter &counter) {
    counter.stop();
}

void counter_clear(Counter &counter) {
    counter.clear();
}

bool counter_is_running(Counter &counter) {
    return counter.isRunning();
}

int64_t counter_get_capture_duration(Counter &counter) {
    return counter.getCaptureDuration();
}

rust::String counter_get_configuration(Counter &counter) {
    std::string config = counter.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> counter_get_data(Counter &counter, bool rolling) {
    std::vector<int32_t> data;
    counter.getData(
        [&data](size_t size_1, size_t size_2) {
            data.resize(size_1 * size_2);
            return data.data();
        },
        rolling
    );
    return to_i32_vec(data);
}

rust::Vec<uint64_t> counter_get_data_total_counts(Counter &counter) {
    std::vector<uint64_t> data;
    counter.getDataTotalCounts([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_u64_vec(data);
}

rust::Vec<int64_t> counter_get_index(Counter &counter) {
    std::vector<timestamp_t> data;
    counter.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

std::unique_ptr<CounterData> counter_get_data_object(
    Counter &counter,
    bool remove
) {
    CounterData counter_data = counter.getDataObject(remove);
    std::unique_ptr<CounterData> counter_data_ptr_unique(&counter_data);
    return counter_data_ptr_unique;
}

/* Countrate */

std::unique_ptr<Countrate> new_countrate(
    TimeTaggerBox &tagger,
    const std::vector<int32_t> &channels
) {
    std::unique_ptr<Countrate>
        countrate_ptr_unique(
            new Countrate(tagger.as_ptr(), channels));
    return countrate_ptr_unique;
}

void countrate_start(Countrate &countrate) {
    countrate.start();
}

void countrate_start_for(Countrate &countrate, int64_t duration, bool clear) {
    countrate.startFor(duration, clear);
}

bool countrate_wait_until_finished(Countrate &countrate, int64_t timeout) {
    return countrate.waitUntilFinished(timeout);
}

void countrate_stop(Countrate &countrate) {
    countrate.stop();
}

void countrate_clear(Countrate &countrate) {
    countrate.clear();
}

bool countrate_is_running(Countrate &countrate) {
    return countrate.isRunning();
}

int64_t countrate_get_capture_duration(Countrate &countrate) {
    return countrate.getCaptureDuration();
}

rust::String countrate_get_configuration(Countrate &countrate) {
    std::string config = countrate.getConfiguration();
    return to_string(config);
}

rust::Vec<double> countrate_get_data(Countrate &countrate) {
    std::vector<double> data;
    countrate.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_f64_vec(data);
}

rust::Vec<int64_t> countrate_get_counts_total(Countrate &countrate) {
    std::vector<int64_t> data;
    countrate.getCountsTotal([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec(data);
}

/* DelayedChannel */

std::unique_ptr<DelayedChannel> new_delayed_channel(
    TimeTaggerBox &tagger,
    int32_t channel,
    int64_t delay
) {
    std::unique_ptr<DelayedChannel>
        delayed_channel_ptr_unique(
            new DelayedChannel(tagger.as_ptr(), channel, delay));
    return delayed_channel_ptr_unique;
}

void delayed_channel_start(DelayedChannel &delayed_channel) {
    delayed_channel.start();
}

void delayed_channel_start_for(
    DelayedChannel &delayed_channel,
    int64_t duration,
    bool clear
) {
    delayed_channel.startFor(duration, clear);
}

bool delayed_channel_wait_until_finished(
    DelayedChannel &delayed_channel,
    int64_t timeout
) {
    return delayed_channel.waitUntilFinished(timeout);
}

void delayed_channel_stop(DelayedChannel &delayed_channel) {
    delayed_channel.stop();
}

void delayed_channel_clear(DelayedChannel &delayed_channel) {
    delayed_channel.clear();
}

bool delayed_channel_is_running(DelayedChannel &delayed_channel) {
    return delayed_channel.isRunning();
}

int64_t delayed_channel_get_capture_duration(DelayedChannel &delayed_channel) {
    return delayed_channel.getCaptureDuration();
}

rust::String delayed_channel_get_configuration(
    DelayedChannel &delayed_channel
) {
    std::string config = delayed_channel.getConfiguration();
    return to_string(config);
}

int32_t delayed_channel_get_channel(DelayedChannel &delayed_channel) {
    return delayed_channel.getChannel();
}

void delayed_channel_set_delay(DelayedChannel &delayed_channel, int64_t delay) {
    delayed_channel.setDelay(delay);
}

/* FrequencyMultiplier */

std::unique_ptr<FrequencyMultiplier> new_frequency_multiplier(
    TimeTaggerBox &tagger,
    int32_t channel,
    int32_t multiplier
) {
    std::unique_ptr<FrequencyMultiplier>
        frequency_multiplier_ptr_unique(
            new FrequencyMultiplier(tagger.as_ptr(), channel, multiplier));
    return frequency_multiplier_ptr_unique;
}

void frequency_multiplier_start(FrequencyMultiplier &frequency_multiplier) {
    frequency_multiplier.start();
}

void frequency_multiplier_start_for(
    FrequencyMultiplier &frequency_multiplier,
    int64_t duration,
    bool clear
) {
    frequency_multiplier.startFor(duration, clear);
}

bool frequency_multiplier_wait_until_finished(
    FrequencyMultiplier &frequency_multiplier,
    int64_t timeout
) {
    return frequency_multiplier.waitUntilFinished(timeout);
}

void frequency_multiplier_stop(FrequencyMultiplier &frequency_multiplier) {
    frequency_multiplier.stop();
}

void frequency_multiplier_clear(FrequencyMultiplier &frequency_multiplier) {
    frequency_multiplier.clear();
}

bool frequency_multiplier_is_running(FrequencyMultiplier &frequency_multiplier) {
    return frequency_multiplier.isRunning();
}

int64_t frequency_multiplier_get_capture_duration(
    FrequencyMultiplier &frequency_multiplier
) {
    return frequency_multiplier.getCaptureDuration();
}

rust::String frequency_multiplier_get_configuration(
    FrequencyMultiplier &frequency_multiplier
) {
    std::string config = frequency_multiplier.getConfiguration();
    return to_string(config);
}

int32_t frequency_multiplier_get_channel(
    FrequencyMultiplier &frequency_multiplier
) {
    return frequency_multiplier.getChannel();
}

int32_t frequency_multiplier_get_multiplier(
    FrequencyMultiplier &frequency_multiplier
) {
    return frequency_multiplier.getMultiplier();
}

/* GatedChannel */

std::unique_ptr<GatedChannel> new_gated_channel(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    int32_t stop_ch,
    uint32_t initial_state
) {
    GatedChannelInitial initial_state_obj;
    switch (initial_state) {
        case 0:
            initial_state_obj = GatedChannelInitial::Closed;
            break;
        case 1:
            initial_state_obj = GatedChannelInitial::Open;
            break;
        default:
            initial_state_obj = GatedChannelInitial::Closed;
            break;
    }
    std::unique_ptr<GatedChannel>
        gated_channel_ptr_unique(
            new GatedChannel(
                tagger.as_ptr(), signal_ch, start_ch, stop_ch, initial_state_obj));
    return gated_channel_ptr_unique;
}

void gated_channel_start(GatedChannel &gated_channel) {
    gated_channel.start();
}

void gated_channel_start_for(
    GatedChannel &gated_channel,
    int64_t duration,
    bool clear
) {
    gated_channel.startFor(duration, clear);
}

bool gated_channel_wait_until_finished(
    GatedChannel &gated_channel,
    int64_t timeout
) {
    return gated_channel.waitUntilFinished(timeout);
}

void gated_channel_stop(GatedChannel &gated_channel) {
    gated_channel.stop();
}

void gated_channel_clear(GatedChannel &gated_channel) {
    gated_channel.clear();
}

bool gated_channel_is_running(GatedChannel &gated_channel) {
    return gated_channel.isRunning();
}

int64_t gated_channel_get_capture_duration(GatedChannel &gated_channel) {
    return gated_channel.getCaptureDuration();
}

rust::String gated_channel_get_configuration(GatedChannel &gated_channel) {
    std::string config = gated_channel.getConfiguration();
    return to_string(config);
}

int32_t gated_channel_get_channel(GatedChannel &gated_channel) {
    return gated_channel.getChannel();
}

/* Histogram */

std::unique_ptr<Histogram> new_histogram(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    int64_t binwidth,
    int32_t n_bins
) {
    std::unique_ptr<Histogram>
        histogram_ptr_unique(
            new Histogram(
                tagger.as_ptr(), signal_ch, start_ch, binwidth, n_bins));
    return histogram_ptr_unique;
}

void histogram_start(Histogram &histogram) {
    histogram.start();
}

void histogram_start_for(Histogram &histogram, int64_t duration, bool clear) {
    histogram.startFor(duration, clear);
}

bool histogram_wait_until_finished(Histogram &histogram, int64_t timeout) {
    return histogram.waitUntilFinished(timeout);
}

void histogram_stop(Histogram &histogram) {
    histogram.stop();
}

void histogram_clear(Histogram &histogram) {
    histogram.clear();
}

bool histogram_is_running(Histogram &histogram) {
    return histogram.isRunning();
}

int64_t histogram_get_capture_duration(Histogram &histogram) {
    return histogram.getCaptureDuration();
}

rust::String histogram_get_configuration(Histogram &histogram) {
    std::string config = histogram.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> histogram_get_data(Histogram &histogram) {
    std::vector<int32_t> data;
    histogram.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<int64_t> histogram_get_index(Histogram &histogram) {
    std::vector<timestamp_t> data;
    histogram.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* Histogram2D */

std::unique_ptr<Histogram2D> new_histogram_2d(
    TimeTaggerBox &tagger,
    int32_t start_ch,
    int32_t stop_ch_1,
    int32_t stop_ch_2,
    int64_t binwidth_1,
    int64_t binwidth_2,
    int32_t n_bins_1,
    int32_t n_bins_2
) {
    std::unique_ptr<Histogram2D>
        histogram_2d_ptr_unique(
            new Histogram2D(
                tagger.as_ptr(),
                start_ch,
                stop_ch_1,
                stop_ch_2,
                binwidth_1,
                binwidth_2,
                n_bins_1,
                n_bins_2
            )
        );
    return histogram_2d_ptr_unique;
}

void histogram_2d_start(Histogram2D &histogram_2d) {
    histogram_2d.start();
}

void histogram_2d_start_for(
    Histogram2D &histogram_2d,
    int64_t duration,
    bool clear
) {
    histogram_2d.startFor(duration, clear);
}

bool histogram_2d_wait_until_finished(
    Histogram2D &histogram_2d,
    int64_t timeout
) {
    return histogram_2d.waitUntilFinished(timeout);
}

void histogram_2d_stop(Histogram2D &histogram_2d) {
    histogram_2d.stop();
}

void histogram_2d_clear(Histogram2D &histogram_2d) {
    histogram_2d.clear();
}

bool histogram_2d_is_running(Histogram2D &histogram_2d) {
    return histogram_2d.isRunning();
}

int64_t histogram_2d_get_capture_duration(Histogram2D &histogram_2d) {
    return histogram_2d.getCaptureDuration();
}

rust::String histogram_2d_get_configuration(Histogram2D &histogram_2d) {
    std::string config = histogram_2d.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> histogram_2d_get_data(Histogram2D &histogram_2d) {
    std::vector<int32_t> data;
    histogram_2d.getData([&data](size_t size_1, size_t size_2) {
        data.resize(size_1 * size_2);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<int64_t> histogram_2d_get_index(Histogram2D &histogram_2d) {
    std::vector<timestamp_t> data;
    histogram_2d.getIndex([&data](size_t size_1, size_t size_2, size_t size_3) {
        data.resize(size_1 * size_2 * size_3);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

rust::Vec<int64_t> histogram_2d_get_index_1(Histogram2D &histogram_2d) {
    std::vector<timestamp_t> data;
    histogram_2d.getIndex_1([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

rust::Vec<int64_t> histogram_2d_get_index_2(Histogram2D &histogram_2d) {
    std::vector<timestamp_t> data;
    histogram_2d.getIndex_2([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* HistogramND */

std::unique_ptr<HistogramND> new_histogram_nd(
    TimeTaggerBox &tagger,
    int32_t start_ch,
    const std::vector<int32_t> &stop_ch,
    const std::vector<int64_t> &binwidth,
    const std::vector<int32_t> &n_bins
) {
    std::vector<timestamp_t> binwidth_timestamp
        = std::vector<timestamp_t>(binwidth.begin(), binwidth.end());
    std::unique_ptr<HistogramND>
        histogram_nd_ptr_unique(
            new HistogramND(
                tagger.as_ptr(), start_ch, stop_ch, binwidth_timestamp, n_bins));
    return histogram_nd_ptr_unique;
}

void histogram_nd_start(HistogramND &histogram_nd) {
    histogram_nd.start();
}

void histogram_nd_start_for(
    HistogramND &histogram_nd,
    int64_t duration,
    bool clear
) {
    histogram_nd.startFor(duration, clear);
}

bool histogram_nd_wait_until_finished(
    HistogramND &histogram_nd,
    int64_t timeout
) {
    return histogram_nd.waitUntilFinished(timeout);
}

void histogram_nd_stop(HistogramND &histogram_nd) {
    histogram_nd.stop();
}

void histogram_nd_clear(HistogramND &histogram_nd) {
    histogram_nd.clear();
}

bool histogram_nd_is_running(HistogramND &histogram_nd) {
    return histogram_nd.isRunning();
}

int64_t histogram_nd_get_capture_duration(HistogramND &histogram_nd) {
    return histogram_nd.getCaptureDuration();
}

rust::String histogram_nd_get_configuration(HistogramND &histogram_nd) {
    std::string config = histogram_nd.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> histogram_nd_get_data(HistogramND &histogram_nd) {
    std::vector<int32_t> data;
    histogram_nd.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<int64_t> histogram_nd_get_index(
    HistogramND &histogram_nd,
    int32_t dim
) {
    std::vector<timestamp_t> data;
    histogram_nd.getIndex(
        [&data](size_t size) {
            data.resize(size);
            return data.data();
        },
        dim
    );
    return to_i64_vec_timestamp(data);
}

/* HistogramLogBins */

std::unique_ptr<HistogramLogBins> new_histogram_log_bins(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    double exp_start,
    double exp_stop,
    int32_t n_bins
) {
    std::unique_ptr<HistogramLogBins>
        histogram_log_bins_ptr_unique(
            new HistogramLogBins(
                tagger.as_ptr(), signal_ch, start_ch, exp_start, exp_stop, n_bins));
    return histogram_log_bins_ptr_unique;
}

void histogram_log_bins_start(HistogramLogBins &histogram_log_bins) {
    histogram_log_bins.start();
}

void histogram_log_bins_start_for(
    HistogramLogBins &histogram_log_bins,
    int64_t duration,
    bool clear
) {
    histogram_log_bins.startFor(duration, clear);
}

bool histogram_log_bins_wait_until_finished(
    HistogramLogBins &histogram_log_bins,
    int64_t timeout
) {
    return histogram_log_bins.waitUntilFinished(timeout);
}

void histogram_log_bins_stop(HistogramLogBins &histogram_log_bins) {
    histogram_log_bins.stop();
}

void histogram_log_bins_clear(HistogramLogBins &histogram_log_bins) {
    histogram_log_bins.clear();
}

bool histogram_log_bins_is_running(HistogramLogBins &histogram_log_bins) {
    return histogram_log_bins.isRunning();
}

int64_t histogram_log_bins_get_capture_duration(
    HistogramLogBins &histogram_log_bins
) {
    return histogram_log_bins.getCaptureDuration();
}

rust::String histogram_log_bins_get_configuration(
    HistogramLogBins &histogram_log_bins
) {
    std::string config = histogram_log_bins.getConfiguration();
    return to_string(config);
}

rust::Vec<uint64_t> histogram_log_bins_get_data(
    HistogramLogBins &histogram_log_bins
) {
    std::vector<uint64_t> data;
    histogram_log_bins.getData([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_u64_vec(data);
}

rust::Vec<double> histogram_log_bins_get_data_normalized_g2(
    HistogramLogBins &histogram_log_bins
) {
    std::vector<double> data;
    histogram_log_bins.getDataNormalizedG2([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_f64_vec(data);
}

rust::Vec<int64_t> histogram_log_bins_get_bin_edges(
    HistogramLogBins &histogram_log_bins
) {
    std::vector<timestamp_t> data;
    histogram_log_bins.getBinEdges([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* StartStop */

std::unique_ptr<StartStop> new_start_stop(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    int64_t binwidth
) {
    std::unique_ptr<StartStop>
        start_stop_ptr_unique(
            new StartStop(tagger.as_ptr(), signal_ch, start_ch, binwidth));
    return start_stop_ptr_unique;
}

void start_stop_start(StartStop &start_stop) {
    start_stop.start();
}

void start_stop_start_for(StartStop &start_stop, int64_t duration, bool clear) {
    start_stop.startFor(duration, clear);
}

bool start_stop_wait_until_finished(StartStop &start_stop, int64_t timeout) {
    return start_stop.waitUntilFinished(timeout);
}

void start_stop_stop(StartStop &start_stop) {
    start_stop.stop();
}

void start_stop_clear(StartStop &start_stop) {
    start_stop.clear();
}

bool start_stop_is_running(StartStop &start_stop) {
    return start_stop.isRunning();
}

int64_t start_stop_get_capture_duration(StartStop &start_stop) {
    return start_stop.getCaptureDuration();
}

rust::String start_stop_get_configuration(StartStop &start_stop) {
    std::string config = start_stop.getConfiguration();
    return to_string(config);
}

rust::Vec<int64_t> start_stop_get_data(StartStop &start_stop) {
    std::vector<timestamp_t> data;
    start_stop.getData([&data](size_t size_1, size_t size_2) {
        data.resize(size_1 * size_2);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* TimeDifferences */

std::unique_ptr<TimeDifferences> new_time_differences(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    int32_t next_ch,
    int32_t sync_ch,
    int64_t binwidth,
    int32_t n_bins,
    int32_t n_hist
) {
    std::unique_ptr<TimeDifferences>
        time_differences_ptr_unique(
            new TimeDifferences(
                tagger.as_ptr(),
                signal_ch,
                start_ch,
                next_ch,
                sync_ch,
                binwidth,
                n_bins,
                n_hist
            )
        );
    return time_differences_ptr_unique;
}

void time_differences_start(TimeDifferences &time_differences) {
    time_differences.start();
}

void time_differences_start_for(
    TimeDifferences &time_differences,
    int64_t duration,
    bool clear
) {
    time_differences.startFor(duration, clear);
}

bool time_differences_wait_until_finished(
    TimeDifferences &time_differences,
    int64_t timeout
) {
    return time_differences.waitUntilFinished(timeout);
}

void time_differences_stop(TimeDifferences &time_differences) {
    time_differences.stop();
}

void time_differences_clear(TimeDifferences &time_differences) {
    time_differences.clear();
}

bool time_differences_is_running(TimeDifferences &time_differences) {
    return time_differences.isRunning();
}

int64_t time_differences_get_capture_duration(
    TimeDifferences &time_differences
) {
    return time_differences.getCaptureDuration();
}

rust::String time_differences_get_configuration(
    TimeDifferences &time_differences
) {
    std::string config = time_differences.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> time_differences_get_data(
    TimeDifferences &time_differences
) {
    std::vector<int32_t> data;
    time_differences.getData([&data](size_t size_1, size_t size_2) {
        data.resize(size_1 * size_2);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<int64_t> time_differences_get_index(
    TimeDifferences &time_differences
) {
    std::vector<timestamp_t> data;
    time_differences.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

void time_differences_set_max_counts(
    TimeDifferences &time_differences,
    uint64_t max_counts
) {
    time_differences.setMaxCounts(max_counts);
}

uint64_t time_differences_get_counts(TimeDifferences &time_differences) {
    return time_differences.getCounts();
}

int32_t time_differences_get_histogram_index(
    TimeDifferences &time_differences
) {
    return time_differences.getHistogramIndex();
}

bool time_differences_ready(TimeDifferences &time_differences) {
    return time_differences.getHistogramIndex();
}

/* TimeDifferencesND */

std::unique_ptr<TimeDifferencesND> new_time_differences_nd(
    TimeTaggerBox &tagger,
    int32_t signal_ch,
    int32_t start_ch,
    const std::vector<int32_t> &next_ch,
    const std::vector<int32_t> &sync_ch,
    const std::vector<int32_t> &n_hist,
    int64_t binwidth,
    int32_t n_bins
) {
    std::unique_ptr<TimeDifferencesND>
        time_differences_nd_ptr_unique(
            new TimeDifferencesND(
                tagger.as_ptr(),
                signal_ch,
                start_ch,
                next_ch,
                sync_ch,
                n_hist,
                binwidth,
                n_bins
            )
        );
    return time_differences_nd_ptr_unique;
}

void time_differences_nd_start(TimeDifferencesND &time_differences_nd) {
    time_differences_nd.start();
}

void time_differences_nd_start_for(
    TimeDifferencesND &time_differences_nd,
    int64_t duration,
    bool clear
) {
    time_differences_nd.startFor(duration, clear);
}

bool time_differences_nd_wait_until_finished(
    TimeDifferencesND &time_differences_nd,
    int64_t timeout
) {
    return time_differences_nd.waitUntilFinished(timeout);
}

void time_differences_nd_stop(TimeDifferencesND &time_differences_nd) {
    time_differences_nd.stop();
}

void time_differences_nd_clear(TimeDifferencesND &time_differences_nd) {
    time_differences_nd.clear();
}

bool time_differences_nd_is_running(TimeDifferencesND &time_differences_nd) {
    return time_differences_nd.isRunning();
}

int64_t time_differences_nd_get_capture_duration(
    TimeDifferencesND &time_differences_nd
) {
    return time_differences_nd.getCaptureDuration();
}

rust::String time_differences_nd_get_configuration(
    TimeDifferencesND &time_differences_nd
) {
    std::string config = time_differences_nd.getConfiguration();
    return to_string(config);
}

rust::Vec<int32_t> time_differences_nd_get_data(
    TimeDifferencesND &time_differences_nd
) {
    std::vector<int32_t> data;
    time_differences_nd.getData([&data](size_t size_1, size_t size_2) {
        data.resize(size_1 * size_2);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<int64_t> time_differences_nd_get_index(
    TimeDifferencesND &time_differences_nd
) {
    std::vector<timestamp_t> data;
    time_differences_nd.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

/* TriggerOnCountrate */

std::unique_ptr<TriggerOnCountrate> new_trigger_on_countrate(
    TimeTaggerBox &tagger,
    int32_t channel,
    double ref_countrate,
    double hysteresis,
    int64_t window
) {
    std::unique_ptr<TriggerOnCountrate>
        trigger_on_countrate_ptr_unique(
            new TriggerOnCountrate(
                tagger.as_ptr(),
                channel,
                ref_countrate,
                hysteresis,
                window
            )
        );
    return trigger_on_countrate_ptr_unique;
}

void trigger_on_countrate_start(TriggerOnCountrate &trigger_on_countrate) {
    trigger_on_countrate.start();
}

void trigger_on_countrate_start_for(
    TriggerOnCountrate &trigger_on_countrate,
    int64_t duration,
    bool clear
) {
    trigger_on_countrate.startFor(duration, clear);
}

bool trigger_on_countrate_wait_until_finished(
    TriggerOnCountrate &trigger_on_countrate,
    int64_t timeout
) {
    return trigger_on_countrate.waitUntilFinished(timeout);
}

void trigger_on_countrate_stop(TriggerOnCountrate &trigger_on_countrate) {
    trigger_on_countrate.stop();
}

void trigger_on_countrate_clear(TriggerOnCountrate &trigger_on_countrate) {
    trigger_on_countrate.clear();
}

bool trigger_on_countrate_is_running(TriggerOnCountrate &trigger_on_countrate) {
    return trigger_on_countrate.isRunning();
}

int64_t trigger_on_countrate_get_capture_duration(
    TriggerOnCountrate &trigger_on_countrate
) {
    return trigger_on_countrate.getCaptureDuration();
}

rust::String trigger_on_countrate_get_configuration(
    TriggerOnCountrate &trigger_on_countrate
) {
    std::string config = trigger_on_countrate.getConfiguration();
    return to_string(config);
}

int32_t trigger_on_countrate_get_channel_above(
    TriggerOnCountrate &trigger_on_countrate
) {
    return trigger_on_countrate.getChannelAbove();
}

int32_t trigger_on_countrate_get_channel_below(
    TriggerOnCountrate &trigger_on_countrate
) {
    return trigger_on_countrate.getChannelBelow();
}

rust::Vec<int32_t> trigger_on_countrate_get_channels(
    TriggerOnCountrate &trigger_on_countrate
) {
    std::vector<int32_t> channels = trigger_on_countrate.getChannels();
    return to_i32_vec(channels);
}

bool trigger_on_countrate_is_above(TriggerOnCountrate &trigger_on_countrate) {
    return trigger_on_countrate.isAbove();
}

bool trigger_on_countrate_is_below(TriggerOnCountrate &trigger_on_countrate) {
    return trigger_on_countrate.isBelow();
}

double trigger_on_countrate_get_current_countrate(
    TriggerOnCountrate &trigger_on_countrate
) {
    return trigger_on_countrate.getCurrentCountrate();
}

bool trigger_on_countrate_inject_current_state(
    TriggerOnCountrate &trigger_on_countrate
) {
    return trigger_on_countrate.injectCurrentState();
}

/* CounterData */

rust::Vec<int32_t> counter_data_get_data(CounterData &counter_data) {
    std::vector<int32_t> data;
    counter_data.getData([&data](size_t size_1, size_t size_2) {
        data.resize(size_1 * size_2);
        return data.data();
    });
    return to_i32_vec(data);
}

rust::Vec<uint64_t> counter_data_get_data_total_counts(
    CounterData &counter_data
) {
    std::vector<uint64_t> data;
    counter_data.getDataTotalCounts([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_u64_vec(data);
}

rust::Vec<int64_t> counter_data_get_index(CounterData &counter_data) {
    std::vector<timestamp_t> data;
    counter_data.getIndex([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

rust::Vec<int64_t> counter_data_get_time(CounterData &counter_data) {
    std::vector<timestamp_t> data;
    counter_data.getTime([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i64_vec_timestamp(data);
}

rust::Vec<signed char> counter_data_get_overflow_mask(
    CounterData &counter_data
) {
    std::vector<signed char> data;
    counter_data.getOverflowMask([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i8_vec(data);
}

rust::Vec<int32_t> counter_data_get_channels(CounterData &counter_data) {
    std::vector<int32_t> data;
    counter_data.getChannels([&data](size_t size) {
        data.resize(size);
        return data.data();
    });
    return to_i32_vec(data);
}

uint32_t counter_data_get_size(CounterData &counter_data) {
    return counter_data.size;
}

uint32_t counter_data_get_dropped_bins(CounterData &counter_data) {
    return counter_data.dropped_bins;
}

bool counter_data_get_overflow(CounterData &counter_data) {
    return counter_data.overflow;
}

