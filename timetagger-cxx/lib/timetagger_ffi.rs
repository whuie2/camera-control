pub(crate) const CHANNEL_UNUSED: i32 = -134_217_728;

#[cxx::bridge]
pub(crate) mod ffi {
    #[cfg(target_family = "unix")]
    unsafe extern "C++" {
        include!("/usr/include/timetagger/TimeTagger.h");
        include!("/usr/include/timetagger/Iterators.h");
        include!("timetagger-cxx/include/timetagger.h");
    }

    #[cfg(target_family = "windows")]
    unsafe extern "C++" {
        include!("C:\\Program Files\\Swabian Instruments\\Time Tagger\\driver\\include\\TimeTagger.h");
        include!("C:\\Program Files\\Swabian Instruments\\Time Tagger\\driver\\include\\Iterators.h");
        include!("timetagger-cxx\\include\\timetagger.h");
    }

    #[allow(clippy::too_many_arguments)]
    unsafe extern "C++" {
        fn scan_devices() -> Vec<String>;
        fn create_timetagger(serial: &CxxString) -> Result<UniquePtr<TimeTaggerBox>>;

        type TimeTaggerBox;
        unsafe fn get_input_delay(tagger: Pin<&mut TimeTaggerBox>, channel: i32) -> Result<i64>;
        unsafe fn set_input_delay(tagger: Pin<&mut TimeTaggerBox>, channel: i32, delay: i64) -> Result<()>;
        unsafe fn get_delay_hardware(tagger: Pin<&mut TimeTaggerBox>, channel: i32) -> Result<i64>;
        unsafe fn set_delay_hardware(tagger: Pin<&mut TimeTaggerBox>, channel: i32, delay: i64) -> Result<()>;
        unsafe fn get_delay_software(tagger: Pin<&mut TimeTaggerBox>, channel: i32) -> Result<i64>;
        unsafe fn set_delay_software(tagger: Pin<&mut TimeTaggerBox>, channel: i32, delay: i64) -> Result<()>;
        unsafe fn reset(tagger: Pin<&mut TimeTaggerBox>) -> Result<()>;
        unsafe fn get_trigger_level(tagger: Pin<&mut TimeTaggerBox>, channel: i32) -> Result<f64>;
        unsafe fn set_trigger_level(tagger: Pin<&mut TimeTaggerBox>, channel: i32, voltage: f64) -> Result<()>;
        unsafe fn get_hardware_buffer_size(tagger: Pin<&mut TimeTaggerBox>) -> Result<i32>;
        unsafe fn set_hardware_buffer_size(tagger: Pin<&mut TimeTaggerBox>, size: i32) -> Result<()>;
        unsafe fn get_serial(tagger: Pin<&mut TimeTaggerBox>) -> Result<String>;
        unsafe fn get_model(tagger: Pin<&mut TimeTaggerBox>) -> Result<String>;
        unsafe fn get_dac_range(tagger: Pin<&mut TimeTaggerBox>) -> Result<Vec<f64>>;
        unsafe fn get_channel_list(tagger: Pin<&mut TimeTaggerBox>) -> Result<Vec<i32>>;
        unsafe fn disable_leds(tagger: Pin<&mut TimeTaggerBox>, disabled: bool) -> Result<()>;

        type Coincidences;
        unsafe fn new_coincidences(tagger: Pin<&mut TimeTaggerBox>, channels: &CxxVector<i32>, groupings: &CxxVector<u32>, window: i64, timestamp: i32) -> Result<UniquePtr<Coincidences>>;
        /* inherited from IteratorBase */
        unsafe fn coincidences_start(counter: Pin<&mut Coincidences>) -> Result<()>;
        unsafe fn coincidences_start_for(counter: Pin<&mut Coincidences>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn coincidences_wait_until_finished(counter: Pin<&mut Coincidences>, timeout: i64) -> Result<bool>;
        unsafe fn coincidences_stop(counter: Pin<&mut Coincidences>) -> Result<()>;
        unsafe fn coincidences_clear(counter: Pin<&mut Coincidences>) -> Result<()>;
        unsafe fn coincidences_is_running(counter: Pin<&mut Coincidences>) -> Result<bool>;
        unsafe fn coincidences_get_capture_duration(counter: Pin<&mut Coincidences>) -> Result<i64>;
        unsafe fn coincidences_get_configuration(counter: Pin<&mut Coincidences>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn coincidences_get_channels(counter: Pin<&mut Coincidences>) -> Result<Vec<i32>>;
        unsafe fn coincidences_set_coincidence_window(counter: Pin<&mut Coincidences>, window: i64) -> Result<()>;

        type Combiner;
        unsafe fn new_combiner(tagger: Pin<&mut TimeTaggerBox>, channels: &CxxVector<i32>) -> Result<UniquePtr<Combiner>>;
        /* inherited from IteratorBase */
        unsafe fn combiner_start(counter: Pin<&mut Combiner>) -> Result<()>;
        unsafe fn combiner_start_for(counter: Pin<&mut Combiner>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn combiner_wait_until_finished(counter: Pin<&mut Combiner>, timeout: i64) -> Result<bool>;
        unsafe fn combiner_stop(counter: Pin<&mut Combiner>) -> Result<()>;
        unsafe fn combiner_clear(counter: Pin<&mut Combiner>) -> Result<()>;
        unsafe fn combiner_is_running(counter: Pin<&mut Combiner>) -> Result<bool>;
        unsafe fn combiner_get_capture_duration(counter: Pin<&mut Combiner>) -> Result<i64>;
        unsafe fn combiner_get_configuration(counter: Pin<&mut Combiner>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn combiner_get_channel_counts(counter: Pin<&mut Combiner>) -> Result<Vec<i64>>;
        unsafe fn combiner_get_channel(counter: Pin<&mut Combiner>) -> Result<i32>;

        type Correlation;
        unsafe fn new_correlation(tagger: Pin<&mut TimeTaggerBox>, channel_1: i32, channel_2: i32, binwidth: i64, n_bins: i32) -> Result<UniquePtr<Correlation>>;
        /* inherited from IteratorBase */
        unsafe fn correlation_start(counter: Pin<&mut Correlation>) -> Result<()>;
        unsafe fn correlation_start_for(counter: Pin<&mut Correlation>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn correlation_wait_until_finished(counter: Pin<&mut Correlation>, timeout: i64) -> Result<bool>;
        unsafe fn correlation_stop(counter: Pin<&mut Correlation>) -> Result<()>;
        unsafe fn correlation_clear(counter: Pin<&mut Correlation>) -> Result<()>;
        unsafe fn correlation_is_running(counter: Pin<&mut Correlation>) -> Result<bool>;
        unsafe fn correlation_get_capture_duration(counter: Pin<&mut Correlation>) -> Result<i64>;
        unsafe fn correlation_get_configuration(counter: Pin<&mut Correlation>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn correlation_get_data(counter: Pin<&mut Correlation>) -> Result<Vec<i32>>;
        unsafe fn correlation_get_data_normalized(counter: Pin<&mut Correlation>) -> Result<Vec<f64>>;
        unsafe fn correlation_get_index(counter: Pin<&mut Correlation>) -> Result<Vec<i64>>;

        type CountBetweenMarkers;
        unsafe fn new_count_between_markers(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, end_ch: i32, n_bins: i32) -> Result<UniquePtr<CountBetweenMarkers>>;
        /* inherited from IteratorBase */
        unsafe fn count_between_markers_start(counter: Pin<&mut CountBetweenMarkers>) -> Result<()>;
        unsafe fn count_between_markers_start_for(counter: Pin<&mut CountBetweenMarkers>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn count_between_markers_wait_until_finished(counter: Pin<&mut CountBetweenMarkers>, timeout: i64) -> Result<bool>;
        unsafe fn count_between_markers_stop(counter: Pin<&mut CountBetweenMarkers>) -> Result<()>;
        unsafe fn count_between_markers_clear(counter: Pin<&mut CountBetweenMarkers>) -> Result<()>;
        unsafe fn count_between_markers_is_running(counter: Pin<&mut CountBetweenMarkers>) -> Result<bool>;
        unsafe fn count_between_markers_get_capture_duration(counter: Pin<&mut CountBetweenMarkers>) -> Result<i64>;
        unsafe fn count_between_markers_get_configuration(counter: Pin<&mut CountBetweenMarkers>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn count_between_markers_ready(counter: Pin<&mut CountBetweenMarkers>) -> Result<bool>;
        unsafe fn count_between_markers_get_data(counter: Pin<&mut CountBetweenMarkers>) -> Result<Vec<i32>>;
        unsafe fn count_between_markers_get_bin_widths(counter: Pin<&mut CountBetweenMarkers>) -> Result<Vec<i64>>;
        unsafe fn count_between_markers_get_index(counter: Pin<&mut CountBetweenMarkers>) -> Result<Vec<i64>>;

        type Counter;
        unsafe fn new_counter(tagger: Pin<&mut TimeTaggerBox>, channels: &CxxVector<i32>, binwidth: i64, n_bins: i32) -> Result<UniquePtr<Counter>>;
        /* inherited from IteratorBase */
        unsafe fn counter_start(counter: Pin<&mut Counter>) -> Result<()>;
        unsafe fn counter_start_for(counter: Pin<&mut Counter>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn counter_wait_until_finished(counter: Pin<&mut Counter>, timeout: i64) -> Result<bool>;
        unsafe fn counter_stop(counter: Pin<&mut Counter>) -> Result<()>;
        unsafe fn counter_clear(counter: Pin<&mut Counter>) -> Result<()>;
        unsafe fn counter_is_running(counter: Pin<&mut Counter>) -> Result<bool>;
        unsafe fn counter_get_capture_duration(counter: Pin<&mut Counter>) -> Result<i64>;
        unsafe fn counter_get_configuration(counter: Pin<&mut Counter>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn counter_get_data(counter: Pin<&mut Counter>, rolling: bool) -> Result<Vec<i32>>;
        unsafe fn counter_get_data_total_counts(counter: Pin<&mut Counter>) -> Result<Vec<u64>>;
        unsafe fn counter_get_index(counter: Pin<&mut Counter>) -> Result<Vec<i64>>;
        unsafe fn counter_get_data_object(counter: Pin<&mut Counter>, remove: bool) -> Result<UniquePtr<CounterData>>;

        type Countrate;
        unsafe fn new_countrate(tagger: Pin<&mut TimeTaggerBox>, channels: &CxxVector<i32>) -> Result<UniquePtr<Countrate>>;
        /* inherited from IteratorBase */
        unsafe fn countrate_start(counter: Pin<&mut Countrate>) -> Result<()>;
        unsafe fn countrate_start_for(counter: Pin<&mut Countrate>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn countrate_wait_until_finished(counter: Pin<&mut Countrate>, timeout: i64) -> Result<bool>;
        unsafe fn countrate_stop(counter: Pin<&mut Countrate>) -> Result<()>;
        unsafe fn countrate_clear(counter: Pin<&mut Countrate>) -> Result<()>;
        unsafe fn countrate_is_running(counter: Pin<&mut Countrate>) -> Result<bool>;
        unsafe fn countrate_get_capture_duration(counter: Pin<&mut Countrate>) -> Result<i64>;
        unsafe fn countrate_get_configuration(counter: Pin<&mut Countrate>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn countrate_get_data(counter: Pin<&mut Countrate>) -> Result<Vec<f64>>;
        unsafe fn countrate_get_counts_total(counter: Pin<&mut Countrate>) -> Result<Vec<i64>>;

        type DelayedChannel;
        unsafe fn new_delayed_channel(tagger: Pin<&mut TimeTaggerBox>, channel: i32, delay: i64) -> Result<UniquePtr<DelayedChannel>>;
        /* inherited from IteratorBase */
        unsafe fn delayed_channel_start(counter: Pin<&mut DelayedChannel>) -> Result<()>;
        unsafe fn delayed_channel_start_for(counter: Pin<&mut DelayedChannel>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn delayed_channel_wait_until_finished(counter: Pin<&mut DelayedChannel>, timeout: i64) -> Result<bool>;
        unsafe fn delayed_channel_stop(counter: Pin<&mut DelayedChannel>) -> Result<()>;
        unsafe fn delayed_channel_clear(counter: Pin<&mut DelayedChannel>) -> Result<()>;
        unsafe fn delayed_channel_is_running(counter: Pin<&mut DelayedChannel>) -> Result<bool>;
        unsafe fn delayed_channel_get_capture_duration(counter: Pin<&mut DelayedChannel>) -> Result<i64>;
        unsafe fn delayed_channel_get_configuration(counter: Pin<&mut DelayedChannel>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn delayed_channel_get_channel(counter: Pin<&mut DelayedChannel>) -> Result<i32>;
        unsafe fn delayed_channel_set_delay(counter: Pin<&mut DelayedChannel>, delay: i64) -> Result<()>;

        type FrequencyMultiplier;
        unsafe fn new_frequency_multiplier(tagger: Pin<&mut TimeTaggerBox>, channel: i32, multiplier: i32) -> Result<UniquePtr<FrequencyMultiplier>>;
        /* inherited from IteratorBase */
        unsafe fn frequency_multiplier_start(counter: Pin<&mut FrequencyMultiplier>) -> Result<()>;
        unsafe fn frequency_multiplier_start_for(counter: Pin<&mut FrequencyMultiplier>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn frequency_multiplier_wait_until_finished(counter: Pin<&mut FrequencyMultiplier>, timeout: i64) -> Result<bool>;
        unsafe fn frequency_multiplier_stop(counter: Pin<&mut FrequencyMultiplier>) -> Result<()>;
        unsafe fn frequency_multiplier_clear(counter: Pin<&mut FrequencyMultiplier>) -> Result<()>;
        unsafe fn frequency_multiplier_is_running(counter: Pin<&mut FrequencyMultiplier>) -> Result<bool>;
        unsafe fn frequency_multiplier_get_capture_duration(counter: Pin<&mut FrequencyMultiplier>) -> Result<i64>;
        unsafe fn frequency_multiplier_get_configuration(counter: Pin<&mut FrequencyMultiplier>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn frequency_multiplier_get_channel(counter: Pin<&mut FrequencyMultiplier>) -> Result<i32>;
        unsafe fn frequency_multiplier_get_multiplier(counter: Pin<&mut FrequencyMultiplier>) -> Result<i32>;

        type GatedChannel;
        unsafe fn new_gated_channel(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, end_ch: i32, initial_state: u32) -> Result<UniquePtr<GatedChannel>>;
        /* inherited from IteratorBase */
        unsafe fn gated_channel_start(counter: Pin<&mut GatedChannel>) -> Result<()>;
        unsafe fn gated_channel_start_for(counter: Pin<&mut GatedChannel>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn gated_channel_wait_until_finished(counter: Pin<&mut GatedChannel>, timeout: i64) -> Result<bool>;
        unsafe fn gated_channel_stop(counter: Pin<&mut GatedChannel>) -> Result<()>;
        unsafe fn gated_channel_clear(counter: Pin<&mut GatedChannel>) -> Result<()>;
        unsafe fn gated_channel_is_running(counter: Pin<&mut GatedChannel>) -> Result<bool>;
        unsafe fn gated_channel_get_capture_duration(counter: Pin<&mut GatedChannel>) -> Result<i64>;
        unsafe fn gated_channel_get_configuration(counter: Pin<&mut GatedChannel>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn gated_channel_get_channel(counter: Pin<&mut GatedChannel>) -> Result<i32>;

        type Histogram;
        unsafe fn new_histogram(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, binwidth: i64, n_bins: i32) -> Result<UniquePtr<Histogram>>;
        /* inherited from IteratorBase */
        unsafe fn histogram_start(counter: Pin<&mut Histogram>) -> Result<()>;
        unsafe fn histogram_start_for(counter: Pin<&mut Histogram>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn histogram_wait_until_finished(counter: Pin<&mut Histogram>, timeout: i64) -> Result<bool>;
        unsafe fn histogram_stop(counter: Pin<&mut Histogram>) -> Result<()>;
        unsafe fn histogram_clear(counter: Pin<&mut Histogram>) -> Result<()>;
        unsafe fn histogram_is_running(counter: Pin<&mut Histogram>) -> Result<bool>;
        unsafe fn histogram_get_capture_duration(counter: Pin<&mut Histogram>) -> Result<i64>;
        unsafe fn histogram_get_configuration(counter: Pin<&mut Histogram>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn histogram_get_data(counter: Pin<&mut Histogram>) -> Result<Vec<i32>>;
        unsafe fn histogram_get_index(counter: Pin<&mut Histogram>) -> Result<Vec<i64>>;

        type Histogram2D;
        unsafe fn new_histogram_2d(tagger: Pin<&mut TimeTaggerBox>, start_ch: i32, stop_ch_1: i32, stop_ch_2: i32, binwidth_1: i64, binwidth_2: i64, n_bins_1: i32, n_bins_2: i32) -> Result<UniquePtr<Histogram2D>>;
        /* inherited from IteratorBase */
        unsafe fn histogram_2d_start(counter: Pin<&mut Histogram2D>) -> Result<()>;
        unsafe fn histogram_2d_start_for(counter: Pin<&mut Histogram2D>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn histogram_2d_wait_until_finished(counter: Pin<&mut Histogram2D>, timeout: i64) -> Result<bool>;
        unsafe fn histogram_2d_stop(counter: Pin<&mut Histogram2D>) -> Result<()>;
        unsafe fn histogram_2d_clear(counter: Pin<&mut Histogram2D>) -> Result<()>;
        unsafe fn histogram_2d_is_running(counter: Pin<&mut Histogram2D>) -> Result<bool>;
        unsafe fn histogram_2d_get_capture_duration(counter: Pin<&mut Histogram2D>) -> Result<i64>;
        unsafe fn histogram_2d_get_configuration(counter: Pin<&mut Histogram2D>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn histogram_2d_get_data(counter: Pin<&mut Histogram2D>) -> Result<Vec<i32>>;
        unsafe fn histogram_2d_get_index(counter: Pin<&mut Histogram2D>) -> Result<Vec<i64>>;
        unsafe fn histogram_2d_get_index_1(counter: Pin<&mut Histogram2D>) -> Result<Vec<i64>>;
        unsafe fn histogram_2d_get_index_2(counter: Pin<&mut Histogram2D>) -> Result<Vec<i64>>;

        type HistogramND;
        unsafe fn new_histogram_nd(tagger: Pin<&mut TimeTaggerBox>, start_ch: i32, stop_ch: &CxxVector<i32>, binwidth: &CxxVector<i64>, n_bins: &CxxVector<i32>) -> Result<UniquePtr<HistogramND>>;
        /* inherited from IteratorBase */
        unsafe fn histogram_nd_start(counter: Pin<&mut HistogramND>) -> Result<()>;
        unsafe fn histogram_nd_start_for(counter: Pin<&mut HistogramND>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn histogram_nd_wait_until_finished(counter: Pin<&mut HistogramND>, timeout: i64) -> Result<bool>;
        unsafe fn histogram_nd_stop(counter: Pin<&mut HistogramND>) -> Result<()>;
        unsafe fn histogram_nd_clear(counter: Pin<&mut HistogramND>) -> Result<()>;
        unsafe fn histogram_nd_is_running(counter: Pin<&mut HistogramND>) -> Result<bool>;
        unsafe fn histogram_nd_get_capture_duration(counter: Pin<&mut HistogramND>) -> Result<i64>;
        unsafe fn histogram_nd_get_configuration(counter: Pin<&mut HistogramND>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn histogram_nd_get_data(counter: Pin<&mut HistogramND>) -> Result<Vec<i32>>;
        unsafe fn histogram_nd_get_index(counter: Pin<&mut HistogramND>, dim: i32) -> Result<Vec<i64>>;

        type HistogramLogBins;
        unsafe fn new_histogram_log_bins(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, exp_start: f64, exp_stop: f64, n_bins: i32) -> Result<UniquePtr<HistogramLogBins>>;
        /* inherited from IteratorBase */
        unsafe fn histogram_log_bins_start(counter: Pin<&mut HistogramLogBins>) -> Result<()>;
        unsafe fn histogram_log_bins_start_for(counter: Pin<&mut HistogramLogBins>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn histogram_log_bins_wait_until_finished(counter: Pin<&mut HistogramLogBins>, timeout: i64) -> Result<bool>;
        unsafe fn histogram_log_bins_stop(counter: Pin<&mut HistogramLogBins>) -> Result<()>;
        unsafe fn histogram_log_bins_clear(counter: Pin<&mut HistogramLogBins>) -> Result<()>;
        unsafe fn histogram_log_bins_is_running(counter: Pin<&mut HistogramLogBins>) -> Result<bool>;
        unsafe fn histogram_log_bins_get_capture_duration(counter: Pin<&mut HistogramLogBins>) -> Result<i64>;
        unsafe fn histogram_log_bins_get_configuration(counter: Pin<&mut HistogramLogBins>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn histogram_log_bins_get_data(counter: Pin<&mut HistogramLogBins>) -> Result<Vec<u64>>;
        unsafe fn histogram_log_bins_get_data_normalized_g2(counter: Pin<&mut HistogramLogBins>) -> Result<Vec<f64>>;
        unsafe fn histogram_log_bins_get_bin_edges(counter: Pin<&mut HistogramLogBins>) -> Result<Vec<i64>>;

        type StartStop;
        unsafe fn new_start_stop(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, binwidth: i64) -> Result<UniquePtr<StartStop>>;
        /* inherited from IteratorBase */
        unsafe fn start_stop_start(counter: Pin<&mut StartStop>) -> Result<()>;
        unsafe fn start_stop_start_for(counter: Pin<&mut StartStop>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn start_stop_wait_until_finished(counter: Pin<&mut StartStop>, timeout: i64) -> Result<bool>;
        unsafe fn start_stop_stop(counter: Pin<&mut StartStop>) -> Result<()>;
        unsafe fn start_stop_clear(counter: Pin<&mut StartStop>) -> Result<()>;
        unsafe fn start_stop_is_running(counter: Pin<&mut StartStop>) -> Result<bool>;
        unsafe fn start_stop_get_capture_duration(counter: Pin<&mut StartStop>) -> Result<i64>;
        unsafe fn start_stop_get_configuration(counter: Pin<&mut StartStop>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn start_stop_get_data(counter: Pin<&mut StartStop>) -> Result<Vec<i64>>;

        type TimeDifferences;
        unsafe fn new_time_differences(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, next_ch: i32, sync_ch: i32, binwidth: i64, n_bins: i32, n_hist: i32) -> Result<UniquePtr<TimeDifferences>>;
        /* inherited from IteratorBase */
        unsafe fn time_differences_start(counter: Pin<&mut TimeDifferences>) -> Result<()>;
        unsafe fn time_differences_start_for(counter: Pin<&mut TimeDifferences>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn time_differences_wait_until_finished(counter: Pin<&mut TimeDifferences>, timeout: i64) -> Result<bool>;
        unsafe fn time_differences_stop(counter: Pin<&mut TimeDifferences>) -> Result<()>;
        unsafe fn time_differences_clear(counter: Pin<&mut TimeDifferences>) -> Result<()>;
        unsafe fn time_differences_is_running(counter: Pin<&mut TimeDifferences>) -> Result<bool>;
        unsafe fn time_differences_get_capture_duration(counter: Pin<&mut TimeDifferences>) -> Result<i64>;
        unsafe fn time_differences_get_configuration(counter: Pin<&mut TimeDifferences>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn time_differences_get_data(counter: Pin<&mut TimeDifferences>) -> Result<Vec<i32>>;
        unsafe fn time_differences_get_index(counter: Pin<&mut TimeDifferences>) -> Result<Vec<i64>>;
        unsafe fn time_differences_set_max_counts(counter: Pin<&mut TimeDifferences>, max_counts: u64) -> Result<()>;
        unsafe fn time_differences_get_counts(counter: Pin<&mut TimeDifferences>) -> Result<u64>;
        unsafe fn time_differences_get_histogram_index(counter: Pin<&mut TimeDifferences>) -> Result<i32>;
        unsafe fn time_differences_ready(counter: Pin<&mut TimeDifferences>) -> Result<bool>;

        type TimeDifferencesND;
        unsafe fn new_time_differences_nd(tagger: Pin<&mut TimeTaggerBox>, signal_ch: i32, start_ch: i32, next_ch: &CxxVector<i32>, sync_ch: &CxxVector<i32>, n_hist: &CxxVector<i32>, binwidth: i64, n_bins: i32) -> Result<UniquePtr<TimeDifferencesND>>;
        /* inherited from IteratorBase */
        unsafe fn time_differences_nd_start(counter: Pin<&mut TimeDifferencesND>) -> Result<()>;
        unsafe fn time_differences_nd_start_for(counter: Pin<&mut TimeDifferencesND>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn time_differences_nd_wait_until_finished(counter: Pin<&mut TimeDifferencesND>, timeout: i64) -> Result<bool>;
        unsafe fn time_differences_nd_stop(counter: Pin<&mut TimeDifferencesND>) -> Result<()>;
        unsafe fn time_differences_nd_clear(counter: Pin<&mut TimeDifferencesND>) -> Result<()>;
        unsafe fn time_differences_nd_is_running(counter: Pin<&mut TimeDifferencesND>) -> Result<bool>;
        unsafe fn time_differences_nd_get_capture_duration(counter: Pin<&mut TimeDifferencesND>) -> Result<i64>;
        unsafe fn time_differences_nd_get_configuration(counter: Pin<&mut TimeDifferencesND>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn time_differences_nd_get_data(counter: Pin<&mut TimeDifferencesND>) -> Result<Vec<i32>>;
        unsafe fn time_differences_nd_get_index(counter: Pin<&mut TimeDifferencesND>) -> Result<Vec<i64>>;

        type TriggerOnCountrate;
        unsafe fn new_trigger_on_countrate(tagger: Pin<&mut TimeTaggerBox>, channel: i32, ref_countrate: f64, hysteresis: f64, window: i64) -> Result<UniquePtr<TriggerOnCountrate>>;
        /* inherited from IteratorBase */
        unsafe fn trigger_on_countrate_start(counter: Pin<&mut TriggerOnCountrate>) -> Result<()>;
        unsafe fn trigger_on_countrate_start_for(counter: Pin<&mut TriggerOnCountrate>, duration: i64, clear: bool) -> Result<()>;
        unsafe fn trigger_on_countrate_wait_until_finished(counter: Pin<&mut TriggerOnCountrate>, timeout: i64) -> Result<bool>;
        unsafe fn trigger_on_countrate_stop(counter: Pin<&mut TriggerOnCountrate>) -> Result<()>;
        unsafe fn trigger_on_countrate_clear(counter: Pin<&mut TriggerOnCountrate>) -> Result<()>;
        unsafe fn trigger_on_countrate_is_running(counter: Pin<&mut TriggerOnCountrate>) -> Result<bool>;
        unsafe fn trigger_on_countrate_get_capture_duration(counter: Pin<&mut TriggerOnCountrate>) -> Result<i64>;
        unsafe fn trigger_on_countrate_get_configuration(counter: Pin<&mut TriggerOnCountrate>) -> Result<String>;
        /* * * * * * * * * * * * * * * */
        unsafe fn trigger_on_countrate_get_channel_above(counter: Pin<&mut TriggerOnCountrate>) -> Result<i32>;
        unsafe fn trigger_on_countrate_get_channel_below(counter: Pin<&mut TriggerOnCountrate>) -> Result<i32>;
        unsafe fn trigger_on_countrate_get_channels(counter: Pin<&mut TriggerOnCountrate>) -> Result<Vec<i32>>;
        unsafe fn trigger_on_countrate_is_above(counter: Pin<&mut TriggerOnCountrate>) -> Result<bool>;
        unsafe fn trigger_on_countrate_is_below(counter: Pin<&mut TriggerOnCountrate>) -> Result<bool>;
        unsafe fn trigger_on_countrate_get_current_countrate(counter: Pin<&mut TriggerOnCountrate>) -> Result<f64>;
        unsafe fn trigger_on_countrate_inject_current_state(counter: Pin<&mut TriggerOnCountrate>) -> Result<bool>;

        type CounterData;
        unsafe fn counter_data_get_data(data: Pin<&mut CounterData>) -> Result<Vec<i32>>;
        unsafe fn counter_data_get_data_total_counts(data: Pin<&mut CounterData>) -> Result<Vec<u64>>;
        unsafe fn counter_data_get_index(data: Pin<&mut CounterData>) -> Result<Vec<i64>>;
        unsafe fn counter_data_get_time(data: Pin<&mut CounterData>) -> Result<Vec<i64>>;
        unsafe fn counter_data_get_overflow_mask(data: Pin<&mut CounterData>) -> Result<Vec<i8>>;
        unsafe fn counter_data_get_channels(data: Pin<&mut CounterData>) -> Result<Vec<i32>>;
        unsafe fn counter_data_get_size(data: Pin<&mut CounterData>) -> Result<u32>;
        unsafe fn counter_data_get_dropped_bins(data: Pin<&mut CounterData>) -> Result<u32>;
        unsafe fn counter_data_get_overflow(data: Pin<&mut CounterData>) -> Result<bool>;
    }
}

