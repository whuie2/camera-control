//! Structures for interacting with a Swabian Instruments TimeTaggerUltra device
//! and its associated functions.

use std::pin::Pin;
use cxx::{ let_cxx_string, CxxVector, UniquePtr, kind::Trivial };
use ndarray as nd;
use thiserror::Error;
use crate::timetagger_ffi::{ CHANNEL_UNUSED, ffi };

#[derive(Debug, Error)]
pub enum TaggerError {
    #[error("timetagger error: {0}")]
    DeviceError(String),

    #[error("timetagger: reached timeout or tried to wait for indefinite acquisition")]
    AcquisitionTimeout,
}
pub type TaggerResult<T> = Result<T, TaggerError>;

impl From<cxx::Exception> for TaggerError {
    fn from(except: cxx::Exception) -> Self {
        return Self::DeviceError(except.what().to_string());
    }
}

fn collect_cxx_vector<'a, I, T>(vals: I) -> UniquePtr<CxxVector<T>>
where
    I: IntoIterator<Item = &'a T>,
    T: Copy + cxx::vector::VectorElement + cxx::ExternType<Kind = Trivial> + 'a,
{
    let mut cxx_vec: UniquePtr<CxxVector<T>> = CxxVector::new();
    let mut cxx_vec_ref = cxx_vec.pin_mut();
    vals.into_iter().for_each(|v| cxx_vec_ref.as_mut().push(*v));
    return cxx_vec;
}

macro_rules! impl_debug_boring {
    ( $t:ident ) => {
        impl std::fmt::Debug for $t {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, stringify!($t))
            }
        }
    }
}

/// Main driver to communicate with a TimeTaggerUltra.
///
/// All times are returned as (signed) integer numbers of picoseconds, and
/// channel numbers are allowed to be negative, following the numbering
/// convention that for some channel with physical numbering *x*, *+x* refers to
/// the channel when in rising edge detection mode and *-x* refers to the
/// channel when in falling edge detection mode.
pub struct TimeTaggerUltra {
    tagger: UniquePtr<ffi::TimeTaggerBox>,
}

unsafe impl Send for TimeTaggerUltra { }
unsafe impl Sync for TimeTaggerUltra { }

impl_debug_boring!(TimeTaggerUltra);

impl TimeTaggerUltra {
    fn pinned(&mut self) -> Pin<&mut ffi::TimeTaggerBox> {
        return self.tagger.pin_mut();
    }

    /// Returns a list of serial numbers that can be connected to.
    pub fn scan_devices() -> Vec<String> { ffi::scan_devices() }

    /// Connect to a TimeTaggerUltra, instantiating `Self`.
    pub fn connect(serial: &str) -> TaggerResult<Self> {
        let_cxx_string!(ser = serial);
        let tagger = ffi::create_timetagger(&ser)?;
        return Ok(Self { tagger });
    }

    /// Disconnect from the TimeTaggerUltra, freeing the internal pointer and
    /// consuming `self`.
    pub fn disconnect(self) { }

    /// Get the time delay of a channel in picoseconds.
    pub fn get_input_delay(&mut self, channel: i32) -> TaggerResult<i64> {
        unsafe {
            return Ok(ffi::get_input_delay(self.pinned(), channel)?);
        }
    }

    /// Set the time delay of a channel in picoseconds.
    pub fn set_input_delay(&mut self, channel: i32, delay: i64)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::set_input_delay(self.pinned(), channel, delay)?;
            return Ok(self);
        }
    }

    /// Get the hardware delay of a channel in picoseconds.
    pub fn get_hardware_delay(&mut self, channel: i32) -> TaggerResult<i64> {
        unsafe {
            return Ok(ffi::get_delay_hardware(self.pinned(), channel)?);
        }
    }

    /// Set the hardware delay of a channel in picoseconds.
    pub fn set_hardware_delay(&mut self, channel: i32, delay: i64)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::set_delay_hardware(self.pinned(), channel, delay)?;
            return Ok(self);
        }
    }

    /// Get the software delay of a channel in picoseconds.
    pub fn get_software_delay(&mut self, channel: i32) -> TaggerResult<i64> {
        unsafe {
            return Ok(ffi::get_delay_software(self.pinned(), channel)?);
        }
    }

    /// Set the software delay of a channel in picoseconds.
    pub fn set_software_delay(&mut self, channel: i32, delay: i64)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::set_delay_software(self.pinned(), channel, delay)?;
            return Ok(self);
        }
    }

    /// Reset to default settings and detach all counters.
    pub fn reset(&mut self) -> TaggerResult<&mut Self> {
        todo!()
    }

    /// Get the trigger voltage threshold of a channel.
    pub fn get_trigger_level(&mut self, channel: i32) -> TaggerResult<f64> {
        unsafe {
            return Ok(ffi::get_trigger_level(self.pinned(), channel)?);
        }
    }

    /// Set the trigger voltage threshold of a channel.
    pub fn set_trigger_level(&mut self, channel: i32, voltage: f64)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::set_trigger_level(self.pinned(), channel, voltage)?;
            return Ok(self);
        }
    }

    /// Get the total number of events the USB queue can hold.
    pub fn get_hardware_buffer_size(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(ffi::get_hardware_buffer_size(self.pinned())?);
        }
    }

    /// Set the total number of events the USB queue can hold.
    ///
    /// This can be used to tune the balance between low input latency and high
    /// throughput.
    pub fn set_hardware_buffer_size(&mut self, size: i32)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::set_hardware_buffer_size(self.pinned(), size)?;
            return Ok(self);
        }
    }

    /// Get the serial number of the currently connected device.
    pub fn get_serial(&mut self) -> TaggerResult<String> {
        unsafe {
            return Ok(ffi::get_serial(self.pinned())?);
        }
    }

    /// Get the model number of the currently connected device.
    pub fn get_model(&mut self) -> TaggerResult<String> {
        unsafe {
            return Ok(ffi::get_model(self.pinned())?);
        }
    }

    /// Returns the minimum and maximum voltages of the DACs as a trigger
    /// references.
    pub fn get_dac_range(&mut self) -> TaggerResult<Vec<f64>> {
        unsafe {
            return Ok(ffi::get_dac_range(self.pinned())?);
        }
    }

    /// Returns a list of all physical channel input IDs.
    ///
    /// Positively numbered channels detect rising edges and negatively numbered
    /// channels detect falling edges.
    pub fn get_channel_list(&mut self) -> TaggerResult<Vec<i32>> {
        unsafe {
            return Ok(ffi::get_channel_list(self.pinned())?);
        }
    }

    /// Turn the LEDs on the front panel of the device on or off.
    pub fn set_leds(&mut self, onoff: bool) -> TaggerResult<&mut Self> {
        unsafe {
            ffi::disable_leds(self.pinned(), !onoff)?;
            return Ok(self);
        }
    }

    /// Create a new [`Coincidences`] counter.
    ///
    /// See also [`Coincidences::new`].
    pub fn new_coincidences<'a, I, J>(
        &mut self, 
        channel_groups: I,
        window: i64,
        timestamp: CoincidencesTimestamp,
    ) -> TaggerResult<Coincidences>
    where
        I: IntoIterator<Item = J>,
        J: IntoIterator<Item = &'a i32>,
    {
        return Coincidences::new(self, channel_groups, window, timestamp);
    }

    /// Create a new [`Combiner`] counter.
    ///
    /// See also [`Combiner::new`].
    pub fn new_combiner<'a, I>(
        &mut self,
        channels: I,
    ) -> TaggerResult<Combiner>
    where I: IntoIterator<Item = &'a i32>
    {
        return Combiner::new(self, channels);
    }

    /// Create a new [`Correlation`] measurement.
    ///
    /// See also [`Correlation::new`].
    pub fn new_correlation(
        &mut self,
        channels: (i32, i32),
        binwidth: i64,
        n_bins: usize,
    ) -> TaggerResult<Correlation>
    {
        return Correlation::new(self, channels, binwidth, n_bins);
    }

    /// Create a new [`CountBetweenMarkers`] counter.
    ///
    /// See also [`CountBetweenMarkers::new`].
    pub fn new_count_between_markers(
        &mut self,
        signal_ch: i32,
        start_ch: i32,
        end_ch: Option<i32>,
        n_bins: usize,
    ) -> TaggerResult<CountBetweenMarkers>
    {
        return CountBetweenMarkers::new(
            self, signal_ch, start_ch, end_ch, n_bins);
    }

    /// Create a new [`Counter`].
    ///
    /// See also [`Counter::new`].
    pub fn new_counter<'a, I>(
        &mut self,
        channels: I,
        binwidth: i64,
        n_bins: i32,
    ) -> TaggerResult<Counter>
    where I: IntoIterator<Item = &'a i32>
    {
        return Counter::new(self, channels, binwidth, n_bins);
    }

    /// Create a new [`Countrate`] measurement.
    ///
    /// See also [`Countrate::new`].
    pub fn new_countrate<'a, I>(
        &mut self,
        channels: I,
    ) -> TaggerResult<Countrate>
    where I: IntoIterator<Item = &'a i32>
    {
        return Countrate::new(self, channels);
    }

    /// Create a new [`DelayedChannel`] counter.
    ///
    /// See also [`DelayedChannel::new`].
    pub fn new_delayed_channel(
        &mut self,
        channel: i32,
        delay: i64,
    ) -> TaggerResult<DelayedChannel>
    {
        return DelayedChannel::new(self, channel, delay);
    }

    /// Create a new [`FrequencyMultiplier`] channel.
    ///
    /// See also [`FrequencyMultiplier::new`].
    pub fn new_frequency_multiplier(
        &mut self,
        channel: i32,
        multiplier: i32,
    ) -> TaggerResult<FrequencyMultiplier>
    {
        return FrequencyMultiplier::new(self, channel, multiplier);
    }

    /// Create a new [`GatedChannel`] channel.
    ///
    /// See also [`GatedChannel::new`].
    pub fn new_gated_channel(
        &mut self,
        signal_ch: i32,
        start_ch: i32,
        end_ch: i32,
        initial_state: GatedChannelInitialState,
    ) -> TaggerResult<GatedChannel>
    {
        return GatedChannel::new(
            self, signal_ch, start_ch, end_ch, initial_state);
    }

    /// Create a new [`Histogram`] measurement.
    ///
    /// See also [`Histogram::new`].
    pub fn new_histogram(
        &mut self,
        signal_ch: i32,
        start_ch: i32,
        binwidth: i64,
        n_bins: i32,
    ) -> TaggerResult<Histogram>
    {
        return Histogram::new(self, signal_ch, start_ch, binwidth, n_bins);
    }

    /// Create a new [`Histogram2D`] measurement.
    ///
    /// See also [`Histogram2D::new`].
    pub fn new_histogram_2d(
        &mut self,
        signal_ch: (i32, i32),
        start_ch: i32,
        binwidth: (i64, i64),
        n_bins: (i32, i32),
    ) -> TaggerResult<Histogram2D>
    {
        return Histogram2D::new(self, signal_ch, start_ch, binwidth, n_bins);
    }

    /// Create a new [`HistogramND`] measurement.
    ///
    /// See also [`HistogramND::new`].
    pub fn new_histogram_nd<'i, 'j, 'k, I, J, K>(
        &mut self,
        signal_ch: I,
        start_ch: i32,
        binwidth: J,
        n_bins: K,
    ) -> TaggerResult<HistogramND>
    where
        I: IntoIterator<Item = &'i i32>,
        J: IntoIterator<Item = &'j i64>,
        K: IntoIterator<Item = &'k i32>,
    {
        return HistogramND::new(self, signal_ch, start_ch, binwidth, n_bins);
    }

    /// Create a new [`HistogramLogBins`] measurement.
    ///
    /// See also [`HistogramLogBins::new`].
    pub fn new_histogram_log_bins(
        &mut self,
        signal_ch: i32,
        start_ch: i32,
        exp_range: (f64, f64),
        n_bins: i32,
    ) -> TaggerResult<HistogramLogBins>
    {
        return HistogramLogBins::new(
            self, signal_ch, start_ch, exp_range, n_bins);
    }

    /// Create a new [`StartStop`] measurement.
    ///
    /// See also [`StartStop::new`].
    pub fn new_start_stop(
        &mut self,
        signal_ch: i32,
        start_ch: i32,
        binwidth: i64,
    ) -> TaggerResult<StartStop>
    {
        return StartStop::new(self, signal_ch, start_ch, binwidth);
    }

    /// Create a new [`TimeDifferences`] measurement.
    ///
    /// See also [`TimeDifferences::new`].
    #[allow(clippy::too_many_arguments)]
    pub fn new_time_differences(
        &mut self,
        signal_ch: i32,
        start_ch: Option<i32>,
        next_ch: Option<i32>,
        sync_ch: Option<i32>,
        binwidth: i64,
        n_bins: i32,
        n_hist: i32,
    ) -> TaggerResult<TimeDifferences>
    {
        return TimeDifferences::new(
            self,
            signal_ch,
            start_ch,
            next_ch,
            sync_ch,
            binwidth,
            n_bins,
            n_hist,
        );
    }

    /// Create a new [`TimeDifferencesND`] measurement.
    ///
    /// See also [`TimeDifferencesND::new`].
    #[allow(clippy::too_many_arguments)]
    pub fn new_time_differences_nd<'i, 'j, 'k, I, J, K>(
        &mut self,
        signal_ch: i32,
        start_ch: i32,
        next_ch: I,
        sync_ch: J,
        n_hist: K,
        binwidth: i64,
        n_bins: i32,
    ) -> TaggerResult<TimeDifferencesND>
    where
        I: IntoIterator<Item = &'i i32>,
        J: IntoIterator<Item = &'j i32>,
        K: IntoIterator<Item = &'k i32>,
    {
        return TimeDifferencesND::new(
            self,
            signal_ch,
            start_ch,
            next_ch,
            sync_ch,
            n_hist,
            binwidth,
            n_bins,
        );
    }

    /// Create new [`TriggerOnCountrate`] virtual channels.
    ///
    /// See also [`TriggerOnCountrate::new`].
    pub fn new_trigger_on_countrate(
        &mut self,
        channel: i32,
        ref_countrate: f64,
        hysteresis: f64,
        window: i64,
    ) -> TaggerResult<TriggerOnCountrate>
    {
        return TriggerOnCountrate::new(
            self,
            channel,
            ref_countrate,
            hysteresis,
            window,
        );
    }
}

/// Captures the basic behavior of all measurement and virtual channel types.
///
/// In the C++ library, these methods are inherited from the `IteratorBase`
/// class.
pub trait ClickCounter {
    /// Starts or continues data acquisition.
    fn start(&mut self) -> TaggerResult<&mut Self>;

    /// Starts or continues data acquisition for the given duration (in
    /// picoseconds). Optionally clears data before starting.
    fn start_for(&mut self, dur: i64, clear: bool)
        -> TaggerResult<&mut Self>;

    /// Blocks the current thread until acquisition has completed or timeout (in
    /// picoseconds) is reached. Pass negative values or `None` for `timeout`
    /// for no timeout.
    ///
    /// Can only be used when acquisition is started using [`Self::start_for`].
    fn wait_until_finished(&mut self, timeout: Option<i64>)
        -> TaggerResult<&mut Self>;

    /// Halt acquisition.
    fn stop(&mut self) -> TaggerResult<&mut Self>;

    /// Clear counter memory.
    fn clear(&mut self) -> TaggerResult<&mut Self>;

    /// Return `true` if acquisition is currently in progress, `false`
    /// otherwise.
    fn is_running(&mut self) -> TaggerResult<&mut Self>;

    /// Return the total capture duration (in picoseconds) since creation or the
    /// last call to [`Self::clear`].
    fn get_capture_duration(&mut self) -> TaggerResult<i64>;

    /// Return a JSON-formatted string with all configuration and status flags.
    fn get_configuration(&mut self) -> TaggerResult<String>;
}

macro_rules! impl_clickcounter_for {
    (
        $t:ident,
        $start:ident,
        $start_for:ident,
        $wait_until_finished:ident,
        $stop:ident,
        $clear:ident,
        $is_running:ident,
        $get_capture_duration:ident,
        $get_configuration:ident,
    ) => {
        impl $t {
            fn pinned(&mut self) -> Pin<&mut ffi::$t> {
                return self.counter.pin_mut();
            }
        }

        unsafe impl Send for $t { }
        unsafe impl Sync for $t { }

        impl ClickCounter for $t {
            fn start(&mut self) -> TaggerResult<&mut Self> {
                unsafe {
                    ffi::$start(self.pinned())?;
                    return Ok(self);
                }
            }

            fn start_for(&mut self, dur: i64, clear: bool)
                -> TaggerResult<&mut Self>
            {
                unsafe {
                    ffi::$start_for(self.pinned(), dur, clear)?;
                    return Ok(self);
                }
            }

            fn wait_until_finished(&mut self, timeout: Option<i64>)
                -> TaggerResult<&mut Self>
            {
                unsafe {
                    let res: bool
                        = ffi::$wait_until_finished(
                            self.pinned(), timeout.unwrap_or(-1))?;
                    return if res {
                        Ok(self)
                    } else {
                        Err(TaggerError::AcquisitionTimeout)
                    };
                }
            }

            fn stop(&mut self) -> TaggerResult<&mut Self> {
                unsafe {
                    ffi::$stop(self.pinned())?;
                    return Ok(self);
                }
            }

            fn clear(&mut self) -> TaggerResult<&mut Self> {
                unsafe {
                    ffi::$clear(self.pinned())?;
                    return Ok(self);
                }
            }

            fn is_running(&mut self) -> TaggerResult<&mut Self> {
                unsafe {
                    ffi::$is_running(self.pinned())?;
                    return Ok(self);
                }
            }

            fn get_capture_duration(&mut self) -> TaggerResult<i64> {
                unsafe {
                    return Ok(ffi::$get_capture_duration(self.pinned())?);
                }
            }

            fn get_configuration(&mut self) -> TaggerResult<String> {
                unsafe {
                    return Ok(ffi::$get_configuration(self.pinned())?);
                }
            }
        }
    }
}

/// Type of the timestamp used by a [`Coincidences`] channel.
#[derive(Copy, Clone, Debug)]
pub enum CoincidencesTimestamp {
    /// Time of the last event in the window (fastest).
    Last = 0,
    /// Average time of all events in the window.
    Average = 1,
    /// Time of the first event in the window.
    First = 2,
    /// Time of the event on the channel listed first in the group.
    ListedFirst = 3,
}

/// Virtual channel to count the coincidences in a number of groups of channels
/// over a window of time (in picoseconds).
///
/// A coincidence for a given group is defined as one or more events occurring
/// on each channel of the group within a given time window.
pub struct Coincidences {
    counter: UniquePtr<ffi::Coincidences>,
}

impl_debug_boring!(Coincidences);

impl_clickcounter_for!(
    Coincidences,
    coincidences_start,
    coincidences_start_for,
    coincidences_wait_until_finished,
    coincidences_stop,
    coincidences_clear,
    coincidences_is_running,
    coincidences_get_capture_duration,
    coincidences_get_configuration,
);

impl Coincidences {
    /// Create a new `Coincidences` channel.
    ///
    /// See also [`TimeTaggerUltra::new_coincidences`].
    pub fn new<'a, I, J>(
        tagger: &mut TimeTaggerUltra,
        channel_groups: I,
        window: i64,
        timestamp: CoincidencesTimestamp,
    ) -> TaggerResult<Self>
    where
        I: IntoIterator<Item = J>,
        J: IntoIterator<Item = &'a i32>,
    {
        let mut channels: UniquePtr<CxxVector<i32>> = CxxVector::new();
        let mut channels_ref = channels.pin_mut();
        let mut groupings: UniquePtr<CxxVector<u32>> = CxxVector::new();
        let mut groupings_ref = groupings.pin_mut();
        let mut group_size: u32;
        for group in channel_groups.into_iter() {
            group_size = 0;
            for channel in group.into_iter() {
                channels_ref.as_mut().push(*channel);
                group_size += 1;
            }
            if group_size > 0 { groupings_ref.as_mut().push(group_size); }
        }
        unsafe {
            let coincidences: UniquePtr<ffi::Coincidences>
                = ffi::new_coincidences(
                    tagger.pinned(),
                    channels.as_ref().unwrap(),
                    groupings.as_ref().unwrap(),
                    window,
                    timestamp as i32,
                )?;
            let mut coincidences = Self { counter: coincidences };
            coincidences.stop()?.clear()?;
            return Ok(coincidences);
        }
    }

    /// Get a list of virtual channel numbers for the coincidence groups.
    pub fn get_channels(&mut self) -> TaggerResult<Vec<i32>> {
        unsafe {
            return Ok(ffi::coincidences_get_channels(self.pinned())?);
        }
    }

    /// Set the width of the coincidence window in picoseconds.
    pub fn set_coincidence_window(&mut self, window: i64)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::coincidences_set_coincidence_window(self.pinned(), window)?;
            return Ok(self);
        }
    }
}

/// Virtual channel to count total clicks for each of a group of channels.
pub struct Combiner {
    counter: UniquePtr<ffi::Combiner>,
}

impl_debug_boring!(Combiner);

impl_clickcounter_for!(
    Combiner,
    combiner_start,
    combiner_start_for,
    combiner_wait_until_finished,
    combiner_stop,
    combiner_clear,
    combiner_is_running,
    combiner_get_capture_duration,
    combiner_get_configuration,
);

impl Combiner {
    /// Create a new `Combiner` channel.
    ///
    /// See also [`TimeTaggerUltra::new_combiner`].
    pub fn new<'a, I>(
        tagger: &mut TimeTaggerUltra,
        channels: I,
    ) -> TaggerResult<Self>
    where I: IntoIterator<Item = &'a i32>
    {
        let channels_cxx: UniquePtr<CxxVector<i32>>
            = collect_cxx_vector(channels);
        unsafe {
            let combiner: UniquePtr<ffi::Combiner>
                = ffi::new_combiner(
                    tagger.pinned(), channels_cxx.as_ref().unwrap())?;
            let mut combiner = Self { counter: combiner };
            combiner.stop()?.clear()?;
            return Ok(combiner);
        }
    }

    /// Return an array containing the total numbers of clicks recorded on each
    /// channel.
    pub fn get_channel_counts(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let counts: Vec<i64>
                = ffi::combiner_get_channel_counts(self.pinned())?;
            return Ok(counts.into());
        }
    }

    /// Get the channel number of `self`.
    pub fn get_channel(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(ffi::combiner_get_channel(self.pinned())?);
        }
    }
}

/// Measures the correlation in time between clicks on two channels.
///
/// The time differences between all pairs of clicks on two channels are
/// collected into a single histogram.
pub struct Correlation {
    counter: UniquePtr<ffi::Correlation>
}

impl_debug_boring!(Correlation);

impl_clickcounter_for!(
    Correlation,
    correlation_start,
    correlation_start_for,
    correlation_wait_until_finished,
    correlation_stop,
    correlation_clear,
    correlation_is_running,
    correlation_get_capture_duration,
    correlation_get_configuration,
);

impl Correlation {
    /// Create a new `Correlation` measurement.
    ///
    /// See also [`TimeTaggerUltra::new_correlation`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        channels: (i32, i32),
        binwidth: i64,
        n_bins: usize,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let correlation: UniquePtr<ffi::Correlation>
                = ffi::new_correlation(
                    tagger.pinned(),
                    channels.0,
                    channels.1,
                    binwidth,
                    n_bins as i32,
                )?;
            let mut correlation = Self { counter: correlation };
            correlation.stop()?.clear()?;
            return Ok(correlation);
        }
    }

    /// Return an array of length `n_bins` containing the correlation histogram.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array1<i32>> {
        unsafe {
            let correlation: Vec<i32>
                = ffi::correlation_get_data(self.pinned())?;
            return Ok(correlation.into());
        }
    }

    /// Return an array of length `n_bins` containing the g(2) normalized
    /// histogram.
    ///
    /// The normalization is chosen such that perfectly uncorrelated signals
    /// would result in a histogram with mean bin value 1.
    pub fn get_data_normalized(&mut self) -> TaggerResult<nd::Array1<f64>> {
        unsafe {
            let correlation: Vec<f64>
                = ffi::correlation_get_data_normalized(self.pinned())?;
            return Ok(correlation.into());
        }
    }

    /// Return an array of length `n_bins` containing the time bin locations in
    /// picoseconds.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let bins: Vec<i64> = ffi::correlation_get_index(self.pinned())?;
            return Ok(bins.into());
        }
    }
}

/// Counts clicks on a single channel with time bins determined by signals on
/// other channels.
///
/// Namely, this counter waits for an event on a `start_ch`, after which it
/// begins integrating clicks on a `signal_ch`. If an `end_ch` is provided,
/// integration for the bin will halt on the next event on that channel,
/// otherwise it will continue until the next event on the `start_ch`, at which
/// point integration will begin again.
pub struct CountBetweenMarkers {
    counter: UniquePtr<ffi::CountBetweenMarkers>
}

impl_debug_boring!(CountBetweenMarkers);

impl_clickcounter_for!(
    CountBetweenMarkers,
    count_between_markers_start,
    count_between_markers_start_for,
    count_between_markers_wait_until_finished,
    count_between_markers_stop,
    count_between_markers_clear,
    count_between_markers_is_running,
    count_between_markers_get_capture_duration,
    count_between_markers_get_configuration,
);

impl CountBetweenMarkers {
    /// Create a new `CountBetweenMarkers`.
    ///
    /// `n_bins` is the maximum number of bins that can be recorded.
    ///
    /// See also `TimeTaggerUltra::new_count_between_markers`.
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: i32,
        end_ch: Option<i32>,
        n_bins: usize,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let count_between_markers: UniquePtr<ffi::CountBetweenMarkers>
                = ffi::new_count_between_markers(
                    tagger.pinned(),
                    signal_ch,
                    start_ch,
                    end_ch.unwrap_or(CHANNEL_UNUSED),
                    n_bins as i32,
                )?;
            let mut count_between_markers
                = Self { counter: count_between_markers };
            count_between_markers.stop()?.clear()?;
            return Ok(count_between_markers);
        }
    }

    /// Returns `true` if the internal buffer is full, otherwise `false`.
    pub fn is_full(&mut self) -> TaggerResult<bool> {
        unsafe {
            return Ok(ffi::count_between_markers_ready(self.pinned())?);
        }
    }

    /// Return an array of length `n_bins` containing recorded event numbers in
    /// each bin.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array1<i32>> {
        unsafe {
            let counts: Vec<i32>
                = ffi::count_between_markers_get_data(self.pinned())?;
            return Ok(counts.into());
        }
    }

    /// Return an array of length `n_bins` containing the widths (in
    /// picoseconds) of each bin.
    pub fn get_bin_widths(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let widths: Vec<i64>
                = ffi::count_between_markers_get_bin_widths(self.pinned())?;
            return Ok(widths.into());
        }
    }

    /// Return an array of length `n_bins` containing the starting times (in
    /// picoseconds) of each bin.
    pub fn get_bin_times(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let times: Vec<i64>
                = ffi::count_between_markers_get_index(self.pinned())?;
            return Ok(times.into());
        }
    }
}

/// A simple counter for time bin-resolved counting on one or more channels.
///
/// The internal buffer is circular, meaning that all counts are shifted each
/// time a new one is registered. The last entry in the buffer is always the
/// most recent.
pub struct Counter {
    counter: UniquePtr<ffi::Counter>,
    shape: (usize, usize),
}

impl_debug_boring!(Counter);

impl_clickcounter_for!(
    Counter,
    counter_start,
    counter_start_for,
    counter_wait_until_finished,
    counter_stop,
    counter_clear,
    counter_is_running,
    counter_get_capture_duration,
    counter_get_configuration,
);

impl Counter {
    /// Create a new `Counter`.
    ///
    /// See also [`TimeTaggerUltra::new_counter`].
    pub fn new<'a, I>(
        tagger: &mut TimeTaggerUltra,
        channels: I,
        binwidth: i64,
        n_bins: i32,
    ) -> TaggerResult<Self>
    where I: IntoIterator<Item = &'a i32>
    {
        let channels_cxx: UniquePtr<CxxVector<i32>>
            = collect_cxx_vector(channels);
        let shape: (usize, usize)
            = (
                channels_cxx.as_ref().unwrap().len(),
                n_bins as usize,
            );
        unsafe {
            let counter: UniquePtr<ffi::Counter>
                = ffi::new_counter(
                    tagger.pinned(),
                    channels_cxx.as_ref().unwrap(),
                    binwidth,
                    n_bins,
                )?;
            let mut counter = Self { counter, shape };
            counter.stop()?.clear()?;
            return Ok(counter);
        }
    }

    /// Return a 2D array containing the current values of the internal data
    /// buffer (i.e. counts in each bin).
    ///
    /// The array has shape `(N, M)` where `N` is the number of channels and `M`
    /// is the number of bins. Pass `rolling = true` to have the returned array
    /// start from the oldest measurement.
    pub fn get_data(&mut self, rolling: bool) -> TaggerResult<nd::Array2<i32>> {
        unsafe {
            let counts: Vec<i32>
                = ffi::counter_get_data(self.pinned(), rolling)?;
            return Ok(
                nd::Array2::from_shape_vec(self.shape, counts)
                    .expect("Counter::get_data: couldn't reshape data array")
            );
        }
    }

    /// Return an array of length `n_channels` containing the total number of
    /// events counted for each channel, including the bin currently being
    /// filled.
    pub fn get_data_total_counts(&mut self) -> TaggerResult<nd::Array1<u64>> {
        unsafe {
            let counts: Vec<u64>
                = ffi::counter_get_data_total_counts(self.pinned())?;
            return Ok(counts.into());
        }
    }

    /// Return an array of length `n_bins` containing the time bin locations in
    /// picoseconds.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let bins: Vec<i64> = ffi::counter_get_index(self.pinned())?;
            return Ok(bins.into());
        }
    }

    /// Return bin data as a `CounterData` object.
    ///
    /// This object is effectively a snapshot of the internal buffer. Pass
    /// `remove = true` to remove all fetched data from the buffer.
    pub fn get_data_object(&mut self, remove: bool) -> TaggerResult<CounterData> {
        unsafe {
            let data: UniquePtr<ffi::CounterData>
                = ffi::counter_get_data_object(self.pinned(), remove)?;
            return Ok(CounterData { data, shape: self.shape });
        }
    }
}

/// Measure the average count rate on one or more channels.
///
/// Average count rate is measured by dividing the number of events recorded
/// by elapsed time since an initial event.
pub struct Countrate {
    counter: UniquePtr<ffi::Countrate>
}

impl_debug_boring!(Countrate);

impl_clickcounter_for!(
    Countrate,
    countrate_start,
    countrate_start_for,
    countrate_wait_until_finished,
    countrate_stop,
    countrate_clear,
    countrate_is_running,
    countrate_get_capture_duration,
    countrate_get_configuration,
);

impl Countrate {
    /// Create a new `Countrate`.
    ///
    /// See also [`TimeTaggerUltra::new_countrate`].
    pub fn new<'a, I>(
        tagger: &mut TimeTaggerUltra,
        channels: I,
    ) -> TaggerResult<Self>
    where I: IntoIterator<Item = &'a i32>
    {
        unsafe {
            let channels_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(channels);
            let countrate: UniquePtr<ffi::Countrate>
                = ffi::new_countrate(
                    tagger.pinned(),
                    channels_cxx.as_ref().unwrap(),
                )?;
            let mut countrate = Self { counter: countrate };
            countrate.stop()?.clear()?;
            return Ok(countrate);
        }
    }

    /// Return an array of length `n_channels` containing the averate count rate
    /// per second on each channel.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array1<f64>> {
        unsafe {
            let data: Vec<f64> = ffi::countrate_get_data(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array of length `n_channels` containing the total number of
    /// counts recorded on each channel.
    pub fn get_counts_total(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64>
                = ffi::countrate_get_counts_total(self.pinned())?;
            return Ok(data.into());
        }
    }
}

/// Virtual channel to delay incoming events by a fixed offset.
pub struct DelayedChannel {
    counter: UniquePtr<ffi::DelayedChannel>
}

impl_debug_boring!(DelayedChannel);

impl_clickcounter_for!(
    DelayedChannel,
    delayed_channel_start,
    delayed_channel_start_for,
    delayed_channel_wait_until_finished,
    delayed_channel_stop,
    delayed_channel_clear,
    delayed_channel_is_running,
    delayed_channel_get_capture_duration,
    delayed_channel_get_configuration,
);

impl DelayedChannel {
    /// Create a new `DelayedChannel`.
    ///
    /// See also [`TimeTaggerUltra::new_delayed_channel`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        channel: i32,
        delay: i64,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let delayed_channel: UniquePtr<ffi::DelayedChannel>
                = ffi::new_delayed_channel(tagger.pinned(), channel, delay)?;
            let mut delayed_channel = Self { counter: delayed_channel };
            delayed_channel.stop()?.clear()?;
            return Ok(delayed_channel);
        }
    }

    /// Get the channel number of `self`.
    pub fn get_channel(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(ffi::delayed_channel_get_channel(self.pinned())?);
        }
    }

    /// Set the delay in picoseconds.
    pub fn set_delay(&mut self, delay: i64) -> TaggerResult<&mut Self> {
        unsafe {
            ffi::delayed_channel_set_delay(self.pinned(), delay)?;
            return Ok(self);
        }
    }
}

/// Virtual channel to scale the count rate on a channel up by a fixed factor.
///
/// Frequency multiplication is done by assuming a constant count rate and
/// inserting events on the virtual channel (in addition to the originals on the
/// signal channel) at linearly interpolated intervals between the last two
/// physical events.
pub struct FrequencyMultiplier {
    counter: UniquePtr<ffi::FrequencyMultiplier>
}

impl_debug_boring!(FrequencyMultiplier);

impl_clickcounter_for!(
    FrequencyMultiplier,
    frequency_multiplier_start,
    frequency_multiplier_start_for,
    frequency_multiplier_wait_until_finished,
    frequency_multiplier_stop,
    frequency_multiplier_clear,
    frequency_multiplier_is_running,
    frequency_multiplier_get_capture_duration,
    frequency_multiplier_get_configuration,
);

impl FrequencyMultiplier {
    /// Create a new `FrequencyMultiplier`.
    ///
    /// See also [`TimeTaggerUltra::new_frequency_multiplier`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        channel: i32,
        multiplier: i32,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let frequency_multiplier: UniquePtr<ffi::FrequencyMultiplier>
                = ffi::new_frequency_multiplier(
                    tagger.pinned(),
                    channel,
                    multiplier,
                )?;
            let mut frequency_multiplier
                = Self { counter: frequency_multiplier };
            frequency_multiplier.stop()?.clear()?;
            return Ok(frequency_multiplier);
        }
    }

    /// Get the channel number of `self`.
    pub fn get_channel(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(ffi::frequency_multiplier_get_channel(self.pinned())?);
        }
    }

    /// Get the multiplier.
    pub fn get_multiplier(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(ffi::frequency_multiplier_get_multiplier(self.pinned())?);
        }
    }
}

/// Initial state of a [`GatedChannel`].
#[derive(Copy, Clone, Debug)]
pub enum GatedChannelInitialState {
    Closed = 0,
    Open = 1,
}

/// Virtual channel to gate events on a signal channel by events on other
/// channels.
///
/// This counter waits for an event on a `start_ch`, after which it begins
/// integrating events on a `signal_ch` until an event on a `stop_ch`. The gate
/// can be opened and closed multiple times.
pub struct GatedChannel {
    counter: UniquePtr<ffi::GatedChannel>
}

impl_debug_boring!(GatedChannel);

impl_clickcounter_for!(
    GatedChannel,
    gated_channel_start,
    gated_channel_start_for,
    gated_channel_wait_until_finished,
    gated_channel_stop,
    gated_channel_clear,
    gated_channel_is_running,
    gated_channel_get_capture_duration,
    gated_channel_get_configuration,
);

impl GatedChannel {
    /// Create a new `GatedChannel`.
    ///
    /// See also [`TimeTaggerUltra::new_gated_channel`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: i32,
        end_ch: i32,
        initial_state: GatedChannelInitialState,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let gated_channel: UniquePtr<ffi::GatedChannel>
                = ffi::new_gated_channel(
                    tagger.pinned(),
                    signal_ch,
                    start_ch,
                    end_ch,
                    initial_state as u32,
                )?;
            let mut gated_channel = Self { counter: gated_channel };
            gated_channel.stop()?.clear()?;
            return Ok(gated_channel);
        }
    }

    /// Get the channel number of `self`.
    pub fn get_channel(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(ffi::gated_channel_get_channel(self.pinned())?);
        }
    }
}

/// Time difference histogram measurement.
///
/// This is known as a multiple-start, multiple-stop measurement, wherein the
/// times of events on a `signal_ch` are recorded relative to an initial event
/// on a `start_ch` and collected into a single histogram. The range and
/// resolution of the histogram are set by `binwidth` and `n_bins`, with all
/// events falling outside this range being ignored.
pub struct Histogram {
    counter: UniquePtr<ffi::Histogram>
}

impl_debug_boring!(Histogram);

impl_clickcounter_for!(
    Histogram,
    histogram_start,
    histogram_start_for,
    histogram_wait_until_finished,
    histogram_stop,
    histogram_clear,
    histogram_is_running,
    histogram_get_capture_duration,
    histogram_get_configuration,
);

impl Histogram {
    /// Create a new `Histogram`.
    ///
    /// See also [`TimeTaggerUltra::new_histogram`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: i32,
        binwidth: i64,
        n_bins: i32,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let histogram: UniquePtr<ffi::Histogram>
                = ffi::new_histogram(
                    tagger.pinned(),
                    signal_ch,
                    start_ch,
                    binwidth,
                    n_bins,
                )?;
            let mut histogram = Self { counter: histogram };
            histogram.stop()?.clear()?;
            return Ok(histogram);
        }
    }

    /// Return an array of length `n_bins` containing the histogram bin values.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array1<i32>> {
        unsafe {
            let data: Vec<i32> = ffi::histogram_get_data(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array of length `n_bins` containing the histogram bin
    /// locations.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::histogram_get_index(self.pinned())?;
            return Ok(data.into());
        }
    }
}

/// Two-channel version of a [`Histogram`], where event times on two signal
/// channels relative to a common start time are collected into a 2D histogram.
pub struct Histogram2D {
    counter: UniquePtr<ffi::Histogram2D>,
    shape: (usize, usize),
}

impl_debug_boring!(Histogram2D);

impl_clickcounter_for!(
    Histogram2D,
    histogram_2d_start,
    histogram_2d_start_for,
    histogram_2d_wait_until_finished,
    histogram_2d_stop,
    histogram_2d_clear,
    histogram_2d_is_running,
    histogram_2d_get_capture_duration,
    histogram_2d_get_configuration,
);

impl Histogram2D {
    /// Create a new `Histogram2D`.
    ///
    /// See also [`TimeTaggerUltra::new_histogram_2d`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: (i32, i32),
        start_ch: i32,
        binwidth: (i64, i64),
        n_bins: (i32, i32),
    ) -> TaggerResult<Self>
    {
        unsafe {
            let histogram_2d: UniquePtr<ffi::Histogram2D>
                = ffi::new_histogram_2d(
                    tagger.pinned(),
                    start_ch,
                    signal_ch.0,
                    signal_ch.1,
                    binwidth.0,
                    binwidth.1,
                    n_bins.0,
                    n_bins.1,
                )?;
            let mut histogram_2d
                = Self {
                    counter: histogram_2d,
                    shape: (n_bins.0 as usize, n_bins.1 as usize),
            };
            histogram_2d.stop()?.clear()?;
            return Ok(histogram_2d);
        }
    }

    /// Return a 2D array containing the histogram bin values. The shape of the
    /// array is determined by the number of bins on each channel provided at
    /// creation.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array2<i32>> {
        unsafe {
            let data: Vec<i32> = ffi::histogram_2d_get_data(self.pinned())?;
            return Ok(
                nd::Array2::from_shape_vec(self.shape, data)
                    .expect("Histogram2D::get_data: couldn't reshape data array")
            );
        }
    }

    /// Return a 3D array containing bin times in picoseconds in the form of a
    /// stacked meshgrid. Axis 0 of the array indexes the channel.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array3<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::histogram_2d_get_index(self.pinned())?;
            return Ok(
                nd::Array3::from_shape_vec(
                    (2, self.shape.0, self.shape.1),
                    data,
                )
                .expect("Histogram2D::get_bins: couldn't reshape data array")
            );
        }
    }

    /// Return an array containing the bin times in picoseconds for the first
    /// channel.
    pub fn get_bins_1(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::histogram_2d_get_index_1(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array containing the bin times in picoseconds for the second
    /// channel.
    pub fn get_bins_2(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::histogram_2d_get_index_2(self.pinned())?;
            return Ok(data.into());
        }
    }
}

/// N-channel version of a [`Histogram`], where event times on multiple signal
/// channels relative to a common start time are collected into a ND histogram.
pub struct HistogramND {
    counter: UniquePtr<ffi::HistogramND>,
    shape: Vec<usize>,
}

impl_debug_boring!(HistogramND);

impl_clickcounter_for!(
    HistogramND,
    histogram_nd_start,
    histogram_nd_start_for,
    histogram_nd_wait_until_finished,
    histogram_nd_stop,
    histogram_nd_clear,
    histogram_nd_is_running,
    histogram_nd_get_capture_duration,
    histogram_nd_get_configuration,
);

impl HistogramND {
    /// Create a new `Histogram2D`.
    ///
    /// See also [`TimeTaggerUltra::new_histogram_nd`].
    pub fn new<'i, 'j, 'k, I, J, K>(
        tagger: &mut TimeTaggerUltra,
        signal_ch: I,
        start_ch: i32,
        binwidth: J,
        n_bins: K,
    ) -> TaggerResult<Self>
    where
        I: IntoIterator<Item = &'i i32>,
        J: IntoIterator<Item = &'j i64>,
        K: IntoIterator<Item = &'k i32>,
    {
        let signal_ch_cxx: UniquePtr<CxxVector<i32>>
            = collect_cxx_vector(signal_ch);
        let binwidth_cxx: UniquePtr<CxxVector<i64>>
            = collect_cxx_vector(binwidth);
        let n_bins_cxx: UniquePtr<CxxVector<i32>>
            = collect_cxx_vector(n_bins);
        let shape: Vec<usize>
            = n_bins_cxx.as_ref().unwrap()
            .iter()
            .copied()
            .map(|nb| nb as usize)
            .collect();
        unsafe {
            let histogram_nd: UniquePtr<ffi::HistogramND>
                = ffi::new_histogram_nd(
                    tagger.pinned(),
                    start_ch,
                    signal_ch_cxx.as_ref().unwrap(),
                    binwidth_cxx.as_ref().unwrap(),
                    n_bins_cxx.as_ref().unwrap(),
                )?;
            let mut histogram_nd = Self { counter: histogram_nd, shape };
            histogram_nd.stop()?.clear()?;
            return Ok(histogram_nd);
        }
    }

    /// Return an ND array containing the histogram bin values. The shape of the
    /// array is determined by the number of bins on each channel provided at
    /// creation.
    pub fn get_data(&mut self) -> TaggerResult<nd::ArrayD<i32>> {
        unsafe {
            let data: Vec<i32> = ffi::histogram_nd_get_data(self.pinned())?;
            return Ok(
                nd::ArrayD::from_shape_vec(&*self.shape, data)
                    .expect("HistogramND::get_data: couldn't reshape data array")
            );
        }
    }

    /// Return an array containing bin times in picoseconds for the `dim`-th
    /// channel.
    pub fn get_bins(&mut self, dim: i32) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64>
                = ffi::histogram_nd_get_index(self.pinned(), dim)?;
            return Ok(data.into());
        }
    }
}

/// Like [`Histogram`], but with logarithmically spaced time bins.
pub struct HistogramLogBins {
    counter: UniquePtr<ffi::HistogramLogBins>
}

impl_debug_boring!(HistogramLogBins);

impl_clickcounter_for!(
    HistogramLogBins,
    histogram_log_bins_start,
    histogram_log_bins_start_for,
    histogram_log_bins_wait_until_finished,
    histogram_log_bins_stop,
    histogram_log_bins_clear,
    histogram_log_bins_is_running,
    histogram_log_bins_get_capture_duration,
    histogram_log_bins_get_configuration,
);

impl HistogramLogBins {
    /// Create a new `HistogramLogBins`.
    ///
    /// See also [`TimeTaggerUltra::new_histogram_log_bins`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: i32,
        exp_range: (f64, f64),
        n_bins: i32,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let histogram_log_bins: UniquePtr<ffi::HistogramLogBins>
                = ffi::new_histogram_log_bins(
                    tagger.pinned(),
                    signal_ch,
                    start_ch,
                    exp_range.0,
                    exp_range.1,
                    n_bins,
                )?;
            let mut histogram_log_bins = Self { counter: histogram_log_bins };
            histogram_log_bins.stop()?.clear()?;
            return Ok(histogram_log_bins);
        }
    }

    /// Return an array containing the histogram bin values.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array1<u64>> {
        unsafe {
            let data: Vec<u64>
                = ffi::histogram_log_bins_get_data(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array of length `n_bins` containing the g(2) normalized
    /// histogram.
    ///
    /// The normalization is chosen such that perfectly uncorrelated signals
    /// would result in a histogram with mean bin value 1.
    pub fn get_data_normalized_g2(&mut self) -> TaggerResult<nd::Array1<f64>> {
        unsafe {
            let data: Vec<f64>
                = ffi::histogram_log_bins_get_data_normalized_g2(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array containing the histgram bin *edge* times in picoseconds.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64>
                = ffi::histogram_log_bins_get_bin_edges(self.pinned())?;
            return Ok(data.into());
        }
    }
}

/// Simple start-stop measurement.
///
/// This measurement performs a start-stop measurement between two channels and
/// collects the result in a histogram. Using this measurement on long-running
/// experiments may significantly decrease system performance.
pub struct StartStop {
    counter: UniquePtr<ffi::StartStop>
}

impl_debug_boring!(StartStop);

impl_clickcounter_for!(
    StartStop,
    start_stop_start,
    start_stop_start_for,
    start_stop_wait_until_finished,
    start_stop_stop,
    start_stop_clear,
    start_stop_is_running,
    start_stop_get_capture_duration,
    start_stop_get_configuration,
);

impl StartStop {
    /// Create a new `StartStop`.
    ///
    /// See also [`TimeTaggerUltra::new_start_stop`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: i32,
        binwidth: i64,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let start_stop: UniquePtr<ffi::StartStop>
                = ffi::new_start_stop(
                    tagger.pinned(),
                    signal_ch,
                    start_ch,
                    binwidth,
                )?;
            let mut start_stop = Self { counter: start_stop };
            start_stop.stop()?.clear()?;
            return Ok(start_stop);
        }
    }

    /// Return a 2D array containing histogram bin times and bin values. The
    /// shape of this array is `(N, 2)`, where `N` is the number of non-empty
    /// bins. The rightmost column contains the bin values.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array2<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::start_stop_get_data(self.pinned())?;
            return Ok(
                nd::Array2::from_shape_vec((data.len() / 2, 2), data)
                    .expect("StartStop::get_data: couldn't reshape data array")
            );
        }
    }
}

/// Status indicator of a [`TimeDifferences`] measurement.
#[derive(Copy, Clone, Debug)]
pub enum TimeDifferencesStatus {
    /// Waiting for the next event on the `sync` channel.
    WaitForSync,
    /// Waiting for the next event on the `next` channel.
    WaitForNext,
    /// Accumulating into the histogram at this index.
    HistIdx(i32),
}

/// Collect the time differences between events on two channels in one or more
/// histograms.
///
/// This measurement waits for an event on a `start_ch`, then collects the times
/// relative to this reference point of all subsequent events on a `signal_ch`
/// in a histogram. If no `start_ch` is specified, then the `signal_ch` is used
/// instead, corresponding to an auto-correlation measurement. All events
/// falling outside the histogram range are discarded.
///
/// Data from subsequent start events can be collected into either the same
/// histogram or different histograms, specified by `n_hist`. Each event on
/// `next_ch` increments the histogram index, wrapping it around to zero after
/// it reaches the last valid index, and starts the next measurement. Events on
/// `sync_ch` reset the histogram index to zero.
pub struct TimeDifferences {
    counter: UniquePtr<ffi::TimeDifferences>,
    shape: (usize, usize),
}

impl_debug_boring!(TimeDifferences);

impl_clickcounter_for!(
    TimeDifferences,
    time_differences_start,
    time_differences_start_for,
    time_differences_wait_until_finished,
    time_differences_stop,
    time_differences_clear,
    time_differences_is_running,
    time_differences_get_capture_duration,
    time_differences_get_configuration,
);

impl TimeDifferences {
    /// Create a new `TimeDifferences` measurement.
    ///
    /// See also [`TimeTaggerUltra::new_time_differences`].
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: Option<i32>,
        next_ch: Option<i32>,
        sync_ch: Option<i32>,
        binwidth: i64,
        n_bins: i32,
        n_hist: i32,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let shape: (usize, usize) = (n_bins as usize, n_hist as usize);
            let time_differences: UniquePtr<ffi::TimeDifferences>
                = ffi::new_time_differences(
                    tagger.pinned(),
                    signal_ch,
                    start_ch.unwrap_or(CHANNEL_UNUSED),
                    next_ch.unwrap_or(CHANNEL_UNUSED),
                    sync_ch.unwrap_or(CHANNEL_UNUSED),
                    binwidth,
                    n_bins,
                    n_hist,
                )?;
            let mut time_differences
                = Self { counter: time_differences, shape };
            time_differences.stop()?.clear()?;
            return Ok(time_differences);
        }
    }

    /// Return a 2D array of shape `(N, M)` where `N` is the number of bins and
    /// `M` is the number of histograms containing histogram bin values.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array2<i32>> {
        unsafe {
            let data: Vec<i32> = ffi::time_differences_get_data(self.pinned())?;
            return Ok(
                nd::Array2::from_shape_vec(self.shape, data)
                    .expect("TimeDifferences::get_data: couldn't reshape data array")
            );
        }
    }

    /// Return an array of length `n_bins` containing histogram bin times in
    /// picoseconds.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64>
                = ffi::time_differences_get_index(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Set the maximum number of `sync_ch` events after which the measurement
    /// stops.
    pub fn set_max_resets(&mut self, max_resets: u64)
        -> TaggerResult<&mut Self>
    {
        unsafe {
            ffi::time_differences_set_max_counts(self.pinned(), max_resets)?;
            return Ok(self);
        }
    }

    /// Get the current number of `sync_ch` events.
    pub fn get_resets(&mut self) -> TaggerResult<u64> {
        unsafe {
            return Ok(ffi::time_differences_get_counts(self.pinned())?);
        }
    }

    /// Get the index of the histogram currently being processed or the state of
    /// the measurement.
    pub fn get_status(&mut self) -> TaggerResult<TimeDifferencesStatus> {
        unsafe {
            let status: i32
                = ffi::time_differences_get_histogram_index(self.pinned())?;
            let status: TimeDifferencesStatus
                = match status {
                    -2 => TimeDifferencesStatus::WaitForSync,
                    -1 => TimeDifferencesStatus::WaitForNext,
                    k => TimeDifferencesStatus::HistIdx(k),
                };
            return Ok(status);
        }
    }

    /// Return `true` if the number of maximum number of `sync_ch` events has
    /// been reached, `false` otherwise.
    ///
    /// See also [`Self::set_max_resets`].
    pub fn reached_max_resets(&mut self) -> TaggerResult<bool> {
        unsafe {
            return Ok(ffi::time_differences_ready(self.pinned())?);
        }
    }
}

/// N-dimensional generalization of a [`TimeDifferences`] measurement, with each
/// "dimension" is associated with its own `next` and `sync` channels, along
/// with some number of histograms.
pub struct TimeDifferencesND {
    counter: UniquePtr<ffi::TimeDifferencesND>,
    shape: (usize, usize),
}

impl_debug_boring!(TimeDifferencesND);

impl_clickcounter_for!(
    TimeDifferencesND,
    time_differences_nd_start,
    time_differences_nd_start_for,
    time_differences_nd_wait_until_finished,
    time_differences_nd_stop,
    time_differences_nd_clear,
    time_differences_nd_is_running,
    time_differences_nd_get_capture_duration,
    time_differences_nd_get_configuration,
);

impl TimeDifferencesND {
    /// Create a new `TimeDifferencesND` measurement.
    ///
    /// See also [`TimeTaggerUltra::new_time_differences_nd`].
    #[allow(clippy::too_many_arguments)]
    pub fn new<'i, 'j, 'k, I, J, K>(
        tagger: &mut TimeTaggerUltra,
        signal_ch: i32,
        start_ch: i32,
        next_ch: I,
        sync_ch: J,
        n_hist: K,
        binwidth: i64,
        n_bins: i32,
    ) -> TaggerResult<Self>
    where
        I: IntoIterator<Item = &'i i32>,
        J: IntoIterator<Item = &'j i32>,
        K: IntoIterator<Item = &'k i32>,
    {
        unsafe {
            let next_ch_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(next_ch);
            let sync_ch_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(sync_ch);
            let n_hist_cxx: UniquePtr<CxxVector<i32>>
                = collect_cxx_vector(n_hist);
            let shape: (usize, usize)
                = (
                    n_bins as usize,
                    n_hist_cxx.as_ref().unwrap().iter().sum::<i32>() as usize,
                );
            let time_differences_nd: UniquePtr<ffi::TimeDifferencesND>
                = ffi::new_time_differences_nd(
                    tagger.pinned(),
                    signal_ch,
                    start_ch,
                    next_ch_cxx.as_ref().unwrap(),
                    sync_ch_cxx.as_ref().unwrap(),
                    n_hist_cxx.as_ref().unwrap(),
                    binwidth,
                    n_bins,
                )?;
            let mut time_differences_nd
                = Self { counter: time_differences_nd, shape };
            time_differences_nd.stop()?.clear()?;
            return Ok(time_differences_nd);
        }
    }

    /// Return a 2D array containing the histogram bin data from all histograms.
    ///
    /// The shape of this array is `(N, M)` where `N` is the number of bins and
    /// `M` is the total number of histograms.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array2<i32>> {
        unsafe {
            let data: Vec<i32>
                = ffi::time_differences_nd_get_data(self.pinned())?;
            return Ok(
                nd::Array2::from_shape_vec(self.shape, data)
                    .expect("TimeDifferencesND::get_data couldn't reshape data array")
            );
        }
    }

    /// Return an array of length `n_bins` containing histogram bin times.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64>
                = ffi::time_differences_nd_get_index(self.pinned())?;
            return Ok(data.into());
        }
    }
}

/// Virtual channel to inject events when exceeding or falling below a certain
/// reference count rate within a rolling time window.
///
/// This channel creates two virtual channels, `above` and `below`, and measures
/// the count rate inside a rolling time window before emitting events on either
/// `above` or `below` depending on whether the measured count rate is greater
/// or less than the reference count rate. A `hysteresis` can be introduced to
/// modify the reference count rate depending on whether the current count rate
/// is below the reference (`ref_countrate -> ref_countrate + hysteresis`) or
/// above the reference (`ref_countrate -> ref_countrate - hysteresis`).
///
/// Events are always emitted at the end of the time window, meaning there is an
/// inherent delay introduced on both of the virtual channels. This delay can be
/// adjusted using a [`DelayedChannel`].
pub struct TriggerOnCountrate {
    counter: UniquePtr<ffi::TriggerOnCountrate>
}

impl_debug_boring!(TriggerOnCountrate);

impl_clickcounter_for!(
    TriggerOnCountrate,
    trigger_on_countrate_start,
    trigger_on_countrate_start_for,
    trigger_on_countrate_wait_until_finished,
    trigger_on_countrate_stop,
    trigger_on_countrate_clear,
    trigger_on_countrate_is_running,
    trigger_on_countrate_get_capture_duration,
    trigger_on_countrate_get_configuration,
);

impl TriggerOnCountrate {
    /// Create a new `TriggerOnCountrate`.
    ///
    /// See also [`TimeTaggerUltra::new_trigger_on_countrate`].
    pub fn new(
        tagger: &mut TimeTaggerUltra,
        channel: i32,
        ref_countrate: f64,
        hysteresis: f64,
        window: i64,
    ) -> TaggerResult<Self>
    {
        unsafe {
            let trigger_on_countrate: UniquePtr<ffi::TriggerOnCountrate>
                = ffi::new_trigger_on_countrate(
                    tagger.pinned(),
                    channel,
                    ref_countrate,
                    hysteresis,
                    window,
                )?;
            let mut trigger_on_countrate
                = Self { counter: trigger_on_countrate };
            trigger_on_countrate.stop()?.clear()?;
            return Ok(trigger_on_countrate);
        }
    }

    /// Get the channel number of the `above` virtual channel.
    pub fn get_channel_above(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(
                ffi::trigger_on_countrate_get_channel_above(self.pinned())?
            );
        }
    }

    /// Get the channel number of the `below` virtual channel.
    pub fn get_channel_below(&mut self) -> TaggerResult<i32> {
        unsafe {
            return Ok(
                ffi::trigger_on_countrate_get_channel_below(self.pinned())?
            );
        }
    }

    /// Get the channel numbers of both the `above` and `below` channels.
    pub fn get_channels(&mut self) -> TaggerResult<(i32, i32)> {
        unsafe {
            let above_below: Vec<i32>
                = ffi::trigger_on_countrate_get_channels(self.pinned())?;
            return Ok((above_below[0], above_below[1]));
        }
    }

    /// Get the current count rate.
    pub fn get_countrate(&mut self) -> TaggerResult<f64> {
        unsafe {
            let count_rate: f64
                = ffi::trigger_on_countrate_get_current_countrate(self.pinned())?;
            return Ok(count_rate);
        }
    }

    /// Manually emit an event into the appropriate virtual channel according to
    /// the current count rate. The returned value indicates whether or not this
    /// was possible. Injection is not possible when the device is in overflow
    /// mode or the first count rate measurement has not yet been completed.
    pub fn inject_current_state(&mut self) -> TaggerResult<bool> {
        unsafe {
            return Ok(
                ffi::trigger_on_countrate_inject_current_state(self.pinned())?
            );
        }
    }
}

/// Stored snapshot of the data stored in a [`Counter`].
///
/// See [`Counter::get_data_object`].
pub struct CounterData {
    data: UniquePtr<ffi::CounterData>,
    shape: (usize, usize),
}

impl_debug_boring!(CounterData);

impl CounterData {
    fn pinned(&mut self) -> Pin<&mut ffi::CounterData> {
        return self.data.pin_mut();
    }

    /// Return a 2D array containing the numbers of events counted per channel
    /// per bin.
    ///
    /// The array has shape `(N, M)` where `N` is the number of channels and `M`
    /// is the number of bins.
    pub fn get_data(&mut self) -> TaggerResult<nd::Array2<i32>> {
        unsafe {
            let data: Vec<i32> = ffi::counter_data_get_data(self.pinned())?;
            return Ok(
                nd::Array2::from_shape_vec(self.shape, data)
                    .expect("CounterData::get_data: couldn't reshape data array")
            );
        }
    }

    /// Return an array of length `n_channels` containing the total number of
    /// events counted per channel.
    pub fn get_data_total_counts(&mut self) -> TaggerResult<nd::Array1<u64>> {
        unsafe {
            let data: Vec<u64>
                = ffi::counter_data_get_data_total_counts(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array of length `n_bins` containing bin indices corresponding
    /// to their timestamps.
    pub fn get_bins(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::counter_data_get_index(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array of length `n_bins` containing bin timestamps.
    pub fn get_bin_times(&mut self) -> TaggerResult<nd::Array1<i64>> {
        unsafe {
            let data: Vec<i64> = ffi::counter_data_get_time(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return an array of length `n_bins` indicated whether each bin was
    /// recorded while in overflow mode.
    pub fn get_overflow_mask(&mut self) -> TaggerResult<nd::Array1<i8>> {
        unsafe {
            let data: Vec<i8>
                = ffi::counter_data_get_overflow_mask(self.pinned())?;
            return Ok(data.into());
        }
    }

    /// Return a list of channels whose events were counted.
    pub fn get_channels(&mut self) -> TaggerResult<Vec<i32>> {
        unsafe {
            return Ok(ffi::counter_data_get_channels(self.pinned())?);
        }
    }

    /// Return the number of bins.
    pub fn get_nbins(&mut self) -> TaggerResult<u32> {
        unsafe {
            return Ok(ffi::counter_data_get_size(self.pinned())?);
        }
    }

    /// Return the number of dropped bins.
    pub fn get_dropped_bins(&mut self) -> TaggerResult<u32> {
        unsafe {
            return Ok(ffi::counter_data_get_dropped_bins(self.pinned())?);
        }
    }

    /// Return whether any bins were recorded in overflow mode.
    pub fn is_overflowed(&mut self) -> TaggerResult<bool> {
        unsafe {
            return Ok(ffi::counter_data_get_overflow(self.pinned())?);
        }
    }
}

