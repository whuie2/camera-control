fn main() {
    #[cfg(target_family = "unix")]
    {
        println!("cargo:rerun-if-changed=include/timetagger.h");
        println!("cargo:rerun-if-changed=lib/timetagger.cc");
        println!("cargo:rerun-if-changed=lib/timetagger_ffi.rs");

        println!("cargo:rustc-link-search=/usr/lib");
        println!("cargo:rustc-link-lib=TimeTagger");
        println!("cargo:rustc-link-lib=okFrontPanel");

        cxx_build::bridge("lib/timetagger_ffi.rs")
            .cpp(true)
            .flag("-std=c++14")
            .include("include")
            .file("lib/timetagger.cc")
            .compile("timetagger-cxx");
    }

    #[cfg(target_family = "windows")]
    {
        println!("cargo:rerun-if-changed=include\\timetagger.h");
        println!("cargo:rerun-if-changed=lib\\timetagger.cc");
        println!("cargo:rerun-if-changed=lib\\timetagger_ffi.rs");

        println!("cargo:rustc-link-search=C:\\Program Files\\Swabian Instruments\\Time Tagger\\driver\\x64");
        println!("cargo:rustc-link-lib=static:+verbatim=TimeTagger.lib");
        println!("cargo:rustc-link-lib=static:+verbatim=okFrontPanel.lib");

        cxx_build::bridge("lib\\timetagger_ffi.rs")
            .cpp(true)
            // .flag("-std=c++14") // this gets ignored on Windows for some reason
            .include("include")
            .include("C:\\Program Files\\Swabian Instruments\\Time Tagger\\driver\\include")
            .file("lib\\timetagger.cc")
            .compile("timetagger-cxx");
    }
}

