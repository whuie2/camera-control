from .andor import iXon888
from .timetagger import TimeTaggerUltra
from .config import load_def_config, Config
from .frame_processing import proc_registry
from .live_plot import init_plot, update_plot
import time
import numpy as np
from pathlib import Path
import matplotlib.pyplot as pp
# from multiprocessing import Process, Queue

def load_config(cam: iXon888, config: Config):
    cam.set_config(config)

def acquire(cam: iXon888, config: Config, tagger: TimeTaggerUltra=None):
    cam.set_config(config)
    proc_list = [
        proc_name for proc_name, proc in config.processing.items()
        if isinstance(proc, dict) and proc["enabled"]
    ]
    if len(proc_list) > 1:
        print("Error: only one frame processor can be enabled at a time")
        return
    proc_name = proc_list[0] if len(proc_list) == 1 else None
    proc = None
    if proc_name is not None and proc_name not in proc_registry.keys():
        print(f"Error: unknown frame processor {proc_name}")
        return
    elif proc_name is not None:
        proc = proc_registry[proc_name]
        proc.init(config)

    outfile = None
    outfile_tt = None
    outpath = Path(config.output.datadir).joinpath(config.output.dir)
    counter = config.output.counter
    if config.output.save:
        if not outpath.is_dir():
            print(f":: mkdir -p {outpath}")
            outpath.mkdir(parents=True)
        while outpath.joinpath(f"{config.output.name}_{counter:03.0f}.npy").is_file():
            counter += 1
        outfile = outpath.joinpath(f"{config.output.name}_{counter:03.0f}.npy")
        outfile_tt = outpath.joinpath(f"{config.output.name}_{counter:03.0f}_tt.npy")
        print(f"Saving to file '{outfile}'")

    img_list = list()
    # img_queue = Queue()
    img_dim = cam.get_roi_dim()
    # roi_queue = Queue()
    show_frames_mode = config.output.show_frames.mode
    proc_out = None
    show_proc = proc is not None and config.processing.show

    tt_count_list = list()
    if tagger is not None:
        # if (
        #     config.timetagger.binwidth_sec * config.timetagger.n_bins
        #     < config.camera.exposure_time_ms * 1e-3
        # ):
        #     print(
        #         "WARNING: timetagger counting buffer may not record all"
        #         "events"
        #     )
        tagger.count_gated(
            (
                config.timetagger.counter_channel,
                config.timetagger.start_channel,
                config.timetagger.stop_channel,
            ),
            config.timetagger.binwidth_sec,
            config.timetagger.n_bins,
        )

    cam.start_acquisition()
    if tagger is not None:
        tagger.start_counting() # change this depending on counting mode
    print("Now acquiring images; CTRL-C to stop")
    # TODO: first/last frame live plotting
    if show_frames_mode in {"first", "last", "all"}:
        colorbar_limits = config.output.show_frames.colorbar_limits
        fig, ax, im, bbox = init_plot(
            image_dim=img_dim,
            colorbar_limits=colorbar_limits,
            color_scale=config.output.show_frames.color_scale,
            figname="Main",
            blit=config.output.show_frames.blit,
        )
        if show_proc:
            fig_proc, ax_proc, im_proc, bbox_proc = init_plot(
                image_dim=config.processing[proc_name]["image_dim"][::-1],
                colorbar_limits=colorbar_limits,
                color_scale=config.output.show_frames.color_scale,
                figname=f"Frame Processor ({proc_name})",
                blit=config.output.show_frames.blit,
            )

    try:
        try:
            while True:
                if tagger is not None:
                    tagger.clear_counter().clear_virtual() # change this depending on counting mode
                cam.wait_for_image(timeout=None)
                img = cam.read_oldest_image()
                # img = cam.wait_for_image(timeout=None)
                if img is None:
                    continue
                img_list.append(img)
                if proc is not None:
                    proc_out = proc.doit(img)
                if tagger is not None:
                    data = tagger.get_counter_data()
                    total_counts = data.getDataTotalCounts()[0]
                    tt_count_list.append(total_counts) # change this depending on counting mode
                    window = tt_count_list[-10:]
                    window_avg = sum(window) / len(window)
                    print(f"\r  timetagger counts = {total_counts:5.0f}; avg = {window_avg:7.3f} ", end="")

                if show_frames_mode in {"first", "last", "all"}:
                    update_plot(img, fig, ax, im, colorbar_limits, bbox)
                    if show_proc and proc_out is not None:
                        update_plot(proc_out, fig_proc, ax_proc, im_proc, colorbar_limits, bbox_proc)
        except KeyboardInterrupt:
            raise KeyboardInterrupt
    except KeyboardInterrupt:
        if tagger is not None:
            print("\r", end="")
            tagger.stop_counting()
            tagger.deinit_counter()
        cam.stop_acquisition()
        print("Acquisition stopped")
        if outfile is not None and len(img_list) > 0:
            np.save(str(outfile), np.array(img_list))
            print(f"Data saved to '{outfile}'")
        if outfile_tt is not None and len(tt_count_list) > 0:
            np.save(str(outfile_tt), np.array(tt_count_list))
            print(f"Timetagger data saved to '{outfile_tt}'")
        if show_frames_mode in {"first", "last", "all"}:
            pp.close(fig)
            del fig, ax, im, bbox
        if proc is not None:
            proc.deinit()
            if show_frames_mode in {"first", "last", "all"} and show_proc:
                pp.close(fig_proc)
                del fig_proc, ax_proc, im_proc, bbox_proc
    print("Done.\n")
    return
