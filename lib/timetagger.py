from __future__ import annotations
from typing import Optional
import numpy as np
import TimeTagger as timetagger
from TimeTagger import TimeTagger
from .config import Config

class TimeTaggerUltra:
    """
    Simple class to handle acquisition from a Swabian Instruments TimeTagger
    Ultra.

    Methods without explicit return types return `self`.
    """
    serial: str
    tagger: Optional[TimeTagger]
    counter: Optional[...]
    virtual: Optional[...]

    @staticmethod
    def scan_devices() -> list[str]:
        return list(timetagger.scanTimeTagger())

    def __init__(self, serial: str):
        self.serial = serial
        self.tagger = None
        self.counter = None
        self.virtual = None

    def connect(self):
        self.tagger = timetagger.createTimeTagger(serial=self.serial)
        return self

    def disconnect(self):
        if self.counter is not None:
            self.counter.stop()
            del self.counter
            self.counter = None
        if self.virtual is not None:
            self.virtual.stop()
            del self.virtual
            self.virtual = None
        timetagger.freeTimeTagger(self.tagger)
        self.tagger = None

    def is_connected(self) -> bool:
        return self.tagger is not None

    def has_counter(self) -> bool:
        return self.counter is not None

    def has_virtual(self) -> bool:
        return self.virtual is not None

    def _check_connected(self):
        assert self.is_connected(), "tagger is not connected"

    def _check_counter(self):
        assert self.has_counter(), "counter has not been initialized"

    def _check_virtual(self):
        assert self.has_virtual(), "virtual counter has not been initialized"

    def set_leds(self, onoff: bool):
        self._check_connected()
        self.tagger.disableLEDs(not onoff)
        return self

    def get_channel_list(self) -> list[int]:
        self._check_connected()
        return list(self.tagger.getChannelList())

    def get_serial(self) -> str:
        self._check_connected()
        return self.tagger.getSerial()

    def reset(self):
        self._check_connected()
        self.tagger.reset()
        return self

    def count(self, channels: list[int], binwidth_sec: float, n_bins: int):
        self.counter = timetagger.Counter(
            self.tagger,
            channels,
            int(round(binwidth_sec * 1e12)),
            n_bins,
        )
        self.stop_counting()
        return self

    def countrate(self, channels: list[int]):
        self.counter = timetagger.Countrate(self.tagger, channels)
        self.stop_counting()
        return self

    def count_gated(
        self,
        channel: (int, int, int),
        binwidth_sec: float,
        n_bins: int,
    ):
        if (
            channel[0] == channel[1]
            or channel[1] == channel[2]
            or channel[0] == channel[2]
        ):
            raise ValueError("gate channels must all be different")
        self.virtual = timetagger.GatedChannel(self.tagger, *channel)
        self.counter = timetagger.Counter(
            self.tagger,
            [self.virtual.getChannel()],
            int(round(binwidth_sec * 1e12)),
            n_bins,
        )
        self.stop_counting()
        return self

    def start_counting(
        self,
        duration_sec: Optional[float]=None,
        clear: bool=True,
    ):
        self._check_counter()
        if duration_sec is not None:
            if self.virtual is not None:
                self.virtual.startFor(int(round(duration_sec * 1e12)), clear)
            self.counter.startFor(int(round(duration_sec * 1e12)), clear)
        else:
            if clear:
                if self.virtual is not None:
                    self.virtual.clear()
                self.counter.clear()
            if self.virtual is not None:
                self.virtual.start()
            self.counter.start()
        return self

    def wait_for_counter(self, timeout_sec: float=-1.0):
        self._check_counter()
        if self.virtual is not None:
            self.virtual.waitUntilFinished(int(round(timeout_sec * 1e3)))
        self.counter.waitUntilFinished(int(round(timeout_sec * 1e3)))
        return self

    def clear_counter(self):
        self._check_counter()
        self.counter.clear()
        return self

    def clear_virtual(self):
        self._check_virtual()
        self.virtual.clear()

    def stop_counting(self):
        self._check_counter()
        if self.virtual is not None:
            self.virtual.stop()
        self.counter.stop()
        return self

    def get_counter_data(self) -> ...:
        self._check_counter()
        return self.counter.getDataObject()

    def deinit_counter(self):
        del self.virtual
        self.virtual = None
        del self.counter
        self.counter = None
        return self



