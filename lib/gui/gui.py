from pathlib import Path
import lib.gui.callbacks as cb
import lib.pyplotdefs as pd
import lib.andor as andor
import lib.gui.theme as theme
import time
import dearpygui.dearpygui as dpg
import numpy as np
from queue import Queue
pd.pp.rcParams["keymap.quit"] = []

class camera_gui:
    def __init__(self):
        self.emccd = andor.iXon888()
        self.color_maps = {
            "Grayscale": dpg.mvPlotColormap_Greys,
            "Hot": dpg.mvPlotColormap_Hot,
            "Cool": dpg.mvPlotColormap_Cool,
            "Dark": dpg.mvPlotColormap_Dark,
            "Deep": dpg.mvPlotColormap_Deep,
            "Jet": dpg.mvPlotColormap_Jet,
            "Plasma": dpg.mvPlotColormap_Plasma,
            "Spectral": dpg.mvPlotColormap_Spectral,
            "Viridis": dpg.mvPlotColormap_Viridis,
            "PiYG": dpg.mvPlotColormap_PiYG,
            "RdBu": dpg.mvPlotColormap_RdBu,
            "BrBG": dpg.mvPlotColormap_BrBG,
        }
        self.fdfwd_idx = []

    def create_window(self):
        # connect camera and load up default parameters
        dpg.create_context()
        custom_theme = theme.create_theme()
        dpg.bind_theme(custom_theme)
        # dpg.show_style_editor()
        with dpg.colormap_registry():
            gray_scale = dpg.add_colormap(
                [[  0,   0,   0],
                 [255, 255, 255]],
                qualitative=False,
            )
            self.color_maps["Grayscale"] = gray_scale

        with dpg.font_registry():
            default_font = dpg.add_font("mononoki/mononoki-Regular.ttf", 14)
        with dpg.value_registry():
            dpg.add_string_value(
                default_value=self.emccd.get_status(),
                tag="camera_status",
            )
            dpg.add_float_value(
                default_value=self.emccd.get_exposure(),
                tag="exposure_time",
            )
            dpg.add_int_value(
                default_value=self.emccd.get_em_gain()[0],
                tag="em_gain",
            )
            dpg.add_int_value(
                default_value=375,
                tag="roi_l",
            )
            dpg.add_int_value(
                default_value=395,
                tag="roi_r",
            )
            dpg.add_int_value(
                default_value=695,
                tag="roi_b",
            )
            dpg.add_int_value(
                default_value=705,
                tag="roi_t",
            )
            dpg.add_bool_value(
                # default_value=self.emccd.is_cooler_on(),
                default_value=False,
                tag="cooling",
                # show=False,
            )
            dpg.add_float_value(
                # default_value=self.emccd.get_temperature(),
                default_value=-80.0,
                tag="temperature",
            )
            dpg.add_bool_value(
                default_value=False,
                tag="double_image",
            )
            dpg.add_float_value(
                default_value=0.0,
                tag="cbar_min",
            )
            dpg.add_float_value(
                default_value=100.0,
                tag="cbar_max",
            )
            dpg.add_string_value(
                default_value=str(r"C:\Users\Covey Lab\Documents\Andor Solis\atomic_data"),
                tag="outdir",
            )
            dpg.add_string_value(
                default_value="output",
                tag="outfile",
            )
            dpg.add_bool_value(
                default_value=False,
                tag="save_data",
            )
            dpg.add_int_value(
                default_value=0,
                tag="counter",
            )
            dpg.add_int_value(
                default_value=3,
                tag="counter_pad",
            )
            dpg.add_int_value(
                default_value=1,
                tag="shots_per_loop"
            )
            dpg.add_int_value(
                default_value=0,
                tag="fdfwd_n"
            )
            dpg.add_bool_value(
                default_value=False,
                tag="feedforward",
            )
            dpg.add_string_value(
                default_value=None,
                tag="write_file"
            )
            dpg.add_bool_value(
                default_value=False,
                tag="is_acquiring"
            )
            dpg.add_int4_value(
                default_value=(9,5,3,3),
                tag="image_roi",
            )
            dpg.add_float_value(
                default_value=0,
                tag="image_min"
            )
            dpg.add_float_value(
                default_value=0,
                tag="image_max"
            )
            dpg.add_string_value(
                default_value=self.emccd.get_temperature_status(),
                tag="temperature_display",
            )
        with dpg.window(
            label="Main",
            tag="main_window",
            # no_scrollbar=True,
            width=1200,
            height=800,
            pos=[0,0],
            no_background=True,
            # no_resize=True,
            no_move=True,
            autosize=True,
        ):
            window_padding = 5
            camera_setting_dim = [380, 450]
            file_output_dim = [380, 200]
            image_view_dim = [-1, -1]
            input_box_width = 125
            dpg.bind_font(default_font)

            with dpg.child_window(
                label="Camera",
                tag="camera_setting_window",
                width=camera_setting_dim[0],
                height=camera_setting_dim[1],
                # no_scrollbar=True,
                pos=[window_padding,window_padding],
            ):
                # camera status with acquisition buttons
                with dpg.group(
                    tag="camera_status_display_group",
                    horizontal=True,
                ):
                    dpg.add_text("Camera status:")
                    dpg.add_text(
                        tag="camera_status_display",
                        source="camera_status",
                        # wrap=-1,
                    )
                    # dpg.add_spacer(width=5)
                with dpg.group(
                    tag="connect_buttons",
                    horizontal=True
                ):
                    dpg.add_button(
                        label="Connect",
                        tag="connect_button",
                        callback=cb.connect_button,
                        enabled=True,
                        user_data=self.emccd,
                    )
                    dpg.add_button(
                        label="Disconnect",
                        tag="disconnect_button",
                        callback=cb.disconnect_button,
                        enabled=False,
                        user_data=self.emccd
                    )
                with dpg.group(
                    tag="acquisition_button_group",
                    horizontal=True,
                ):
                    dpg.add_button(
                        label="Acquire",
                        tag="acquire_button",
                        callback=cb.acquire_button,
                        enabled=not self.emccd.is_acquiring() if self.emccd.is_connected() else False,
                        user_data=(self.emccd, self.fdfwd_idx)
                    )
                    dpg.add_button(
                        label="Stop",
                        tag="stop_button",
                        callback=cb.stop_button,
                        enabled=self.emccd.is_acquiring(),
                        user_data=self.emccd
                    )
                with dpg.group(
                    label="debug buttons",
                    horizontal=True,
                    show=True,
                ):
                    # dpg.add_input_text(
                    #     tag="test_str",
                    #     # on_enter=True,
                    #     width=input_box_width,
                    #     show=True,
                    # )
                    dpg.add_button(
                        label="test",
                        tag="test_button",
                        callback=cb.test_button,
                        enabled=True,
                        show=True,
                        user_data=self.emccd,
                    )
                    dpg.add_button(
                        label="grab",
                        tag="grab_button",
                        callback=cb.grab_button,
                        user_data=self.emccd,
                        enabled=False,
                        show=False,
                    )
                    dpg.add_button(
                        label="trig",
                        tag="soft_trig_button",
                        callback=cb.soft_trig_button,
                        user_data=self.emccd,
                        enabled=False,
                        show=False,
                    )
                    dpg.add_button(
                        label="print info",
                        tag="print_info_button",
                        callback=cb.print_info_button,
                        user_data=self.emccd,
                        enabled=True,
                        show=True,
                    )
                    dpg.add_button(
                        label="reset lib",
                        tag="reset_lib_button",
                        callback=cb.reset_lib_button,
                        user_data=self.emccd,
                        enabled=False,
                        show=False,
                    )

                with dpg.group(
                    tag="exposure_time_group",
                    horizontal=True,
                ):
                    dpg.add_text("Exposure time (s):")
                    dpg.add_input_float(
                        tag="exposure_time_input",
                        source="exposure_time",
                        callback=cb.set_exposure_time,
                        user_data=self.emccd,
                        min_value=0.0,
                        max_value=10000.0,
                        min_clamped=True,
                        max_clamped=True,
                        step=5.0,
                        step_fast=100.0,
                        on_enter=True,
                        width=input_box_width,
                    )
                with dpg.group(
                    tag="em_gain_group",
                    horizontal=True,
                ):
                    dpg.add_text("EM gain:")
                    dpg.add_input_int(
                        tag="em_gain_input",
                        source="em_gain",
                        callback=cb.set_em_gain,
                        user_data=self.emccd,
                        min_value=2,
                        max_value=200,
                        min_clamped=True,
                        max_clamped=True,
                        step=1,
                        step_fast=10,
                        on_enter=True,
                        width=input_box_width,
                    )
                with dpg.group(
                    tag="temperature_control",
                    horizontal=True,
                ):
                    # dpg.add_checkbox(
                    #     label="Cooling",
                    #     tag="cooling_checkbox",
                    #     source="cooling",
                    #     callback=cb.set_cooling,
                    #     user_data=self.emccd,
                    #     show=False,
                    #     enabled=False,
                    # )
                    dpg.add_input_float(
                        tag="temperature_input",
                        source="temperature",
                        callback=cb.set_temperature,
                        user_data=self.emccd,
                        min_value=-100.0,
                        max_value=20.0,
                        min_clamped=True,
                        max_clamped=True,
                        step=1.0,
                        step_fast=10.0,
                        on_enter=True,
                        width=input_box_width,
                    )
                    dpg.add_text(source="temperature_display")
                    dpg.add_text("", tag="current_temp")
                dpg.add_button(
                    tag="more_setting_button",
                    label="more settings",
                )
                with dpg.popup(
                    tag="more_setting_popup",
                    parent="more_setting_button",
                    mousebutton=dpg.mvMouseButton_Left,
                    modal=True,
                ):
                    dpg.add_text("TODO")
                with dpg.child_window(
                    height=100,
                    tag="ROI_window"
                    # border=False
                ):
                    dpg.add_text("ROI:")
                    with dpg.group(
                        tag="roi_group",
                        horizontal=True,

                    ):
                        with dpg.group(
                            tag="roi_x_group",
                            width=100,
                        ):
                            with dpg.group(horizontal=True):
                                dpg.add_text("  Left:")
                                dpg.add_input_int(
                                    tag="roi_l_input",
                                    source="roi_l",
                                    callback=cb.set_roi,
                                    user_data=self.emccd,
                                    min_value=0,
                                    max_value=1023,
                                    min_clamped=True,
                                    max_clamped=True,
                                    step=1,
                                    step_fast=10,
                                    on_enter=True,
                                    width=-1,
                                )
                            with dpg.group(horizontal=True):
                                dpg.add_text(" Right:")
                                dpg.add_input_int(
                                    tag="roi_r_input",
                                    source="roi_r",
                                    callback=cb.set_roi,
                                    user_data=self.emccd,
                                    min_value=0,
                                    max_value=1023,
                                    min_clamped=True,
                                    max_clamped=True,
                                    step=1,
                                    step_fast=10,
                                    on_enter=True,
                                    width=-1,
                                )
                        with dpg.group(
                            tag="roi_y_group",
                            width=100
                        ):
                            with dpg.group(horizontal=True):
                                dpg.add_text("   Top:")
                                dpg.add_input_int(
                                    tag="roi_t_input",
                                    source="roi_t",
                                    callback=cb.set_roi,
                                    user_data=self.emccd,
                                    min_value=0,
                                    max_value=1023,
                                    min_clamped=True,
                                    max_clamped=True,
                                    step=1,
                                    step_fast=10,
                                    on_enter=True,
                                    width=-1,
                                )
                            with dpg.group(horizontal=True):
                                dpg.add_text("Bottom:")
                                dpg.add_input_int(
                                    tag="roi_b_input",
                                    source="roi_b",
                                    callback=cb.set_roi,
                                    user_data=self.emccd,
                                    min_value=0,
                                    max_value=1023,
                                    min_clamped=True,
                                    max_clamped=True,
                                    step=1,
                                    step_fast=10,
                                    on_enter=True,
                                    width=-1,
                                )
                with dpg.child_window(
                    height=-1,
                    tag="acquisition_settings",
                    horizontal_scrollbar=True,
                    # border=False
                ):
                    dpg.add_text("Acquisition loop settings:")
                    with dpg.group(
                        tag="image_number_group",
                        horizontal=True
                    ):
                        dpg.add_text("Number of shots per loop:")
                        dpg.add_input_int(
                            tag="image_loop_input",
                            source="shots_per_loop",
                            callback=cb.set_image_loop,
                            min_value=0,
                            max_value=20,
                            min_clamped=True,
                            max_clamped=True,
                            step=1,
                            # on_enter=True,
                            width=input_box_width,
                        )
                    with dpg.group(
                        tag="fdfwd_setting_group",
                        horizontal=True
                    ):
                        dpg.add_checkbox(
                            label="feedforward",
                            tag="fdfwd_checkbox",
                            source="feedforward",
                            callback=cb.enable_feedforward,
                        )
                        # dpg.add_text("Image number before pausing:")
                    with dpg.group(
                        tag="fdfwd_idx_group",
                        horizontal=True,
                        show=dpg.get_value("feedforward"),
                    ):
                        dpg.add_text(
                            "Enter image index:",
                            tag="fdfwd_text",
                        )
                        dpg.add_input_text(
                            # label="Enter image index:",
                            tag="fdfwd_idx_input",
                            hint="0,1,2,...",
                            width=-1,
                            # decimal=True,
                            callback=cb.set_fdfwd_idx,
                            user_data=self.fdfwd_idx,
                            on_enter=True,
                        )
                    with dpg.group(
                        tag="image_roi_group",
                        show=dpg.get_value("feedforward"),
                    ):
                        dpg.add_text(
                            "Enter image ROI(x,y,w,h):",
                        )
                        dpg.add_input_intx(
                            tag="image_roi_input",
                            size=4,
                            source="image_roi",
                            min_value=0,
                            max_value=1024,
                            min_clamped=True,
                            max_clamped=True,
                            callback=cb.image_roi_input,
                            # on_enter=True,
                            # width=-1,
                        )

            # dpg.add_spacer(height=50, width=-1, parent="main_window")
            with dpg.child_window(
                label="File Output",
                tag="file_output_window",
                width=file_output_dim[0],
                height=file_output_dim[1],
                # no_scrollbar=True,
                parent="main_window",
                pos=[window_padding, 2*window_padding+camera_setting_dim[1]]
            ):
                with dpg.group(
                    tag="outdir_group",
                    horizontal=True
                ):
                    dpg.add_button(
                        label="Output directory:",
                        tag="outdir_button",
                        callback=cb.outdir_selector,
                    )
                    dpg.add_input_text(
                        source="outdir",
                        readonly=True,
                        width=-1
                    )
                with dpg.group(
                    tag="outfile_group",
                    horizontal=True,
                ):
                    dpg.add_button(
                        label="Output file name:",
                        tag="outfile_button",
                        callback=cb.outfile_selector,
                    )
                    dpg.add_input_text(
                        tag="outfile_input",
                        source="outfile",
                        hint="output.npy",
                        callback=cb.check_outfile_ext,
                        multiline=False,
                        # on_enter=True,
                        width=-1,
                    )
                with dpg.group(
                    tag="counter_group",
                    horizontal=True,
                ):
                    # dpg.add_text("Counter pad:")
                    # dpg.add_input_int(
                    #     tag="counter_pad_input",
                    #     source="counter_pad",
                    #     min_value=0,
                    #     step=1,
                    #     step_fast=1,
                    #     on_enter=True,
                    #     width=100,
                    # )
                    dpg.add_text("Counter:")
                    dpg.add_input_int(
                        tag="counter_input",
                        source="counter",
                        callback=cb.set_counter,
                        min_value=0,
                        step=1,
                        min_clamped=True,
                        max_clamped=True,
                        step_fast=5,
                        # on_enter=True,
                        width=input_box_width,
                    )
                dpg.add_checkbox(
                    label="Save data",
                    tag="save_data_checkbox",
                    source="save_data",
                )

            with dpg.child_window(
                label="Live View",
                tag="live_view",
                pos=[camera_setting_dim[0] + 2*window_padding, window_padding],
                width=image_view_dim[0],
                height=image_view_dim[1],
                # no_scrollbar=True,
                parent="main_window",
                horizontal_scrollbar=True,
            ):
                with dpg.group(
                    tag="cbar_palette_group",
                    horizontal=True,
                ):
                    dpg.add_text("Palette:")
                    dpg.add_combo(
                        [*self.color_maps],
                        tag="color_scale_input",
                        callback=cb.set_color_scale,
                        default_value="Grayscale",
                        user_data=self.color_maps,
                        width=200,
                    )
                with dpg.group(
                    tag="cbar_group",
                    horizontal=True,
                ):
                    # dpg.add_spacer(width=30)
                    dpg.add_text("cbar_min")
                    dpg.add_input_float(
                        source="cbar_min",
                        on_enter=True,
                        callback=cb.set_cbar_min,
                        width=input_box_width,
                        step=1,
                        step_fast=5,
                    )
                    dpg.add_text("cbar_max:")
                    dpg.add_input_float(
                        source="cbar_max",
                        on_enter=True,
                        callback=cb.set_cbar_max,
                        width=input_box_width,
                        step=1,
                        step_fast=5,
                    )
                    dpg.add_spacer(width=30)
                    dpg.add_button(
                        label="Autoscale",
                        tag="autoscale_colorbar_button",
                        callback=cb.autoscale_cbar,
                        width=100,

                    )
                with dpg.group(
                    tag="display_idx_group",
                    horizontal=True,
                ):
                    dpg.add_text("display image idx:")
                    dpg.add_input_int(
                        default_value=-1,
                        width=input_box_width,
                        step=1,
                        tag="display_idx_input",
                    )

                with dpg.group(
                    tag="image_plot_group",
                    horizontal=True,
                ):
                    dpg.add_plot(
                            tag="image_plot",
                            width=600,
                            # height=500,
                            height=-1,
                            no_menus=True,
                            no_highlight=True,
                            no_box_select=True,
                            equal_aspects=True,
                    )
                    dpg.add_plot_axis(
                        axis=0,
                        label="X",
                        tag="xaxis",
                        parent="image_plot",
                        no_gridlines=True,
                        # no_tick_marks=True,
                        # no_tick_labels=True,
                        # show=False,
                    )
                    dpg.add_plot_axis(
                        axis=0,
                        label="Y",
                        tag="yaxis",
                        parent="image_plot",
                        no_gridlines=True,
                        # no_tick_marks=True,
                        # no_tick_labels=True,
                        # show=False,
                    )
                    test_data = cb._load_test_data()
                    row, col = test_data.shape
                    dpg.set_value("cbar_min", test_data.min())
                    dpg.set_value("cbar_max", test_data.max())
                    dpg.set_value("image_min", test_data.min())
                    dpg.set_value("image_max", test_data.max())
                    dpg.add_heat_series(
                        test_data,
                        tag="image",
                        rows=row,
                        cols=col,
                        parent="yaxis",
                        scale_min=dpg.get_value("cbar_min"),
                        scale_max=dpg.get_value("cbar_max"),
                        bounds_min=(dpg.get_value("roi_l")-0.5, dpg.get_value("roi_b")-0.5),
                        bounds_max=(dpg.get_value("roi_r")+0.5, dpg.get_value("roi_t")+0.5),
                        format="",
                    )
                    dpg.bind_colormap("image_plot", self.color_maps["Grayscale"])
                    dpg.add_colormap_scale(
                        tag="cbar",
                        min_scale=dpg.get_value("cbar_min"),
                        max_scale=dpg.get_value("cbar_max"),
                        width=50,
                        height=-1,
                        # pos=(1,1)
                    )
                    dpg.bind_colormap("cbar", self.color_maps["Grayscale"])

    def update_camera_status(self):
        dpg.set_value("camera_status", self.emccd.get_status())
        dpg.set_value("temperature_display", self.emccd.get_temperature_status())
        dpg.set_value("em_gain", self.emccd.get_em_gain()[0])
        # cooling = not self.emccd.get_temperature_status() == "off"
        # dpg.set_value("cooling", cooling)
        dpg.set_value("exposure_time", self.emccd.get_exposure())
        dpg.set_value("current_temp", f"{self.emccd.get_temperature():3.1f}")

    def run_gui(self):
        dpg.create_viewport(title="Solace from Solis")
        dpg.setup_dearpygui()
        dpg.show_viewport(maximized=True,)
        dpg.set_primary_window("main_window", True)
        # self.init_child_threads()
        time0 = time.time()
        while dpg.is_dearpygui_running():
            dt = time.time() - time0
            if dt >= 3:
                self.update_camera_status()
                time0 = time.time()
            dpg.render_dearpygui_frame()
        self.emccd.disconnect()
        dpg.destroy_context()

