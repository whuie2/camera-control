from __future__ import annotations
import numpy as np
import matplotlib.pyplot as pp
pp.rcParams["keymap.quit"] = []
import matplotlib.image as image
from typing import TypeVar

BBox = TypeVar("BBox")

def init_plot(
	image_dim: tuple[int, int],
	colorbar_limits: tuple[int, int],
	color_scale: str="gray",
	figname: str=None,
	blit: bool=True,
) -> (pp.Figure, pp.Axes, image.AxesImage, BBox | None):
	fig, ax = pp.subplots(num=figname, dpi=150, figsize=(3.8, 3.5))
	im = ax.imshow(
		np.zeros(image_dim),
		vmin=None if colorbar_limits[0] < 0 else colorbar_limits[0],
		vmax=None if colorbar_limits[1] < 0 else colorbar_limits[1],
		cmap=color_scale,
		aspect="equal",
		interpolation="none",
		origin="upper",
	)
	cbar = fig.colorbar(im, ax=ax)

	im.set_clim(*[None if x < 0 else x for x in colorbar_limits])
	fig.canvas.draw()
	if blit:
		bbox = fig.canvas.copy_from_bbox(ax.bbox)
	else:
		bbox = None
	# pp.show(block=False)
	pp.ion()
	pp.pause(0.001)
	return (fig, ax, im, bbox)

def update_plot(
	frame: np.ndarray,
	fig: pp.Figure,
	ax: pp.Axes,
	im: image.AxesImage,
	colorbar_limits: list[float, float],
	blit_with: BBox=None
):
	im.set_data(frame)
	# im.set_extent([ # [l, r, b, t] with y-axis reversed
	# 	-0.5, frame.shape[1] + 0.5,
	# 	frame.shape[0] + 0.5, -0.5,
	# ])
	im.set_clim(
		frame.min() if colorbar_limits[0] < 0 else colorbar_limits[0],
		frame.max() if colorbar_limits[1] < 0 else colorbar_limits[1],
	)
	ax.set_aspect(1)
	if blit_with is not None:
		fig.canvas.restore_region(blit_with)
		ax.draw_artist(im)
		fig.canvas.blit(ax.bbox)
	else:
		fig.canvas.draw()
	fig.canvas.flush_events()


