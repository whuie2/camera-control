from __future__ import annotations
import numpy as np
import scipy.ndimage as ndimg
from astropy.io import fits
from pathlib import Path
from itertools import product
from typing import Sequence
import sys

class Slicer:
    """
    Just an easy way to create `slice` objects.
    """
    def __getitem__(self, s):
        return s

    def sort_idx_nd(
        self,
        A: np.ndarray,
        other_idx: Sequence[int],
        *sort_indices: np.ndarray
    ) -> np.ndarray:
        if len(sort_indices) < 1:
            return A
        else:
            return np.array([
                self.sort_idx_nd(
                    A[(
                        *other_idx,
                        k,
                        *((len(sort_indices) - 1) * [self[:]])
                    )],
                    tuple(),
                    *sort_indices[1:]
                )
                for k in sort_indices[0]
            ])
S = Slicer()

class ROI:
    """
    Class to handle parsing the total, multi-dimensional array generated from a
    run of data.
    """

    def __init__(self, loc: (int, int), size: (int, int), optim_pad: int=0):
        """
        Constructor.

        Parameters
        ----------
        loc : (int, int)
            (x, y) coordinates of the nominal upper-left corner of the ROI. Note
            that these are treated as array indices, so (0, 0) corresponds to
            the upper-left corner of an image.
        size : (int, int)
            (width, height) of the ROI.
        optim_pad : int = 0 (optional)
            Padding distance from the edges of the nominal ROI over which an
            optimum can be selected. If the nominal ROI has size (w, h), then
            the total area over which an optimal can be chosen is
            (w + 2 * optim_pad, h + 2 * optim_pad).
        """
        self.loc = (abs(loc[0]), abs(loc[1]))
        self.size = (abs(size[0]), abs(size[1]))
        self.optim_pad = abs(optim_pad)

    def _optim_locs(
        self,
        sum_array: np.ndarray,
        threshold: float,
        N_ax: int
    ) -> np.ndarray:
        # sum_array[i, j] is the roi total for a ROI cornered at i, j
        # of the padded ROI (= image[y - pad + i, x - pad + j])
        if N_ax < 1: # N_ax is the number of non-image coordinate axes
            # assume we've already checked
            # self.optim_pad == 0 or threshold > sum_array.max()
            x, y = self.loc
            i, j = np.unravel_index(sum_array.argmax(), sum_array.shape)
            if sum_array[i, j] >= threshold:
                return np.array([
                    [x - self.optim_pad + j, y - self.optim_pad + i],
                    self.size,
                ], dtype=np.uint32)
            else:
                return np.array([self.loc, self.size], dtype=np.uint32)
        else:
            return np.array([
                self._optim_locs(sum_array_k, threshold, N_ax - 1)
                for sum_array_k in sum_array
            ], dtype=np.uint32)

    def optim_locs(self, array: np.ndarray, threshold: float) -> np.ndarray:
        """
        Find the locations of optimal ROIs within the padding area based on
        total counts with the ROI and a threshold: If the optimal total count
        is greater than the threshold, use the optimum, otherwise use the
        nominal location.

        Parameters
        ----------
        array : numpy.ndarray[ndim=N, dtype=numpy.float64]
            Data array. Axes -2 and -1 are assumed to corresponds to image
            coordinates.
        threshold : float
            Count threshold.

        Returns
        -------
        locs : numpy.ndarray[ndim=N, dtype=numpy.uint32]
            Locations and sizes of optimal ROIs. The sizes of these ROIs are not
            optimized on; they are always equal to `self.size`. The number of
            dimensions of this array is the same as the input array; the last
            two have been replaced with the 2x2 array
                array([[ x, y ],
                       [ w, h ]])
            for each combination of parameters.
        """
        x, y = self.loc
        w, h = self.size
        N = h + 2 * self.optim_pad
        M = w + 2 * self.optim_pad
        N_ax = len(array.shape) - 2
        sum_array = sum(
            array[(
                *(N_ax * [S[:]]),
                S[y - self.optim_pad : y + h + self.optim_pad],
                S[x - self.optim_pad : x + w + self.optim_pad],
            )][(
                *(N_ax * [S[:]]),
                S[i : i + (N - h + 1)],
                S[j : j + (M - w + 1)]
            )]
            for i, j in product(range(h), range(w))
        )
        if self.optim_pad == 0 or threshold > sum_array.max():
            ret = np.zeros((*array.shape[:-2], 2, 2), dtype=np.uint32)
            ret[(*(N_ax * [S[:]]), 0, S[:])] = self.loc
            ret[(*(N_ax * [S[:]]), 1, S[:])] = self.size
            return ret
        else:
            return self._optim_locs(sum_array, threshold, len(array.shape) - 2)

    def _select_optim(
        self,
        array: np.ndarray,
        locs: np.ndarray,
        N_ax: int
    ) -> np.ndarray:
        if N_ax <= 1:
            return np.array([
                A[i : i + h, j : j + w]
                for A, [[j, i], [w, h]] in zip(array, locs)
            ], dtype=np.float64)
        else:
            return np.array([
                self._select_optim(array_k, locs_k, N_ax - 1)
                for array_k, locs_k in zip(array, locs)
            ], dtype=np.float64)

    def select_optim(
        self,
        array: np.ndarray,
        threshold: float,
        per_shot: bool=True
    ) -> tuple[np.ndarray, np.ndarray]:
        """
        Select an optimal ROI within the padding area based on total counts and
        a threshold: If the total count is greater than the threshold, use the
        optimum, otherwise use the nominal location.

        Parameters
        ----------
        array : numpy.ndarray[ndim=N, dtype=numpy.float64]
            Array to be sliced. Axis 0 is assumed to index the repetitions under
            a certain parameter set; Axes -2 and -1 are assumed to correspond to
            image coordinates. All other axes are unaffected.
        threshold : float
            Count threshold.
        per_shot : bool = False (optional)
            Optimize the ROI location on a per-shot basis instead of an average
            over all shots.

        Returns
        -------
        optim : numpy.ndarray[ndim=N, dtype=numpy.float64]
            The array after slicing down to optimal ROIs. Axis structure is
            maintained except for the last two axes, whose sizes have been
            reduced to the ROI size.
        locs : numpy.ndarray[ndim=N, dtype=numpy.uint32]
            Locations and sizes of optimal ROIs. The sizes of these ROIs are not
            optimized on; they are always equal to `self.size`. The number of
            dimensions of this array is the same as the input array; the last
            two have been replaced with the 2x2 array
                array([[ x, y ],
                       [ w, h ]])
            for each combination of parameters.
        """
        if per_shot:
            N_ax = len(array.shape) - 2
            locs = self.optim_locs(array, threshold)
            return (
                self._select_optim(array, locs, N_ax),
                locs,
            )
        else:
            N_ax = len(array.shape) - 3
            locs = self.optim_locs(array.mean(axis=0), threshold)
            return (
                np.array([
                    self._select_optim(array_k, locs, N_ax)
                    for array_k in array
                ]),
                np.array(array.shape[0] * [locs])
            )

    def box_coords(self) -> tuple[list[float], list[float]]:
        """
        Generate lists of x and y coordinates for plotting.
        """
        x, y = self.loc
        w, h = self.size
        return tuple([
            list(xy) for xy in zip(*[
                ( x - 0.5,     y - 0.5     ),
                ( x - 0.5 + w, y - 0.5     ),
                ( x - 0.5 + w, y - 0.5 + h ),
                ( x - 0.5,     y - 0.5 + h ),
                ( x - 0.5,     y - 0.5     ),
            ])
        ])

    @staticmethod
    def _box_coords_multi(
        locs: np.ndarray,
        N_ax: int,
    ) -> np.ndarray:
        # roi = array([[ x, y ],
        #              [ w, h ]])
        conv = lambda roi: np.array([
            [ roi[0, 0] - 0.5,             roi[0, 1] - 0.5             ],
            [ roi[0, 0] - 0.5 + roi[1, 0], roi[0, 1] - 0.5             ],
            [ roi[0, 0] - 0.5 + roi[1, 0], roi[0, 1] - 0.5 + roi[1, 1] ],
            [ roi[0, 0] - 0.5,             roi[0, 1] - 0.5 + roi[1, 1] ],
            [ roi[0, 0] - 0.5,             roi[0, 1] - 0.5             ],
        ]).T
        if N_ax <= 1:
            return np.array([conv(roi) for roi in locs])
        else:
            return np.array([
                ROI._box_coords_multi(loc, N_ax - 1) for loc in locs
            ])

    @staticmethod
    def box_coords_multi(
        locs: np.ndarray
    ) -> np.ndarray:
        """
        Generate lists of x and y coordinates for plotting multiple ROI boxes.

        Parameters
        ----------
        locs : numpy.ndarray[ndim=N, dtype=numpy.uint32]
            Locations and sizes of optimal ROIs, as generated by ROI.optim_locs.

        Returns
        -------
        boxes : numpy.ndarray[ndim=N, dtype=numpy.float64]
            x and y coordinate arrays for plotting, generated for each location
            defined in `locs`. The first N - 2 axes are structured the same as
            in `locs`; the last two are replaced with the 2x2 array
                array([[ x0, x1, x2, x3, x4 ],
                       [ y0, y1, y2, y3, y4 ]])
        """
        N_ax = len(locs.shape) - 2
        return ROI._box_coords_multi(locs, N_ax)

    def padded_box_coords(self) -> tuple[list[float], list[float]]:
        """
        Generate lists of x and y coordinates for the padded ROI box for
        plotting.
        """
        x, y = self.loc
        w, h = self.size
        pad = self.optim_pad
        return tuple([
            list(xy) for xy in zip(*[
                ( x - 0.5 - pad,     y - 0.5 - pad     ),
                ( x - 0.5 + w + pad, y - 0.5 - pad     ),
                ( x - 0.5 + w + pad, y - 0.5 + h + pad ),
                ( x - 0.5 - pad,     y - 0.5 + h + pad ),
                ( x - 0.5 - pad,     y - 0.5 - pad     ),
            ])
        ])

def find_rois(
    img: np.ndarray,
    N: int,
    size: (int, int),
    rois_nom: list[ROI]=None,
    dist_limit: float=None,
    optim_pad: int=0,
) -> list[ROI]:
    """
    Generate a list of ROIs from an image, optionally matching to a list of
    expected ROIs.

    Parameters
    ----------
    img : numpy.ndarray[ndim=2]
        2D image array. Data must be representable as a numpy float.
    N : int >= 0
        Number of ROIs to find.
    size : (int >= 1, int >= 1)
        Size (w, h) of ROIs to find.
    rois_nom : list[ROI] (optional)
        List of ROI objects.
    dist_limit: float
        Maximum distance to consider when matching found ROIs to `rois_nom` if
        passed as a list. Defaults to the average dimension of the ROI size.
    optim_pad : int
        Construct new ROI objects with this value for `optim_pad`. Overridden
        if `rois_nom` is passed as a list.

    Returns
    -------
    rois : list[ROI]
        List of ROIs, matched by distance to `rois_nom` if passed as a list.
    """
    if N == 0:
        return list()
    img = img.astype(np.float64)
    if rois_nom is not None:
        match_locs = True
        if not all(isinstance(roi, ROI) for roi in rois_nom):
            raise Exception("find_rois: encountered non-ROI")

    # have to reverse size to convert to array dimensions
    filtered = ndimg.maximum_filter(img, size=size[::-1])
    loc_mask = img == filtered
    locs = [(filtered[pos], pos) for pos in zip(*np.where(loc_mask))]
    # take the `N` brightest locs
    locs = sorted(locs, key=lambda x: x[0], reverse=True)[:N]

    if not match_locs:
        locs = sorted(locs, key=lambda x: x[1][1]) # sort by x-coord
        return [ROI(loc[1][::-1], size, optim_pad) for loc in locs]
    else:
        d = (size[0] + size[1]) / 2 if dist_limit is None else dist_limit
        ret = [None for _ in rois_nom]
        matched = [False for _ in rois_nom]
        for (n, (_, loc)) in enumerate(locs):
            if n >= len(rois_nom):
                ret[ret.index(None)] = ROI(
                    (loc[1] - size[0] // 2, loc[0] - size[1] // 2),
                    size,
                    dist_limit,
                )
                continue
            dists = np.array([
                np.sqrt((loc[1] - roi.loc[0])**2 + (loc[0] - roi.loc[1])**2)
                if not m else np.inf for (m, roi) in zip(matched, rois_nom)
            ])
            k0 = np.argmin(dists)
            matched[k0] = True
            nom = rois_nom[k0]
            ret[k0] = (
                nom if dists[k0] > d
                else ROI(
                    (loc[1] - size[0] // 2, loc[0] - size[1] // 2),
                    size,
                    nom.optim_pad,
                )
            )
        return ret

def load_image(datadir: Path, infile: str) -> np.ndarray:
    """
    Load raveled image arrays from .fits archives with a common base file name,
    expecting extending archives to have "_X#" to be appended (where "#" is a
    number >= 2).

    Parameters
    ----------
    datadir : pathlib.Path
        Path to the directory containing .fits files.
    infile : str
        Base file name including the ".fits".

    Returns
    -------
    image_data : numpy.ndarray[ndim=3, dtype=numpy.float64]
        Array containing all images stacked on axis 0.
    """
    infile_path = datadir.joinpath(infile)
    image_data = fits.getdata(str(infile_path)).astype(np.float64)
    k = 2
    infile_path_X = datadir.joinpath(infile_path.stem + f"_X{k:.0f}.fits")
    while infile_path_X.is_file():
        image_data_X = fits.getdata(infile_path_X).astype(np.float64)
        image_data = np.append(image_data, image_data_X, axis=0)
        k += 1
        infile_path_X = datadir.joinpath(infile_path.stem + f"_X{k:.0f}.fits")
    return image_data

def load_image_npy(datadir: Path, infile: str) -> np.ndarray:
    return np.load(str(datadir.joinpath(infile)))

def load_timetagger_npy(datadir: Path, infile: str) -> np.ndarray:
    return np.load(str(datadir.joinpath(infile)))

def load_image_pll(
    datadir: Path,
    infile: str,
    image_dim: tuple[int, int],
) -> np.ndarray:
    """
    Load a flattened image array from a raw binary file, optionally reshaping.

    Parameters
    ----------
    datadir : pathlib.Path
        Path to the directory containing the data file.
    infile : str
        File name, including any extension.
    image_dim : tuple[int, int]
        Pixel dimensions (width, height) of each image. Raises ValueError if
        the dimensions are incompatible with the data.

    Returns
    -------
    image_data : numpy.ndarray[ndim=3, dtype=numpy.float64]
        Array containing all images stacked on axis 0.
    """
    image_data = np.fromfile(str(datadir.joinpath(infile)), dtype=np.uint16)
    A = image_dim[0] * image_dim[1]
    if image_data.shape[0] % A != 0:
        raise ValueError(
            f"Image dimensions {image_dim}"
            f" do not match data length {image_data.shape[0]}"
        )
    else:
        Nshots = image_data.shape[0] // A
        return image_data.reshape((Nshots, *image_dim[::-1])).astype(np.float64)

def e_per_adc(
    readout_rate: float,
    preamp: int,
) -> float:
    """
    Get the count -> photon conversion factor
    """

def _e_per_count(
    readout_rate: float,
    preamp: int,
) -> float:
    if readout_rate == 1.0:
        return 16.1 if preamp == 1 else 3.89
    elif readout_rate == 10.0:
        return 16.0 if preamp == 1 else 4.01
    elif readout_rate == 20.0:
        return 16.9 if preamp == 1 else 4.63
    elif readout_rate == 30.0:
        return 18.0 if preamp == 1 else 5.70
    else:
        raise Exception("invalid camera readout rate or preamp level")

def to_photons(
    X: float | np.ndarray,
    readout_rate: float,
    preamp: int,
    em_gain: int,
    count_bias: float,
    QE: float,
) -> float | np.ndarray:
    """
    Convert photoelectron counts to photons.

    Parameters
    ----------
    X : float | numpy.ndarray[dtype=numpy.float64]
        Photoelectron count data. Can be a scalar or an array of any size.
    readout_rate : float
        Camera readout rate in MHz.
    preamp : int
        Camera preamp level.
    em_gain : int
        EM gain factor.
    count_bias : float
        Mean camera dark count.
    QE : float
        Quantum efficiency.

    Returns
    -------
    P : float | numpy.ndarray[dtype=numpy.float64]
        Photon count data.
    P_err : float | numpy.ndarray[dtype=numpy.float64]
    """
    e_per_count = _e_per_count(readout_rate, preamp)
    return (
        (X - count_bias) * e_per_count / em_gain / QE,
        np.sqrt(X) * e_per_count / em_gain / QE,
    )

