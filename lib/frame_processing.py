from __future__ import annotations
import numpy as np
import serial
from .config import Config
from .image import to_photons, _e_per_count

def qq(X):
	print(X)
	return X

class Slicer:
	def __getitem__(self, x):
		return x
S = Slicer()

class IFrameProcessor:
	def init(self, config: Config):
		raise NotImplementedError

	def doit(self, frame: np.ndarray) -> np.ndarray | None:
		raise NotImplementedError

	def deinit(self):
		raise NotImplementedError

class WindowAvg(IFrameProcessor):
	def init(self, config: Config):
		self.window_size = config.processing.window_avg.window_size
		self.current_idx = 0
		self.frames = np.zeros(
			(
				self.window_size,
				*config.processing.window_avg.image_dim[::-1],
			),
			dtype=np.uint16
		)

	def doit(self, frame: np.ndarray) -> np.ndarray:
		self.frames[self.current_idx, :, :] = frame
		self.current_idx = (self.current_idx + 1) % self.window_size
		return self.frames.mean(axis=0)

	def deinit(self):
		pass

class FFPrep(IFrameProcessor):
	def init(self, config: Config):
		self.hs_rate = {
			0: 30.0,
			1: 20.0,
			2: 10.0,
			3: 1.0,
		}[config.camera.read.hsspeed_idx]
		self.preamp_idx = {
			0: 1.0,
			1: 2.0,
		}[config.camera.amp.preamp_gain_idx]
		self.em_gain = config.camera.amp.em_gain
		self.count_bias = config.processing.count_bias
		self.qe = config.processing.qe

		self.n_loop = config.processing.ff_prep.n_loop
		self.mode = config.processing.ff_prep.mode
		self.box = config.processing.ff_prep.box
		self.threshold = config.processing.ff_prep.threshold
		self.arduino = serial.Serial(port="COM4", baudrate=int(2e6), timeout=0.1)
		self.current_idx = 0
		self.slicer = S[
			self.box[1] : self.box[1] + self.box[3],
			self.box[0] : self.box[0] + self.box[2],
		]
		self.to_photons = lambda x: to_photons(
			x,
			self.hs_rate,
			self.preamp_idx,
			self.em_gain,
			self.count_bias,
			self.qe,
		)[0]

	def doit(self, frame: np.ndarray) -> np.ndarray:
		roi = frame[self.slicer]
		photons = self.to_photons(roi).sum()
		is_bright = photons >= self.threshold

		do_pulse = {
			"bright": lambda: (not is_bright) or self.current_idx != 0,
			"dark": lambda: is_bright or self.current_idx != 0,
		}[self.mode]()

		self.arduino.write(b"1100\n" if do_pulse else b"1111\n")
		print(f"{self.current_idx:3.0f} : {int(photons):4.0f} => {str(do_pulse)}")
		self.current_idx = (self.current_idx + 1) % self.n_loop
		return roi

	def deinit(self):
		self.arduino.write(b"1100\n")
		self.arduino.close()

class FFSeries(IFrameProcessor):
	def init(self, config: Config):
		self.hs_rate = {
			0: 30.0,
			1: 20.0,
			2: 10.0,
			3: 1.0,
		}[config.camera.read.hsspeed_idx]
		self.preamp = {
			0: 1.0,
			1: 2.0,
		}[config.camera.amp.preamp_gain_idx]
		self.em_gain = config.camera.amp.em_gain
		self.count_bias = config.processing.count_bias
		self.qe = config.processing.qe

		self.n_loop = config.processing.ff_series.n_loop
		self.mode = config.processing.ff_series.mode
		self.box = config.processing.ff_series.box
		self.threshold = config.processing.ff_series.threshold
		self.arduino = serial.Serial(port="COM4", baudrate=int(2e6), timeout=0.1)
		self.current_idx = 0
		self.init_state = None
		self.slicer = S[
			self.box[1] : self.box[1] + self.box[3],
			self.box[0] : self.box[0] + self.box[2],
		]

		self.e_per_count = _e_per_count(self.hs_rate, self.preamp)
		self.a = self.e_per_count * self.em_gain * self.qe
		self.b = self.count_bias
		self.to_photons = lambda x: (x - self.b) / self.a

		# self.to_photons = lambda x: to_photons(
		# 	x,
		# 	self.hs_rate,
		# 	self.preamp,
		# 	self.em_gain,
		# 	self.count_bias,
		# 	self.qe,
		# )[0]

		self.logic = {
			1: {
				"xor": lambda x: self.init_state ^ x,
				"xnor": lambda x: not (self.init_state ^ x),
				"alternate": lambda x: not (self.init_state ^ x),
			},
			3: {
				"xor": lambda x: self.init_state ^ x,
				"xnor": lambda x: not (self.init_state ^ x),
				"alternate": lambda x: self.init_state ^ x,
			},
		}

	def doit(self, frame: np.ndarray) -> np.ndarray:
		roi = frame[self.slicer]
		photons = self.to_photons(roi).sum()
		is_bright = photons >= self.threshold

		if self.current_idx == 0:
			self.init_state = is_bright
			do_pulse = True
		elif self.current_idx == self.n_loop - 1:
			do_pulse = True
		elif (self.current_idx % 2) == 1:
			do_pulse = self.logic[self.current_idx % 4][self.mode](is_bright)
		else:
			do_pulse = True

		self.arduino.write(b"1100\n" if do_pulse else b"1111\n")
		print(f"{self.current_idx:3.0f} : {int(photons):4.0f} => {str(do_pulse)}")
		self.current_idx = (self.current_idx + 1) % self.n_loop
		return roi

	def deinit(self):
		self.arduino.write(b"1100\n")
		self.arduino.close()

proc_registry = {
	"window_avg": WindowAvg(),
	"ff_prep": FFPrep(),
	"ff_series": FFSeries(),
}
