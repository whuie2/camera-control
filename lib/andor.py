import numpy as np
from pylablib.devices import Andor
from typing import Tuple, Dict, BinaryIO
from .config import Config

def qq(X):
    print(X)
    return X

def restart_lib():
    Andor.AndorSDK2.restart_lib()

class iXon888:
    """
    Simple class to handle image acquisition from an Andor iXon 888.

    Methods without explicit return types return `self`.
    """
    def __init__(self, idx: int=0):
        self.cam_idx = idx
        self.cam: Andor.AndorSDK2Camera = None

    def is_connected(self) -> bool:
        return self.cam is not None and self.cam.is_opened()

    def set_config(self, config: Config):
        if self.is_connected():
            try:
                self.set_temperature(
                    config.camera.cooler.temperature_c,
                    config.camera.cooler.on,
                )
                self.set_amp_mode(
                    channel=None,
                    oamp=config.camera.amp.oamp,
                    hsspeed=config.camera.read.hsspeed_idx,
                    preamp=config.camera.amp.preamp_gain_idx,
                )
                self.set_em_gain(config.camera.amp.em_gain)
                self.set_acquisition_mode(config.camera.acquisition_mode)
                if config.camera.acquisition_mode in {"kinetic", "fast_kinetic"}:
                    self.setup_kinetics_mode(
                        config.camera.kinetic.buffer_size,
                        cycle_time=config.camera.kinetic.cycle_time,
                        num_acc=config.camera.kinetic.num_acc,
                        cycle_time_acc=config.camera.kinetic.cycle_time_acc,
                        num_prescan=config.camera.kinetic.num_prescan,
                    )
                self.setup_image_mode(*config.camera.roi, *config.camera.bin)
                self.set_roi(*config.camera.roi)
                self.set_exposure(config.camera.exposure_time_ms * 1e-3)
                self.cam.set_fan_mode(config.camera.cooler.fan_mode)
                self.setup_shutter(
                    mode=config.camera.shutter.mode,
                    ttl_mode=config.camera.shutter.ttl_mode,
                    open_time=config.camera.shutter.open_time_ms,
                    close_time=config.camera.shutter.close_time_ms,
                )
                self.set_trigger_mode(config.camera.trigger.mode)
                self.set_read_mode(config.camera.read.mode)
                self.set_vsspped(config.camera.read.vsspeed_idx)
                self.set_frame_format(config.camera.frame_format)
            except BaseException as err:
                print(f"Error setting camera config:\n{err}")
        return self

    def open(self) -> bool:
        try:
            self.cam = Andor.AndorSDK2Camera(
                self.cam_idx,
                temperature=-80,
                fan_mode="full",
                amp_mode=None,
            )
            self.cam.open()
            # self.set_trigger_mode("ext")
            # self.set_exposure(0.02)
            # self.set_read_mode("image")
            # self.set_vsspped(1)
            # self.set_amp_mode(
            #     channel=0, # unsure what this corresponds to
            #     oamp=0,  # 0 for EM multiplying, 1 for conventional
            #     hsspeed=0,
            #     preamp=1,
            # )
            # self.set_em_gain(100)
            # self.set_frame_format("list")
            # self.setup_shutter()
            # self.set_temperature(-80, cooler_on=True)
            # self.cam.set_fan_mode("full")
            # self.set_cooler(False)
            # print(self.is_acquisition_setup())
            return True
        except BaseException as err:
            print(f"Error connecting to camera:\n{err}")
            return False

    def connect(self) -> bool:
        return self.open()

    def close(self):
        if self.is_connected():
            # self.set_cooler(False)
            self.cam.close()
            self.cam = None
        return self

    def disconnect(self):
        return self.close()

    def is_acquiring(self) -> bool:
        if self.is_connected():
            return self.cam.acquisition_in_progress()
        return False

    def clear_acquisition_settings(self):
        if self.is_connected():
            self.cam.clear_acquisition()
        return self

    def get_device_info(self) -> (str, str, int):
        if self.is_connected():
            return self.cam.get_device_info()
        return "", "", -1

    def get_status(self) -> str:
        if self.is_connected():
            return self.cam.get_status()
        return "camera not connected"

    def set_frame_transfer_mode(self, onoff: bool):
        if self.is_connected():
            self.cam.enable_frame_transfer_mode(onoff)
        return self

    def get_em_gain(self) -> (int, int):
        if self.is_connected():
            return self.cam.get_EMCCD_gain()
        return -1, -1

    def set_em_gain(self, gain):
        if self.is_connected():
            self.cam.set_EMCCD_gain(gain)
        return self

    def set_temperature(self, temp:int=-80, cooler_on=True):
        if self.is_connected():
            self.cam.set_temperature(temp, cooler_on)
        return self

    def get_temperature(self) -> int:
        if self.is_connected():
            return self.cam.get_temperature()
        return 0

    def get_temperature_status(self) -> str:
        if self.is_connected():
            return self.cam.get_temperature_status()
        return "off"

    def set_cooler(self, on=True):
        if self.is_connected():
            print("setting cooler to", on)
            self.cam.set_cooler(on)
        return self

    def is_cooler_on(self) -> bool:
        if self.is_connected():
            return self.cam.is_cooler_on()
        return False

    def set_exposure(self, exposure): # exposure in seconds
        if self.is_connected():
            self.cam.set_exposure(exposure)
        return self

    def get_exposure(self) -> int:
        if self.is_connected():
            return self.cam.get_exposure()
        return -1

    def set_acquisition_mode(
        self,
        mode:str="single" # single, accum, kinetic, fast_kinetic, cont
    ):
        if self.is_connected():
            self.cam.set_acquisition_mode(mode)
        return self

    def setup_image_mode(
        self,
        hstart=0,
        hend=None,
        vstart=0,
        vend=None,
        hbin=1,
        vbin=1
    ):
        if self.is_connected():
            self.cam.setup_image_mode(hstart, hend, vstart, vend, hbin, vbin)

    def setup_kinetics_mode(
        self,
        buffer_size=10000,
        cycle_time: float=0.0,
        num_acc: int=1,
        cycle_time_acc: float=0.0,
        num_prescan: int=0,
    ) -> bool:
        if self.is_connected():
            self.set_acquisition_mode("kinetic")
            self.cam.setup_kinetic_mode(
                buffer_size, cycle_time, num_acc, cycle_time_acc, num_prescan)
            return True
        return False

    def setup_shutter(
        self,
        mode:str="auto",
        ttl_mode:int=1,
        open_time=27,
        close_time=27
    ):
        if self.is_connected():
            self.cam.setup_shutter(mode, ttl_mode, open_time, close_time)
        return self

    def set_trigger_mode(
        self,
        mode:str="ext", # int, ext, software
    ):
        if self.is_connected():
            self.cam.set_trigger_mode(mode)
        return self

    def setup_ext_trigger(self, level=None, invert=None, term_highZ=None):
        self.cam.setup_ext_trigger(level, invert, term_highZ)
        return self

    def set_read_mode(
        self,
        mode='image',  # fvb, single_track, multi_track, random_track, image
    ):
        if self.is_connected():
            self.cam.set_read_mode(mode)
        return self

    def set_roi(self, xmin: int, xmax: int, ymin: int, ymax: int):
        if self.is_connected():
            self.cam.set_roi(xmin, xmax, ymin, ymax)
        return self

    def get_roi(self) -> (int, int, int, int, int, int):  # tuple(hstart, hend, vstart, vend, hbin, vbin)
        if self.is_connected():
            return self.cam.get_roi()
        return -1, -1, -1, -1, -1, -1

    def get_roi_dim(self) -> (int, int):
        if self.is_connected():
            hs, he, vs, ve, _, _ = self.get_roi()
            return ve-vs, he-hs
        return -1, -1

    def start_acquisition(self) -> bool:
        # if self.is_connected() and self.is_acquisition_setup():
        if self.is_connected():
            self.cam.start_acquisition()
            return True
        return False

    def stop_acquisition(self) -> bool:
        if self.is_connected() and self.is_acquiring():
            self.cam.stop_acquisition()
            return True
        return False

    def get_acquisition_params(self) -> Dict[str, str]:
        if self.is_connected():
            return self.cam.get_acquisition_parameters()
        return dict()

    def get_full_info(self) -> Dict[str, str]:
        if self.is_connected():
            return self.cam.get_full_info()
        return dict()

    def is_acquisition_setup(self) -> bool:
        if self.is_connected():
            return self.cam.is_acquisition_setup()
        return False

    def read_newest_image(self, peek=False):
        if self.is_acquiring():
            return self.cam.read_newest_image(peek)
        return None

    def read_oldest_image(self, peek=False):
        if self.is_connected():
            return self.cam.read_oldest_image(peek)
        return list()

    def set_frame_format(self, fmt='list'):  # "list", "array", "chunks"
        if self.is_connected():
            self.cam.set_frame_format(fmt)
        return self

    def wait_for_image(self, since='lastread', nimages=1, timeout=600):
        if self.is_acquiring():
            return self.cam.wait_for_frame(since, nimages, timeout)
        return None

    def read_multiple_images(
        self,
        nimages:int = None,  # (first, last), first inclusive
        peek:bool = False,
        missing:str = 'skip',  # None, zero, skip
    ):
        if self.is_acquiring():
            return self.cam.read_multiple_images((0, nimages), peek, missing)
        return None

    def get_frames_status(self) -> Tuple[int, int, int, int]:
        # tuple(acquired, unread, skipped, size)
        if self.is_connected():
            return self.cam.get_frames_status()
        return -1, -1, -1, -1

    def get_new_images_range(self) -> (int, int):
        if self.is_acquiring():
            return self.cam.get_new_images_range()
        return None

    def grab(self):
        if self.is_connected():
            img = self.cam.snap()
            return img
        return None

    def send_software_trig(self):
        if self.is_connected():
            self.cam.send_software_trigger()

    def set_amp_mode(
        self,
        channel=None,
        oamp=None,
        hsspeed=None,
        preamp=None,
    ) -> bool:
        if self.is_connected():
            self.cam.set_amp_mode(channel, oamp, hsspeed, preamp)
            return True
        return False

    def get_amp_mode(self, full=True):
        if self.is_connected():
            return self.cam.get_amp_mode(full)
        return None

    def get_all_amp_modes(self):
        if self.is_connected():
            return self.cam.get_all_amp_modes()
        return []

    def get_vsspeed(self) -> int:
        if self.is_connected():
            return self.cam.get_vsspeed()
        return None

    def set_vsspped(self, vsspeed) -> bool:
        if self.is_connected():
            self.cam.set_vsspeed(vsspeed)
            return True
        return False

    # def setup_camera(self, config_path=None) -> bool:
    #     self.connect(ini_path=config_path)
    #     if self.is_connected():
    #         print(self.get_full_info())
    #         print(self.get_acquisition_params())
    #         print("Camera is connected")
    #         return True
    #     else:
    #         print("Cam connection failed")
    #         return False

    # def start_acquisition(
    #         self,
    #         loop_params: (int,int) = (0,1),
    # ) -> (bool, BinaryIO):
    #     is_setup = False
    #     if self.is_acquisition_setup():
    #         self.acquire()
    #         is_setup = True
    #         print("Acquisition started")
    #     else:
    #         print("Acquisition not setup")
    #     return is_setup

    # def stop_acquisition(self, outfile):
    #     if self.is_connected() and self.is_acquiring():
    #         self.stop_acquisition()
    #     if outfile is not None:
    #         outfile.close()
    #         outfile = None
    #     return self
