from __future__ import annotations
import code
from dataclasses import dataclass
import readline
import rlcompleter
import lib.andor as andor
import lib.timetagger as timetagger
from lib.config import load_def_config, Config
from pathlib import Path
import getopt
import lib.cli_actions as actions
import sys
import time

cam = None
tagger = None

@dataclass
class CLI:
    config_path: Path
    config: Config | None
    cam_idx: int
    init: bool
    connect: bool

if __name__ == "__main__":
    def connect() -> iXon888:
        if cam is not None:
            cam.connect()

    def disconnect():
        if cam is not None:
            cam.disconnect()

    def load_config():
        return actions.load_config(cam, cli.config)

    def acquire(with_tagger: bool=True):
        global tagger
        cli.config = load_def_config(cli.config_path)
        if with_tagger and cli.config.timetagger.use:
            if tagger is None:
                try:
                    print(f"Connecting to timetagger with serial {cli.config.timetagger.serial}...")
                    tagger = timetagger.TimeTaggerUltra(cli.config.timetagger.serial)
                    tagger.connect()
                except BaseException as err:
                    print(f"Failed to connect:\n{err}")
                    sys.exit(1)
            return actions.acquire(cam, cli.config, tagger)
        else:
            return actions.acquire(cam, cli.config, None)

    def temp():
        return (
            cam.get_temperature(),
            cam.get_temperature_status(),
        )

    def temp_mon():
        print("Temperature monitor; CTRL-C to stop")
        while True:
            try:
                T, status = temp()
                print(f"  {T = :+7.3f}; {status = }            ", end="\r", flush=True)
                time.sleep(1.0)
            except KeyboardInterrupt:
                print()
                break

    cli = CLI(
        config_path=Path("config.toml"),
        config=None,
        cam_idx=0,
        init=True,
        connect=True,
    )
    shortopts = "c:C:IN"
    longopts = [
        "config=",
        "camera-index=",
        "no-init",
        "no-connect",
    ]
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
        for opt, optarg in opts:
            if opt in {"-c", "--config"}:
                cli.config_path = Path(optarg)
            elif opt in {"-C", "--camera-index"}:
                cli.cam_idx = int(optarg)
            elif opt in {"-I", "--no-init"}:
                cli.init = False
            elif opt in {"-N", "--no-connect"}:
                cli.connect = False
    except Exception as ERR:
        raise ERR

    try:
        print(f"Loading config from '{cli.config_path}'...")
        cli.config = load_def_config(cli.config_path)
    except BaseException as err:
        print(f"Failed to load config:\n{err}")
        sys.exit(1)

    if cli.connect:
        try:
            print(f"Connecting to camera at index {cli.cam_idx}...")
            cam = andor.iXon888(idx=cli.cam_idx)
            cam.connect()
        except BaseException as err:
            print(f"Failed to connect:\n{err}")
            sys.exit(1)
    else:
        print(f"[info] camera at index {cli.cam_idx} not connected...")
        cam = andor.iXon888(idx=cli.cam_idx)

    if cli.init:
        print("Initializing camera...")
        if cam.is_connected():
            actions.load_config(cam, cli.config)
        else:
            print("Camera is not connected!")

    if cli.config.timetagger.use:
        try:
            print(f"Connecting to timetagger with serial {cli.config.timetagger.serial}...")
            tagger = timetagger.TimeTaggerUltra(cli.config.timetagger.serial)
            tagger.connect()
        except BaseException as err:
            print(f"Failed to connect:\n{err}")
            sys.exit(1)
    else:
        tagger = None


    # console
    vars = globals()
    vars.update(locals())
    readline.set_completer(rlcompleter.Completer(vars).complete)
    readline.parse_and_bind("tab: complete")
    code.InteractiveConsole(vars).interact(
        banner="Start Andor iXon 888 EMCCD interactive console"
    )
    cam.disconnect()

